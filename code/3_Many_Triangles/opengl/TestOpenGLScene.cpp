#include "TestOpenGLScene.h"

#include "../../common/renders/base/scene/model/IGeometryBufferVisitor.h"

void TestOpenGLScene::init(const std::wstring& path, SceneParameters rp)
{
	OpenGLScene::init(path, rp);
	// ��� ������� ������ (������������) � ���������
	buffer = std::make_shared<OpenGLGeometryBufferA>();
	if (!buffer)
		throw ExceptionMemory(L"OpenGLGeometryBufferA is not create, memory not found!");
	// init model
	VisitorGeometryBufferCreateAndDrawSmartPtr visitor = std::make_shared<VisitorGeometryBufferCreateAndDraw>(shadersBuffer);
	if (!visitor)
		throw ExceptionMemory(L"VisitorGeometryBufferCreateAndDraw is not create, memory not found!");
	buffer->init(L"shaders/opengl/color.gl_vs", L"shaders/opengl/color.gl_ps", visitor);

	// ������� �������
	std::vector<Index> ind =
	{
		0, 1, 2
	};
	ModelParameters mp;
	mp.type = TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_A;
	mp.indeces = ind;

	// ������� ������� 1
	std::vector<VertexColor> v =
	{
		{gmath::Point3(-0.8f, 0.2f, 0.5f), gmath::Color(1.f, 0.f, 0.f, 1.f)},
		{gmath::Point3(-0.5f, 0.8f, 0.5f), gmath::Color(0.f, 1.f, 0.f, 1.f)},
		{gmath::Point3(-0.2f, 0.2f, 0.5f), gmath::Color(1.f, 0.49f, 0.31f, 1.f)}
	};
	// ������ 1
	mp.verticesColor = v;
	if (!buffer->addModel(mp))
		return;

	// ������� ������� 2
	v =
	{
		{gmath::Point3(0.2f, 0.2f, 0.5f), gmath::Color(1.f, 0.f, 0.f, 1.f)},
		{gmath::Point3(0.5f, 0.8f, 0.5f), gmath::Color(0.f, 1.f, 0.f, 1.f)},
		{gmath::Point3(0.8f, 0.2f, 0.5f), gmath::Color(1.f, 0.49f, 0.31f, 1.f)}
	};
	// ������ 2
	mp.verticesColor = v;
	if (!buffer->addModel(mp))
		return;

	// ������� ������� 3
	v =
	{
		{gmath::Point3(-0.8f, -0.2f, 0.5f), gmath::Color(1.f, 0.f, 0.f, 1.f)},
		{gmath::Point3(-0.2f, -0.2f, 0.5f), gmath::Color(0.f, 1.f, 0.f, 1.f)},
		{gmath::Point3(-0.5f, -0.8f, 0.5f), gmath::Color(1.f, 0.49f, 0.31f, 1.f)}
	};
	// ������ 3
	mp.verticesColor = v;
	if (!buffer->addModel(mp))
		return;

	// ������� ������� 4
	v =
	{
		{gmath::Point3(0.2f, -0.2f, 0.5f), gmath::Color(1.f, 0.f, 0.f, 1.f)},
		{gmath::Point3(0.8f, -0.2f, 0.5f), gmath::Color(0.f, 1.f, 0.f, 1.f)},
		{gmath::Point3(0.5f, -0.8f, 0.5f), gmath::Color(1.f, 0.49f, 0.31f, 1.f)}
	};
	// ������ 4
	mp.verticesColor = v;
	if (!buffer->addModel(mp))
		return;

	 buffer->reinit();
}

bool TestOpenGLScene::update(SceneDynamicParameters& sdp)
{
	return OpenGLScene::update(sdp);
}

TestOpenGLScene::~TestOpenGLScene()
{
	clear();
}

void TestOpenGLScene::clear()
{
	OpenGLScene::clear();
}

void TestOpenGLScene::render()
{
	gmath::Color color = { 0.0f, 0.0f, 1.0f, 1.0f };
	glClearColor(color[gmtypes::cr], color[gmtypes::cg], color[gmtypes::cb], color[gmtypes::ca]);
	glClear(GL_COLOR_BUFFER_BIT);

	// ������ ������ (�����������)
	buffer->draw();
}
