#include "TestOpenGLWindow.h"

void TestOpenGLWindow::init(WindowParameters wp)
{
	OSWindowsWindow::init(wp);
	render.reset(new TestOpenGLRender());
	if(!render)
		throw ExceptionMemory(L"TestOpenGLRender is not create, memory not found!");
	RenderParameters rp;
	rp.setHwnd(hwnd);
	RECT rect;
	if (!GetWindowRect(hwnd, &rect))
		throw new ExceptionWindow(L"Error get client area size window.");
	rp.setClientWidth(rect.right - rect.left);
	rp.setClientHeight(rect.bottom - rect.top);
	render->init(rp);
	render->initScene();
}

