#include "TestOpenGLRender.h"

void TestOpenGLRender::init(RenderParameters rp)
{
	vsync = false;
	OSWindowsOpenGLRender::init(rp);
	OSWindowsOpenGLRender::setTypeRasterizer(TypeRasterizer::SOLID_RASTERIZED);
}

bool TestOpenGLRender::updateScene(SceneDynamicParameters& sdp)
{
	return scene->update(sdp);
}

void TestOpenGLRender::initScene()
{
	scene.reset(new TestOpenGLScene());
	if(!scene)
		throw ExceptionMemory(L"TestOpenGLScene is not create, memory not found!");
	SceneParameters rp;
	rp.setClientHeight(-1);
	rp.setClientWidth(-1);
	scene->init(L"empty", rp);
}

void TestOpenGLRender::render()
{
	scene->render();
	OSWindowsOpenGLRender::render();
}
