#pragma once

#include "../../common/renders/base/scene/OpenGLScene.h"
#include "../../common/renders/base/scene/model/OpenGLGeometryBufferA.h"
#include "../../common/utils/base/ExceptionMemory.h"

class TestOpenGLScene : public OpenGLScene
{
	OpenGLGeometryBufferASmartPtr buffer;

public:
	void init(const std::wstring& path, SceneParameters rp) override;
	bool update(SceneDynamicParameters& sdp) override;

	~TestOpenGLScene()override;
	void clear()override;

	void render();
};