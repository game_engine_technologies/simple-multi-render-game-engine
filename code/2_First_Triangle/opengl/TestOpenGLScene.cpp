#include "TestOpenGLScene.h"

#include "../../common/renders/base/scene/model/IGeometryBufferVisitor.h"

void TestOpenGLScene::init(const std::wstring& path, SceneParameters rp)
{
	OpenGLScene::init(path, rp);
	// ��� ������� ������ (�����������) � ���������
	buffer = std::make_shared<OpenGLGeometryBufferA>();
	if (!buffer)
		throw ExceptionMemory(L"OpenGLGeometryBufferA is not create, memory not found!");
	// init model
	VisitorGeometryBufferCreateAndDrawSmartPtr visitor = std::make_shared<VisitorGeometryBufferCreateAndDraw>(shadersBuffer);
	if (!visitor)
		throw ExceptionMemory(L"VisitorGeometryBufferACreateAndDraw is not create, memory not found!");
	buffer->init(L"shaders/opengl/color.gl_vs", L"shaders/opengl/color.gl_ps", visitor);

	// ������� �������
	std::vector<VertexColor> v =
	{
		{gmath::Point3(0.f, 0.5f, 0.5f), gmath::Color(1.f, 0.f, 0.f, 1.f)},
		{gmath::Point3(0.5f, -0.5f, 0.5f), gmath::Color(0.f, 1.f, 0.f, 1.f)},
		{gmath::Point3(-0.5f, -0.5f, 0.5f), gmath::Color(1.f, 0.49f, 0.31f, 1.f)}
	};
	// ������� �������
	std::vector<Index> ind =
	{
		0, 1, 2
	};
	// ������
	ModelParameters mp;
	mp.type = TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_A;
	mp.verticesColor = v;
	mp.indeces = ind;
	if (!buffer->addModel(mp))
		return;
	buffer->reinit();
}

bool TestOpenGLScene::update(SceneDynamicParameters& sdp)
{
	return true;
}

TestOpenGLScene::~TestOpenGLScene()
{
	clear();
}

void TestOpenGLScene::clear()
{
	OpenGLScene::clear();
}

void TestOpenGLScene::render()
{
	gmath::Color color = { 0.0f, 0.0f, 1.0f, 1.0f };
	glClearColor(color[gmtypes::cr], color[gmtypes::cg], color[gmtypes::cb], color[gmtypes::ca]);
	glClear(GL_COLOR_BUFFER_BIT);

	// ������ ������ (�����������)
	buffer->draw();
}
