#include "TestDirect3D11Scene.h"

#include "../../common/renders/base/scene/model/IGeometryBufferVisitor.h"

TestDirect3D11Scene::TestDirect3D11Scene(const ComPtr<ID3D11Device>& d3dDevice,
	const ComPtr<ID3D11DeviceContext>& d3dImmediateContext):Direct3D11Scene(d3dDevice, d3dImmediateContext)
{
	
}

void TestDirect3D11Scene::init(const std::wstring& path, SceneParameters rp)
{
	Direct3D11Scene::init(path, rp);
	// ��� ������� ������ (�����������) � ���������
	buffer = std::make_shared<Direct3D11GeometryBufferA>();
	if(!buffer)
		throw ExceptionMemory(L"Direct3D11GeometryBufferA is not create, memory not found!");
	// init model
	VisitorGeometryBufferCreateAndDrawSmartPtr visitor = std::make_shared<VisitorGeometryBufferCreateAndDraw>(
		d3dDevice,
		d3dImmediateContext, 
		shadersBuffer);
	if (!visitor)
		throw ExceptionMemory(L"VisitorGeometryBufferCreateAndDraw is not create, memory not found!");
	buffer->init(L"shaders/d3d11/color.d3d11_vs", L"shaders/d3d11/color.d3d11_ps", visitor);

	// ������� �������
	std::vector<VertexColor> v =
	{
		{gmath::Point3(0.f, 0.5f, 0.5f), gmath::Color(1.f, 0.f, 0.f, 1.f)},
		{gmath::Point3(0.5f, -0.5f, 0.5f), gmath::Color(0.f, 1.f, 0.f, 1.f)},
		{gmath::Point3(-0.5f, -0.5f, 0.5f), gmath::Color(1.f, 0.49f, 0.31f, 1.f)}
	};
	// ������� �������
	std::vector<Index> ind =
	{
		0, 1, 2
	};
	// ������
	ModelParameters mp;
	mp.type = TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_A;
	mp.verticesColor = v;
	mp.indeces = ind;
	if(buffer->addModel(mp))
		buffer->reinit();
}

bool TestDirect3D11Scene::update(SceneDynamicParameters& sdp)
{
	return true;
}

TestDirect3D11Scene::~TestDirect3D11Scene()
{
	clear();
}

void TestDirect3D11Scene::clear()
{
	Direct3D11Scene::clear();
}

void TestDirect3D11Scene::render(const ComPtr<ID3D11DeviceContext>& d3dImmediateContext, const ComPtr<ID3D11RenderTargetView>& d3dRenderTargetView,
	const ComPtr<ID3D11DepthStencilView>& d3dDepthStencilView, const ComPtr<ID3D11DepthStencilState>& d3dDepthStencilEnableDepth,
	const ComPtr<ID3D11DepthStencilState>& d3dDepthStencilDisableDepth)
{
	gmath::Color color = { 0.0f, 0.0f, 1.0f, 1.0f };
	d3dImmediateContext->ClearRenderTargetView(d3dRenderTargetView.Get(), color.toArray().data());
	d3dImmediateContext->ClearDepthStencilView(d3dDepthStencilView.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
	// ������ ����� (���� ������ �����������)
	buffer->draw();
}
