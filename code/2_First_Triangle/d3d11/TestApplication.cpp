#include "TestApplication.h"

void TestApplication::init(WindowParameters wp)
{
	window.reset(new TestDirect3D11Window());
	if(!window)
		throw ExceptionMemory(L"TestDirect3D11Window is not create, memory not found!");
	window->init(wp);
}

