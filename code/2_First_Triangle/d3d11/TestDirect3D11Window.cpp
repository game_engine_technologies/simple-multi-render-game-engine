#include "TestDirect3D11Window.h"

void TestDirect3D11Window::init(WindowParameters wp)
{
	OSWindowsWindow::init(wp);
	render.reset(new TestDirect3D11Render());
	if(!render)
		throw ExceptionMemory(L"TestDirect3D11Window is not create, memory not found!");
	RenderParameters rp;
	rp.setHwnd(hwnd);
	RECT rect;
	if (!GetWindowRect(hwnd, &rect))
		throw new ExceptionMemory(L"Error get client area size window.");
	rp.setClientWidth(rect.right - rect.left);
	rp.setClientHeight(rect.bottom - rect.top);
	render->init(rp);
	render->initScene();
}

