#include "TestApplication.h"

void TestApplication::init(WindowParameters wp)
{
	window = std::make_shared<TestOpenGLWindow>();
	if(!window)
		throw ExceptionMemory(L"TestOpenGLWindow is not create, memory not found!");
	window->init(wp);
}

