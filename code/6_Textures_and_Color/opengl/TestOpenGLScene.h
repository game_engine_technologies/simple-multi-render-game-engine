#pragma once

#include "../../common/renders/base/scene/OpenGLScene.h"
#include "../../common/renders/base/scene/model/OpenGLGeometryBufferC.h"
#include "../../common/utils/base/ExceptionMemory.h"
#include "../../common/renders/base/scene/camera/OpenGLCamera.h"
#include "../../common/renders/base/scene/model/OpenGLGeometryBufferB.h"

class TestOpenGLScene : public OpenGLScene
{
	OpenGLGeometryBufferBSmartPtr bufferColors;
	OpenGLGeometryBufferCSmartPtr bufferTextures;
	OpenGLCameraSmartPtr camera;


public:
	void init(const std::wstring& path, SceneParameters rp) override;
	bool update(SceneDynamicParameters& sdp) override;

	~TestOpenGLScene()override;
	void clear()override;

	void render();
};