#pragma once

#include "common/os/base/LevelOperationSystemSingleton.h"
#include "common/renders/base/Application.h"
#include "common/renders/base/OSWindowsDirect3D11Render.h"
#include "common/renders/base/OSWindowsOpenGLRender.h"

#include "common/renders/base/OSWindowsWindow.h"

#include "common/utils/base/ExceptionDirect3D11.h"
#include "common/utils/base/ExceptionDirect3D11BufferCreator.h"
#include "common/utils/base/ExceptionDirect3D11GeometryBuffer.h"
#include "common/utils/base/ExceptionDirect3D11TextBuffer.h"
#include "common/utils/base/ExceptionInputSystem.h"
#include "common/utils/base/ExceptionLoader.h"
#include "common/utils/base/ExceptionMemory.h"
#include "common/utils/base/ExceptionOpenGL.h"
#include "common/utils/base/ExceptionOpenGLGeometryBuffer.h"
#include "common/utils/base/ExceptionShaders.h"
#include "common/utils/base/ExceptionTextures.h"
#include "common/utils/base/ExceptionWindow.h"

#include "common/utils/base/loaders/Materials.h"
#include "common/utils/base/loaders/MaterialsLoader.h"
#include "common/utils/base/loaders/TextConfigLoader.h"
#include "common/utils/base/loaders/TextProperty.h"
#include "common/utils/base/loaders/TexturesLoaderSOIL.h"

#include "common/utils/base/input/InputSystem.h"

#include "common/renders/base/d3d11_header.h"
#include "common/renders/base/gl_header.h"
#include "common/renders/base/math_header.h"

#include <psapi.h>
