#include "SingletonHelper.h"
#include "../../utils/base/ExceptionMemory.h"

SingletonHelperSmartPtr SingletonHelper::instance;

SingletonHelper::SingletonHelper()
{

}

SingletonHelperSmartPtr& SingletonHelper::getInstance()
{
	if (!instance)
	{
		instance.reset(new SingletonHelper);
		if(!instance)
			throw ExceptionMemory(L"SingletonHelper is not create, memory not found!");
	}
	return instance;
}

IRender* SingletonHelper::getCurrentRender()
{
	return render;
}


void SingletonHelper::setCurrentRender(IRender* wsp)
{
	this->render = wsp;
}
