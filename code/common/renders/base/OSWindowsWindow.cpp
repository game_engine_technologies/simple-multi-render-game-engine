#include "OSWindowsWindow.h"
#include "../../utils/base/ExceptionMemory.h"
#include "../../utils/base/input/InputSystem.h"
#include "../../os/base/LevelOperationSystemSingleton.h"
#include "SingletonHelper.h"
//#include "../../os/base/LevelOperationSystemSingleton.h"

void OSWindowsWindow::init(WindowParameters wp)
{
	// SingletonHelper::getInstance()->setCurrentWindow(this);

	classWin = L"oswindows_class_window";
	WNDCLASSEX wc = { 0 };
	wc.cbSize = sizeof(wc);
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WndProc; 
	wc.hInstance = wp.getHinstance();
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	// wc.lpszMenuName = NULL;
	wc.lpszClassName = classWin.c_str();
	if (!RegisterClassEx(&wc))
		throw ExceptionWindow(L"Error register window");
	hwnd = CreateWindowEx(0,
		classWin.c_str(),
		L"Test OS Windows Window",
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT,
		wp.getWidth(), wp.getHeight(),
		NULL,
		NULL,
		wp.getHinstance(),
		NULL);
	if (!hwnd)
		throw ExceptionWindow(L"Error create window");

	ShowWindow(hwnd, SW_SHOW); // ���������� ���� �� ����� (����������� �� ������������� �������)

	//hookKeyboardProc(wp.getHinstance()); // ������������ ��� ���������� - ���������, ��� ������� �������
}

void OSWindowsWindow::resizeViewport(int w, int h)
{
	render->resizeViewport(w, h);
}

void OSWindowsWindow::resizeWindow(int x, int y, int w, int h)
{
	render->resizeWindow(x, y, w, h);
}

void OSWindowsWindow::run()
{
	MSG msg;
	SceneDynamicParameters sdp = { 0 };
	sdp.start = true;
	unsigned fps(0);

	SetWindowLongPtr(hwnd, GWLP_USERDATA, (LONG_PTR)(this)); // �������� ��������� �� ����
	render->setRunning(true); // ��������� ������� ����
	resizeAll(); // �������� ������ ���� �������

	//UpdateWindow(hwnd);

	LevelOperationSystemSingleton::getInstance()->startTimer();
	while (true)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				break;
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			if (render->isRunning())
			{
				// FPS - ������� � ��������� �����
				fps++;
				double time = LevelOperationSystemSingleton::getInstance()->getTime();
				if (time > 0.05f)
					sdp.second = true;
				if (time > 1.0f)
				{
					sdp.textBuffStack.push(std::make_shared<TextBufferDynamicParameterFPS>(fps));
					fps = 0;
					LevelOperationSystemSingleton::getInstance()->startTimer();
				}
				//

				sdp.frameTime = LevelOperationSystemSingleton::getInstance()->getFrameTime();
				if (!render->updateScene(sdp))
					break;
				render->render();
			}
			else
				Sleep(100);
		}
	}
	render->setRunning(false);
	//return msg;
}

void OSWindowsWindow::clear()
{
	unHookKeyboardProc();
}

OSWindowsWindow::~OSWindowsWindow()
{
	//clear();// ��������� �������� ����
}

LRESULT OSWindowsWindow::wndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_SIZE:
	{
		resizeAll();
		break;
	}

	case WM_ERASEBKGND:
	{
		return 0;
	}

	case WM_ACTIVATE:
	{
		if (WA_ACTIVE == LOWORD(wParam) || WA_CLICKACTIVE == LOWORD(wParam)) // �������������� ����� alt + tab, ���� ���� ����� �������
		{
			//OutputDebugString(L"wm activate\n");
			render->pressAltTab(false);
		}
		else if(WA_INACTIVE == LOWORD(wParam))
			render->pressAltTab(true);
		break;
	}

	// input

	case WM_KEYDOWN: case WM_SYSKEYDOWN:
	{
		InputSystem::getInstance()->setKeyboardKey(static_cast<InputSystemKeys>(wParam), true);
		break;
	}
	
	case WM_KEYUP: case WM_SYSKEYUP:
	{
		InputSystem::getInstance()->setKeyboardKey(static_cast<InputSystemKeys>(wParam), false);
		break;
	}

	case WM_MOUSEMOVE:
	{
		//int xPos = GET_X_LPARAM(lParam);
		//int yPos = GET_Y_LPARAM(lParam);
		//InputSystem::getInstance()->setMousePosition(xPos, yPos);

		POINT p;
		GetCursorPos(&p);
		//OutputDebugString((std::to_wstring(p.x) + L"-" + std::to_wstring(p.y) + L"\n").c_str());
		InputSystem::getInstance()->setMousePosition(p.x, p.y);
		break;
	}

	case WM_LBUTTONDOWN:
	{
		InputSystem::getInstance()->setMouseKey(InputSystemMouse::MOUSE_LEFT, true);
		break;
	}

	case WM_LBUTTONUP:
	{
		InputSystem::getInstance()->setMouseKey(InputSystemMouse::MOUSE_LEFT, false);
		break;
	}

	case WM_RBUTTONDOWN:
	{
		InputSystem::getInstance()->setMouseKey(InputSystemMouse::MOUSE_RIGHT, true);
		break;
	}

	case WM_RBUTTONUP:
	{
		InputSystem::getInstance()->setMouseKey(InputSystemMouse::MOUSE_RIGHT, false);
		break;
	}

	case WM_MBUTTONDOWN:
	{
		InputSystem::getInstance()->setMouseKey(InputSystemMouse::MOUSE_WHEEL, true);
		break;
	}

	case WM_MBUTTONUP:
	{
		InputSystem::getInstance()->setMouseKey(InputSystemMouse::MOUSE_WHEEL, false);
		break;
	}

	case WM_MOUSEWHEEL:
	{
		int zDelta = GET_WHEEL_DELTA_WPARAM(wParam);
		InputSystem::getInstance()->setMouseWheel(zDelta);
		break;
	}

	// out input

	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;

	}
	return DefWindowProc(hwnd, msg, wParam, lParam);
}

HHOOK OSWindowsWindow::ExistingKeyboardProc;

LRESULT CALLBACK OSWindowsWindow::keyboardProcLowLevel(int nCode, WPARAM wParam, LPARAM lParam)
{
	KBDLLHOOKSTRUCT* hookstruct = (KBDLLHOOKSTRUCT*)(lParam);

	switch (wParam)
	{
	case WM_KEYDOWN:
		break;
	case WM_SYSKEYDOWN:
		if ((((hookstruct->flags) >> 5) & 1))
		{
			// ALT +
			switch (hookstruct->vkCode)
			{
			case VK_TAB: // ALT+TAB: ������������
			{
				SingletonHelper::getInstance()->getCurrentRender()->pressAltTab(true);
				break;
			}
			break;
			case VK_RETURN: // ALT+ENTER
				break;
			case VK_ESCAPE: // ALT+ESC
				break;
			case VK_DELETE: // ALT+DEL
				break;
			};
		}
		break;
	case WM_KEYUP:
		break;
	case WM_SYSKEYUP:
		break;
	}

	return CallNextHookEx(ExistingKeyboardProc, nCode, wParam, lParam);
}

bool OSWindowsWindow::hookKeyboardProc(HINSTANCE hinst)
{
	ExistingKeyboardProc = SetWindowsHookEx(
		WH_KEYBOARD_LL,
		keyboardProcLowLevel,
		hinst,
		NULL);

	if (!ExistingKeyboardProc)
		throw ExceptionWindow(L"Error OSWindowsWindow: SetWindowsHookEx");
}

void OSWindowsWindow::unHookKeyboardProc()
{
	if (!ExistingKeyboardProc)
		throw ExceptionWindow(L"Error OSWindowsWindow: hook descriptor is NULL");
	BOOL retcode = UnhookWindowsHookEx(ExistingKeyboardProc);
	if (!retcode)
		throw ExceptionWindow(L"Error OSWindowsWindow: UnhookWindowsHookEx");
}

void OSWindowsWindow::resizeAll()
{
	WINDOWPLACEMENT winPlace;
	winPlace.length = sizeof(WINDOWPLACEMENT);
	GetWindowPlacement(hwnd, &winPlace); // ���������� ������ !!!
	if (winPlace.showCmd == SW_HIDE || winPlace.showCmd == SW_SHOWMINIMIZED) // ���� ���� ��������
	{
		render->setRunning(false); // ��������� ����������
		return;
	}
	else if (winPlace.showCmd == SW_RESTORE) // ���� ���� �������������
	{
		render->setRunning(true); // ���������� ���������
	}

	if (render->isRunning())
	{
		RECT rect;
		if (!GetClientRect(hwnd, &rect))
		{
			// todo
		}
		resizeViewport(rect.right - rect.left, rect.bottom - rect.top);

		if (!GetWindowRect(hwnd, &rect))
		{
			// todo
		}
		resizeWindow(rect.left, rect.top, rect.right, rect.bottom);

		OutputDebugString(L"resize\n");
		render->render();
	}
}
