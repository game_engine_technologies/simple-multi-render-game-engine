#pragma once

class Direct3D11ShadersBuffer;
class OpenGLShadersBuffer;

#include "../../base/d3d11_header.h"
#include "../../abstract/shaders/TypeShader.h"

#include <fstream>
#include <sstream>

class IShadersBufferVisitorCreator
{
public:
	virtual void create(const std::wstring& p, Direct3D11ShadersBuffer& a) = 0;
	virtual void create(const std::wstring& p, OpenGLShadersBuffer& a) = 0;
	virtual ~IShadersBufferVisitorCreator() = default;
};

class IShadersBufferVisitorLoader
{
public:
	virtual void load(const std::wstring& p, const TypeShader& ts, Direct3D11ShadersBuffer& a) = 0;
	virtual void load(const std::wstring& p, const TypeShader& ts, OpenGLShadersBuffer& a) = 0;
	virtual ~IShadersBufferVisitorLoader() = default;
};

class VisitorShaderBufferCreateAndLoader : public IShadersBufferVisitorCreator, public IShadersBufferVisitorLoader
{
	ComPtr<ID3D11Device> d3dDevice;
public:
	VisitorShaderBufferCreateAndLoader() = default;
	VisitorShaderBufferCreateAndLoader(const ComPtr<ID3D11Device>& d3dDevice);
	void create(const std::wstring& p, Direct3D11ShadersBuffer& a) override;
	void create(const std::wstring& p, OpenGLShadersBuffer& a) override;
	void load(const std::wstring& p, const TypeShader& ts, Direct3D11ShadersBuffer& a) override;
	void load(const std::wstring& p, const TypeShader& ts, OpenGLShadersBuffer& a) override;
	~VisitorShaderBufferCreateAndLoader()override {};
};