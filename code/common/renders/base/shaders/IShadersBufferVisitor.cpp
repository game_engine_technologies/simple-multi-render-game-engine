#include "IShadersBufferVisitor.h"

#include "Direct3D11ShadersBuffer.h"
#include "OpenGLShadersBuffer.h"

VisitorShaderBufferCreateAndLoader::VisitorShaderBufferCreateAndLoader(const ComPtr<ID3D11Device>& d3dDevice):d3dDevice(d3dDevice)
{
}

void VisitorShaderBufferCreateAndLoader::create(const std::wstring& p, Direct3D11ShadersBuffer& d3dShBuff)
{
	fs::path shaderCatalog(p);
	for (fs::recursive_directory_iterator iter(shaderCatalog); iter != fs::recursive_directory_iterator(); ++iter)
	{
		// ����� ���������� � �������� ������� �������, �� ����������
		std::wstring name = iter->path().wstring();
		if (iter->path().extension().wstring() == L".d3d11_vs")
			load(name, TypeShader::VertexShaderType, d3dShBuff);
		else if (iter->path().extension().wstring() == L".d3d11_ps")
			load(name, TypeShader::PixelShaderType, d3dShBuff);
		// etc
	}
}

void VisitorShaderBufferCreateAndLoader::create(const std::wstring& p, OpenGLShadersBuffer& glShBuff)
{
	fs::path shaderCatalog(p);
	for (fs::recursive_directory_iterator iter(shaderCatalog); iter != fs::recursive_directory_iterator(); ++iter)
	{
		// ����� ���������� � �������� ������� �������, �� ����������
		std::wstring name = iter->path().wstring();
		if (iter->path().extension().wstring() == L".gl_vs")
			load(name, TypeShader::VertexShaderType, glShBuff);
		else if(iter->path().extension().wstring() == L".gl_ps")
			load(name, TypeShader::PixelShaderType, glShBuff);
		// etc
	}
}

void VisitorShaderBufferCreateAndLoader::load(const std::wstring& p, const TypeShader& ts, Direct3D11ShadersBuffer& d3dShBuff)
{
	// ���������� ������� ��� ������� �������, �� ����
	ComPtr<ID3D11VertexShader> d3dVertexShader;
	ComPtr<ID3D11PixelShader> d3dPixelShader;
	ComPtr<ID3DBlob> blob;

	HRESULT hr;
	std::string ver;
	switch (ts)
	{
	case TypeShader::VertexShaderType:
		ver = "vs_" + d3dShBuff.getVersion();
		blob = d3dShBuff.compileShaderFromFile(p, "main", ver.c_str());
		hr = d3dDevice->CreateVertexShader(blob->GetBufferPointer(), blob->GetBufferSize(), NULL, &d3dVertexShader);
		if (FAILED(hr))
			throw ExceptionShaders(L"error create vertex shader - " + p);
		d3dShBuff.getVertexShaders().insert({ p, d3dVertexShader });
		d3dShBuff.getVertexShaderBlobs().insert({ p, blob });
		break;

	case TypeShader::PixelShaderType:
		ver = "ps_" + d3dShBuff.getVersion();
		blob = d3dShBuff.compileShaderFromFile(p, "main", ver.c_str());
		hr = d3dDevice->CreatePixelShader(blob->GetBufferPointer(), blob->GetBufferSize(), NULL, &d3dPixelShader);
		if (FAILED(hr))
			throw ExceptionShaders(L"error create pixel shader - " + p);
		d3dShBuff.getPixelShaders().insert({ p, d3dPixelShader });
		d3dShBuff.getPixelShaderBlobs().insert({ p, blob });
		break;

	default:
		debug::printError(L"This shader functional is not released\n");
		break;
	}
}

void VisitorShaderBufferCreateAndLoader::load(const std::wstring& p, const TypeShader& ts, OpenGLShadersBuffer& glShBuff)
{
	// ����� ���������� ������� � �������� ������� (�� ����)
	GLuint shader;
	switch (ts)
	{
	case TypeShader::VertexShaderType:
		shader = OpenGLFunctional::getInstance()->glCreateShader(GL_VERTEX_SHADER);
		break;

	case TypeShader::PixelShaderType:
		shader = OpenGLFunctional::getInstance()->glCreateShader(GL_FRAGMENT_SHADER);
		break;

	default:
		// std::cout << L"This shader functional is not released" << std::endl; // �������� �����
		break;
	}
	std::wifstream sh(p);
	if(!sh)
		throw ExceptionShaders(L"error open shader: " + p);
	//sh.imbue(std::locale(std::locale::empty(), new std::codecvt_utf8<wchar_t>));

	// load shader
	std::wstringstream wss;
	wss << sh.rdbuf();
	std::wstring tmp(wss.str());
	// proprocessor
	fs::path filenameShader(p);
	std::wstring pathShader(p);
	pathShader.resize(pathShader.length() - filenameShader.filename().string().length());
	glShBuff.preprocessor(tmp, pathShader);
	// final merge
	std::string shader_code(tmp.begin(), tmp.end());
	const GLchar* _code = shader_code.c_str();

	OpenGLFunctional::getInstance()->glShaderSource(shader, 1, &_code, NULL);
	OpenGLFunctional::getInstance()->glCompileShader(shader);
	GLint success;
	GLchar infoLog[512];
	OpenGLFunctional::getInstance()->glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		OpenGLFunctional::getInstance()->glGetShaderInfoLog(shader, 512, NULL, infoLog);
		std::wstring log(infoLog, infoLog + strlen(infoLog));
		throw ExceptionShaders(L"ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" + log + L"\n");
	}
	glShBuff.getShadersBuffer().emplace(std::make_pair(p, new OpenGLShader(shader)));
}
