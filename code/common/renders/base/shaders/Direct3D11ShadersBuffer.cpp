#include "Direct3D11ShadersBuffer.h"

#include "IShadersBufferVisitor.h"

Direct3D11ShadersBuffer::Direct3D11ShadersBuffer(const std::wstring& ver):ver(ver.begin(), ver.end())
{
}

ComPtr<ID3DBlob> Direct3D11ShadersBuffer::compileShaderFromFile(const std::wstring& fileName, LPCSTR entryPoint, LPCSTR shaderModel)
{
	ID3DBlob* error(nullptr);
	ID3DBlob* blob; // �������� ������
	UINT flags(NULL);
#ifdef DEBUG
	flags = D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
#endif

	HRESULT hr(D3DCompileFromFile(fileName.c_str(), NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE,
		entryPoint, shaderModel, /*D3DCOMPILE_PACK_MATRIX_ROW_MAJOR*/flags,
		0, &blob, &error));
	std::string err;
	if (error)
	{
		for (int i(0); i < error->GetBufferSize(); i++)
			err += ((char*)(error->GetBufferPointer()))[i];
		std::wstring mes(err.begin(), err.end());
		//OutputDebugString(mes.c_str());
		throw ExceptionShaders(L"path shader - " + fileName + L"\nMessage error: " + mes + L"\n\n");
	}
	if (hr != S_OK)
		return nullptr;
	return blob;
}


void Direct3D11ShadersBuffer::init(const std::wstring& p, VisitorShaderBufferCreateAndLoaderSmartPtr& shBuffCreator)
{
	creator = shBuffCreator;
	creator->create(p, *this);
}

void Direct3D11ShadersBuffer::loadShader(const std::wstring& p, const TypeShader& ts)
{
	creator->load(p, ts, *this);
}

void Direct3D11ShadersBuffer::clear()
{

}

Direct3D11ShadersBuffer::~Direct3D11ShadersBuffer()
{
	clear();
}

ComPtr<ID3D11VertexShader> Direct3D11ShadersBuffer::getVertexShader(const std::wstring& key)
{
	if(vertexShaders.find(key) == vertexShaders.end())
		throw ExceptionShaders(L"error find vertex shader - " + key);
	return vertexShaders[key];
}

ComPtr<ID3DBlob> Direct3D11ShadersBuffer::getVertexShaderBlob(const std::wstring& key)
{
	if (vertexShadersBlobs.find(key) == vertexShadersBlobs.end())
		throw ExceptionShaders(L"error find vertex shader blob - " + key);
	return vertexShadersBlobs[key];
}

ComPtr<ID3D11PixelShader> Direct3D11ShadersBuffer::getPixelShader(const std::wstring& key)
{
	if (pixelShaders.find(key) == pixelShaders.end())
		throw ExceptionShaders(L"error find pixel shader - " + key);
	return pixelShaders[key];
}

ComPtr<ID3DBlob> Direct3D11ShadersBuffer::getPixelShaderBlob(const std::wstring& key)
{
	if (pixelShadersBlobs.find(key) == pixelShadersBlobs.end())
		throw ExceptionShaders(L"error find vertex shader blob - " + key);
	return pixelShadersBlobs[key];
}

auto Direct3D11ShadersBuffer::getVertexShaders() -> std::map<std::wstring, ComPtr<ID3D11VertexShader>>&
{
	return vertexShaders;
}

auto Direct3D11ShadersBuffer::getVertexShaderBlobs() -> std::map<std::wstring, ComPtr<ID3DBlob>>&
{
	return vertexShadersBlobs;
}

auto Direct3D11ShadersBuffer::getPixelShaders() -> std::map<std::wstring, ComPtr<ID3D11PixelShader>>&
{
	return pixelShaders;
}

auto Direct3D11ShadersBuffer::getPixelShaderBlobs() -> std::map<std::wstring, ComPtr<ID3DBlob>>&
{
	return pixelShadersBlobs;
}

std::string Direct3D11ShadersBuffer::getVersion()
{
	return ver;
}


