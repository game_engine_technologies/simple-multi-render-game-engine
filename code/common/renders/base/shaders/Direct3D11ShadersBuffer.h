#pragma once

#include "../../abstract/shaders/TypeShader.h"
#include "../../abstract/IDisposable.h"

#include "../d3d11_header.h"

#include <map>
#include <filesystem>

#include "../../../utils/base/ExceptionShaders.h"
#include "../../abstract/shaders/IShadersBuffer.h"

namespace fs = std::filesystem;

class Direct3D11ShadersBuffer : public IDisposable, public IShadersBuffer
{
	std::map<std::wstring, ComPtr<ID3D11VertexShader>> vertexShaders;
	std::map<std::wstring, ComPtr<ID3D11PixelShader>> pixelShaders;
	std::map<std::wstring, ComPtr<ID3DBlob>> vertexShadersBlobs;
	std::map<std::wstring, ComPtr<ID3DBlob>> pixelShadersBlobs;

	VisitorShaderBufferCreateAndLoaderSmartPtr creator;
	std::string ver;
public:
	Direct3D11ShadersBuffer() = default;
	Direct3D11ShadersBuffer(const std::wstring& ver);
	ComPtr<ID3DBlob> compileShaderFromFile(const std::wstring& fileName, LPCSTR entryPoint, LPCSTR shaderModel);

	void init(const std::wstring& path, VisitorShaderBufferCreateAndLoaderSmartPtr& shBuffCreator) override;
	void loadShader(const std::wstring& path, const TypeShader& ts) override;

	void clear();
	~Direct3D11ShadersBuffer();

	ComPtr<ID3D11VertexShader> getVertexShader(const std::wstring& key);
	ComPtr<ID3DBlob> getVertexShaderBlob(const std::wstring& key);
	ComPtr<ID3D11PixelShader> getPixelShader(const std::wstring& key);
	ComPtr<ID3DBlob> getPixelShaderBlob(const std::wstring& key);

	auto getVertexShaders()->std::map<std::wstring, ComPtr<ID3D11VertexShader>>&;
	auto getVertexShaderBlobs()->std::map<std::wstring, ComPtr<ID3DBlob>>&;
	auto getPixelShaders()->std::map<std::wstring, ComPtr<ID3D11PixelShader>>&;
	auto getPixelShaderBlobs()->std::map<std::wstring, ComPtr<ID3DBlob>>&;
	std::string getVersion();
};

using Direct3D11ShadersBufferSmartPtr = std::shared_ptr<Direct3D11ShadersBuffer>;