#include "OpenGLShadersBuffer.h"

#include "IShadersBufferVisitor.h"


size_t OpenGLShaderProgramComparator::operator()(const std::vector<GLuint>& p) const
{
	std::hash<std::wstring> hasher;
	std::wstring k;
	std::for_each(p.begin(), p.end(), [&k](GLuint c)
		{
			k += std::to_wstring(c) + L"-";
		});
	return hasher(k);
}

GLuint OpenGLShadersBuffer::createShaderProgram(const std::vector<GLuint>& key)
{
	GLuint shaderProgram = OpenGLFunctional::getInstance()->glCreateProgram();
	for(auto&& k: key)
		OpenGLFunctional::getInstance()->glAttachShader(shaderProgram, k);
	OpenGLFunctional::getInstance()->glLinkProgram(shaderProgram);
	GLint success;
	GLchar infoLog[512];
	OpenGLFunctional::getInstance()->glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
	if (!success) 
	{
		OpenGLFunctional::getInstance()->glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
		std::wstring log(infoLog, infoLog + strlen(infoLog));
		throw ExceptionShaders(L"ERROR::SHADER::PROGRAM::LINKING_FAILED\n" + log + L"\n");
	}
	bufferShaderPrograms.insert({ key, shaderProgram });
	return shaderProgram;
}

void OpenGLShadersBuffer::init(const std::wstring& p, VisitorShaderBufferCreateAndLoaderSmartPtr& shBuffCreator)
{
	creator = shBuffCreator;
	creator->create(p, *this);
}

void OpenGLShadersBuffer::loadShader(const std::wstring& p, const TypeShader& ts)
{
	creator->load(p, ts, *this);
}

void OpenGLShadersBuffer::preprocessor(std::wstring& codeShader, std::wstring& pathShader)
{
	std::vector<std::function<bool(std::wstring&, std::wstring&)>> partsPreprocessor =
	{
		[](std::wstring& code, std::wstring& shaderDir) -> bool // macro insert
		{
				std::wistringstream iss(code);
				std::wstring inc = L"#include";
				std::wstring newCode;
				size_t countInclude(0);
				std::list<std::wstring> lines;
				for (auto iter = std::istream_iterator<locale::LineUnicode, wchar_t>(iss); iter != std::istream_iterator<locale::LineUnicode, wchar_t>(); ++iter)
				{
					size_t startIndex(0);
					std::wstring line(*iter);
					if ((startIndex = line.find(inc)) != std::string::npos)
					{
						std::array<std::wstring, 2> comm = { L"//", L"/*" };
						for (auto&& c : comm)
						{
							size_t ind(0);
							if ((ind = line.find(c)) != std::wstring::npos)
								line.erase(ind);
						}
						line = std::regex_replace(line, std::wregex(L"^[ ]*(.*?)[ ]*$"), L"$1");
						if (line.empty())
							continue;
						std::wcmatch result;
						std::wregex reg(L"(\\s*)(#include)(\\s+)(\")([\\\/\.\\w-]+)(\")(\\s*)");
						if (!std::regex_match(line.c_str(), result, reg))
							throw ExceptionShaders(L"Error #include directive. Shader: " + shaderDir);
						fs::path p(line.substr(startIndex + inc.size() + 2));
						std::wstring _fpath = p.wstring(); // path to include shader
						_fpath.resize(_fpath.size() - 1);
						fs::path fPath = shaderDir + _fpath;
						std::wifstream includeShader(fPath);
						if (!includeShader)
							throw ExceptionShaders(L"error find include shader: " + fPath.wstring());
						std::wstringstream _code;
						_code << includeShader.rdbuf();
						std::wstring codeIncludeShader = _code.str();
						line = codeIncludeShader;
						++countInclude;
					}
					lines.emplace_back(line);
				};
				code.clear();
				for (auto&& l : lines) code += l + L"\n";
				if (countInclude > 0)
					return true;
				return false;
		},
		//[](string& code, string& shaderDir) -> bool // matrix -> mat4
		//{
		//	size_t index;
		//	string findStr = "matrix";
		//	string replaceStr = "mat4";
		//	while ((index = code.find(findStr)) != string::npos)
		//		code.replace(index, findStr.size(), replaceStr);
		//	return true;
		//},
		//[](string& code, string& shaderDir) -> bool // float4 -> vec4
		//{
		//	size_t index;
		//	string findStr = "float4";
		//	string replaceStr = "vec4";
		//	while ((index = code.find(findStr)) != string::npos)
		//		code.replace(index, findStr.size(), replaceStr);
		//	return true;
		//},
		//[](string& code, string& shaderDir) -> bool // float3 -> vec3
		//{
		//	size_t index;
		//	string findStr = "float3";
		//	string replaceStr = "vec3";
		//	while ((index = code.find(findStr)) != string::npos)
		//		code.replace(index, findStr.size(), replaceStr);
		//	return true;
		//},
		//[](string& code, string& shaderDir) -> bool // float2 -> vec2
		//{
		//	size_t index;
		//	string findStr = "float2";
		//	string replaceStr = "vec2";
		//	while ((index = code.find(findStr)) != string::npos)
		//		code.replace(index, findStr.size(), replaceStr);
		//	return true;
		//},
		//[](string& code, string& shaderDir) -> bool // static -> ""
		//{
		//	size_t index;
		//	string findStr = "static";
		//	string replaceStr = "";
		//	while ((index = code.find(findStr)) != string::npos)
		//		code.replace(index, findStr.size(), replaceStr);
		//	return true;
		//}
	};

	bool isIteration(false);
	for (auto iter = partsPreprocessor.begin(); iter != partsPreprocessor.end(); ++iter)
	{
		bool result = (*iter)(codeShader, pathShader);
		if (iter == partsPreprocessor.begin())
		{
			isIteration = result;
		}
	}
	if (isIteration)
		preprocessor(codeShader, pathShader);
}

void OpenGLShadersBuffer::clear()
{

}

OpenGLShadersBuffer::~OpenGLShadersBuffer()
{
	clear();
}

GLuint& OpenGLShadersBuffer::getShader(const std::wstring& key)
{
	if (bufferShaders.find(key) == bufferShaders.end())
		throw ExceptionShaders(L"error find shader - " + key);
	return bufferShaders[key]->getShader();
}

GLuint OpenGLShadersBuffer::getShaderProgram(const std::vector<GLuint>& key)
{
	if (bufferShaderPrograms.find(key) == bufferShaderPrograms.end())
		return createShaderProgram(key);
	return bufferShaderPrograms[key];
}

auto OpenGLShadersBuffer::getShadersBuffer() -> std::map<std::wstring, OpenGLShaderSmartPtr>&
{
	return bufferShaders;
}

auto OpenGLShadersBuffer::getShaderProgramsBuffer() -> std::unordered_map<std::vector<GLuint>, GLuint, OpenGLShaderProgramComparator>&
{
	return bufferShaderPrograms;
}

OpenGLShader::OpenGLShader(const GLuint& sh): shader(sh)
{
}

GLuint& OpenGLShader::getShader()
{
	return shader;
}

OpenGLShader::~OpenGLShader()
{
	OpenGLFunctional::getInstance()->glDeleteShader(shader);
}
