#pragma once

#include "../../abstract/shaders/TypeShader.h"
#include "../../abstract/IDisposable.h"

#include "../gl_header.h"

#include <map>
#include <unordered_map>
#include <filesystem>
#include <string>
#include <array>
#include <regex>

#include "../../../utils/base/ExceptionShaders.h"
#include "../../abstract/shaders/IShadersBuffer.h"

namespace fs = std::filesystem;

class OpenGLShader
{
	GLuint shader;

	OpenGLShader(const OpenGLShader&) = delete;
	void operator=(const OpenGLShader&) = delete;
public:
	OpenGLShader(const GLuint& sh);
	GLuint& getShader();
	~OpenGLShader();
};

using OpenGLShaderSmartPtr = std::unique_ptr<OpenGLShader>;

class OpenGLShaderProgramComparator
{
public:
	size_t operator()(const std::vector<GLuint>& p) const;
};

class OpenGLShadersBuffer : public IDisposable, public IShadersBuffer
{
	std::map<std::wstring, OpenGLShaderSmartPtr> bufferShaders;
	std::unordered_map<std::vector<GLuint>, GLuint, OpenGLShaderProgramComparator> bufferShaderPrograms;

	VisitorShaderBufferCreateAndLoaderSmartPtr creator;

	GLuint createShaderProgram(const std::vector<GLuint>& key);
public:
	OpenGLShadersBuffer() = default;

	void init(const std::wstring& path, VisitorShaderBufferCreateAndLoaderSmartPtr& shBuffCreator) override;
	void loadShader(const std::wstring& path, const TypeShader& ts) override;
	void preprocessor(std::wstring& codeShader, std::wstring& pathShader);

	void clear();
	~OpenGLShadersBuffer();

	GLuint& getShader(const std::wstring& key);
	GLuint getShaderProgram(const std::vector<GLuint>& key);
	auto getShadersBuffer()->std::map<std::wstring, OpenGLShaderSmartPtr>&;
	auto getShaderProgramsBuffer()->std::unordered_map<std::vector<GLuint>, GLuint, OpenGLShaderProgramComparator>&;
	
};

using OpenGLShadersBufferSmartPtr = std::shared_ptr<OpenGLShadersBuffer>;