#define WIN32_LEAN_AND_MEAN

#pragma once

#include <Windows.h>

#include "../../utils/base/TypeCamera.h"

class RenderParameters 
{
	HWND hwnd;
	int w;
	int h;

public:
	HWND getHwnd();
	int getClientWidth();
	int getClientHeight();
	void setHwnd(HWND hInst);
	void setClientWidth(int w);
	void setClientHeight(int h);
};