#pragma once

#include "../abstract/IWindow.h"
#include "../abstract/IDisposable.h"

#include <windowsx.h>

class OSWindowsWindow : public IWindow, public IDisposable
{
protected:
	HWND hwnd;
	std::wstring classWin;

	static HHOOK ExistingKeyboardProc;

public:
	virtual void init(WindowParameters wp) override;
	virtual void resizeViewport(int w, int h) override;
	virtual void resizeWindow(int x, int y, int w, int h) override;
	virtual void run() override;
	virtual void clear() override;
	virtual LRESULT wndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) override;
	virtual ~OSWindowsWindow() override;

	static LRESULT CALLBACK keyboardProcLowLevel(int nCode, WPARAM wParam, LPARAM lParam);
	bool hookKeyboardProc(HINSTANCE hinst);
	void unHookKeyboardProc();

	void resizeAll();
};

