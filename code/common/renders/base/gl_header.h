#pragma once

//#define GLEW_STATIC
//#include "../../../../libs/opengl/glew/include/GL/glew.h"
//// GLFW
//#include "../../../../libs/opengl/glfw/include/GLFW/glfw3.h"
//
//#pragma comment(lib, "../../../../libs/opengl/glew/lib/Release/x64/glew32s.lib")
//#pragma comment(lib, "../../../../libs/opengl/glfw/lib-vc2019/glfw3.lib")

//#ifndef WIN32
////#define WINAPI_FAMILY_PARTITION
//#endif

#include <memory>
#include <functional>

#include "gl/glcorearb.h"

#ifdef WIN32
#include "gl/wglext.h"
#endif
#include <GL/GL.h>

// #include "gl/glxext.h" - linux, macos

#pragma comment(lib, "opengl32.lib")

using FNGLUNIFORMMATRIX4FVPROC = void(GLint location, GLsizei count, GLboolean transpose, const GLfloat* value);
using FNGLGETUNIFORMLOCATIONPROC = GLint(GLuint program, const GLchar* name);
using FNGLGENVERTEXARRAYSPROC = void(GLsizei n, GLuint * arrays);
using FNGLDELETEBUFFERSPROC = void(GLsizei n, const GLuint * buffers);
using FNGLGENBUFFERSPROC = void(GLsizei n, GLuint * buffers);
using FNGLBINDVERTEXARRAYPROC = void(GLuint array);
using FNGLBINDBUFFERPROC = void(GLenum target, GLuint buffer);
using FNGLBUFFERDATAPROC = void(GLenum target, GLsizeiptr size, const void* data, GLenum usage);
using FNGLVERTEXATTRIBPOINTERPROC = void(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const void* pointer);
using FNGLENABLEVERTEXATTRIBARRAYPROC = void(GLuint index);
using FNGLUSEPROGRAMPROC = void(GLuint program);
using FNGLACTIVETEXTUREPROC = void(GLenum texture);
using FNGLUNIFORM1IPROC = void(GLint location, GLint v0);
using FNGLCREATESHADERPROC = GLuint(GLenum type);
using FNGLSHADERSOURCEPROC = void(GLuint shader, GLsizei count, const GLchar* const* string, const GLint * length);
using FNGLCOMPILESHADERPROC = void(GLuint shader);
using FNGLGETSHADERIVPROC = void(GLuint shader, GLenum pname, GLint * params);
using FNGLGETSHADERINFOLOGPROC = void(GLuint shader, GLsizei bufSize, GLsizei * length, GLchar * infoLog);
using FNGLGENERATEMIPMAPPROC = void(GLenum target);
using FNGLDELETEVERTEXARRAYSPROC = void(GLsizei n, const GLuint * arrays);
using FNGLATTACHSHADERPROC = void(GLuint program, GLuint shader);
using FNGLLINKPROGRAMPROC = void(GLuint program);
using FNGLGETPROGRAMIVPROC = void(GLuint program, GLenum pname, GLint * params);
using FNGLGETPROGRAMINFOLOGPROC = void(GLuint program, GLsizei bufSize, GLsizei * length, GLchar * infoLog);
using FNGLDELETESHADERPROC = void(GLuint shader);
using FNGLCREATEPROGRAMPROC = GLuint();
using FNGLMAPBUFFERPROC = void* (GLenum target, GLenum access);
using FNGLUNMAPBUFFERPROC = GLboolean(GLenum target);
using FNGLUNIFORM4FPROC = void(GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3);
using FNGLBINDTEXTUREUNITPROC = void(GLuint unit, GLuint texture);

using FNGLGETDEBUGMESSAGELOGARBPROC = void(GLDEBUGPROCARB callback, const void* userParam);

#ifdef WIN32
using FNWGLCREATECONTEXTATTRIBSARBPROC = HGLRC WINAPI(HDC hDC, HGLRC hShareContext, const int* attribList);
using FNWGLSWAPINTERVALEXTPROC = BOOL(int interval);
#endif

class OpenGLFunctional;
using OpenGLFunctionalSmartPtr = std::shared_ptr<OpenGLFunctional>;

class OpenGLFunctional
{
	static OpenGLFunctionalSmartPtr instance;

	// functionals
	std::function<FNGLUNIFORMMATRIX4FVPROC> _glUniformMatrix4fv;
	std::function<FNGLGETUNIFORMLOCATIONPROC> _glGetUniformLocation;
	std::function<FNGLGENVERTEXARRAYSPROC> _glGenVertexArrays;
	std::function<FNGLDELETEBUFFERSPROC> _glDeleteBuffers;
	std::function<FNGLGENBUFFERSPROC> _glGenBuffers;
	std::function<FNGLBINDVERTEXARRAYPROC> _glBindVertexArray;
	std::function<FNGLBINDBUFFERPROC> _glBindBuffer;
	std::function<FNGLBUFFERDATAPROC> _glBufferData;
	std::function<FNGLVERTEXATTRIBPOINTERPROC> _glVertexAttribPointer;
	std::function<FNGLENABLEVERTEXATTRIBARRAYPROC> _glEnableVertexAttribArray;
	std::function<FNGLUSEPROGRAMPROC> _glUseProgram;
	std::function<FNGLACTIVETEXTUREPROC> _glActiveTexture;
	std::function<FNGLUNIFORM1IPROC> _glUniform1i;
	std::function<FNGLCREATESHADERPROC> _glCreateShader;
	std::function<FNGLSHADERSOURCEPROC> _glShaderSource;
	std::function<FNGLCOMPILESHADERPROC> _glCompileShader;
	std::function<FNGLGETSHADERIVPROC> _glGetShaderiv;
	std::function<FNGLGETSHADERINFOLOGPROC> _glGetShaderInfoLog;
	std::function<FNGLGENERATEMIPMAPPROC> _glGenerateMipmap;
	std::function<FNGLDELETEVERTEXARRAYSPROC> _glDeleteVertexArrays;
	std::function<FNGLATTACHSHADERPROC> _glAttachShader;
	std::function<FNGLLINKPROGRAMPROC> _glLinkProgram;
	std::function<FNGLGETPROGRAMIVPROC> _glGetProgramiv;
	std::function<FNGLGETPROGRAMINFOLOGPROC> _glGetProgramInfoLog;
	std::function<FNGLDELETESHADERPROC> _glDeleteShader;
	std::function<FNGLCREATEPROGRAMPROC> _glCreateProgram;
	std::function<FNGLMAPBUFFERPROC> _glMapBuffer;
	std::function<FNGLUNMAPBUFFERPROC> _glUnmapBuffer;
	std::function<FNGLUNIFORM4FPROC> _glUniform4f;
	std::function<FNGLBINDTEXTUREUNITPROC> _glBindTextureUnit;

	std::function<FNGLGETDEBUGMESSAGELOGARBPROC> _glDebugMessageCallbackARB;

#ifdef WIN32
	std::function<FNWGLCREATECONTEXTATTRIBSARBPROC> _wglCreateContextAttribsARB;
	std::function<FNWGLSWAPINTERVALEXTPROC> _wglSwapIntervalEXT;
#endif

	OpenGLFunctional();
	OpenGLFunctional(const OpenGLFunctional&) = delete;
	void operator=(const OpenGLFunctional&) = delete;

	void* getAnyGLFuncAddress(const char* name);
	void init();

	template<class Signature>
	std::function<Signature> to_function(Signature f)
	{
		return std::function<Signature>(reinterpret_cast<Signature>(f));
	}
public:
	static OpenGLFunctionalSmartPtr& getInstance();

	void glUniformMatrix4fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat* value);
	GLint glGetUniformLocation(GLuint program, const GLchar* name);
	void glGenVertexArrays(GLsizei n, GLuint* arrays);
	void glDeleteBuffers(GLsizei n, const GLuint* buffers);
	void glGenBuffers(GLsizei n, GLuint* buffers);
	void glBindVertexArray(GLuint array);
	void glBindBuffer(GLenum target, GLuint buffer);
	void glBufferData(GLenum target, GLsizeiptr size, const void* data, GLenum usage);
	void glVertexAttribPointer(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const void* pointer);
	void glEnableVertexAttribArray(GLuint index);
	void glUseProgram(GLuint program);
	void glActiveTexture(GLenum texture);
	void glUniform1i(GLint location, GLint v0);
	GLuint glCreateShader(GLenum type);
	void glShaderSource(GLuint shader, GLsizei count, const GLchar* const* string, const GLint* length);
	void glCompileShader(GLuint shader);
	void glGetShaderiv(GLuint shader, GLenum pname, GLint* params);
	void glGetShaderInfoLog(GLuint shader, GLsizei bufSize, GLsizei* length, GLchar* infoLog);
	void glGenerateMipmap(GLenum target);
	void glDeleteVertexArrays(GLsizei n, const GLuint* arrays);
	void glAttachShader(GLuint program, GLuint shader);
	void glLinkProgram(GLuint program);
	void glGetProgramiv(GLuint program, GLenum pname, GLint* params);
	void glGetProgramInfoLog(GLuint program, GLsizei bufSize, GLsizei* length, GLchar* infoLog);
	void glDeleteShader(GLuint shader);
	void* glMapBuffer(GLenum target, GLenum access);
	GLboolean glUnmapBuffer(GLenum target);
	void glUniform4f(GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3);
	void glBindTextureUnit(GLuint unit, GLuint texture);

	GLuint glCreateProgram();

	void glDebugMessageCallbackARB(GLDEBUGPROCARB callback, const void* userParam);

#ifdef WIN32
	HGLRC WINAPI wglCreateContextAttribsARB(HDC hDC, HGLRC hShareContext, const int* attribList);
	BOOL wglSwapIntervalEXT(int interval);
#endif
};
