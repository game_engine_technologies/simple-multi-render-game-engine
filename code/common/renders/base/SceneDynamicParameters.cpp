#include "SceneDynamicParameters.h"

TextBufferDynamicParameterFPS::TextBufferDynamicParameterFPS(int fps):fps(fps)
{
}

std::wstring TextBufferDynamicParameterFPS::to_wstring()
{
	return std::to_wstring(fps);
}

std::wstring TextBufferDynamicParameterFPS::get_key()
{
	return L"l_fps";
}

TextBufferDynamicParameterVSync::TextBufferDynamicParameterVSync(bool vsync):vsync(vsync)
{
}

std::wstring TextBufferDynamicParameterVSync::to_wstring()
{
	return vsync ? L"on" : L"off";
}

std::wstring TextBufferDynamicParameterVSync::get_key()
{
	return L"l_vsunc";
}

TextBufferDynamicParameterFullscreen::TextBufferDynamicParameterFullscreen(bool fullscreen):fullscreen(fullscreen)
{
}

std::wstring TextBufferDynamicParameterFullscreen::to_wstring()
{
	return fullscreen ? L"on" : L"off";
}

std::wstring TextBufferDynamicParameterFullscreen::get_key()
{
	return L"l_fscreen";
}

TextBufferDynamicParameterSizeWindow::TextBufferDynamicParameterSizeWindow(int w, int h):w(w), h(h)
{
}

std::wstring TextBufferDynamicParameterSizeWindow::to_wstring()
{
	return std::to_wstring(w) + L" x " + std::to_wstring(h);
}

std::wstring TextBufferDynamicParameterSizeWindow::get_key()
{
	return L"l_sizewin";
}