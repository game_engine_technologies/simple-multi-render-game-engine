#pragma once

#include "../abstract/IRender.h"
#include "../abstract/IDisposable.h"
#include "../../utils/base/ExceptionOpenGL.h"

#include <set>

#include "scene/OpenGLScene.h"

//using WglCreateContextAttribsARB = HGLRC(APIENTRY*)(HDC, HGLRC, const int*);
//using WglSwapIntervalEXT = BOOL(WINAPI*)(int interval);

namespace debug
{
	void APIENTRY glDebugOutput(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam);
}

class OSWindowsOpenGLRender : public IRender, public IDisposable
{
protected:
	HDC hdc;
	HGLRC hrc;
	HWND hwnd;

	OpenGLSceneSmartPtr scene;

	// fullscreen
	DWORD savedExStyle; // ����������� ����������� ����� ����
	DWORD savedStyle; // ����������� ����� ����
	RECT rectRestoreWindow; // ����������� ������������� �������� ������

	std::vector<DEVMODE> allModesScreen; // ��� �������������� ��������� ����������
	DEVMODE currentModeDescription; //  ������� �������� ���������� ������
	DEVMODE fullscreenModeDescription; // ���������� ������ ��� fullscreen

	int versionAPIMinor;
	int versionAPIMajor;

public:
	OSWindowsOpenGLRender();
	void init(RenderParameters wp) override;
	void resizeViewport(int w, int h) override;
	void resizeWindow(int x, int y, int w, int h) override;
	void clear()override;
	void setTypeRasterizer(const TypeRasterizer& tr)override;
	void render() override;
	TypeRasterizer getTypeRasterizer() override;
	void setVsync(bool r) override;
	void changeModeScreen(bool fs)override;

	~OSWindowsOpenGLRender() override;

	void pressAltTab(bool stat) override;
	void getAllSupportedModes() override;
	void setFullscreen();
	void restoreScreen();
};
