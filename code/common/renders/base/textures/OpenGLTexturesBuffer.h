#pragma once
#include "../../abstract/textures/ITexturesBuffer.h"

#include "../gl_header.h"

//class IOpenGLSampler
//{
//public:
//	virtual void setSampler, (GLuint texture) = 0;
//};
//
//using OpenGLSamplerSmartPtr = std::shared_ptr<IOpenGLSampler>;

class OpenGLSamplerRepeat/*: public IOpenGLSampler*/
{
public:
	static void setSampler(GLuint texture, const std::wstring& type);
};


class OpenGLTexturesBuffer : public ITexturesBuffer
{
	std::map<std::wstring, GLuint> bufferTextures;
	//std::map<std::wstring, OpenGLSamplerSmartPtr> bufferSamples;

public:
	void init(const std::wstring path, const TexturesLoaderSmartPtr& loader, const TexturesBufferVisitorCreatorAndAddedSmartPtr& visitor) override;
	void add(const std::wstring path) override;
	TexturesLoaderSmartPtr& getLoaderTextures() override;

	auto getBufferTextures()->std::map<std::wstring, GLuint>&;
	//auto getBufferSamples()->std::map<std::wstring, OpenGLSamplerSmartPtr>&;
	auto getTexture(const std::wstring& s)->GLuint&;
	//auto getSampler(const std::wstring& s)->OpenGLSamplerSmartPtr&;
};


using OpenGLTexturesBufferSmartPtr = std::shared_ptr<OpenGLTexturesBuffer>;
