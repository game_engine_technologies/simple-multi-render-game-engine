#pragma once

#include <string>

#include "../d3d11_header.h"
#include "../../abstract/shaders/TypeShader.h"

#include <array>
#include <filesystem>

namespace fs = std::filesystem;

class Direct3D11TexturesBuffer;
class OpenGLTexturesBuffer;

class ITexturesBufferVisitorCreator
{
public:
	virtual void create(const std::wstring& path, Direct3D11TexturesBuffer& a) = 0;
	virtual void create(const std::wstring& path, OpenGLTexturesBuffer& a) = 0;
};

class ITexturesBufferVisitorAdded
{
public:
	virtual void add(const std::wstring& path, Direct3D11TexturesBuffer& a) = 0;
	virtual void add(const std::wstring& path, OpenGLTexturesBuffer& a) = 0;
};

class TexturesBufferVisitorCreatorAndAdded: public ITexturesBufferVisitorCreator, public ITexturesBufferVisitorAdded
{
	ComPtr<ID3D11Device> d3dDevice;

	bool isImg(std::wstring& ex);
public:
	TexturesBufferVisitorCreatorAndAdded();
	TexturesBufferVisitorCreatorAndAdded(const ComPtr<ID3D11Device>& d3dDevice);
	void create(const std::wstring& path, Direct3D11TexturesBuffer& a) override;
	void create(const std::wstring& path, OpenGLTexturesBuffer& a) override;
	void add(const std::wstring& path, Direct3D11TexturesBuffer& a) override;
	void add(const std::wstring& path, OpenGLTexturesBuffer& a) override;
};