#include "OpenGLTexturesBuffer.h"

#include "ITexturesBufferVisitor.h"

void OpenGLSamplerRepeat::setSampler(GLuint texture, const std::wstring& type)
{
    glBindTexture(GL_TEXTURE_2D, texture);
    // Set our texture parameters
    if (type == L"default")
    {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// Set texture wrapping to GL_REPEAT
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }
    else
    {
        throw ExceptionTextures(L"error find sampler - " + type);
    }
    // Set texture filtering
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0); // Unbind texture when done, so we won't accidentily mess up our texture.
}


void OpenGLTexturesBuffer::init(const std::wstring path, const TexturesLoaderSmartPtr& loader, const TexturesBufferVisitorCreatorAndAddedSmartPtr& visitor)
{
    this->texturesLoader = loader;
    this->visitor = visitor;
    this->visitor->create(path, *this);
}

void OpenGLTexturesBuffer::add(const std::wstring path)
{
    this->visitor->add(path, *this);
}

TexturesLoaderSmartPtr& OpenGLTexturesBuffer::getLoaderTextures()
{
    return texturesLoader;
}

auto OpenGLTexturesBuffer::getBufferTextures() -> std::map<std::wstring, GLuint>&
{
    return bufferTextures;
}

//auto OpenGLTexturesBuffer::getBufferSamples() -> std::map<std::wstring, OpenGLSamplerSmartPtr>&
//{
//    return bufferSamples;
//}

auto OpenGLTexturesBuffer::getTexture(const std::wstring& key) -> GLuint&
{
    if (bufferTextures.find(key) == bufferTextures.end())
        throw ExceptionTextures(L"error find texture - " + key);
    return bufferTextures[key];
}

//auto OpenGLTexturesBuffer::getSampler(const std::wstring& key) -> OpenGLSamplerSmartPtr&
//{
//    if (bufferSamples.find(key) == bufferSamples.end())
//        throw ExceptionTextures(L"error find sampler - " + key);
//    return bufferSamples[key];
//}
