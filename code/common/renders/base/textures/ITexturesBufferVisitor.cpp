#include "ITexturesBufferVisitor.h"

#include "../../../utils/abstract/loaders/ITexturesLoader.h"

#include "Direct3D11TexturesBuffer.h"
#include "OpenGLTexturesBuffer.h"

bool TexturesBufferVisitorCreatorAndAdded::isImg(std::wstring& ex)
{
	std::array<std::wstring, 7> _ex =
	{
		L".dds", L".png", L".jpg", L".jpeg", L".tga", L".bmp", L".gif"
	};
	std::transform(ex.begin(), ex.end(), ex.begin(), towlower);
	return !(std::find(_ex.begin(), _ex.end(), ex) == _ex.end());
}

TexturesBufferVisitorCreatorAndAdded::TexturesBufferVisitorCreatorAndAdded()
{
}

TexturesBufferVisitorCreatorAndAdded::TexturesBufferVisitorCreatorAndAdded(const ComPtr<ID3D11Device>& d3dDevice): d3dDevice(d3dDevice)
{
}

void TexturesBufferVisitorCreatorAndAdded::create(const std::wstring& path, Direct3D11TexturesBuffer& a)
{
	// create default sampler
	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;      // ��� ����������
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;         // ������ ����������
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
	// ������� ��������� ������ ���������������
	ComPtr<ID3D11SamplerState> samplerDefault;
	HRESULT hr = d3dDevice->CreateSamplerState(&sampDesc, samplerDefault.GetAddressOf());
	if (FAILED(hr))
		throw ExceptionTextures(L"Error create default sampler in Direct3D11 render\n");
	a.getBufferSamples().insert({ L"default", samplerDefault });

	fs::path shaderCatalog(path);
	for (fs::recursive_directory_iterator iter(shaderCatalog); iter != fs::recursive_directory_iterator(); ++iter)
	{
		std::wstring name(iter->path().wstring());
		std::replace(name.begin(), name.end(), L'\\', L'/');
		std::wstring ext(iter->path().extension().wstring());
		if (isImg(ext))
			add(name, a);
	}
}

void TexturesBufferVisitorCreatorAndAdded::create(const std::wstring& path, OpenGLTexturesBuffer& a)
{
	fs::path shaderCatalog(path);
	for (fs::recursive_directory_iterator iter(shaderCatalog); iter != fs::recursive_directory_iterator(); ++iter)
	{
		std::wstring name(iter->path().wstring());
		std::replace(name.begin(), name.end(), L'\\', L'/');
		std::wstring ext(iter->path().extension().wstring());
		if (isImg(ext))
			add(name, a);
	}
}

void TexturesBufferVisitorCreatorAndAdded::add(const std::wstring& path, Direct3D11TexturesBuffer& a)
{
	Texture texture = a.getLoaderTextures()->loadTexture(path);
	ComPtr<ID3D11ShaderResourceView> textureTarget;
	ComPtr<ID3D11Texture2D> image;
	D3D11_TEXTURE2D_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
	desc.Width = texture.width;
	desc.Height = texture.height;
	desc.MipLevels = desc.ArraySize = 1;
	desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	desc.SampleDesc.Count = 1;
	desc.Usage = D3D11_USAGE_DEFAULT;
	desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	D3D11_SUBRESOURCE_DATA initData;
	ZeroMemory(&initData, sizeof(initData));
	initData.pSysMem = texture.image.data();
	initData.SysMemPitch = texture.width * 4; // ��� �� ����, ��� �� ������
	HRESULT hr = d3dDevice->CreateTexture2D(&desc, &initData, image.GetAddressOf());
	if (FAILED(hr))
		throw ExceptionTextures(L"Error create Texture2D Direct3D11 render - " + path + L"\n");
	D3D11_SHADER_RESOURCE_VIEW_DESC descSRD;
	ZeroMemory(&descSRD, sizeof(descSRD));
	descSRD.Format = desc.Format;
	descSRD.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	descSRD.Texture2D.MostDetailedMip = 0;
	descSRD.Texture2D.MipLevels = 1;
	hr = d3dDevice->CreateShaderResourceView(image.Get(), &descSRD, textureTarget.GetAddressOf());
	if (FAILED(hr))
		throw ExceptionTextures(L"Error create shader resource view Direct3D11 render - " + path + L"\n");
	a.getBufferTextures().insert({ path, textureTarget });
}

void TexturesBufferVisitorCreatorAndAdded::add(const std::wstring& path, OpenGLTexturesBuffer& a)
{
	Texture texture = a.getLoaderTextures()->loadTexture(path);
	GLuint glTexture(-1);
	glGenTextures(1, &glTexture);
	glBindTexture(GL_TEXTURE_2D, glTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texture.width, texture.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture.image.data());
	OpenGLFunctional::getInstance()->glGenerateMipmap(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);
	a.getBufferTextures().insert({ path, glTexture });
}
