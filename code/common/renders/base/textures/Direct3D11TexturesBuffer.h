#pragma once
#include "../../abstract/textures/ITexturesBuffer.h"

#include "../d3d11_header.h"

class Direct3D11TexturesBuffer: public ITexturesBuffer
{
	std::map<std::wstring, ComPtr<ID3D11ShaderResourceView>> bufferTextures;
	std::map<std::wstring, ComPtr<ID3D11SamplerState>> bufferSamples;

public:
	void init(const std::wstring path, const TexturesLoaderSmartPtr& loader, const TexturesBufferVisitorCreatorAndAddedSmartPtr& visitor) override;
	void add(const std::wstring path) override;
	TexturesLoaderSmartPtr& getLoaderTextures() override;

	auto getBufferTextures()->std::map<std::wstring, ComPtr<ID3D11ShaderResourceView>>&;
	auto getBufferSamples()->std::map<std::wstring, ComPtr<ID3D11SamplerState>>&;
	auto getTexture(const std::wstring& s)->ComPtr<ID3D11ShaderResourceView>&;
	auto getSampler(const std::wstring& s)->ComPtr<ID3D11SamplerState>&;

};


using Direct3D11TexturesBufferSmartPtr = std::shared_ptr<Direct3D11TexturesBuffer>;