#include "Direct3D11TexturesBuffer.h"

#include "ITexturesBufferVisitor.h"

void Direct3D11TexturesBuffer::init(const std::wstring path, const TexturesLoaderSmartPtr& loader, const TexturesBufferVisitorCreatorAndAddedSmartPtr& visitor)
{
	this->texturesLoader = loader;
	this->visitor = visitor;
	this->visitor->create(path, *this);
}

void Direct3D11TexturesBuffer::add(const std::wstring path)
{
	this->visitor->add(path, *this);
}

TexturesLoaderSmartPtr& Direct3D11TexturesBuffer::getLoaderTextures()
{
	return texturesLoader;
}

auto Direct3D11TexturesBuffer::getBufferTextures() -> std::map<std::wstring, ComPtr<ID3D11ShaderResourceView>>&
{
	return bufferTextures;
}

auto Direct3D11TexturesBuffer::getBufferSamples() -> std::map<std::wstring, ComPtr<ID3D11SamplerState>>&
{
	return bufferSamples;
}

auto Direct3D11TexturesBuffer::getTexture(const std::wstring& key) -> ComPtr<ID3D11ShaderResourceView>&
{
	if (bufferTextures.find(key) == bufferTextures.end())
		throw ExceptionTextures(L"error find texture - " + key);
	return bufferTextures[key];
}

auto Direct3D11TexturesBuffer::getSampler(const std::wstring& key) -> ComPtr<ID3D11SamplerState>&
{
	if (bufferSamples.find(key) == bufferSamples.end())
		throw ExceptionTextures(L"error find sampler - " + key);
	return bufferSamples[key];
}
