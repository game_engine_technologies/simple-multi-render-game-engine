#include "OSWindowsOpenGLRender.h"
#include "SingletonHelper.h"
#include "../../os/base/LevelOperationSystemSingleton.h"

OSWindowsOpenGLRender::OSWindowsOpenGLRender()
{
	sizeWindowChanged = false;
	running = true;
	vsync = false;
	fullscreen = false;
}

void OSWindowsOpenGLRender::init(RenderParameters rp)
{
	SingletonHelper::getInstance()->setCurrentRender(this);

	hwnd = rp.getHwnd();
	// init
	hdc = GetDC(hwnd);
	int pf = 0;
	PIXELFORMATDESCRIPTOR pfd = { 0 };
	pfd.nSize = sizeof(pfd);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 24;
	pfd.cDepthBits = 16;
	pfd.iLayerType = PFD_MAIN_PLANE;
	pf = ChoosePixelFormat(hdc, &pfd);
	if (!SetPixelFormat(hdc, pf, &pfd))
		throw ExceptionOpenGL(L"Error SetPixelFormat from OpenGL render");
	versionAPIMajor = 4;
	versionAPIMinor = 5;
	int flags[] = { WGL_CONTEXT_MAJOR_VERSION_ARB, versionAPIMajor,
		WGL_CONTEXT_MINOR_VERSION_ARB, versionAPIMinor,
		WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB | WGL_CONTEXT_DEBUG_BIT_ARB,
		WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
		0 };

	// make context
	HGLRC TempRC = wglCreateContext(hdc);
	if (!TempRC)
		throw ExceptionOpenGL(L"Error create temp (old - style) context from OpenGL render");
	if (!wglMakeCurrent(hdc, TempRC))
	{
		if(!wglDeleteContext(TempRC))
			throw ExceptionOpenGL(L"Error delete temp (old - style) context from OpenGL render");
		throw ExceptionOpenGL(L"Error make temp (old - style) context from OpenGL render");
	}
	hrc = OpenGLFunctional::getInstance()->wglCreateContextAttribsARB(hdc, 0, flags);
	if (!wglMakeCurrent(hdc, hrc))
	{
		if(!wglDeleteContext(hrc))
			throw ExceptionOpenGL(L"Error delete (new - style) context from OpenGL render");
		throw ExceptionOpenGL(L"Error make (new - style) context from OpenGL render");
	}
	if(!wglDeleteContext(TempRC))
		throw ExceptionOpenGL(L"Error delete temp (old - style) context from OpenGL render");

	// debug
	glEnable(GL_DEBUG_OUTPUT);
	OpenGLFunctional::getInstance()->glDebugMessageCallbackARB(debug::glDebugOutput, 0);

	// vsync
	setVsync(vsync);

	// set props
	setTypeRasterizer(TypeRasterizer::SOLID_RASTERIZED);

	// fullscreen mode
	savedExStyle = GetWindowLong(hwnd, GWL_EXSTYLE);
	savedStyle = GetWindowLong(hwnd, GWL_STYLE);
	GetWindowRect(hwnd, &rectRestoreWindow);
	getAllSupportedModes();
	resizeViewport(rp.getClientWidth(), rp.getClientHeight());
	changeModeScreen(false); // init fullscreen

	// ��������� ������ �����������
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CW); // �� ������� ����� ������
}

void OSWindowsOpenGLRender::resizeViewport(int w, int h)
{
	OutputDebugString(L"resize\n");
	if (fullscreen)
	{
		w = fullscreenModeDescription.dmPelsWidth;
		h = fullscreenModeDescription.dmPelsHeight;
	}
	setWidth(w);
	setHeight(h);
	glViewport(0, 0, w, h);
}

void OSWindowsOpenGLRender::resizeWindow(int x, int y, int w, int h)
{
	if (scene)
	{
		sizeWindowChanged = true;
		scene->setWindowBBox(x, y, w, h);
	}
}


void OSWindowsOpenGLRender::clear()
{

}

void OSWindowsOpenGLRender::setTypeRasterizer(const TypeRasterizer& tr)
{
	typeRasterizer = tr;
	if (typeRasterizer == TypeRasterizer::WIREFRAME_RASTERIZED)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	else
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void OSWindowsOpenGLRender::render()
{
	SwapBuffers(hdc);
}

TypeRasterizer OSWindowsOpenGLRender::getTypeRasterizer()
{
	return typeRasterizer;
}

void OSWindowsOpenGLRender::setVsync(bool r)
{
	IRender::setVsync(r);
	if (!OpenGLFunctional::getInstance()->wglSwapIntervalEXT(vsync ? 1 : 0))
		throw ExceptionOpenGL(L"Error enable / disable vsync from OpenGL render");
}

OSWindowsOpenGLRender::~OSWindowsOpenGLRender()
{
	clear();
}

void OSWindowsOpenGLRender::pressAltTab(bool stat)
{
	if (fullscreen)
	{
		if (stat) // ������������ fullscreen & alt + tab
		{
			ChangeDisplaySettings(&currentModeDescription, CDS_FULLSCREEN);
			ShowWindow(hwnd, SW_SHOWMINNOACTIVE);
			running = false;
		}
		else // �������������� ����� fullscreen & alt + tab
		{
			ShowWindow(hwnd, SW_SHOWNORMAL);
			running = true;
			ChangeDisplaySettings(&fullscreenModeDescription, CDS_FULLSCREEN);
		}
	}
	else
	{
		running = !stat;
	}
}

void OSWindowsOpenGLRender::getAllSupportedModes()
{
	int i(0);
	DEVMODE devMode = {};
	memset(&devMode, 0, sizeof(DEVMODE));
	devMode.dmSize = sizeof(DEVMODE);
	auto comp = [](DEVMODE x, DEVMODE y) 
	{
		return x.dmPelsWidth != y.dmPelsWidth ? x.dmPelsWidth < y.dmPelsWidth : x.dmPelsHeight < y.dmPelsHeight;
	};
	std::set<DEVMODE, decltype(comp)> _modes;

	while(true)
	{
		if (!EnumDisplaySettings(NULL, i++, &devMode))
			break;
		_modes.insert(devMode); // �� ��������� �������
	};
	std::copy(_modes.begin(), _modes.end(), std::back_inserter(allModesScreen));
	for (auto&& m : allModesScreen)
		std::cout << (std::to_string(m.dmPelsWidth) + " - " + std::to_string(m.dmPelsHeight) + "\n").c_str() << std::endl;
	EnumDisplaySettings(NULL, ENUM_CURRENT_SETTINGS, &currentModeDescription);
	fullscreenModeDescription = allModesScreen[allModesScreen.size() - 1];
}

void OSWindowsOpenGLRender::changeModeScreen(bool fs)
{
	if (fs)
		fullscreen = !fullscreen;

	if (fullscreen)
		setFullscreen();
	else
		restoreScreen();
}

void OSWindowsOpenGLRender::setFullscreen()
{
	savedExStyle = GetWindowLong(hwnd, GWL_EXSTYLE);
	savedStyle = GetWindowLong(hwnd, GWL_STYLE);
	GetWindowRect(hwnd, &rectRestoreWindow);

	SetWindowLong(hwnd, GWL_EXSTYLE, 0);
	SetWindowLong(hwnd, GWL_STYLE, WS_POPUP | WS_CLIPCHILDREN | WS_CLIPSIBLINGS);
	SetWindowPos(hwnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED | SWP_SHOWWINDOW);
	SetWindowPos(hwnd, HWND_TOPMOST, 0, 0, fullscreenModeDescription.dmPelsWidth, fullscreenModeDescription.dmPelsHeight, SWP_SHOWWINDOW);

	ChangeDisplaySettings(&fullscreenModeDescription, CDS_FULLSCREEN);
}

void OSWindowsOpenGLRender::restoreScreen()
{
	ChangeDisplaySettings(&currentModeDescription, CDS_FULLSCREEN);

	SetWindowLong(hwnd, GWL_EXSTYLE, savedExStyle);
	SetWindowLong(hwnd, GWL_STYLE, savedStyle);
	SetWindowPos(hwnd, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED | SWP_SHOWWINDOW);

	SetWindowPos(hwnd, HWND_NOTOPMOST, rectRestoreWindow.left, rectRestoreWindow.top,
		rectRestoreWindow.right - rectRestoreWindow.left, rectRestoreWindow.bottom - rectRestoreWindow.top, SWP_SHOWWINDOW);
}

void debug::glDebugOutput(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam)
{
	// ignore non-significant error/warning codes
	if (id == 131169 || id == 131185 || id == 131218 || id == 131204)
		return;

	std::wstringstream error;

	error << "---------------" << std::endl;
	error << L"Debug message (" << id << L"): " << std::wstring(message, message + strlen(message)) << std::endl;

	switch (source)
	{
	case GL_DEBUG_SOURCE_API:
		error << "Source: API";
		break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
		error << "Source: Window System";
		break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER:
		error << "Source: Shader Compiler";
		break;
	case GL_DEBUG_SOURCE_THIRD_PARTY:
		error << "Source: Third Party";
		break;
	case GL_DEBUG_SOURCE_APPLICATION:
		error << "Source: Application";
		break;
	case GL_DEBUG_SOURCE_OTHER:
		error << "Source: Other";
		break;
	}
	error << std::endl;

	switch (type)
	{
	case GL_DEBUG_TYPE_ERROR:
		error << L"Type: Error";
		break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
		error << L"Type: Deprecated Behaviour";
		break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
		error << L"Type: Undefined Behaviour";
		break;
	case GL_DEBUG_TYPE_PORTABILITY:
		error << L"Type: Portability";
		break;
	case GL_DEBUG_TYPE_PERFORMANCE:
		error << L"Type: Performance";
		break;
	case GL_DEBUG_TYPE_MARKER:
		error << L"Type: Marker";
		break;
	case GL_DEBUG_TYPE_PUSH_GROUP:
		error << L"Type: Push Group";
		break;
	case GL_DEBUG_TYPE_POP_GROUP:
		error << L"Type: Pop Group";
		break;
	case GL_DEBUG_TYPE_OTHER:
		error << L"Type: Other";
		break;
	}
	error << std::endl;

	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:
		error << L"Severity: high";
		break;
	case GL_DEBUG_SEVERITY_MEDIUM:
		error << L"Severity: medium";
		break;
	case GL_DEBUG_SEVERITY_LOW:
		error << L"Severity: low";
		break;
	case GL_DEBUG_SEVERITY_NOTIFICATION:
		error << L"Severity: notification";
		break;
	}
	error << std::endl;
	error << std::endl;

	throw ExceptionOpenGL(L"Error render ExceptionOpenGL. \n" + error.str());
}

