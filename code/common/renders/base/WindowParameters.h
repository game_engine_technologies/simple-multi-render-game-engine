#define WIN32_LEAN_AND_MEAN

#pragma once

#ifndef WIN32
#include <Windows.h>
#endif

class WindowParameters 
{
	HINSTANCE hInst;
	int w;
	int h;
public:
	HINSTANCE getHinstance();
	int getWidth();
	int getHeight();
	void setHinstance(HINSTANCE hInst);
	void setWidth(int w);
	void setHeight(int h);
};