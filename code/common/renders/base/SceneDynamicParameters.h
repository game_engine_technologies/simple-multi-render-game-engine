#pragma once

#include <stack>
#include <string>
#include <memory>

class TextBufferDynamicParameter
{
public:
	virtual std::wstring to_wstring() = 0;
	virtual std::wstring get_key() = 0;
};

class TextBufferDynamicParameterFPS : public TextBufferDynamicParameter
{
	int fps;
public:
	TextBufferDynamicParameterFPS(int fps);
	std::wstring to_wstring() override;
	std::wstring get_key() override;
};

class TextBufferDynamicParameterVSync : public TextBufferDynamicParameter
{
	bool vsync;
public:
	TextBufferDynamicParameterVSync(bool vsync);
	std::wstring to_wstring() override;
	std::wstring get_key() override;
};

class TextBufferDynamicParameterFullscreen : public TextBufferDynamicParameter
{
	bool fullscreen;
public:
	TextBufferDynamicParameterFullscreen(bool fullscreen);
	std::wstring to_wstring() override;
	std::wstring get_key() override;
};

class TextBufferDynamicParameterSizeWindow : public TextBufferDynamicParameter
{
	int w;
	int h;
public:
	TextBufferDynamicParameterSizeWindow(int w, int h);
	std::wstring to_wstring() override;
	std::wstring get_key() override;
};

using TextBufferDynamicParameterSmartPtr = std::shared_ptr<TextBufferDynamicParameter>;
//using TextBufferDynamicParameterFPSSmartPtr = std::shared_ptr<TextBufferDynamicParameterFPS>;
//using TextBufferDynamicParameterVSyncSmartPtr = std::shared_ptr<TextBufferDynamicParameterVSync>;
//using TextBufferDynamicParameterFullscreenSmartPtr = std::shared_ptr<TextBufferDynamicParameterFullscreen>;
//using TextBufferDynamicParameterSizeWindowSmartPtr = std::shared_ptr<TextBufferDynamicParameterSizeWindow>;

struct SceneDynamicParameters
{
	bool start;
	double frameTime;
	std::stack<TextBufferDynamicParameterSmartPtr> textBuffStack;

	int fps;
	bool vsync;
	bool fullscreen;
	bool second;
};

