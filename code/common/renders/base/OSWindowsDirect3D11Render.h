#pragma once

#include "../abstract/IRender.h"
#include "../abstract/IDisposable.h"
#include "../../utils/base/ExceptionDirect3D11.h"

#include "scene/Direct3D11Scene.h"

//#if defined(DEBUG) || defined(_DEBUG)  
//#pragma comment(lib, "../../../../output/libs/Debug/x86/DXUT.lib")
//#else
//
//#endif

namespace debug
{
#if defined(DEBUG) | defined(_DEBUG)
	template<class T>
	void checkHresult(T val)
	{
		HRESULT hr(val);
		if (FAILED(hr))
		{
			const char* line(__FILE__);
			std::wstring wline(line, line + strlen(line));
			std::string err(std::system_category().message(hr));
			std::wstring werr(err.begin(), err.end());
			DXTrace(wline.c_str(), (DWORD)__LINE__, hr, werr.c_str(), true);
			//throw ExceptionDirect3D11(L"Error in checkHresult");
		}
	}
#else
	template<class T>
	bool checkHresult(T val)
	{
		return true;
	}
#endif 
}

class OSWindowsDirect3D11Render : public IRender, public IDisposable
{
protected:
	ComPtr<ID3D11Device> d3dDevice;
	D3D_DRIVER_TYPE d3dDriverType;
	ComPtr<ID3D11DeviceContext> d3dImmediateContext;
	ComPtr<IDXGISwapChain> d3dSwapChain;
	ComPtr<ID3D11RasterizerState> d3dRasterizer;

	ComPtr<ID3D11Texture2D> d3dDepthStencilBuffer;
	ComPtr<ID3D11DepthStencilState> d3dDepthStencilEnableDepth;
	ComPtr<ID3D11DepthStencilState> d3dDepthStencilDisableDepth;
	ComPtr<ID3D11RenderTargetView> d3dRenderTargetView;
	ComPtr<ID3D11DepthStencilView> d3dDepthStencilView;
	D3D11_VIEWPORT d3dScreenViewport;

	UINT d3d4xMsaaQuality;
	bool d3dEnable4xMsaa;

	Direct3D11SceneSmartPtr scene;
	HWND hwnd;

	// fullscreen
	RECT rectRestoreWindow; // ����������� ������������� �������� ������
	bool maximazeRestoreWindow; // ���� �� ���� ��������������� � ������� ������?

	// WINDOWPLACEMENT windowedPlacement;
	DXGI_FORMAT desiredColourFormat; // �������� ������
	std::vector<DXGI_MODE_DESC> allModesScreen; // ��� �������������� ��������� ����������
	DXGI_MODE_DESC fullscreenModeDescription; // ������� �������� ���������� ������
	
	int versionAPI;

public:
	OSWindowsDirect3D11Render();
	void init(RenderParameters wp) override;
	void resizeViewport(int w, int h) override;
	void resizeWindow(int x, int y, int w, int h) override;
	void clear()override;
	void setTypeRasterizer(const TypeRasterizer& tr)override;
	TypeRasterizer getTypeRasterizer() override;
	void render()override;
	void changeModeScreen(bool fs)override;

	~OSWindowsDirect3D11Render() override;

	void pressAltTab(bool stat) override;
	void getAllSupportedModes() override;
};

