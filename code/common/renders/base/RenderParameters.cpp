#include "RenderParameters.h"

HWND RenderParameters::getHwnd()
{
	return hwnd;
}

int RenderParameters::getClientWidth()
{
	return w;
}

int RenderParameters::getClientHeight()
{
	return h;
}

void RenderParameters::setHwnd(HWND hwnd)
{
	this->hwnd = hwnd;
}

void RenderParameters::setClientWidth(int w)
{
	this->w = w;
}

void RenderParameters::setClientHeight(int h)
{
	this->h = h;
}

