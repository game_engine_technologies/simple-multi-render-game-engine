#include "gl_header.h"
#include "../../utils/base/ExceptionMemory.h"
#include "../../utils/base/ExceptionOpenGL.h"

OpenGLFunctionalSmartPtr OpenGLFunctional::instance;

OpenGLFunctional::OpenGLFunctional()
{
}

void* OpenGLFunctional::getAnyGLFuncAddress(const char* name)
{
    void* p(nullptr);
#ifdef WIN32
    p = (void*)wglGetProcAddress(name);
    if (p == 0 || (p == (void*)0x1) || (p == (void*)0x2) || (p == (void*)0x3) || (p == (void*)-1))
    {
        HMODULE module = LoadLibraryA("opengl32.dll");
        p = (void*)GetProcAddress(module, name);
    }
#endif
    return p;
}



void OpenGLFunctional::init()
{
    if(!(_glUniformMatrix4fv = reinterpret_cast<PFNGLUNIFORMMATRIX4FVPROC>(getAnyGLFuncAddress("glUniformMatrix4fv"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load glUniformMatrix4fv function!");
    if (!(_glGetUniformLocation = reinterpret_cast<PFNGLGETUNIFORMLOCATIONPROC>(getAnyGLFuncAddress("glGetUniformLocation"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load glGetUniformLocation function!");
    if (!(_glGenVertexArrays = reinterpret_cast<PFNGLGENVERTEXARRAYSPROC>(getAnyGLFuncAddress("glGenVertexArrays"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load glGenVertexArrays function!");
    if (!(_glDeleteBuffers = reinterpret_cast<PFNGLDELETEBUFFERSPROC>(getAnyGLFuncAddress("glDeleteBuffers"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load glDeleteBuffers function!");
    if (!(_glGenBuffers = reinterpret_cast<PFNGLGENBUFFERSPROC>(getAnyGLFuncAddress("glGenBuffers"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load glGenBuffers function!");
    if (!(_glBindVertexArray = reinterpret_cast<PFNGLBINDVERTEXARRAYPROC>(getAnyGLFuncAddress("glBindVertexArray"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load glBindVertexArray function!");
    if (!(_glBindBuffer = reinterpret_cast<PFNGLBINDBUFFERPROC>(getAnyGLFuncAddress("glBindBuffer"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load glBindBuffer function!");
    if (!(_glBufferData = reinterpret_cast<PFNGLBUFFERDATAPROC>(getAnyGLFuncAddress("glBufferData"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load glBufferData function!");
    if (!(_glVertexAttribPointer = reinterpret_cast<PFNGLVERTEXATTRIBPOINTERPROC>(getAnyGLFuncAddress("glVertexAttribPointer"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load glVertexAttribPointer function!");
    if (!(_glEnableVertexAttribArray = reinterpret_cast<PFNGLENABLEVERTEXATTRIBARRAYPROC>(getAnyGLFuncAddress("glEnableVertexAttribArray"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load glEnableVertexAttribArray function!");
    if (!(_glUseProgram = reinterpret_cast<PFNGLUSEPROGRAMPROC>(getAnyGLFuncAddress("glUseProgram"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load glUseProgram function!");
    if (!(_glActiveTexture = reinterpret_cast<PFNGLACTIVETEXTUREPROC>(getAnyGLFuncAddress("glActiveTexture"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load glActiveTexture function!");
    if (!(_glUniform1i = reinterpret_cast<PFNGLUNIFORM1IPROC>(getAnyGLFuncAddress("glUniform1i"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load glUniform1i function!");
    if (!(_glCreateShader = reinterpret_cast<PFNGLCREATESHADERPROC>(getAnyGLFuncAddress("glCreateShader"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load glCreateShader function!");
    if (!(_glShaderSource = reinterpret_cast<PFNGLSHADERSOURCEPROC>(getAnyGLFuncAddress("glShaderSource"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load glShaderSource function!");
    if (!(_glCompileShader = reinterpret_cast<PFNGLCOMPILESHADERPROC>(getAnyGLFuncAddress("glCompileShader"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load glCompileShader function!");
    if (!(_glGetShaderiv = reinterpret_cast<PFNGLGETSHADERIVPROC>(getAnyGLFuncAddress("glGetShaderiv"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load glGetShaderiv function!");
    if (!(_glGetShaderInfoLog = reinterpret_cast<PFNGLGETSHADERINFOLOGPROC>(getAnyGLFuncAddress("glGetShaderInfoLog"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load glGetShaderInfoLog function!");
    if (!(_glGenerateMipmap = reinterpret_cast<PFNGLGENERATEMIPMAPPROC>(getAnyGLFuncAddress("glGenerateMipmap"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load glGenerateMipmap function!");
    if (!(_glDeleteVertexArrays = reinterpret_cast<PFNGLDELETEVERTEXARRAYSPROC>(getAnyGLFuncAddress("glDeleteVertexArrays"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load glDeleteVertexArrays function!");
    if (!(_glAttachShader = reinterpret_cast<PFNGLATTACHSHADERPROC>(getAnyGLFuncAddress("glAttachShader"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load glAttachShader function!");
    if (!(_glLinkProgram = reinterpret_cast<PFNGLLINKPROGRAMPROC>(getAnyGLFuncAddress("glLinkProgram"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load glLinkProgram function!");
    if (!(_glGetProgramiv = reinterpret_cast<PFNGLGETPROGRAMIVPROC>(getAnyGLFuncAddress("glGetProgramiv"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load glGetProgramiv function!");
    if (!(_glGetProgramInfoLog = reinterpret_cast<PFNGLGETPROGRAMINFOLOGPROC>(getAnyGLFuncAddress("glGetProgramInfoLog"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load glGetProgramInfoLog function!");
    if (!(_glDeleteShader = reinterpret_cast<PFNGLDELETESHADERPROC>(getAnyGLFuncAddress("glDeleteShader"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load glDeleteShader function!");
    if (!(_glCreateProgram = reinterpret_cast<PFNGLCREATEPROGRAMPROC>(getAnyGLFuncAddress("glCreateProgram"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load glCreateProgram function!");
    if (!(_glMapBuffer = reinterpret_cast<PFNGLMAPBUFFERPROC>(getAnyGLFuncAddress("glMapBuffer"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load glMapBuffer function!");
    if (!(_glUnmapBuffer = reinterpret_cast<PFNGLUNMAPBUFFERPROC>(getAnyGLFuncAddress("glUnmapBuffer"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load glUnmapBuffer function!");
    if (!(_glUniform4f = reinterpret_cast<PFNGLUNIFORM4FPROC>(getAnyGLFuncAddress("glUniform4f"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load glUniform4f function!");
    if (!(_glBindTextureUnit = reinterpret_cast<PFNGLBINDTEXTUREUNITPROC>(getAnyGLFuncAddress("glBindTextureUnit"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load glBindTextureUnit function!");

    if (!(_glDebugMessageCallbackARB = reinterpret_cast<PFNGLDEBUGMESSAGECALLBACKARBPROC>(getAnyGLFuncAddress("glDebugMessageCallbackARB"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load glDebugMessageCallbackARB function!");

#ifdef WIN32
    if (!(_wglCreateContextAttribsARB = reinterpret_cast<PFNWGLCREATECONTEXTATTRIBSARBPROC>(getAnyGLFuncAddress("wglCreateContextAttribsARB"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load wglCreateContextAttribsARB function!");
    if (!(_wglSwapIntervalEXT = reinterpret_cast<PFNWGLSWAPINTERVALEXTPROC>(getAnyGLFuncAddress("wglSwapIntervalEXT"))))
        throw ExceptionOpenGL(L"OpenGLFunctional is not load wglSwapIntervalEXT function!");
#endif
}

OpenGLFunctionalSmartPtr& OpenGLFunctional::getInstance()
{
    if (!instance)
    {
        instance.reset(new OpenGLFunctional());
        if(!instance)
            throw ExceptionMemory(L"OpenGLFunctional is not create, memory not found!");
        instance->init();
    }
    return instance;
}

void OpenGLFunctional::glUniformMatrix4fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat* value)
{
    _glUniformMatrix4fv(location, count, transpose, value);
}

GLint OpenGLFunctional::glGetUniformLocation(GLuint program, const GLchar* name)
{
    return _glGetUniformLocation(program, name);
}

void OpenGLFunctional::glGenVertexArrays(GLsizei n, GLuint* arrays)
{
    _glGenVertexArrays(n, arrays);
}

void OpenGLFunctional::glDeleteBuffers(GLsizei n, const GLuint* buffers)
{
    _glDeleteBuffers(n, buffers);
}

void OpenGLFunctional::glGenBuffers(GLsizei n, GLuint* buffers)
{
    _glGenBuffers(n, buffers);
}

void OpenGLFunctional::glBindVertexArray(GLuint array)
{
    _glBindVertexArray(array);
}

void OpenGLFunctional::glBindBuffer(GLenum target, GLuint buffer)
{
    _glBindBuffer(target, buffer);
}

void OpenGLFunctional::glBufferData(GLenum target, GLsizeiptr size, const void* data, GLenum usage)
{
    _glBufferData(target, size, data, usage);
}

void OpenGLFunctional::glVertexAttribPointer(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const void* pointer)
{
    _glVertexAttribPointer(index, size, type, normalized, stride, pointer);
}

void OpenGLFunctional::glEnableVertexAttribArray(GLuint index)
{
    _glEnableVertexAttribArray(index);
}

void OpenGLFunctional::glUseProgram(GLuint program)
{
    _glUseProgram(program);
}

void OpenGLFunctional::glActiveTexture(GLenum texture)
{
    _glActiveTexture(texture);
}

void OpenGLFunctional::glUniform1i(GLint location, GLint v0)
{
    _glUniform1i(location, v0);
}

GLuint OpenGLFunctional::glCreateShader(GLenum type)
{
    return _glCreateShader(type);
}

void OpenGLFunctional::glShaderSource(GLuint shader, GLsizei count, const GLchar* const* string, const GLint* length)
{
    _glShaderSource(shader, count, string, length);
}

void OpenGLFunctional::glCompileShader(GLuint shader)
{
    _glCompileShader(shader);
}

void OpenGLFunctional::glGetShaderiv(GLuint shader, GLenum pname, GLint* params)
{
    _glGetShaderiv(shader, pname, params);
}

void OpenGLFunctional::glGetShaderInfoLog(GLuint shader, GLsizei bufSize, GLsizei* length, GLchar* infoLog)
{
    _glGetShaderInfoLog(shader, bufSize, length, infoLog);
}

void OpenGLFunctional::glGenerateMipmap(GLenum target)
{
    _glGenerateMipmap(target);
}

void OpenGLFunctional::glDeleteVertexArrays(GLsizei n, const GLuint* arrays)
{
    _glDeleteVertexArrays(n, arrays);
}

void OpenGLFunctional::glAttachShader(GLuint program, GLuint shader)
{
    _glAttachShader(program, shader);
}

void OpenGLFunctional::glLinkProgram(GLuint program)
{
    _glLinkProgram(program);
}

void OpenGLFunctional::glGetProgramiv(GLuint program, GLenum pname, GLint* params)
{
    _glGetProgramiv(program, pname, params);
}

void OpenGLFunctional::glGetProgramInfoLog(GLuint program, GLsizei bufSize, GLsizei* length, GLchar* infoLog)
{
    _glGetProgramInfoLog(program, bufSize, length, infoLog);
}

void OpenGLFunctional::glDeleteShader(GLuint shader)
{
    _glDeleteShader(shader);
}

void* OpenGLFunctional::glMapBuffer(GLenum target, GLenum access)
{
    return _glMapBuffer(target, access);
}

GLboolean OpenGLFunctional::glUnmapBuffer(GLenum target)
{
    return _glUnmapBuffer(target);
}

void OpenGLFunctional::glUniform4f(GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3)
{
    _glUniform4f(location, v0, v1, v2, v3);
}

void OpenGLFunctional::glBindTextureUnit(GLuint unit, GLuint texture)
{
    _glBindTextureUnit(unit, texture);
}

GLuint OpenGLFunctional::glCreateProgram()
{
    return _glCreateProgram();
}

void OpenGLFunctional::glDebugMessageCallbackARB(GLDEBUGPROCARB callback, const void* userParam)
{
    _glDebugMessageCallbackARB(callback, userParam);
}

#ifdef WIN32

HGLRC __stdcall OpenGLFunctional::wglCreateContextAttribsARB(HDC hDC, HGLRC hShareContext, const int* attribList)
{
    return _wglCreateContextAttribsARB(hDC, hShareContext, attribList);
}

BOOL OpenGLFunctional::wglSwapIntervalEXT(int interval)
{
    return _wglSwapIntervalEXT(interval);
}

#endif
