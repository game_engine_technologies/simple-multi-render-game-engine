#include "OpenGLScene.h"

#include "../shaders/IShadersBufferVisitor.h"
#include "../textures/OpenGLTexturesBuffer.h"
#include "../../../utils/base/loaders/TexturesLoaderSOIL.h"
#include "../textures/ITexturesBufferVisitor.h"
#include "../materials/MaterialsBuffer.h"

void OpenGLScene::init(const std::wstring& path, SceneParameters rp)
{
	// ��������� ��������� ����� (����� �� ���� � ��)

	// materials buffer
	materialsBuffer = std::make_shared<MaterialsBuffer>();
	if (!materialsBuffer)
		throw ExceptionMemory(L"MaterialsBuffer is not create, memory not found!");
	materialsBuffer->init(L"configs/materials.pCfg");

	// shaders buffer
	shadersBuffer = std::make_shared<OpenGLShadersBuffer>();
	if (!shadersBuffer)
		throw ExceptionMemory(L"OpenGLShadersBuffer is not create, memory not found!");
	auto visitorSB = std::make_shared<VisitorShaderBufferCreateAndLoader>();
	if (!visitorSB)
		throw ExceptionMemory(L"VisitorShaderBufferCreateAndLoader is not create, memory not found!");
	shadersBuffer->init(L"shaders/opengl/", visitorSB);

	// textures buffer
	texturesBuffer = std::make_shared<OpenGLTexturesBuffer>();
	if (!texturesBuffer)
		throw ExceptionMemory(L"OpenGLexturesBuffer is not create, memory not found!");
	auto visitorTB = std::make_shared<TexturesBufferVisitorCreatorAndAdded>();
	if (!visitorTB)
		throw ExceptionMemory(L"TexturesBufferVisitorCreatorAndAdded is not create, memory not found!");
	auto texturesLoader = std::make_shared<TexturesLoaderSOIL>();
	if (!texturesLoader)
		throw ExceptionMemory(L"TexturesLoaderSOIL is not create, memory not found!");
	texturesBuffer->init(L"textures", texturesLoader, visitorTB);

}

bool OpenGLScene::update(SceneDynamicParameters& sdp)
{
	// ��������� ������� � ��
	return true;
}

OpenGLScene::~OpenGLScene()
{
	clear();
}

void OpenGLScene::clear()
{
	// ��������� �������
}
