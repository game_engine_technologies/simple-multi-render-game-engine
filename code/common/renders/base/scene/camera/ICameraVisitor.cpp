#include "ICameraVisitor.h"

#include "Direct3D11Camera.h"
#include "OpenGLCamera.h"
#include "../../../../utils/base/input/InputSystem.h"
#include "../../../../os/base/LevelOperationSystemSingleton.h"



VisitorCameraCreateAndUpdate::VisitorCameraCreateAndUpdate(const ComPtr<ID3D11Device>& d3dDevice,
	const ComPtr<ID3D11DeviceContext>& d3dImmediateContext):
	d3dDevice(d3dDevice), d3dImmediateContext(d3dImmediateContext)
{
}

void VisitorCameraCreateAndUpdate::create(Direct3D11Camera& a)
{
	// ������� ����������� ����� ��� ������
	a.getConstantBuffer() = Direct3D11CreatorBuffer::createBuffer(d3dDevice, static_cast<UINT>(sizeof(ConstantBufferCamera)), 
		D3D11_BIND_CONSTANT_BUFFER, D3D11_USAGE_DEFAULT,0 );
}

void VisitorCameraCreateAndUpdate::create(OpenGLCamera& a)
{
	
}

void VisitorCameraCreateAndUpdate::update(Direct3D11Camera& a, double frameTime, gmtypes::WindowBBox& winBbox)
{
	a.updateOrtho(winBbox.w, winBbox.h);
	if (a.isUpdate())
	{
		// ��� ������ ���� ���������
		ConstantBufferCamera cbc;
		cbc.view = a.getView().getGPUMatrix();
		cbc.perspective = a.getPerspective().getGPUMatrix();
		cbc.ortho = a.getOrtho().getGPUMatrix();
		d3dImmediateContext->UpdateSubresource(a.getConstantBuffer().Get(), 0, NULL, &cbc, 0, 0);
		a.clearFlags();
	}
}

void VisitorCameraCreateAndUpdate::update(OpenGLCamera& a, double frameTime, gmtypes::WindowBBox& winBbox)
{
	a.updateOrtho(winBbox.w, winBbox.h);
}

void VisitorCameraCreateAndUpdate::sendGPU(Direct3D11Camera& a, int additionalData)
{
	// �������� ������������ ������ ������ �� �������� ����������
	d3dImmediateContext->VSSetConstantBuffers(0, 1, a.getConstantBuffer().GetAddressOf());
}

void VisitorCameraCreateAndUpdate::sendGPU(OpenGLCamera& a, int shaderProgramGL)
{
	GLint viewLocation = OpenGLFunctional::getInstance()->glGetUniformLocation(shaderProgramGL, "view");
	GLint perspectiveLocation = OpenGLFunctional::getInstance()->glGetUniformLocation(shaderProgramGL, "perspective");
	GLint orthoLocation = OpenGLFunctional::getInstance()->glGetUniformLocation(shaderProgramGL, "ortho");
	OpenGLFunctional::getInstance()->glUniformMatrix4fv(viewLocation, 1, GL_FALSE, a.getView().getCPUMatrix().toArray().data());
	OpenGLFunctional::getInstance()->glUniformMatrix4fv(perspectiveLocation, 1, GL_FALSE, a.getPerspective().getCPUMatrix().toArray().data());
	OpenGLFunctional::getInstance()->glUniformMatrix4fv(orthoLocation, 1, GL_FALSE, a.getOrtho().getCPUMatrix().toArray().data());
}

void VisitorCameraCreateAndUpdate::create(Direct3D11FPSCamera& a)
{
	// ������� ����������� ����� ��� ������
	LevelOperationSystemSingleton::getInstance()->showCursor();
	a.getConstantBuffer() = Direct3D11CreatorBuffer::createBuffer(d3dDevice, static_cast<UINT>(sizeof(ConstantBufferCamera)),
		D3D11_BIND_CONSTANT_BUFFER, D3D11_USAGE_DEFAULT,0 );
}

void VisitorCameraCreateAndUpdate::create(OpenGLFPSCamera& a)
{
	LevelOperationSystemSingleton::getInstance()->showCursor();
}

void VisitorCameraCreateAndUpdate::update(Direct3D11FPSCamera& a, double frameTime, gmtypes::WindowBBox& winBbox)
{
	a.updateOrtho(winBbox.w, winBbox.h);

	gmath::Vector3 camTarget(a.getTarget());
	gmath::Vector3 camRight;
	gmath::Vector3 camUp(a.getUp());
	gmath::Vector3 camForward;
	gmath::Vector3 camPosition(a.getPosition());
	gmath::Point2 lastPosMouse(a.getLastPosMouse());

	float speed(30 * frameTime);
	float moveLeftRight(0);
	float moveBackForward(0);

	// move
	if (InputSystem::getInstance()->getKeyboardKey(InputSystemKeys::KEY_A))
		moveLeftRight -= speed;
	if (InputSystem::getInstance()->getKeyboardKey(InputSystemKeys::KEY_D))
		moveLeftRight += speed;
	if (InputSystem::getInstance()->getKeyboardKey(InputSystemKeys::KEY_W))
		moveBackForward += speed;
	if (InputSystem::getInstance()->getKeyboardKey(InputSystemKeys::KEY_S))
		moveBackForward -= speed;

	// rotate
	MousePosition mousePos(InputSystem::getInstance()->getMousePosition());
	CenterWindow centerWindow((winBbox.x + winBbox.w) / 2, (winBbox.y + winBbox.h) / 2);
	DeltaMousePosition delta(mousePos.x - centerWindow.x, mousePos.y - centerWindow.y);

	if (delta.x != 0 || delta.y != 0)
	{
		a.getCamYaw() += delta.x * 0.001f;
		a.getCamPitch() += delta.y * 0.001f;
	}

	// calculate view
	float angle(1.483f); // 85 ��������
	if (a.getCamPitch() > 0)
		a.getCamPitch() = min(a.getCamPitch(), angle);
	else
		a.getCamPitch() = max(a.getCamPitch(), -angle);

	gmath::Matrix4 camRotationMatrix = gmath::Matrix4::CreateRotateMatrixXYZ(a.getCamPitch(), a.getCamYaw(), 0);
	camTarget = a.getDefaultForward();
	camTarget.vector3TransformCoord(camRotationMatrix);
	camTarget.normalize();

	gmath::Matrix4 RotateYTempMatrix = gmath::Matrix4::CreateRotateMatrixY(a.getCamYaw());
	camRight = a.getDefaultRight();
	camRight.vector3TransformCoord(RotateYTempMatrix);
	camUp.vector3TransformCoord(RotateYTempMatrix);
	camForward = a.getDefaultForward();
	camForward.vector3TransformCoord(RotateYTempMatrix);

	camPosition += camRight * moveLeftRight;
	camPosition += camForward * moveBackForward;

	camTarget = camPosition + camTarget;

	a.updateView(camPosition, camTarget, camUp);

	// ������ ���� ��������
	ConstantBufferCamera cbc;
	cbc.view = a.getView().getGPUMatrix();
	cbc.perspective = a.getPerspective().getGPUMatrix();
	cbc.ortho = a.getOrtho().getGPUMatrix();
	d3dImmediateContext->UpdateSubresource(a.getConstantBuffer().Get(), 0, NULL, &cbc, 0, 0);
	a.clearFlags();

	LevelOperationSystemSingleton::getInstance()->setCursorCenterScreen(centerWindow.x, centerWindow.y);
	a.setLastPosMouse({ mousePos.x, mousePos.y });
}

void VisitorCameraCreateAndUpdate::update(OpenGLFPSCamera& a, double frameTime, gmtypes::WindowBBox& winBbox)
{
	a.updateOrtho(winBbox.w, winBbox.h);

	gmath::Vector3 camTarget(a.getTarget());
	gmath::Vector3 camRight;
	gmath::Vector3 camUp(a.getUp());
	gmath::Vector3 camForward;
	gmath::Vector3 camPosition(a.getPosition());
	gmath::Point2 lastPosMouse(a.getLastPosMouse());

	float speed(30 * frameTime);
	float moveLeftRight(0);
	float moveBackForward(0);

	// move
	if (InputSystem::getInstance()->getKeyboardKey(InputSystemKeys::KEY_A))
		moveLeftRight += speed;
	if (InputSystem::getInstance()->getKeyboardKey(InputSystemKeys::KEY_D))
		moveLeftRight -= speed;
	if (InputSystem::getInstance()->getKeyboardKey(InputSystemKeys::KEY_W))
		moveBackForward += speed;
	if (InputSystem::getInstance()->getKeyboardKey(InputSystemKeys::KEY_S))
		moveBackForward -= speed;

	// rotate
	MousePosition mousePos(InputSystem::getInstance()->getMousePosition());
	CenterWindow centerWindow((winBbox.x + winBbox.w) / 2, (winBbox.y + winBbox.h) / 2);
	DeltaMousePosition delta(mousePos.x - centerWindow.x, mousePos.y - centerWindow.y);

	if (delta.x != 0 || delta.y != 0)
	{
		a.getCamYaw() -= delta.x * 0.001f;
		a.getCamPitch() += delta.y * 0.001f;
	}

	// calculate view
	float angle(1.483f); // 85 ��������
	if (a.getCamPitch() > 0)
		a.getCamPitch() = min(a.getCamPitch(), angle);
	else
		a.getCamPitch() = max(a.getCamPitch(), -angle);

	gmath::Matrix4 camRotationMatrix = gmath::Matrix4::CreateRotateMatrixXYZ(a.getCamPitch(), a.getCamYaw(), 0);
	camTarget = a.getDefaultForward();
	camTarget.vector3TransformCoord(camRotationMatrix);
	camTarget.normalize();

	gmath::Matrix4 RotateYTempMatrix = gmath::Matrix4::CreateRotateMatrixY(a.getCamYaw());
	camRight = a.getDefaultRight();
	camRight.vector3TransformCoord(RotateYTempMatrix);
	camUp.vector3TransformCoord(RotateYTempMatrix);
	camForward = a.getDefaultForward();
	camForward.vector3TransformCoord(RotateYTempMatrix);

	camPosition += camRight * moveLeftRight;
	camPosition += camForward * moveBackForward;

	camTarget = camPosition + camTarget;

	a.updateView(camPosition, camTarget, camUp);


	LevelOperationSystemSingleton::getInstance()->setCursorCenterScreen(centerWindow.x, centerWindow.y);
	a.setLastPosMouse({ mousePos.x, mousePos.y });
}

void VisitorCameraCreateAndUpdate::sendGPU(Direct3D11FPSCamera& a, int additionalData)
{
	// �������� ������������ ������ ������ �� �������� ����������
	d3dImmediateContext->VSSetConstantBuffers(0, 1, a.getConstantBuffer().GetAddressOf());
}

void VisitorCameraCreateAndUpdate::sendGPU(OpenGLFPSCamera& a, int shaderProgramGL)
{
	GLint viewLocation = OpenGLFunctional::getInstance()->glGetUniformLocation(shaderProgramGL, "view");
	GLint perspectiveLocation = OpenGLFunctional::getInstance()->glGetUniformLocation(shaderProgramGL, "perspective");
	GLint orthoLocation = OpenGLFunctional::getInstance()->glGetUniformLocation(shaderProgramGL, "ortho");
	OpenGLFunctional::getInstance()->glUniformMatrix4fv(viewLocation, 1, GL_FALSE, a.getView().getCPUMatrix().toArray().data());
	OpenGLFunctional::getInstance()->glUniformMatrix4fv(perspectiveLocation, 1, GL_FALSE, a.getPerspective().getCPUMatrix().toArray().data());
	OpenGLFunctional::getInstance()->glUniformMatrix4fv(orthoLocation, 1, GL_FALSE, a.getOrtho().getCPUMatrix().toArray().data());
}

