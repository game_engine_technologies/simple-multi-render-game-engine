#pragma once

#include "../../BufferCreator.h"
#include "../../gl_header.h"

#include "../../math_header.h"

class Direct3D11Camera;
class OpenGLCamera;
class Direct3D11FPSCamera;
class OpenGLFPSCamera;

class ICameraVisitorCreator
{
public:
	virtual void create(Direct3D11Camera& a) = 0;
	virtual void create(OpenGLCamera& a) = 0;
	virtual void create(Direct3D11FPSCamera& a) = 0;
	virtual void create(OpenGLFPSCamera& a) = 0;
	virtual ~ICameraVisitorCreator() = default;
};

class ICameraVisitorUpdater
{
public:
	virtual void update(Direct3D11Camera& a, double frameTime, gmtypes::WindowBBox& winBbox) = 0;
	virtual void update(OpenGLCamera& a, double frameTime, gmtypes::WindowBBox& winBbox) = 0;
	virtual void update(Direct3D11FPSCamera& a, double frameTime, gmtypes::WindowBBox& winBbox) = 0;
	virtual void update(OpenGLFPSCamera& a, double frameTime, gmtypes::WindowBBox& winBbox) = 0;
	virtual ~ICameraVisitorUpdater() = default;
};

class ICameraVisitorSendGPU
{
public:
	virtual void sendGPU(Direct3D11Camera& a, int additionalData) = 0;
	virtual void sendGPU(OpenGLCamera& a, int additionalData) = 0;
	virtual void sendGPU(Direct3D11FPSCamera& a, int additionalData) = 0;
	virtual void sendGPU(OpenGLFPSCamera& a, int additionalData) = 0;
	virtual ~ICameraVisitorSendGPU() = default;
};

class VisitorCameraCreateAndUpdate : public ICameraVisitorCreator, public ICameraVisitorUpdater, public ICameraVisitorSendGPU
{
	ComPtr<ID3D11Device> d3dDevice;
	ComPtr<ID3D11DeviceContext> d3dImmediateContext;


public:
	VisitorCameraCreateAndUpdate() = default;
	VisitorCameraCreateAndUpdate(const ComPtr<ID3D11Device>& d3dDevice, const ComPtr<ID3D11DeviceContext>& d3dImmediateContext);

	void create(Direct3D11Camera& a) override;
	void create(OpenGLCamera& a) override;
	void update(Direct3D11Camera& a, double frameTime, gmtypes::WindowBBox& winBbox) override;
	void update(OpenGLCamera& a, double frameTime, gmtypes::WindowBBox& winBbox) override;
	void sendGPU(Direct3D11Camera& a, int additionalData) override;
	void sendGPU(OpenGLCamera& a, int shaderProgramGL) override;

	void create(Direct3D11FPSCamera& a) override;
	void create(OpenGLFPSCamera& a) override;
	void update(Direct3D11FPSCamera& a, double frameTime, gmtypes::WindowBBox& winBbox) override;
	void update(OpenGLFPSCamera& a, double frameTime, gmtypes::WindowBBox& winBbox) override;
	void sendGPU(Direct3D11FPSCamera& a, int additionalData) override;
	void sendGPU(OpenGLFPSCamera& a, int shaderProgramGL) override;

	~VisitorCameraCreateAndUpdate()override {};
};