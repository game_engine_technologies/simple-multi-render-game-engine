#pragma once

#include "../../../abstract/scene/camera/ICamera.h"

#include "../../gl_header.h"

class OpenGLCamera : public ICamera
{ 
	VisitorCameraCreateAndUpdateSmartPtr visitor;

public:
	void init(VisitorCameraCreateAndUpdateSmartPtr& visitor) override;
	void updateOrtho(float w, float h) override;
	void updatePerspective(float fov, float aspectW, float aspectH) override;
	void updateView(const gmath::Vector3& p, const gmath::Vector3& d, const gmath::Vector3& h) override;
	void update(double frameTime, gmtypes::WindowBBox& winBbox) override;
	void sendToGPU(int shaderProgramGL) override;
	~OpenGLCamera() = default;

};

using OpenGLCameraSmartPtr = std::shared_ptr<OpenGLCamera>;

class OpenGLFPSCamera : public ICamera
{
	VisitorCameraCreateAndUpdateSmartPtr visitor;

	gmath::Vector3 camPosition;
	gmath::Vector3 camUp;
	gmath::Vector3 camTarget;
	gmath::Point2 lastPosMouse;

	gmath::Vector3 defaultForward;
	gmath::Vector3 defaultRight;
	float camYaw;
	float camPitch;


public:
	void init(VisitorCameraCreateAndUpdateSmartPtr& visitor) override;
	void updateOrtho(float w, float h) override;
	void updatePerspective(float fov, float aspectW, float aspectH) override;
	void updateView(const gmath::Vector3& p, const gmath::Vector3& d, const gmath::Vector3& h) override;
	void update(double frameTime, gmtypes::WindowBBox& winBbox) override;
	void sendToGPU(int shaderProgramGL) override;
	~OpenGLFPSCamera() = default;

	gmath::Vector3 getPosition()const;
	gmath::Vector3 getUp()const;
	gmath::Vector3 getTarget()const;
	gmath::Point2 getLastPosMouse() const;
	void setLastPosMouse(const gmath::Point2& lp);

	gmath::Vector3 getDefaultForward()const;
	gmath::Vector3 getDefaultRight()const;

	float& getCamYaw();
	float& getCamPitch();

};

using OpenGLFPSCameraSmartPtr = std::shared_ptr<OpenGLFPSCamera>;