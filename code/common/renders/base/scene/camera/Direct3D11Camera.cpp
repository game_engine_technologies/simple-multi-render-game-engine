#include "Direct3D11Camera.h"

#include "ICameraVisitor.h"

void Direct3D11Camera::init(VisitorCameraCreateAndUpdateSmartPtr& visitor)
{
	this->visitor = visitor;
	this->visitor->create(*this);
}

void Direct3D11Camera::updateOrtho(float w, float h)
{
	ortho.updateCPUMatrix(gmath::Matrix::CreateOrthoLHMatrix(w, h, nearZOrtho, farZ));
	_update = true;
}

void Direct3D11Camera::updatePerspective(float fov, float aspectW, float aspectH)
{
	perspective.updateCPUMatrix(gmath::Matrix::CreatePerspectiveFovLHMatrix(fov, aspectW / aspectH, nearZPerspective, farZ));
	_update = true;
}

void Direct3D11Camera::updateView(const gmath::Vector3& p, const gmath::Vector3& d, const gmath::Vector3& h)
{
	view.updateCPUMatrix(gmath::Matrix::CreateLookAtLHMatrix(p, d, h));
	_update = true;
}

void Direct3D11Camera::update(double frameTime, gmtypes::WindowBBox& winBbox)
{
	this->visitor->update(*this, frameTime, winBbox);
}

void Direct3D11Camera::sendToGPU(int additionalData)
{
	this->visitor->sendGPU(*this, additionalData);
}

ComPtr<ID3D11Buffer>& Direct3D11Camera::getConstantBuffer()
{
	return constantBuffer;
}

void Direct3D11Camera::setConstantBuffer(const ComPtr<ID3D11Buffer>& b)
{
	this->constantBuffer = b;
}

void Direct3D11FPSCamera::init(VisitorCameraCreateAndUpdateSmartPtr& visitor)
{
	defaultForward = gmath::Vector3(0.0f, 0.0f, 1.0f);
	defaultRight = gmath::Vector3(1.0f, 0.0f, 0.0f);
	camYaw = camPitch = 0.f;
	this->visitor = visitor;
	this->visitor->create(*this);
}

void Direct3D11FPSCamera::updateOrtho(float w, float h)
{
	ortho.updateCPUMatrix(gmath::Matrix::CreateOrthoLHMatrix(w, h, nearZOrtho, farZ));
	_update = true;
}

void Direct3D11FPSCamera::updatePerspective(float fov, float aspectW, float aspectH)
{
	perspective.updateCPUMatrix(gmath::Matrix::CreatePerspectiveFovLHMatrix(fov, aspectW / aspectH, nearZPerspective, farZ));
	_update = true;
}

void Direct3D11FPSCamera::updateView(const gmath::Vector3& p, const gmath::Vector3& d, const gmath::Vector3& h)
{
	camPosition = p;
	camTarget = d;
	camUp = h;
	view.updateCPUMatrix(gmath::Matrix::CreateLookAtLHMatrix(p, d, h));
	_update = true;
}

void Direct3D11FPSCamera::update(double frameTime, gmtypes::WindowBBox& winBbox)
{
	this->visitor->update(*this, frameTime, winBbox);
}

void Direct3D11FPSCamera::sendToGPU(int additionalData)
{
	this->visitor->sendGPU(*this, additionalData);
}

ComPtr<ID3D11Buffer>& Direct3D11FPSCamera::getConstantBuffer()
{
	return constantBuffer;
}

void Direct3D11FPSCamera::setConstantBuffer(const ComPtr<ID3D11Buffer>& b)
{
	this->constantBuffer = b;
}

gmath::Vector3 Direct3D11FPSCamera::getPosition() const
{
	return camPosition;
}

gmath::Vector3 Direct3D11FPSCamera::getUp() const
{
	return camUp;
}

gmath::Vector3 Direct3D11FPSCamera::getTarget() const
{
	return camTarget;
}

gmath::Point2 Direct3D11FPSCamera::getLastPosMouse() const
{
	return lastPosMouse;
}

void Direct3D11FPSCamera::setLastPosMouse(const gmath::Point2& lp)
{
	lastPosMouse = lp;
}

gmath::Vector3 Direct3D11FPSCamera::getDefaultForward() const
{
	return defaultForward;
}

gmath::Vector3 Direct3D11FPSCamera::getDefaultRight() const
{
	return defaultRight;
}

float& Direct3D11FPSCamera::getCamYaw()
{
	return camYaw;
}

float& Direct3D11FPSCamera::getCamPitch()
{
	return camPitch;
}
