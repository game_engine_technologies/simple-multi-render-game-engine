#include "OpenGLCamera.h"

#include "ICameraVisitor.h"

void OpenGLCamera::init(VisitorCameraCreateAndUpdateSmartPtr& visitor)
{
	this->visitor = visitor;
	this->visitor->create(*this);
}

void OpenGLCamera::updateOrtho(float w, float h)
{
	ortho.updateCPUMatrix(gmath::Matrix::CreateOrthoRHMatrix(w, h, nearZOrtho, farZ));
	_update = true;
}

void OpenGLCamera::updatePerspective(float fov, float aspectW, float aspectH)
{
	perspective.updateCPUMatrix(gmath::Matrix::CreatePerspectiveFovRHMatrix(fov, aspectW / aspectH, nearZPerspective, farZ));
	_update = true;
}

void OpenGLCamera::updateView(const gmath::Vector3& p, const gmath::Vector3& d, const gmath::Vector3& h)
{
	view.updateCPUMatrix(gmath::Matrix::CreateLookAtRHMatrix(p, d, h));
	_update = true;
}

void OpenGLCamera::update(double frameTime, gmtypes::WindowBBox& winBbox)
{
	this->visitor->update(*this, frameTime, winBbox);
}

void OpenGLCamera::sendToGPU(int shaderProgramGL)
{
	this->visitor->sendGPU(*this, shaderProgramGL);
}

void OpenGLFPSCamera::init(VisitorCameraCreateAndUpdateSmartPtr& visitor)
{
	defaultForward = gmath::Vector3(0.0f, 0.0f, 1.0f);
	defaultRight = gmath::Vector3(1.0f, 0.0f, 0.0f);
	camYaw = camPitch = 0.f;
	this->visitor = visitor;
	this->visitor->create(*this);
}

void OpenGLFPSCamera::updateOrtho(float w, float h)
{
	ortho.updateCPUMatrix(gmath::Matrix::CreateOrthoRHMatrix(w, h, nearZOrtho, farZ));
	_update = true;
}

void OpenGLFPSCamera::updatePerspective(float fov, float aspectW, float aspectH)
{
	perspective.updateCPUMatrix(gmath::Matrix::CreatePerspectiveFovRHMatrix(fov, aspectW / aspectH, nearZPerspective, farZ));
	_update = true;
}

void OpenGLFPSCamera::updateView(const gmath::Vector3& p, const gmath::Vector3& d, const gmath::Vector3& h)
{
	camPosition = p;
	camTarget = d;
	camUp = h;
	view.updateCPUMatrix(gmath::Matrix::CreateLookAtRHMatrix(p, d, h));
	_update = true;
}

void OpenGLFPSCamera::update(double frameTime, gmtypes::WindowBBox& winBbox)
{
	this->visitor->update(*this, frameTime, winBbox);
}

void OpenGLFPSCamera::sendToGPU(int shaderProgramGL)
{
	this->visitor->sendGPU(*this, shaderProgramGL);
}

gmath::Vector3 OpenGLFPSCamera::getPosition() const
{
	return camPosition;
}

gmath::Vector3 OpenGLFPSCamera::getUp() const
{
	return camUp;
}

gmath::Vector3 OpenGLFPSCamera::getTarget() const
{
	return camTarget;
}

gmath::Point2 OpenGLFPSCamera::getLastPosMouse() const
{
	return lastPosMouse;
}

void OpenGLFPSCamera::setLastPosMouse(const gmath::Point2& lp)
{
	lastPosMouse = lp;
}

gmath::Vector3 OpenGLFPSCamera::getDefaultForward() const
{
	return defaultForward;
}

gmath::Vector3 OpenGLFPSCamera::getDefaultRight() const
{
	return defaultRight;
}

float& OpenGLFPSCamera::getCamYaw()
{
	return camYaw;
}

float& OpenGLFPSCamera::getCamPitch()
{
	return camPitch;
}