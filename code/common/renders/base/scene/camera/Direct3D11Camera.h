#pragma once

#include "../../../abstract/scene/camera/ICamera.h"

#include "../../d3d11_header.h"

class Direct3D11Camera : public ICamera
{
	VisitorCameraCreateAndUpdateSmartPtr visitor;
	ComPtr<ID3D11Buffer> constantBuffer;

public:
	void init(VisitorCameraCreateAndUpdateSmartPtr& visitor) override;
	void updateOrtho(float w, float h) override;
	void updatePerspective(float fov, float aspectW, float aspectH) override;
	void updateView(const gmath::Vector3& p, const gmath::Vector3& d, const gmath::Vector3& h) override;
	void update(double frameTime, gmtypes::WindowBBox& winBbox) override;
	void sendToGPU(int additionalData) override;
	~Direct3D11Camera() = default;

	ComPtr<ID3D11Buffer>& getConstantBuffer();
	void setConstantBuffer(const ComPtr<ID3D11Buffer>& b);

};

using Direct3D11CameraSmartPtr = std::shared_ptr<Direct3D11Camera>;

class Direct3D11FPSCamera : public ICamera
{
	VisitorCameraCreateAndUpdateSmartPtr visitor;
	ComPtr<ID3D11Buffer> constantBuffer;

	gmath::Vector3 camPosition;
	gmath::Vector3 camUp;
	gmath::Vector3 camTarget;
	gmath::Point2 lastPosMouse; 

	gmath::Vector3 defaultForward;
	gmath::Vector3 defaultRight;
	float camYaw;
	float camPitch;

public:
	void init(VisitorCameraCreateAndUpdateSmartPtr& visitor) override;
	void updateOrtho(float w, float h) override;
	void updatePerspective(float fov, float aspectW, float aspectH) override;
	void updateView(const gmath::Vector3& p, const gmath::Vector3& d, const gmath::Vector3& h) override;
	void update(double frameTime, gmtypes::WindowBBox& winBbox) override;
	void sendToGPU(int additionalData) override;
	~Direct3D11FPSCamera() = default;

	ComPtr<ID3D11Buffer>& getConstantBuffer();
	void setConstantBuffer(const ComPtr<ID3D11Buffer>& b);

	gmath::Vector3 getPosition()const;
	gmath::Vector3 getUp()const;
	gmath::Vector3 getTarget()const;
	gmath::Point2 getLastPosMouse() const; 
	void setLastPosMouse(const gmath::Point2& lp);

	gmath::Vector3 getDefaultForward()const;
	gmath::Vector3 getDefaultRight()const;

	float& getCamYaw();
	float& getCamPitch();

};

using Direct3D11FPSCameraSmartPtr = std::shared_ptr<Direct3D11FPSCamera>;