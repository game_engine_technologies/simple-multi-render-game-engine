#pragma once

#include "../../../abstract/scene/text/ITextBufferB.h"
#include "../../gl_header.h"

struct TextBufferBOpenGL
{
	GLuint glVertexBuffer;
	GLuint glVao;
	std::wstring text;
	gmath::Matrix position;
	gmath::Color textColor;
	unsigned count;
	TypeTextBufferB type;
	//GLint worldLocation;
	//GLint textColorLocation;
};

class OpenGLTextBufferB: public ITextBufferB
{
	GLuint shaderProgram;
	std::map<int, int> textures;
	std::wstring sampler;
	GLint worldLocation;
	GLint textColorLocation;
	std::map<std::wstring, TextBufferBOpenGL> buffersGPU;

	VisitorTextBufferCreateAndDrawSmartPtr visitor;


public:
	void init(const std::wstring& vs, const std::wstring& ps,
		const std::wstring cfg, const std::wstring& sampler, int h, VisitorTextBufferCreateAndDrawSmartPtr& visitor) override;
	
	void addText(const std::wstring& key, const std::wstring& text, const gmath::Matrix4& pos, const gmath::Color& color, const TypeTextBufferB& type) override;
	void replaceText(const std::wstring& key, const std::wstring& new_string) override;

	void updatePosition(const std::wstring& key, const gmath::Matrix4& pos) override;
	void updateColor(const std::wstring& key, const gmath::Color& c)override;
	void reinit() override;
	void draw() override;

	GLuint& getShaderProgram();
	std::wstring& getSampler();
	std::map<int, int>& getTextures();
	GLint& getWorldLocation();
	GLint& getTextColorLocation();
	std::map<std::wstring, TextBufferBOpenGL>& getBuffersGPU();
};

using OpenGLTextBufferBSmartPtr = std::shared_ptr<OpenGLTextBufferB>;
