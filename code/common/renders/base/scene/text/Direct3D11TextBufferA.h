#pragma once

#include "../../../abstract/scene/text/ITextBufferA.h"
#include "../../d3d11_header.h"

class Direct3D11TextBufferA: public ITextBufferA
{
	ComPtr<ID3D11Buffer> d3dVertexBuffer;
	ComPtr<ID3D11InputLayout> d3dInputLayout;
	ComPtr<ID3D11VertexShader> d3dVertexShader;
	ComPtr<ID3D11PixelShader> d3dPixelShader;
	ComPtr<ID3D11Buffer> constantsBufferWorld;
	ComPtr<ID3D11Buffer> constantsBufferTextColor;
	std::map<int, ComPtr<ID3D11ShaderResourceView>> textures;
	ComPtr<ID3D11SamplerState> sampler;

	VisitorTextBufferCreateAndDrawSmartPtr visitor;

public:
	void init(const std::wstring& vs, const std::wstring& ps,
		const std::wstring cfg, const std::wstring& sampler, VisitorTextBufferCreateAndDrawSmartPtr& visitor) override;
	void mapVertexBuffer() override;
	void unmapVertexBuffer() override;

	int getHeightText(const std::wstring& key) override;
	
	void addText(const std::wstring& key, const std::wstring& text, const gmath::Matrix4& pos, const gmath::Color& color, int h) override;
	void replaceTextForEnter(const std::wstring& key, int ind_str, int ind_start, const std::wstring& new_string) override;
	void updatePosition(const std::wstring& key, const gmath::Matrix4& pos) override;
	void updateColor(const std::wstring& key, const gmath::Color& c)override;
	void reinit() override;
	void draw() override;

	ComPtr<ID3D11Buffer>& getVertexBuffer();
	ComPtr<ID3D11InputLayout>& getInputLayout();
	ComPtr<ID3D11VertexShader>& getVertexShader();
	ComPtr<ID3D11PixelShader>& getPixelShader();
	ComPtr<ID3D11Buffer>& getConstantBufferWorld();
	ComPtr<ID3D11Buffer>& getConstantBufferTextColor();
	std::map<int, ComPtr<ID3D11ShaderResourceView>>& getTextures();
	ComPtr<ID3D11SamplerState>& getSampler();

};

using Direct3D11TextBufferASmartPtr = std::shared_ptr<Direct3D11TextBufferA>;
