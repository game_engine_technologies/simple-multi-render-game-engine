#include "Direct3D11TextBufferB.h"

#include "ITextBufferVisitor.h"


void Direct3D11TextBufferB::init(const std::wstring& vs, const std::wstring& ps, const std::wstring cfg, const std::wstring& sampler, int h, VisitorTextBufferCreateAndDrawSmartPtr& visitor)
{
	this->visitor = visitor;
	this->visitor->create(vs, ps, cfg, sampler, h, *this);
}

void Direct3D11TextBufferB::addText(const std::wstring& key, const std::wstring& text, const gmath::Matrix4& pos, const gmath::Color& color, const TypeTextBufferB& type)
{
	this->visitor->addText(key, text, pos, color, type, *this);
}

void Direct3D11TextBufferB::replaceText(const std::wstring& key, const std::wstring& new_string)
{
	this->visitor->replaceText(key, new_string, *this);
}

void Direct3D11TextBufferB::updatePosition(const std::wstring& key, const gmath::Matrix4& pos)
{
	this->visitor->updatePosition(key, pos, *this);
}

void Direct3D11TextBufferB::updateColor(const std::wstring& key, const gmath::Color& c)
{
	this->visitor->updateColor(key, c, *this);
}

void Direct3D11TextBufferB::reinit()
{
	this->visitor->reinit(*this);
}

void Direct3D11TextBufferB::draw()
{
	this->visitor->draw(*this);
}

ComPtr<ID3D11InputLayout>& Direct3D11TextBufferB::getInputLayout()
{
	return d3dInputLayout;
}

ComPtr<ID3D11VertexShader>& Direct3D11TextBufferB::getVertexShader()
{
	return d3dVertexShader;
}

ComPtr<ID3D11PixelShader>& Direct3D11TextBufferB::getPixelShader()
{
	return d3dPixelShader;
}

ComPtr<ID3D11Buffer>& Direct3D11TextBufferB::getConstantBufferWorld()
{
	return constantsBufferWorld;
}

ComPtr<ID3D11Buffer>& Direct3D11TextBufferB::getConstantBufferTextColor()
{
	return constantsBufferTextColor;
}

ComPtr<ID3D11SamplerState>& Direct3D11TextBufferB::getSampler()
{
	return sampler;
}

std::map<std::wstring, TextBufferBDirect3D11>& Direct3D11TextBufferB::getBuffersGPU()
{
	return buffersGPU;
}

std::map<int, ComPtr<ID3D11ShaderResourceView>>& Direct3D11TextBufferB::getTextures()
{
	return textures;
}
