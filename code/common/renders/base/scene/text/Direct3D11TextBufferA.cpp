#include "Direct3D11TextBufferA.h"

#include "ITextBufferVisitor.h"

void Direct3D11TextBufferA::init(const std::wstring& vs, const std::wstring& ps, const std::wstring cfg, const std::wstring& sampler, VisitorTextBufferCreateAndDrawSmartPtr& visitor)
{
	this->visitor = visitor;
	this->visitor->create(vs, ps, cfg, sampler, *this);
}

void Direct3D11TextBufferA::mapVertexBuffer()
{
	this->visitor->mapVertexBuffer(*this);
}

void Direct3D11TextBufferA::unmapVertexBuffer()
{
	this->visitor->unmapVertexBuffer(*this);
}

int Direct3D11TextBufferA::getHeightText(const std::wstring& key)
{
	return this->visitor->getHeightText(key, *this);
}

void Direct3D11TextBufferA::addText(const std::wstring& key, const std::wstring& text, const gmath::Matrix4& pos, const gmath::Color& color, int h)
{
	this->visitor->addText(key, text, pos, color, h, *this);
}

void Direct3D11TextBufferA::replaceTextForEnter(const std::wstring& key, int ind_str, int ind_start, const std::wstring& new_string)
{
	this->visitor->replaceTextForEnter(key, ind_str, ind_start, new_string, *this);
}

void Direct3D11TextBufferA::updatePosition(const std::wstring& key, const gmath::Matrix4& pos)
{
	this->visitor->updatePosition(key, pos, *this);
}

void Direct3D11TextBufferA::updateColor(const std::wstring& key, const gmath::Color& c)
{
	this->visitor->updateColor(key, c, *this);
}

void Direct3D11TextBufferA::reinit()
{
	this->visitor->reinit(*this);
}

void Direct3D11TextBufferA::draw()
{
	this->visitor->draw(*this);
}

ComPtr<ID3D11Buffer>& Direct3D11TextBufferA::getVertexBuffer()
{
	return d3dVertexBuffer;
}

ComPtr<ID3D11InputLayout>& Direct3D11TextBufferA::getInputLayout()
{
	return d3dInputLayout;
}

ComPtr<ID3D11VertexShader>& Direct3D11TextBufferA::getVertexShader()
{
	return d3dVertexShader;
}

ComPtr<ID3D11PixelShader>& Direct3D11TextBufferA::getPixelShader()
{
	return d3dPixelShader;
}

ComPtr<ID3D11Buffer>& Direct3D11TextBufferA::getConstantBufferWorld()
{
	return constantsBufferWorld;
}

ComPtr<ID3D11Buffer>& Direct3D11TextBufferA::getConstantBufferTextColor()
{
	return constantsBufferTextColor;
}

ComPtr<ID3D11SamplerState>& Direct3D11TextBufferA::getSampler()
{
	return sampler;
}

std::map<int, ComPtr<ID3D11ShaderResourceView>>& Direct3D11TextBufferA::getTextures()
{
	return textures;
}
