#pragma once

#include "../../../abstract/scene/text/ITextBufferB.h"
#include "../../d3d11_header.h"

struct TextBufferBDirect3D11
{
	ComPtr<ID3D11Buffer> d3dVertexBuffer;
	std::wstring text;
	gmath::Matrix position;
	gmath::Color textColor;
	unsigned count;
	TypeTextBufferB type;
};

class Direct3D11TextBufferB: public ITextBufferB
{
	ComPtr<ID3D11InputLayout> d3dInputLayout;
	ComPtr<ID3D11VertexShader> d3dVertexShader;
	ComPtr<ID3D11PixelShader> d3dPixelShader;
	ComPtr<ID3D11Buffer> constantsBufferWorld;
	ComPtr<ID3D11Buffer> constantsBufferTextColor;
	std::map<int, ComPtr<ID3D11ShaderResourceView>> textures;
	ComPtr<ID3D11SamplerState> sampler;
	std::map<std::wstring, TextBufferBDirect3D11> buffersGPU;

	VisitorTextBufferCreateAndDrawSmartPtr visitor;

public:
	void init(const std::wstring& vs, const std::wstring& ps,
		const std::wstring cfg, const std::wstring& sampler, int h, VisitorTextBufferCreateAndDrawSmartPtr& visitor) override;
	
	void addText(const std::wstring& key, const std::wstring& text, const gmath::Matrix4& pos, const gmath::Color& color, const TypeTextBufferB& type) override;
	void replaceText(const std::wstring& key, const std::wstring& new_string) override;
	void updatePosition(const std::wstring& key, const gmath::Matrix4& pos) override;
	void updateColor(const std::wstring& key, const gmath::Color& c)override;
	void reinit() override;
	void draw() override;

	ComPtr<ID3D11InputLayout>& getInputLayout();
	ComPtr<ID3D11VertexShader>& getVertexShader();
	ComPtr<ID3D11PixelShader>& getPixelShader();
	ComPtr<ID3D11Buffer>& getConstantBufferWorld();
	ComPtr<ID3D11Buffer>& getConstantBufferTextColor();
	std::map<int, ComPtr<ID3D11ShaderResourceView>>& getTextures();
	ComPtr<ID3D11SamplerState>& getSampler();
	std::map<std::wstring, TextBufferBDirect3D11>& getBuffersGPU();
};

using Direct3D11TextBufferBSmartPtr = std::shared_ptr<Direct3D11TextBufferB>;
