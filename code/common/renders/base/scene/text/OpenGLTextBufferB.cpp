#include "OpenGLTextBufferB.h"

#include "ITextBufferVisitor.h"

void OpenGLTextBufferB::init(const std::wstring& vs, const std::wstring& ps, const std::wstring cfg, const std::wstring& sampler, int h, VisitorTextBufferCreateAndDrawSmartPtr& visitor)
{
	this->visitor = visitor;
	this->visitor->create(vs, ps, cfg, sampler, h, *this);
}

void OpenGLTextBufferB::addText(const std::wstring& key, const std::wstring& text, const gmath::Matrix4& pos, const gmath::Color& color, const TypeTextBufferB& type)
{
	this->visitor->addText(key, text, pos, color, type, *this);
}

void OpenGLTextBufferB::replaceText(const std::wstring& key, const std::wstring& new_string)
{
	this->visitor->replaceText(key, new_string, *this);
}

void OpenGLTextBufferB::updatePosition(const std::wstring& key, const gmath::Matrix4& pos)
{
	this->visitor->updatePosition(key, pos, *this);
}

void OpenGLTextBufferB::updateColor(const std::wstring& key, const gmath::Color& c)
{
	this->visitor->updateColor(key, c, *this);
}

void OpenGLTextBufferB::reinit()
{
	this->visitor->reinit(*this);
}

void OpenGLTextBufferB::draw()
{
	this->visitor->draw(*this);
}

std::wstring& OpenGLTextBufferB::getSampler()
{
	return sampler;
}

std::map<int, int>& OpenGLTextBufferB::getTextures()
{
	return textures;
}

GLuint& OpenGLTextBufferB::getShaderProgram()
{
	return shaderProgram;
}

GLint& OpenGLTextBufferB::getWorldLocation()
{
	return worldLocation;
}

GLint& OpenGLTextBufferB::getTextColorLocation()
{
	return textColorLocation;
}

std::map<std::wstring, TextBufferBOpenGL>& OpenGLTextBufferB::getBuffersGPU()
{
	return buffersGPU;
}
