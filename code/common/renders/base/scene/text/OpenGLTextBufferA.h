#pragma once

#include "../../../abstract/scene/text/ITextBufferA.h"
#include "../../gl_header.h"

class OpenGLTextBufferA: public ITextBufferA
{
	GLuint glVertexBuffer;
	GLuint glVao;
	GLuint shaderProgram;
	std::map<int, int> textures;
	std::wstring sampler;
	GLint worldLocation;
	GLint textColorLocation;

	VisitorTextBufferCreateAndDrawSmartPtr visitor;


public:
	void init(const std::wstring& vs, const std::wstring& ps,
		const std::wstring cfg, const std::wstring& sampler, VisitorTextBufferCreateAndDrawSmartPtr& visitor) override;
	void mapVertexBuffer() override;
	void unmapVertexBuffer() override;

	int getHeightText(const std::wstring& key) override;
	
	void addText(const std::wstring& key, const std::wstring& text, const gmath::Matrix4& pos, const gmath::Color& color, int h) override;
	void replaceTextForEnter(const std::wstring& key, int ind_str, int ind_start, const std::wstring& new_string) override;
	void updatePosition(const std::wstring& key, const gmath::Matrix4& pos) override;
	void updateColor(const std::wstring& key, const gmath::Color& c)override;
	void reinit() override;
	void draw() override;

	GLuint& getVertexBuffer();
	GLuint& getVao();
	GLuint& getShaderProgram();
	std::wstring& getSampler();
	std::map<int, int>& getTextures();
	GLint& getWorldLocation();
	GLint& getTextColorLocation();

};

using OpenGLTextBufferASmartPtr = std::shared_ptr<OpenGLTextBufferA>;
