#include "ITextBufferVisitor.h"

#include "Direct3D11TextBufferA.h"
#include "OpenGLTextBufferA.h"

#include "Direct3D11TextBufferB.h"
#include "OpenGLTextBufferB.h"

#include "../../BufferCreator.h"
#include "../model/Buffers.h"
#include "../../../../utils/base/ExceptionDirect3D11TextBuffer.h"
#include "../../../../utils/base/loaders/TextConfigLoader.h"
#include "../../../../utils/base/ExceptionMemory.h"
#include "../model/VertexText.h"

VisitorTextBufferCreateAndDraw::VisitorTextBufferCreateAndDraw(const ComPtr<ID3D11Device>& d3dDevice,
	const ComPtr<ID3D11DeviceContext>& d3dImmediateContext,
	const Direct3D11ShadersBufferSmartPtr& shadersBufferD3d11,
	const Direct3D11TexturesBufferSmartPtr& texturesBufferD3d11, 
	const CameraSmartPtr& camera): d3dDevice(d3dDevice),
	d3dImmediateContext(d3dImmediateContext),
	shadersBufferD3d11(shadersBufferD3d11),
	texturesBufferD3d11(texturesBufferD3d11),
	camera(camera)
{
}

VisitorTextBufferCreateAndDraw::VisitorTextBufferCreateAndDraw(const OpenGLShadersBufferSmartPtr& shadersBufferOpenGl,
	const OpenGLTexturesBufferSmartPtr& texturesBufferOpenGl,
	const CameraSmartPtr& camera):shadersBufferOpenGl(shadersBufferOpenGl),
	texturesBufferOpenGl(texturesBufferOpenGl), camera(camera)
{
}

void VisitorTextBufferCreateAndDraw::create(const std::wstring& vs, const std::wstring& ps, const std::wstring cfg, const std::wstring& sampler, Direct3D11TextBufferA& a)
{
	// ���������� �������
	a.getVertexShader() = shadersBufferD3d11->getVertexShader(vs);
	a.getPixelShader() = shadersBufferD3d11->getPixelShader(ps);
	// �������� ������� ������
	std::vector<D3D11_INPUT_ELEMENT_DESC> layout =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	auto vsBlob = shadersBufferD3d11->getVertexShaderBlob(vs);
	HRESULT hr = d3dDevice->CreateInputLayout(layout.data(), static_cast<UINT>(layout.size()), vsBlob->GetBufferPointer(),
		vsBlob->GetBufferSize(), a.getInputLayout().GetAddressOf());
	if (FAILED(hr))
		throw ExceptionDirect3D11TextBuffer(L"error create input layout!");
	a.getConstantBufferWorld() = Direct3D11CreatorBuffer::createBuffer(d3dDevice, static_cast<UINT>(sizeof(ConstantBufferWorld)),
		D3D11_BIND_CONSTANT_BUFFER, D3D11_USAGE_DEFAULT, 0);
	a.getConstantBufferTextColor() = Direct3D11CreatorBuffer::createBuffer(d3dDevice, static_cast<UINT>(sizeof(ConstantBufferColorText)),
		D3D11_BIND_CONSTANT_BUFFER, D3D11_USAGE_DEFAULT, 0);
	// load config
	configs::TextConfigLoaderSmartPtr loader(new configs::TextConfigLoader());
	if(!loader)
		throw ExceptionMemory(L"TextConfigLoaderSmartPtr is not create, memory not found!");
	MapTextProperties mtp;
	TexturesProperties tp;
	loader->loadConfig(cfg, &mtp, &tp);
	a.setBufferSymbols(std::move(mtp));
	// get texture
	for (auto& _tp : tp)
		a.getTextures().insert({ _tp.first, texturesBufferD3d11->getTexture(_tp.second) });
	// get sampler
	a.getSampler() = texturesBufferD3d11->getSampler(sampler);
	// ����� ������
	a.setMaxCountVertex(Direct3D11TextBufferA::DEFAULT_MAX_COUNT_VERTEX);
	a.setCountVertex(0);
	std::vector< VertexTexture> v; v.resize(a.getMaxCountVertex());
	a.getVertexBuffer() = Direct3D11CreatorBuffer::createBuffer(d3dDevice, static_cast<UINT>(sizeof(VertexTexture) * a.getMaxCountVertex()),
		D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_DYNAMIC, D3D11_CPU_ACCESS_WRITE, v.data());
}

void VisitorTextBufferCreateAndDraw::create(const std::wstring& vs, const std::wstring& ps, const std::wstring cfg, const std::wstring& sampler, OpenGLTextBufferA& a)
{
	GLuint _vs = shadersBufferOpenGl->getShader(vs);
	GLuint _ps = shadersBufferOpenGl->getShader(ps);
	std::vector<unsigned> key = { _vs, _ps };
	a.getShaderProgram() = shadersBufferOpenGl->getShaderProgram(key);
	a.getWorldLocation() = OpenGLFunctional::getInstance()->glGetUniformLocation(a.getShaderProgram(), "world");
	a.getTextColorLocation() = OpenGLFunctional::getInstance()->glGetUniformLocation(a.getShaderProgram(), "textColor");
	// load config
	configs::TextConfigLoaderSmartPtr loader(new configs::TextConfigLoader());
	if (!loader)
		throw ExceptionMemory(L"TextConfigLoaderSmartPtr is not create, memory not found!");
	MapTextProperties mtp;
	TexturesProperties tp;
	loader->loadConfig(cfg, &mtp, &tp);
	a.setBufferSymbols(std::move(mtp));
	// get texture
	for (auto& _tp : tp)
		a.getTextures().insert({ _tp.first, texturesBufferOpenGl->getTexture(_tp.second) });
	// get sampler
	a.getSampler() = sampler;
	// create dynamic vertex buffer
	a.setMaxCountVertex(OpenGLTextBufferA::DEFAULT_MAX_COUNT_VERTEX);
	a.setCountVertex(0);
	OpenGLFunctional::getInstance()->glGenVertexArrays(1, &a.getVao());
	OpenGLFunctional::getInstance()->glGenBuffers(1, &a.getVertexBuffer());
	OpenGLFunctional::getInstance()->glBindVertexArray(a.getVao());
	// �������� ���������� ������
	OpenGLFunctional::getInstance()->glBindBuffer(GL_ARRAY_BUFFER, a.getVertexBuffer());
	OpenGLFunctional::getInstance()->glBufferData(GL_ARRAY_BUFFER, sizeof(VertexTexture) * a.getMaxCountVertex(), nullptr, GL_DYNAMIC_DRAW);
	// �������� ������� ������
	OpenGLFunctional::getInstance()->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
	OpenGLFunctional::getInstance()->glEnableVertexAttribArray(0);
	OpenGLFunctional::getInstance()->glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	OpenGLFunctional::getInstance()->glEnableVertexAttribArray(1);
	// ���������� ������
	OpenGLFunctional::getInstance()->glBindBuffer(GL_ARRAY_BUFFER, 0);
	OpenGLFunctional::getInstance()->glBindVertexArray(0);
}

int VisitorTextBufferCreateAndDraw::getHeightText(const std::wstring& key, Direct3D11TextBufferA& a)
{
	return a.getTextFromBuffer(key).height;
}

int VisitorTextBufferCreateAndDraw::getHeightText(const std::wstring& key, OpenGLTextBufferA& a)
{
	return a.getTextFromBuffer(key).height;
}

void VisitorTextBufferCreateAndDraw::addText(const std::wstring& key, const std::wstring& text, const gmath::Matrix4& pos, const gmath::Color& color, int h, Direct3D11TextBufferA& a)
{
	std::wstring _text(text);
	if (_text.back() != L'\n')
		_text += L'\n';
	// ��������� ����� � �����
	TextA t;
	t.start = a.getCountVertex();
	t.position = pos;
	t.text = _text;
	t.textColor = color;
	t.height = h;
	t.count = 0;

	// ��������� ������� � �����
	// �������� ��������� �����
	auto verticesPtr = a.getMappedArrayVertex();

	// ������� �� ��� �������� �� ������ � ��������� � ��������� �����
	float posX(0);
	float posY(0);
	float height(t.height); // ���� ���������
	unsigned indexVertices(a.getCountVertex());
	int startEnter(0);
	unsigned startVertex(a.getCountVertex());

	for (auto&& s : _text)
	{
		if (s == L'\n')
		{
			posX = 0;
			posY -= height;
			a.addCountEnter(startEnter, t.count);
			startEnter = t.count;
			continue;
		}

		TextProperty metrics = a.getMetricsSymbol().at({ s, h });
		VertexTexture leftTop = { {posX, posY + height, 0.f}, {metrics.leftTop} };
		VertexTexture rightTop = { {posX + metrics.width, posY + height, 0.f}, {metrics.rightTop} };
		VertexTexture rightBottom = { {posX + metrics.width, posY, 0.f}, {metrics.rightBottom} };
		VertexTexture leftBottom = { {posX, posY, 0.f}, {metrics.leftBottom} };
		t.count += 4;

		verticesPtr[indexVertices++] = leftBottom;
		verticesPtr[indexVertices++] = leftTop;
		verticesPtr[indexVertices++] = rightBottom;
		verticesPtr[indexVertices++] = rightTop;

		posX += metrics.width;
	}
	//memcpy(verticesPtr, (void*)(a.getBufferCPU().data() + startVertex), sizeof(VertexTexture) * t.count);
	//memcpy(verticesPtr, (void*)a.getBufferCPU().data(), sizeof(VertexTexture) * a.getCountVertex());

	// todo: ��������, ���� �������� �� ������� ������������� ���������� ������ ��� �������� - ����� ������������ ������
	a.setCountVertex(indexVertices);
	a.addTextFromBuffer(key, t);
}

void VisitorTextBufferCreateAndDraw::addText(const std::wstring& key, const std::wstring& text, const gmath::Matrix4& pos, const gmath::Color& color, int h, OpenGLTextBufferA& a)
{
	std::wstring _text(text);
	if (_text.back() != L'\n')
		_text += L'\n';
	// ��������� ����� � �����
	TextA t;
	t.start = a.getCountVertex();
	t.position = pos;
	t.text = _text;
	t.textColor = color;
	t.height = h;
	t.count = 0;

	// ��������� ������� � �����
	// �������� ��������� �����
	auto verticesPtr = a.getMappedArrayVertex();

	// ������� �� ��� �������� �� ������ � ��������� � ��������� �����
	float posX(0);
	float posY(0);
	float height(t.height); // ���� ���������
	unsigned indexVertices(a.getCountVertex());
	int startEnter(0);

	for (auto&& s : _text)
	{
		if (s == L'\n')
		{
			verticesPtr[indexVertices++] = { {posX, posY, 0.f}, {0,0} };
			verticesPtr[indexVertices++] = { {0, posY, 0.f}, {0,0} };
			posX = 0;
			posY -= height;
			t.count += 2;
			a.addCountEnter(startEnter, t.count);
			startEnter = t.count;
			continue;
		}
		
		TextProperty metrics = a.getMetricsSymbol().at({ s, h });
		VertexTexture leftTop = { {posX, posY + height, 0.f}, {metrics.leftTop} };
		VertexTexture rightTop = { {posX + metrics.width, posY + height, 0.f}, {metrics.rightTop} };
		VertexTexture rightBottom = { {posX + metrics.width, posY, 0.f}, {metrics.rightBottom} };
		VertexTexture leftBottom = { {posX, posY, 0.f}, {metrics.leftBottom} };

		// ccw
		//verticesPtr[indexVertices++] = leftTop;
		//verticesPtr[indexVertices++] = leftBottom;
		//verticesPtr[indexVertices++] = rightTop;
		//verticesPtr[indexVertices++] = rightBottom;
		// cw
		verticesPtr[indexVertices++] = leftBottom;
		verticesPtr[indexVertices++] = leftTop;
		verticesPtr[indexVertices++] = rightBottom;
		verticesPtr[indexVertices++] = rightTop;

		t.count += 4;

		posX += metrics.width;
	}

	// todo: ��������, ���� �������� �� ������� ������������� ���������� ������ ��� �������� - ����� ������������ ������
	a.setCountVertex(indexVertices);

	a.addTextFromBuffer(key, t);
}

void VisitorTextBufferCreateAndDraw::replaceTextForEnter(const std::wstring& key, int ind_str, int ind_start, const std::wstring& new_string, Direct3D11TextBufferA& a)
{
	std::wstring newString(new_string);
	if (a.getCountEnter().size() >= ind_str) // ���� ������ ���
	{
		// throw
	}
	auto pair = a.getCountEnter().at(ind_str);
	int length = pair.second;
	int index(pair.first + (ind_start * 4));

	if (newString.length() > (length - index) / 4) // ���� ������ ������ ������������
		newString = newString.substr(0, (length - index) / 4);
	else
	if (newString.length() < (length - index) / 4) // ���� ������ ������ ������������
		newString.append((length - index) / 4 - newString.length(), L' ');

	auto verticesPtr(a.getMappedArrayVertex());
	float posX(verticesPtr[index].position[gmtypes::vx]);
	float posY(verticesPtr[index].position[gmtypes::vy]);
	int height(a.getTextFromBuffer(key).height);

	for (int k(0); index < pair.second; k++)
	{
		TextProperty metrics = a.getMetricsSymbol().at({ newString[k], height });

		VertexTexture leftTop = { {posX, posY + height, 0.f}, {metrics.leftTop} };
		VertexTexture rightTop = { {posX + metrics.width, posY + height, 0.f}, {metrics.rightTop} };
		VertexTexture rightBottom = { {posX + metrics.width, posY, 0.f}, {metrics.rightBottom} };
		VertexTexture leftBottom = { {posX, posY, 0.f}, {metrics.leftBottom} };

		verticesPtr[index++] = leftBottom;
		verticesPtr[index++] = leftTop;
		verticesPtr[index++] = rightBottom;
		verticesPtr[index++] = rightTop;

		posX += metrics.width;
	}
}

void VisitorTextBufferCreateAndDraw::replaceTextForEnter(const std::wstring& key, int ind_str, int ind_start, const std::wstring& new_string, OpenGLTextBufferA& a)
{
	std::wstring newString(new_string);
	if (a.getCountEnter().size() >= ind_str) // ���� ������ ���
	{
		// throw
	}
	auto pair = a.getCountEnter().at(ind_str);
	int length = pair.second;
	int index(pair.first + (ind_start * 4));

	if (newString.length() > (length - index) / 4) // ���� ������ ������ ������������
		newString = newString.substr(0, (length - index) / 4);
	else
		if (newString.length() < (length - index) / 4) // ���� ������ ������ ������������
			newString.append((length - index) / 4 - newString.length(), L' ');

	auto verticesPtr(a.getMappedArrayVertex());
	float posX(verticesPtr[index + 1].position[gmtypes::vx]);
	float posY(verticesPtr[index + 1].position[gmtypes::vy]);
	int height(a.getTextFromBuffer(key).height);

	for (int k(0); index < pair.second - 2; k++)
	{
		TextProperty metrics = a.getMetricsSymbol().at({ newString[k], height });

		VertexTexture leftTop = { {posX, posY + height, 0.f}, {metrics.leftTop} };
		VertexTexture rightTop = { {posX + metrics.width, posY + height, 0.f}, {metrics.rightTop} };
		VertexTexture rightBottom = { {posX + metrics.width, posY, 0.f}, {metrics.rightBottom} };
		VertexTexture leftBottom = { {posX, posY, 0.f}, {metrics.leftBottom} };

		// ccw
		//verticesPtr[index++] = leftTop;
		//verticesPtr[index++] = leftBottom;
		//verticesPtr[index++] = rightTop;
		//verticesPtr[index++] = rightBottom;
		verticesPtr[index++] = leftBottom;
		verticesPtr[index++] = leftTop;
		verticesPtr[index++] = rightBottom;
		verticesPtr[index++] = rightTop;

		posX += metrics.width;
	}
	verticesPtr[index++] = { {posX, posY, 0.f}, {0,0} };
	verticesPtr[index++] = { {0, posY, 0.f}, {0,0} };
}

void VisitorTextBufferCreateAndDraw::updatePosition(const std::wstring& key, const gmath::Matrix4& pos, Direct3D11TextBufferA& a)
{
	a.getTextFromBuffer(key).position = pos;
}

void VisitorTextBufferCreateAndDraw::updatePosition(const std::wstring& key, const gmath::Matrix4& pos, OpenGLTextBufferA& a)
{
	a.getTextFromBuffer(key).position = pos;
}

void VisitorTextBufferCreateAndDraw::updateColor(const std::wstring& key, const gmath::Color& c, Direct3D11TextBufferA& a)
{
	a.getTextFromBuffer(key).textColor = c;
}

void VisitorTextBufferCreateAndDraw::updateColor(const std::wstring& key, const gmath::Color& c, OpenGLTextBufferA& a)
{
	a.getTextFromBuffer(key).textColor = c;
}

void VisitorTextBufferCreateAndDraw::reinit(Direct3D11TextBufferA& a)
{
	// todo: ������������ ������ ������ �/��� �������� (���������� ������ �����, ����� ������ ����� �����, ����� ����� ������)
}

void VisitorTextBufferCreateAndDraw::reinit(OpenGLTextBufferA& a)
{
	// todo: ������������ ������ ������ �/��� �������� (���������� ������ �����, ����� ������ ����� �����, ����� ����� ������)
}

void VisitorTextBufferCreateAndDraw::draw(Direct3D11TextBufferA& a)
{
	// ����������� ������� ������
	d3dImmediateContext->IASetInputLayout(a.getInputLayout().Get());
	// ��������� ���������� ������
	UINT stride = sizeof(VertexTexture);
	UINT offset = 0;
	d3dImmediateContext->IASetVertexBuffers(0, 1, a.getVertexBuffer().GetAddressOf(), &stride, &offset);
	// ��������� ���������� ������
	//d3dImmediateContext->IASetIndexBuffer(a.getIndexBuffer().Get(), DXGI_FORMAT_R32_UINT, 0);
	// ��������� ���������
	d3dImmediateContext->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
	// ��������� ��������
	d3dImmediateContext->VSSetShader(a.getVertexShader().Get(), NULL, 0);
	d3dImmediateContext->PSSetShader(a.getPixelShader().Get(), NULL, 0);
	camera->sendToGPU(-1);
	// ����� ���������	
	auto w = a.getConstantBufferWorld();
	auto c = a.getConstantBufferTextColor();
	d3dImmediateContext->PSSetSamplers(0, 1, a.getSampler().GetAddressOf());
	d3dImmediateContext->VSSetConstantBuffers(1, 1, w.GetAddressOf());
	d3dImmediateContext->PSSetConstantBuffers(0, 1, c.GetAddressOf());
	for (auto&& t: a.getTexts())
	{
		d3dImmediateContext->PSSetShaderResources(0, 1, a.getTextures().at(t.second.height).GetAddressOf());
		ConstantBufferWorld cbw = { gmath::Matrix::Transponse(t.second.position) };
		d3dImmediateContext->UpdateSubresource(w.Get(), 0, NULL, &cbw, 0, 0);
		ConstantBufferColorText buf = { t.second.textColor };
		d3dImmediateContext->UpdateSubresource(c.Get(), 0, NULL, &buf, 0, 0);
		d3dImmediateContext->Draw(t.second.count, t.second.start);
	}
}

void VisitorTextBufferCreateAndDraw::draw(OpenGLTextBufferA& a)
{
	OpenGLFunctional::getInstance()->glUseProgram(a.getShaderProgram());
	camera->sendToGPU(a.getShaderProgram());
	OpenGLFunctional::getInstance()->glBindVertexArray(a.getVao());
	for (auto&& t : a.getTexts())
	{
		// update color
		OpenGLFunctional::getInstance()->glUniform4f(a.getTextColorLocation(), 
			t.second.textColor[gmtypes::cr],
			t.second.textColor[gmtypes::cg],
			t.second.textColor[gmtypes::cb],
			t.second.textColor[gmtypes::ca]);
		// draw
		int cv(t.second.count);
		OpenGLFunctional::getInstance()->glUniformMatrix4fv(a.getWorldLocation(), 1, GL_FALSE, t.second.position.toArray().data());
		GLuint texture = a.getTextures().at(t.second.height);
		OpenGLSamplerRepeat::setSampler(texture, a.getSampler());
		OpenGLFunctional::getInstance()->glBindTextureUnit(0, texture);
		//OpenGLFunctional::getInstance()->glActiveTexture(GL_TEXTURE0);
		//glBindTexture(GL_TEXTURE_2D, texture);
		//OpenGLFunctional::getInstance()->glUniform1i(OpenGLFunctional::getInstance()->glGetUniformLocation(a.getShaderProgram(), "inTexture"), 0);
		glDrawArrays(GL_TRIANGLE_STRIP, t.second.start, cv);
	}
	OpenGLFunctional::getInstance()->glBindVertexArray(0);
}

void VisitorTextBufferCreateAndDraw::mapVertexBuffer(Direct3D11TextBufferA& a)
{
	// ��������� ������� � �����
	D3D11_MAPPED_SUBRESOURCE mappedResourceVertexBuffer;
	// ����� ����� ������
	HRESULT hr = d3dImmediateContext->Map(a.getVertexBuffer().Get(), 0, D3D11_MAP_WRITE_NO_OVERWRITE, 0, &mappedResourceVertexBuffer);
	if (FAILED(hr))
		throw ExceptionDirect3D11TextBuffer(L"Error mapped vertex buffer!\n");
	a.setMappedArrayVertex(static_cast<VertexTexture*>(mappedResourceVertexBuffer.pData));
	if (!a.getMappedArrayVertex())
		throw ExceptionDirect3D11TextBuffer(L"Error get vertex array!\n");
}

void VisitorTextBufferCreateAndDraw::mapVertexBuffer(OpenGLTextBufferA& a)
{
	// ����� ����� ������
	OpenGLFunctional::getInstance()->glBindVertexArray(a.getVao());
	OpenGLFunctional::getInstance()->glBindBuffer(GL_ARRAY_BUFFER, a.getVertexBuffer());
	a.setMappedArrayVertex(static_cast<VertexTexture*>(OpenGLFunctional::getInstance()->glMapBuffer(GL_ARRAY_BUFFER, GL_READ_WRITE)));
}

void VisitorTextBufferCreateAndDraw::unmapVertexBuffer(Direct3D11TextBufferA& a)
{
	// ������� ����� ������
	d3dImmediateContext->Unmap(a.getVertexBuffer().Get(), 0);
}

void VisitorTextBufferCreateAndDraw::unmapVertexBuffer(OpenGLTextBufferA& a)
{
	// ������� ����� ������
	OpenGLFunctional::getInstance()->glUnmapBuffer(GL_ARRAY_BUFFER);
	OpenGLFunctional::getInstance()->glBindBuffer(GL_ARRAY_BUFFER, 0);
	OpenGLFunctional::getInstance()->glBindVertexArray(0);
}




void VisitorTextBufferCreateAndDraw::create(const std::wstring& vs, const std::wstring& ps, const std::wstring cfg, const std::wstring& sampler, int h, Direct3D11TextBufferB& a)
{
	// ���������� �������
	a.getVertexShader() = shadersBufferD3d11->getVertexShader(vs);
	a.getPixelShader() = shadersBufferD3d11->getPixelShader(ps);
	// �������� ������� ������
	std::vector<D3D11_INPUT_ELEMENT_DESC> layout =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 8, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	auto vsBlob = shadersBufferD3d11->getVertexShaderBlob(vs);
	HRESULT hr = d3dDevice->CreateInputLayout(layout.data(), static_cast<UINT>(layout.size()), vsBlob->GetBufferPointer(),
		vsBlob->GetBufferSize(), a.getInputLayout().GetAddressOf());
	if (FAILED(hr))
		throw ExceptionDirect3D11TextBuffer(L"error create input layout!");
	a.getConstantBufferWorld() = Direct3D11CreatorBuffer::createBuffer(d3dDevice, static_cast<UINT>(sizeof(ConstantBufferWorld)),
		D3D11_BIND_CONSTANT_BUFFER, D3D11_USAGE_DEFAULT, 0);
	a.getConstantBufferTextColor() = Direct3D11CreatorBuffer::createBuffer(d3dDevice, static_cast<UINT>(sizeof(ConstantBufferColorText)),
		D3D11_BIND_CONSTANT_BUFFER, D3D11_USAGE_DEFAULT, 0);
	// load config
	configs::TextConfigLoaderSmartPtr loader(new configs::TextConfigLoader());
	if (!loader)
		throw ExceptionMemory(L"TextConfigLoaderSmartPtr is not create, memory not found!");
	MapTextProperties mtp;
	TexturesProperties tp;
	loader->loadConfig(cfg, &mtp, &tp);
	a.setBufferSymbols(std::move(mtp));
	// get texture
	for (auto& _tp : tp)
		a.getTextures().insert({ _tp.first, texturesBufferD3d11->getTexture(_tp.second) });
	// get sampler
	a.getSampler() = texturesBufferD3d11->getSampler(sampler);
	// set height
	a.setHeight(h);
}

void VisitorTextBufferCreateAndDraw::create(const std::wstring& vs, const std::wstring& ps, const std::wstring cfg, const std::wstring& sampler, int h, OpenGLTextBufferB& a)
{
	GLuint _vs = shadersBufferOpenGl->getShader(vs);
	GLuint _ps = shadersBufferOpenGl->getShader(ps);
	std::vector<unsigned> key = { _vs, _ps };
	a.getShaderProgram() = shadersBufferOpenGl->getShaderProgram(key);
	a.getWorldLocation() = OpenGLFunctional::getInstance()->glGetUniformLocation(a.getShaderProgram(), "world");
	a.getTextColorLocation() = OpenGLFunctional::getInstance()->glGetUniformLocation(a.getShaderProgram(), "textColor");
	// load config
	configs::TextConfigLoaderSmartPtr loader(new configs::TextConfigLoader());
	if (!loader)
		throw ExceptionMemory(L"TextConfigLoaderSmartPtr is not create, memory not found!");
	MapTextProperties mtp;
	TexturesProperties tp;
	loader->loadConfig(cfg, &mtp, &tp);
	a.setBufferSymbols(std::move(mtp));
	// get texture
	for (auto& _tp : tp)
		a.getTextures().insert({ _tp.first, texturesBufferOpenGl->getTexture(_tp.second) });
	// get sampler
	a.getSampler() = sampler;
	// set height
	a.setHeight(h);
}

void VisitorTextBufferCreateAndDraw::addText(const std::wstring& key, const std::wstring& text, const gmath::Matrix4& pos, const gmath::Color& color, const TypeTextBufferB& type, Direct3D11TextBufferB& a)
{
	// ��������� ����� � �����
	std::wstring _text(text);
	if (_text.back() != L'\n')
		_text += L'\n';
	TextBufferBDirect3D11 t;
	t.position = pos;
	t.text = _text;
	t.textColor = color;
	t.count = 0;
	t.type = type;
	std::vector<VertexText> buffer;
	// ������� �� ��� �������� �� ������ � ��������� � ��������� �����
	float posX(0);
	float posY(0);
	int height(a.getHeight());

	for (auto&& s : _text)
	{
		if (s == L'\n')
		{
			posX = 0;
			posY -= height;
			continue;
		}

		TextProperty metrics = a.getMetricsSymbol().at({ s, height });
		VertexText leftTop = { {posX, posY + height}, {metrics.leftTop} };
		VertexText rightTop = { {posX + metrics.width, posY + height}, {metrics.rightTop} };
		VertexText rightBottom = { {posX + metrics.width, posY}, {metrics.rightBottom} };
		VertexText leftBottom = { {posX, posY}, {metrics.leftBottom} };
		t.count += 4;

		buffer.push_back(leftBottom);
		buffer.push_back(leftTop);
		buffer.push_back(rightBottom);
		buffer.push_back(rightTop);

		posX += metrics.width;
	}
	// ����� ������
	if(type == TypeTextBufferB::DYNAMIC_TEXT_BUFFER)
		t.d3dVertexBuffer = Direct3D11CreatorBuffer::createBuffer(d3dDevice, static_cast<UINT>(sizeof(VertexText) * buffer.size()),
			D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_DYNAMIC, D3D11_CPU_ACCESS_WRITE, buffer.data());
	else
		t.d3dVertexBuffer = Direct3D11CreatorBuffer::createBuffer(d3dDevice, static_cast<UINT>(sizeof(VertexText) * buffer.size()),
			D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_DEFAULT, 0, buffer.data());
	// ��������� ��� � ����� ������� ������
	a.getBuffersGPU().insert({ key, t });
}

void VisitorTextBufferCreateAndDraw::addText(const std::wstring& key, const std::wstring& text, const gmath::Matrix4& pos, const gmath::Color& color, const TypeTextBufferB& type, OpenGLTextBufferB& a)
{
	// ��������� ����� � �����
	std::wstring _text(text);
	if (_text.back() != L'\n')
		_text += L'\n';
	TextBufferBOpenGL t;
	t.position = pos;
	t.text = _text;
	t.textColor = color;
	t.count = 0;
	t.type = type;
	std::vector<VertexText> buffer;
	// ������� �� ��� �������� �� ������ � ��������� � ��������� �����
	float posX(0);
	float posY(0);
	int height(a.getHeight()); // ���� ���������

	for (auto&& s : _text)
	{
		if (s == L'\n')
		{
			buffer.push_back({ {posX, posY}, {0,0} });
			buffer.push_back({ {0, posY}, {0,0} });
			posX = 0;
			posY -= height;
			t.count += 2;
			continue;
		}

		TextProperty metrics = a.getMetricsSymbol().at({ s, height });
		VertexText leftTop = { {posX, posY + height}, {metrics.leftTop} };
		VertexText rightTop = { {posX + metrics.width, posY + height}, {metrics.rightTop} };
		VertexText rightBottom = { {posX + metrics.width, posY}, {metrics.rightBottom} };
		VertexText leftBottom = { {posX, posY}, {metrics.leftBottom} };

		// ccw
		/*buffer.push_back(leftTop);
		buffer.push_back(leftBottom);
		buffer.push_back(rightTop);
		buffer.push_back(rightBottom);*/
		// cw
		buffer.push_back(leftBottom);
		buffer.push_back(leftTop);
		buffer.push_back(rightBottom);
		buffer.push_back(rightTop);

		t.count += 4;

		posX += metrics.width;
	}

	// ������� ����� VAO � ������� ������
	GLuint glVertexBuffer;
	GLuint glVao;
	OpenGLFunctional::getInstance()->glGenVertexArrays(1, &glVao);
	OpenGLFunctional::getInstance()->glGenBuffers(1, &glVertexBuffer);

	// ��������� ��� �������
	OpenGLFunctional::getInstance()->glBindVertexArray(glVao);
	// �������� ���������� ������
	OpenGLFunctional::getInstance()->glBindBuffer(GL_ARRAY_BUFFER, glVertexBuffer);
	OpenGLFunctional::getInstance()->glBufferData(GL_ARRAY_BUFFER, sizeof(VertexTexture) * buffer.size(), buffer.data(), type == TypeTextBufferB::DYNAMIC_TEXT_BUFFER ? GL_DYNAMIC_DRAW : GL_STATIC_DRAW);
	// �������� ������� ������
	OpenGLFunctional::getInstance()->glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0);
	OpenGLFunctional::getInstance()->glEnableVertexAttribArray(0);
	OpenGLFunctional::getInstance()->glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)(2 * sizeof(GLfloat)));
	OpenGLFunctional::getInstance()->glEnableVertexAttribArray(1);
	// ���������� ������
	OpenGLFunctional::getInstance()->glBindBuffer(GL_ARRAY_BUFFER, 0);
	OpenGLFunctional::getInstance()->glBindVertexArray(0);

	// ��������� ��� � ����� ������� VAO
	t.glVao = glVao;
	t.glVertexBuffer = glVertexBuffer;
	a.getBuffersGPU().insert({ key, t });
}

void VisitorTextBufferCreateAndDraw::replaceText(const std::wstring& key, const std::wstring& new_string, Direct3D11TextBufferB& a)
{
	auto text = a.getBuffersGPU().at(key);
	if (text.type != TypeTextBufferB::DYNAMIC_TEXT_BUFFER)
		return;

	// ��������, ��� ������ ������ ��� ������ � ������ 
	std::wstring newText(new_string);
	if (newText.length() > text.count / 4)
		newText.resize(text.count / 4);
	else if (newText.length() < text.count / 4)
		newText.append((text.count / 4) - newText.length(), L' ');
	if (newText.back() != L'\n')
		newText += L'\n';

	float posX(0);
	float posY(0);
	int height(a.getHeight());
	int index(0);

	// ��������� ������� � �����
	D3D11_MAPPED_SUBRESOURCE mappedResourceVertexBuffer;
	// ����� ����� ������
	HRESULT hr = d3dImmediateContext->Map(text.d3dVertexBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResourceVertexBuffer);
	if (FAILED(hr))
		throw ExceptionDirect3D11TextBuffer(L"Error mapped vertex buffer!\n");
	VertexText* mappedMem = static_cast<VertexText*>(mappedResourceVertexBuffer.pData);
	if (!mappedMem)
		throw ExceptionDirect3D11TextBuffer(L"Error get vertex array!\n");

	for (auto&& s : newText)
	{
		if (s == L'\n')
		{
			posX = 0;
			posY -= height;
			continue;
		}

		TextProperty metrics = a.getMetricsSymbol().at({ s, height });
		VertexText leftTop = { {posX, posY + height}, {metrics.leftTop} };
		VertexText rightTop = { {posX + metrics.width, posY + height}, {metrics.rightTop} };
		VertexText rightBottom = { {posX + metrics.width, posY}, {metrics.rightBottom} };
		VertexText leftBottom = { {posX, posY}, {metrics.leftBottom} };

		mappedMem[index++] = (leftBottom);
		mappedMem[index++] = (leftTop);
		mappedMem[index++] = (rightBottom);
		mappedMem[index++] = (rightTop);

		posX += metrics.width;
	}

	// ������� ����� ������
	d3dImmediateContext->Unmap(text.d3dVertexBuffer.Get(), 0);
}

void VisitorTextBufferCreateAndDraw::replaceText(const std::wstring& key, const std::wstring& new_string, OpenGLTextBufferB& a)
{
	auto text = a.getBuffersGPU().at(key);
	if (text.type != TypeTextBufferB::DYNAMIC_TEXT_BUFFER)
		return;

	// ��������, ��� ������ ������ ��� ������ � ������ 
	std::wstring newText(new_string);
	if (newText.length() > text.count / 4)
		newText.resize(text.count / 4);
	else if (newText.length() < text.count / 4)
		newText.append((text.count / 4) - newText.length(), L' ');
	if (newText.back() != L'\n')
		newText += L'\n';

	float posX(0);
	float posY(0);
	int height(a.getHeight());
	int index(0);

	// ����� ����� ������
	OpenGLFunctional::getInstance()->glBindVertexArray(text.glVao);
	OpenGLFunctional::getInstance()->glBindBuffer(GL_ARRAY_BUFFER, text.glVertexBuffer);
	auto mappedMem = static_cast<VertexText*>(OpenGLFunctional::getInstance()->glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY));

	for (auto&& s : newText)
	{
		if (s == L'\n')
		{
			mappedMem[index++] = { {posX, posY}, {0,0} };
			mappedMem[index++] = { {0, posY}, {0,0} };
			posX = 0;
			posY -= height;
			continue;
		}
		TextProperty metrics = a.getMetricsSymbol().at({ s, height });
		VertexText leftTop = { {posX, posY + height}, {metrics.leftTop} };
		VertexText rightTop = { {posX + metrics.width, posY + height}, {metrics.rightTop} };
		VertexText rightBottom = { {posX + metrics.width, posY}, {metrics.rightBottom} };
		VertexText leftBottom = { {posX, posY}, {metrics.leftBottom} };

		// ccw
		//mappedMem[index++] = (leftTop);
		//mappedMem[index++] = (leftBottom);
		//mappedMem[index++] = (rightTop);
		//mappedMem[index++] = (rightBottom);
		// cw
		mappedMem[index++] = (leftBottom);
		mappedMem[index++] = (leftTop);
		mappedMem[index++] = (rightBottom);
		mappedMem[index++] = (rightTop);

		posX += metrics.width;
	}

	// ������� �����
	OpenGLFunctional::getInstance()->glUnmapBuffer(GL_ARRAY_BUFFER);
	OpenGLFunctional::getInstance()->glBindBuffer(GL_ARRAY_BUFFER, 0);
	OpenGLFunctional::getInstance()->glBindVertexArray(0);
}

void VisitorTextBufferCreateAndDraw::updatePosition(const std::wstring& key, const gmath::Matrix4& pos, Direct3D11TextBufferB& a)
{
	a.getBuffersGPU().at(key).position = pos;
}

void VisitorTextBufferCreateAndDraw::updatePosition(const std::wstring& key, const gmath::Matrix4& pos, OpenGLTextBufferB& a)
{
	a.getBuffersGPU().at(key).position = pos;
}

void VisitorTextBufferCreateAndDraw::updateColor(const std::wstring& key, const gmath::Color& c, Direct3D11TextBufferB& a)
{
	a.getBuffersGPU().at(key).textColor = c;
}

void VisitorTextBufferCreateAndDraw::updateColor(const std::wstring& key, const gmath::Color& c, OpenGLTextBufferB& a)
{
	a.getBuffersGPU().at(key).textColor = c;
}

void VisitorTextBufferCreateAndDraw::reinit(Direct3D11TextBufferB& a)
{
}

void VisitorTextBufferCreateAndDraw::reinit(OpenGLTextBufferB& a)
{

}

void VisitorTextBufferCreateAndDraw::draw(Direct3D11TextBufferB& a)
{
	// ����������� ������� ������
	d3dImmediateContext->IASetInputLayout(a.getInputLayout().Get());
	// ��������� ���������� ������
	UINT stride = sizeof(VertexText);
	UINT offset = 0;
	// ��������� ���������
	d3dImmediateContext->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
	// ��������� ��������
	d3dImmediateContext->VSSetShader(a.getVertexShader().Get(), NULL, 0);
	d3dImmediateContext->PSSetShader(a.getPixelShader().Get(), NULL, 0);
	camera->sendToGPU(-1);
	// ����� ���������	
	auto w = a.getConstantBufferWorld();
	auto c = a.getConstantBufferTextColor();
	d3dImmediateContext->PSSetSamplers(0, 1, a.getSampler().GetAddressOf());
	d3dImmediateContext->VSSetConstantBuffers(1, 1, w.GetAddressOf());
	d3dImmediateContext->PSSetConstantBuffers(0, 1, c.GetAddressOf());
	d3dImmediateContext->PSSetShaderResources(0, 1, a.getTextures().at(a.getHeight()).GetAddressOf());
	for (auto&& t : a.getBuffersGPU())
	{
		TextBufferBDirect3D11 text = t.second;
		// ��������� ���������� ������
		d3dImmediateContext->IASetVertexBuffers(0, 1, text.d3dVertexBuffer.GetAddressOf(), &stride, &offset);
		ConstantBufferWorld cbw = { gmath::Matrix::Transponse(text.position) };
		d3dImmediateContext->UpdateSubresource(w.Get(), 0, NULL, &cbw, 0, 0);
		ConstantBufferColorText buf = { text.textColor };
		d3dImmediateContext->UpdateSubresource(c.Get(), 0, NULL, &buf, 0, 0);
		d3dImmediateContext->Draw(text.count, 0);
	}
}

void VisitorTextBufferCreateAndDraw::draw(OpenGLTextBufferB& a)
{
	OpenGLFunctional::getInstance()->glUseProgram(a.getShaderProgram());
	camera->sendToGPU(a.getShaderProgram());

	GLuint texture = a.getTextures().at(a.getHeight());
	OpenGLSamplerRepeat::setSampler(texture, a.getSampler());
	OpenGLFunctional::getInstance()->glBindTextureUnit(0, texture);
	for (auto&& t: a.getBuffersGPU())
	{
		TextBufferBOpenGL text = t.second;
		OpenGLFunctional::getInstance()->glBindVertexArray(text.glVao);
		// update color
		OpenGLFunctional::getInstance()->glUniform4f(a.getTextColorLocation(),
			text.textColor[gmtypes::cr],
			text.textColor[gmtypes::cg],
			text.textColor[gmtypes::cb],
			text.textColor[gmtypes::ca]);
		// draw
		OpenGLFunctional::getInstance()->glUniformMatrix4fv(a.getWorldLocation(), 1, GL_FALSE, text.position.toArray().data());
		glDrawArrays(GL_TRIANGLE_STRIP, 0, text.count);
		OpenGLFunctional::getInstance()->glBindVertexArray(0);
	}
}

