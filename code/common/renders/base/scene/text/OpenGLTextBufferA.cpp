#include "OpenGLTextBufferA.h"

#include "ITextBufferVisitor.h"

void OpenGLTextBufferA::init(const std::wstring& vs, const std::wstring& ps, const std::wstring cfg, const std::wstring& sampler, VisitorTextBufferCreateAndDrawSmartPtr& visitor)
{
	this->visitor = visitor;
	this->visitor->create(vs, ps, cfg, sampler, *this);
}

void OpenGLTextBufferA::mapVertexBuffer()
{
	this->visitor->mapVertexBuffer(*this);
}

void OpenGLTextBufferA::unmapVertexBuffer()
{
	this->visitor->unmapVertexBuffer(*this);
}

int OpenGLTextBufferA::getHeightText(const std::wstring& key)
{
	return this->visitor->getHeightText(key, *this);
}

void OpenGLTextBufferA::addText(const std::wstring& key, const std::wstring& text, const gmath::Matrix4& pos, const gmath::Color& color, int h)
{
	this->visitor->addText(key, text, pos, color, h, *this);
}

void OpenGLTextBufferA::replaceTextForEnter(const std::wstring& key, int ind_str, int ind_start, const std::wstring& new_string)
{
	this->visitor->replaceTextForEnter(key, ind_str, ind_start, new_string, *this);
}

void OpenGLTextBufferA::updatePosition(const std::wstring& key, const gmath::Matrix4& pos)
{
	this->visitor->updatePosition(key, pos, *this);
}

void OpenGLTextBufferA::updateColor(const std::wstring& key, const gmath::Color& c)
{
	this->visitor->updateColor(key, c, *this);
}

void OpenGLTextBufferA::reinit()
{
	this->visitor->reinit(*this);
}

void OpenGLTextBufferA::draw()
{
	this->visitor->draw(*this);
}

std::wstring& OpenGLTextBufferA::getSampler()
{
	return sampler;
}

std::map<int, int>& OpenGLTextBufferA::getTextures()
{
	return textures;
}

GLuint& OpenGLTextBufferA::getVertexBuffer()
{
	return glVertexBuffer;
}

GLuint& OpenGLTextBufferA::getVao()
{
	return glVao;
}

GLuint& OpenGLTextBufferA::getShaderProgram()
{
	return shaderProgram;
}

GLint& OpenGLTextBufferA::getWorldLocation()
{
	return worldLocation;
}

GLint& OpenGLTextBufferA::getTextColorLocation()
{
	return textColorLocation;
}