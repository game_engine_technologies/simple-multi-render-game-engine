#pragma once

enum TypeTextBufferB : unsigned short
{
	DYNAMIC_TEXT_BUFFER = 0,
	STATIC_TEXT_BUFFER
};