#pragma once

class Direct3D11TextBufferA;
class OpenGLTextBufferA;
class Direct3D11TextBufferB;
class OpenGLTextBufferB;

#include "../../shaders/OpenGLShadersBuffer.h"
#include "../../shaders/Direct3D11ShadersBuffer.h"
#include "../../textures/Direct3D11TexturesBuffer.h"
#include "../../../abstract/scene/camera/ICamera.h"
#include "../../textures/OpenGLTexturesBuffer.h"

#include "TypeTextBuffer.h"

class ITextBufferVisitorCreator
{
public:
	virtual void create(const std::wstring& vs, const std::wstring& ps,
		const std::wstring cfg, const std::wstring& sampler, Direct3D11TextBufferA& a) = 0;
	virtual void create(const std::wstring& vs, const std::wstring& ps,
		const std::wstring cfg, const std::wstring& sampler, OpenGLTextBufferA& a) = 0;

	virtual int getHeightText(const std::wstring& key, Direct3D11TextBufferA& a) = 0;
	virtual int getHeightText(const std::wstring& key, OpenGLTextBufferA& a) = 0;

	virtual void addText(const std::wstring& key, const std::wstring& text, const gmath::Matrix4& pos, const gmath::Color& color, int h, Direct3D11TextBufferA& a) = 0;
	virtual void addText(const std::wstring& key, const std::wstring& text, const gmath::Matrix4& pos, const gmath::Color& color, int h, OpenGLTextBufferA& a) = 0;

	virtual void replaceTextForEnter(const std::wstring& key, int ind_str, int ind_start, const std::wstring& new_string, Direct3D11TextBufferA& a) = 0;
	virtual void replaceTextForEnter(const std::wstring& key, int ind_str, int ind_start, const std::wstring& new_string, OpenGLTextBufferA& a) = 0;

	virtual void updatePosition(const std::wstring& key, const gmath::Matrix4& pos, Direct3D11TextBufferA& a) = 0;
	virtual void updatePosition(const std::wstring& key, const gmath::Matrix4& pos, OpenGLTextBufferA& a) = 0;

	virtual void updateColor(const std::wstring& key, const gmath::Color& c, Direct3D11TextBufferA& a) =0;
	virtual void updateColor(const std::wstring& key, const gmath::Color& c, OpenGLTextBufferA& a) = 0;

	virtual void reinit(Direct3D11TextBufferA& a) = 0;
	virtual void reinit(OpenGLTextBufferA& a) = 0;

	virtual void mapVertexBuffer(Direct3D11TextBufferA& a) = 0;
	virtual void mapVertexBuffer(OpenGLTextBufferA& a) = 0;

	virtual void unmapVertexBuffer(Direct3D11TextBufferA& a) = 0;
	virtual void unmapVertexBuffer(OpenGLTextBufferA& a) = 0;

	virtual void create(const std::wstring& vs, const std::wstring& ps,
		const std::wstring cfg, const std::wstring& sampler, int h, Direct3D11TextBufferB& a) = 0;
	virtual void create(const std::wstring& vs, const std::wstring& ps,
		const std::wstring cfg, const std::wstring& sampler, int h, OpenGLTextBufferB& a) = 0;

	virtual void addText(const std::wstring& key, const std::wstring& text, const gmath::Matrix4& pos, const gmath::Color& color, const TypeTextBufferB& type, Direct3D11TextBufferB& a) = 0;
	virtual void addText(const std::wstring& key, const std::wstring& text, const gmath::Matrix4& pos, const gmath::Color& color, const TypeTextBufferB& type, OpenGLTextBufferB& a) = 0;

	virtual void replaceText(const std::wstring& key, const std::wstring& new_string, Direct3D11TextBufferB& a) = 0;
	virtual void replaceText(const std::wstring& key, const std::wstring& new_string, OpenGLTextBufferB& a) = 0;
	
	virtual void updatePosition(const std::wstring& key, const gmath::Matrix4& pos, Direct3D11TextBufferB& a) = 0;
	virtual void updatePosition(const std::wstring& key, const gmath::Matrix4& pos, OpenGLTextBufferB& a) = 0;

	virtual void updateColor(const std::wstring& key, const gmath::Color& c, Direct3D11TextBufferB& a) = 0;
	virtual void updateColor(const std::wstring& key, const gmath::Color& c, OpenGLTextBufferB& a) = 0;

	virtual void reinit(Direct3D11TextBufferB& a) = 0;
	virtual void reinit(OpenGLTextBufferB& a) = 0;

};

class ITextBufferVisitorDrawer
{
public:
	virtual void draw(Direct3D11TextBufferA& a) = 0;
	virtual void draw(OpenGLTextBufferA& a) = 0;

	virtual void draw(Direct3D11TextBufferB& a) = 0;
	virtual void draw(OpenGLTextBufferB& a) = 0;
};

class VisitorTextBufferCreateAndDraw : public ITextBufferVisitorCreator, public ITextBufferVisitorDrawer
{
	Direct3D11ShadersBufferSmartPtr shadersBufferD3d11;
	OpenGLShadersBufferSmartPtr shadersBufferOpenGl;
	ComPtr<ID3D11Device> d3dDevice;
	ComPtr<ID3D11DeviceContext> d3dImmediateContext;
	Direct3D11TexturesBufferSmartPtr texturesBufferD3d11;
	OpenGLTexturesBufferSmartPtr texturesBufferOpenGl;
	CameraSmartPtr camera;

public:
	VisitorTextBufferCreateAndDraw(const ComPtr<ID3D11Device>& d3dDevice, const ComPtr<ID3D11DeviceContext>& d3dImmediateContext,
		const Direct3D11ShadersBufferSmartPtr& shadersBufferD3d11, const Direct3D11TexturesBufferSmartPtr& texturesBufferD3d11,
		const CameraSmartPtr& camera);
	VisitorTextBufferCreateAndDraw(const OpenGLShadersBufferSmartPtr& shadersBufferOpenGl,
		const OpenGLTexturesBufferSmartPtr& texturesBufferOpenGl,
		const CameraSmartPtr& camera);

	void create(const std::wstring& vs, const std::wstring& ps,
		const std::wstring cfg, const std::wstring& sampler, Direct3D11TextBufferA& a)  override;
	void create(const std::wstring& vs, const std::wstring& ps,
		const std::wstring cfg, const std::wstring& sampler, OpenGLTextBufferA& a)  override;

	int getHeightText(const std::wstring& key, Direct3D11TextBufferA& a) override;
	int getHeightText(const std::wstring& key, OpenGLTextBufferA& a) override;

	void addText(const std::wstring& key, const std::wstring& text, const gmath::Matrix4& pos, const gmath::Color& color, int h, Direct3D11TextBufferA& a)  override;
	void addText(const std::wstring& key, const std::wstring& text, const gmath::Matrix4& pos, const gmath::Color& color, int h, OpenGLTextBufferA& a)  override;

	void replaceTextForEnter(const std::wstring& key, int ind_str, int ind_start, const std::wstring& new_string, Direct3D11TextBufferA& a) override;
	void replaceTextForEnter(const std::wstring& key, int ind_str, int ind_start, const std::wstring& new_string, OpenGLTextBufferA& a) override;

	void updatePosition(const std::wstring& key, const gmath::Matrix4& pos, Direct3D11TextBufferA& a)  override;
	void updatePosition(const std::wstring& key, const gmath::Matrix4& pos, OpenGLTextBufferA& a)  override;

	void updateColor(const std::wstring& key, const gmath::Color& c, Direct3D11TextBufferA& a)override;
	void updateColor(const std::wstring& key, const gmath::Color& c, OpenGLTextBufferA& a) override;

	void reinit(Direct3D11TextBufferA& a)  override;
	void reinit(OpenGLTextBufferA& a)  override;

	void draw(Direct3D11TextBufferA& a)  override;
	void draw(OpenGLTextBufferA& a)  override;

	void mapVertexBuffer(Direct3D11TextBufferA& a) override;
	void mapVertexBuffer(OpenGLTextBufferA& a) override;

	void unmapVertexBuffer(Direct3D11TextBufferA& a) override;
	void unmapVertexBuffer(OpenGLTextBufferA& a) override;


	void create(const std::wstring& vs, const std::wstring& ps,
		const std::wstring cfg, const std::wstring& sampler, int h, Direct3D11TextBufferB& a) override;
	void create(const std::wstring& vs, const std::wstring& ps,
		const std::wstring cfg, const std::wstring& sampler, int h, OpenGLTextBufferB& a) override;

	void addText(const std::wstring& key, const std::wstring& text, const gmath::Matrix4& pos, const gmath::Color& color, const TypeTextBufferB& type, Direct3D11TextBufferB& a) override;
	void addText(const std::wstring& key, const std::wstring& text, const gmath::Matrix4& pos, const gmath::Color& color, const TypeTextBufferB& type, OpenGLTextBufferB& a) override;

	void replaceText(const std::wstring& key, const std::wstring& new_string, Direct3D11TextBufferB& a) override;
	void replaceText(const std::wstring& key, const std::wstring& new_string, OpenGLTextBufferB& a) override;

	void updatePosition(const std::wstring& key, const gmath::Matrix4& pos, Direct3D11TextBufferB& a) override;
	void updatePosition(const std::wstring& key, const gmath::Matrix4& pos, OpenGLTextBufferB& a) override;

	void updateColor(const std::wstring& key, const gmath::Color& c, Direct3D11TextBufferB& a) override;
	void updateColor(const std::wstring& key, const gmath::Color& c, OpenGLTextBufferB& a) override;

	void reinit(Direct3D11TextBufferB& a) override;
	void reinit(OpenGLTextBufferB& a) override;

	void draw(Direct3D11TextBufferB& a) override;
	void draw(OpenGLTextBufferB& a) override;
};
