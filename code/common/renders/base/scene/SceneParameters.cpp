#include "SceneParameters.h"

int SceneParameters::getClientWidth()
{
	return w;
}

int SceneParameters::getClientHeight()
{
	return h;
}

std::wstring& SceneParameters::getVersionAPI()
{
	return versionAPI;
}


void SceneParameters::setClientWidth(int w)
{
	this->w = w;
}

void SceneParameters::setClientHeight(int h)
{
	this->h = h;
}

void SceneParameters::setVersionAPI(const std::wstring& v)
{
	versionAPI = v;
}
