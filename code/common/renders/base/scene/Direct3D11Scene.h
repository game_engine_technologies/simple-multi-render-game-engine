#pragma once

#include "../../abstract/scene/IScene.h"
#include "../../abstract/IDisposable.h"

#include "../shaders/Direct3D11ShadersBuffer.h"
#include "../../../utils/base/ExceptionMemory.h"

#include <memory>

#include "../textures/Direct3D11TexturesBuffer.h"

class Direct3D11Scene : public IScene, public IDisposable
{
protected:
	Direct3D11ShadersBufferSmartPtr shadersBuffer;
	Direct3D11TexturesBufferSmartPtr texturesBuffer;
	ComPtr<ID3D11Device> d3dDevice;
	ComPtr<ID3D11DeviceContext> d3dImmediateContext;

public:
	Direct3D11Scene(const ComPtr<ID3D11Device>& d3dDevice, const ComPtr<ID3D11DeviceContext>& d3dImmediateContext);
	virtual void init(const std::wstring& path, SceneParameters rp) override;
	virtual bool update(SceneDynamicParameters& sdp) override;

	virtual ~Direct3D11Scene()override;
	virtual void clear();

	virtual void render(const ComPtr<ID3D11DeviceContext>& d3dImmediateContext, const ComPtr<ID3D11RenderTargetView>& d3dRenderTargetView,
		const ComPtr<ID3D11DepthStencilView>& d3dDepthStencilView, const ComPtr<ID3D11DepthStencilState>& d3dDepthStencilEnableDepth,
		const ComPtr<ID3D11DepthStencilState>& d3dDepthStencilDisableDepth) = 0;
};

using Direct3D11SceneSmartPtr = std::shared_ptr<Direct3D11Scene>;