#include "OpenGLDirectionalStaticLight.h"

#include "IStaticLightVisitor.h"

void OpenGLDirectionalStaticLight::init(const StaticLightVisitorSmartPtr& visitor)
{
	this->visitor = visitor;
	this->visitor->create(*this);
}

void OpenGLDirectionalStaticLight::update()
{
	this->visitor->update(*this);
}

void OpenGLDirectionalStaticLight::sendGPU(int addData)
{
	this->visitor->sendGPU(*this, addData);
}

GLint& OpenGLDirectionalStaticLight::getMaterialLocation()
{
	return materialLocation;
}

GLint OpenGLDirectionalStaticLight::getMaterialLocation() const
{
	return materialLocation;
}

void OpenGLDirectionalStaticLight::setMaterialLocation(GLint ml)
{
	materialLocation = ml;
}
