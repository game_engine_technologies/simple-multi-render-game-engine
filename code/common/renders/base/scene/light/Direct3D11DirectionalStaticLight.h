#pragma once

#include "../../../abstract/scene/light/IStaticLight.h"

#include "../../d3d11_header.h"

class Direct3D11DirectionalStaticLight: public IStaticLight
{
	ComPtr<ID3D11Buffer> constantBuffer;

	StaticLightVisitorSmartPtr visitor;
public:
	void init(const StaticLightVisitorSmartPtr& visitor) override;
	void update() override;
	void sendGPU(int addData) override;

	ComPtr<ID3D11Buffer>& getConstantBuffer();
};

using Direct3D11DirectionalStaticLightSmartPtr = std::shared_ptr<Direct3D11DirectionalStaticLight>;
