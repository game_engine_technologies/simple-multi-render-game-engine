#pragma once

#include "../../../abstract/scene/light/IStaticLight.h"

class Sun 
{
	StaticLightSmartPtr directionalLight;

	gmath::Vector center; // ������ ������ � 0,0,0

	gmath::Vector currentPosition;
	gmath::Vector startPosition;

	float currentAngle;
	float stepAngle;

public:
	void init(const StaticLightSmartPtr& directionalLight, const gmath::Vector& startPos, float stepAgl);
	void updateDirectional();

	StaticLightSmartPtr& getDirectionalLight();
};

using SunSmartPtr = std::shared_ptr<Sun>;