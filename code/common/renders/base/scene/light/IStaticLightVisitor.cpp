#include "IStaticLightVisitor.h"

#include "Direct3D11DirectionalStaticLight.h"
#include "OpenGLDirectionalStaticLight.h"

StaticLightVisitor::StaticLightVisitor(const ComPtr<ID3D11Device>& d3dDevice, const ComPtr<ID3D11DeviceContext>& d3dImmediateContext):
	d3dDevice(d3dDevice), d3dImmediateContext(d3dImmediateContext)
{
}

void StaticLightVisitor::create(Direct3D11DirectionalStaticLight& a)
{
	// ������� ����������� ����� ��� ������
	a.getConstantBuffer() = Direct3D11CreatorBuffer::createBuffer(d3dDevice, static_cast<UINT>(sizeof(ConstantBufferStaticLight)),
		D3D11_BIND_CONSTANT_BUFFER, D3D11_USAGE_DEFAULT, 0);
}

void StaticLightVisitor::create(OpenGLDirectionalStaticLight& a)
{
	
}

void StaticLightVisitor::update(Direct3D11DirectionalStaticLight& a)
{
	if (a.isUpdate())
	{
		ConstantBufferStaticLight cbsl;
		cbsl.camPos = gmath::Point4(a.getDirectionalLight().camPos[gmtypes::vx],
			a.getDirectionalLight().camPos[gmtypes::vy],
			a.getDirectionalLight().camPos[gmtypes::vz],
			-1);
		cbsl.directionalLightPosition = gmath::Point4(a.getDirectionalLight().directional[gmtypes::vx],
			a.getDirectionalLight().directional[gmtypes::vy],
			a.getDirectionalLight().directional[gmtypes::vz],
			-1);
		//cbsl.directionalLightPosition = a.getDirectionalLight().position;
		cbsl.directionalLightColor = a.getDirectionalLight().color;

		d3dImmediateContext->UpdateSubresource(a.getConstantBuffer().Get(), 0, NULL, &cbsl, 0, 0);
		a.setUpdate(false);
	}
}

void StaticLightVisitor::update(OpenGLDirectionalStaticLight& a)
{
	
}

void StaticLightVisitor::sendGPU(Direct3D11DirectionalStaticLight& a, int additionalData)
{
	d3dImmediateContext->PSSetConstantBuffers(0, 1, a.getConstantBuffer().GetAddressOf());
}

void StaticLightVisitor::sendGPU(OpenGLDirectionalStaticLight& a, int shaderProgramGL)
{
	// ������� ����������� �����
	OpenGLFunctional::getInstance()->glUniform4f(OpenGLFunctional::getInstance()->glGetUniformLocation(shaderProgramGL, "light.camPos"),
		a.getDirectionalLight().camPos[gmtypes::vx],
		a.getDirectionalLight().camPos[gmtypes::vy],
		a.getDirectionalLight().camPos[gmtypes::vz],
		-1);
	OpenGLFunctional::getInstance()->glUniform4f(OpenGLFunctional::getInstance()->glGetUniformLocation(shaderProgramGL, "light.directionalLightDirectional"),
		a.getDirectionalLight().directional[gmtypes::vx],
		a.getDirectionalLight().directional[gmtypes::vy],
		a.getDirectionalLight().directional[gmtypes::vz],
		-1);
	OpenGLFunctional::getInstance()->glUniform4f(OpenGLFunctional::getInstance()->glGetUniformLocation(shaderProgramGL, "light.directionalLightColor"),
		a.getDirectionalLight().color[gmtypes::vx],
		a.getDirectionalLight().color[gmtypes::vy],
		a.getDirectionalLight().color[gmtypes::vz],
		a.getDirectionalLight().color[gmtypes::vw]);
}
