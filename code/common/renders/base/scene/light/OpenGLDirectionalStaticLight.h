#pragma once

#include "../../../abstract/scene/light/IStaticLight.h"

#include "../../gl_header.h"

class OpenGLDirectionalStaticLight: public IStaticLight
{
	GLint materialLocation;

	StaticLightVisitorSmartPtr visitor;
public:
	void init(const StaticLightVisitorSmartPtr& visitor) override;
	void update() override;
	void sendGPU(int addData) override;

	GLint& getMaterialLocation();

	GLint getMaterialLocation()const;

	void setMaterialLocation(GLint ml);

};

using OpenGLDirectionalStaticLightSmartPtr = std::shared_ptr<OpenGLDirectionalStaticLight>;
