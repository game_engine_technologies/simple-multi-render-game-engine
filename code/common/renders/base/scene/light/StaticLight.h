#pragma once

#include "../../math_header.h"

struct alignas(64) ConstantBufferStaticLight
{
	// static light
	gmath::Point4 camPos;
	gmath::Point4 directionalLightPosition;
	gmath::Color directionalLightColor;

};

struct DirectionalLight
{
	gmath::Point camPos;
	gmath::Point directional;
	gmath::Color color;
};