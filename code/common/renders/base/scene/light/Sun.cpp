#include "Sun.h"

#include <Windows.h>

void Sun::init(const StaticLightSmartPtr& directionalLight, const gmath::Vector& startPos, float stepAgl)
{
	stepAngle = stepAgl;
	currentAngle = 0;
	center = { 0,0,0,0 };
	this->directionalLight = directionalLight;
	startPosition = currentPosition = startPos;
}

void Sun::updateDirectional()
{
	currentAngle += stepAngle;
	if (currentAngle > 180)
		currentAngle = 0;
	OutputDebugString((std::to_wstring(currentAngle) + L"\n").c_str());
	gmath::Matrix rotate = gmath::Matrix::CreateRotateMatrixZ(gmath::GeneralMath::degreesToRadians(-currentAngle));
	currentPosition = startPosition * rotate;
	directionalLight->getDirectionalLight().directional = (gmath::Point)gmath::Vector::createVectorForDoublePoints(currentPosition, center);
}

StaticLightSmartPtr& Sun::getDirectionalLight()
{
	return directionalLight;
}
