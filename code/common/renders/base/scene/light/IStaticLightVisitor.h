#pragma once

#include "../../BufferCreator.h"
#include "../../gl_header.h"

#include "../../math_header.h"
#include "../../shaders/OpenGLShadersBuffer.h"

class Direct3D11DirectionalStaticLight;
class OpenGLDirectionalStaticLight;

class IStaticLightCreate
{
public:
	virtual void create(Direct3D11DirectionalStaticLight& a) = 0;
	virtual void create(OpenGLDirectionalStaticLight& a) = 0;
	virtual ~IStaticLightCreate() = default;
};

class IStaticLightUpdate
{
public:
	virtual void update(Direct3D11DirectionalStaticLight& a) = 0;
	virtual void update(OpenGLDirectionalStaticLight& a) = 0;
	virtual ~IStaticLightUpdate() = default;
};


class IStaticLightSendGPU
{
public:
	virtual void sendGPU(Direct3D11DirectionalStaticLight& a, int additionalData) = 0;
	virtual void sendGPU(OpenGLDirectionalStaticLight& a, int additionalData) = 0;
	virtual ~IStaticLightSendGPU() = default;
};

class StaticLightVisitor : public IStaticLightCreate, public IStaticLightUpdate, public IStaticLightSendGPU
{
	ComPtr<ID3D11Device> d3dDevice;
	ComPtr<ID3D11DeviceContext> d3dImmediateContext;

public:
	StaticLightVisitor() = default;
	StaticLightVisitor(const ComPtr<ID3D11Device>& d3dDevice, const ComPtr<ID3D11DeviceContext>& d3dImmediateContext);

	void create(Direct3D11DirectionalStaticLight& a) override;
	void create(OpenGLDirectionalStaticLight& a) override;

	void update(Direct3D11DirectionalStaticLight& a) override;
	void update(OpenGLDirectionalStaticLight& a) override;

	void sendGPU(Direct3D11DirectionalStaticLight& a, int additionalData) override;
	void sendGPU(OpenGLDirectionalStaticLight& a, int shaderProgramGL) override;

	~StaticLightVisitor()override {};
};