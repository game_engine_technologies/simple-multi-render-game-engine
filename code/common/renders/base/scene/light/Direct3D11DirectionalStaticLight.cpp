#include "Direct3D11DirectionalStaticLight.h"

#include "IStaticLightVisitor.h"

void Direct3D11DirectionalStaticLight::init(const StaticLightVisitorSmartPtr& visitor)
{
	this->visitor = visitor;
	this->visitor->create(*this);
}

void Direct3D11DirectionalStaticLight::update()
{
	this->visitor->update(*this);
}

void Direct3D11DirectionalStaticLight::sendGPU(int addData)
{
	this->visitor->sendGPU(*this, addData);
}

ComPtr<ID3D11Buffer>& Direct3D11DirectionalStaticLight::getConstantBuffer()
{
	return constantBuffer;
}