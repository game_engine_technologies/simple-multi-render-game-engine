#include <string>

#include "../../../utils/base/TypeCamera.h"

class SceneParameters 
{
	int w;
	int h;
	std::wstring versionAPI;

public:
	int getClientWidth();
	int getClientHeight();
	std::wstring& getVersionAPI();

	void setClientWidth(int w);
	void setClientHeight(int h);
	void setVersionAPI(const std::wstring& v);
};