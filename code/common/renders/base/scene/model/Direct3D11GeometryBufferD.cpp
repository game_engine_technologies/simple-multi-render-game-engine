#include "Direct3D11GeometryBufferD.h"

#include "IGeometryBufferVisitor.h"

void Direct3D11GeometryBufferD::init(const std::wstring& vs, const std::wstring& ps, VisitorGeometryBufferCreateAndDrawSmartPtr& visitor)
{
	//newSizeConstBuffWorld = 0;
	clearFlags();
	this->visitor = visitor;
	this->visitor->create(vs, ps, *this);
}

void Direct3D11GeometryBufferD::reinit()
{
	visitor->reinit(*this);
}

void Direct3D11GeometryBufferD::draw()
{
	visitor->draw(*this);
}


ComPtr<ID3D11Buffer>& Direct3D11GeometryBufferD::getVertexBuffer()
{
	return d3dVertexBuffer;
}

ComPtr<ID3D11Buffer>& Direct3D11GeometryBufferD::getIndexBuffer()
{
	return d3dIndexBuffer;
}

ComPtr<ID3D11InputLayout>& Direct3D11GeometryBufferD::getInputLayout()
{
	return d3dInputLayout;
}

ComPtr<ID3D11VertexShader>& Direct3D11GeometryBufferD::getVertexShader()
{
	return d3dVertexShader;
}

ComPtr<ID3D11PixelShader>& Direct3D11GeometryBufferD::getPixelShader()
{
	return d3dPixelShader;
}

bool Direct3D11GeometryBufferD::addModel(const ModelParameters& mp)
{
	ModelD model;
	if (mp.type == TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_D)
	{
		if (mp.key == L"")
		{
			debug::printError(L"Error: key is empty");
			return false;
		}
		auto iter = std::find_if(models.begin(), models.end(), [&mp](const ModelD& m)
			{
				return mp.key == m.getKey();
			});
		if (iter == models.end()) // ������ ��� - �������� �������
		{
			if (mp.verticesStaticLight.size() == 0 || mp.indeces.size() == 0)
			{
				debug::printError(L"Error: vertex buffer or index buffer is empty");
				return false;
			}
			int size(this->vertices.size());
			this->vertices.insert(this->vertices.end(), mp.verticesStaticLight.begin(), mp.verticesStaticLight.end());
			model.setOffsetIndex(this->indeces.size());
			std::transform(mp.indeces.begin(), mp.indeces.end(), std::back_inserter(this->indeces), [&size](Index indeces)
				{
					return indeces + size;
				});
			model.setCountIndex(mp.indeces.size());
			model.setKey(mp.key);
			model.setStartIndex(indeces.size() - mp.indeces.size());
			//model.setWorld(mp.world);
			_updateAll = true;
		}
		else // ������ ���� - ��������� � ������ �����
		{
			// ��� ����� ������������ ������� ��������
			model.setCountIndex(iter->getCountIndex());
			model.setKey(iter->getKey());
			model.setOffsetIndex(iter->getOffsetIndex());
			model.setStartIndex(iter->getStartIndex());
			//model.setWorld(mp.world);
		}
		model.setWorld(mp.world);
		model.setNormals(mp.normals);
		model.setMaterialKey(mp.materialKey);
		model.setTextureKey(mp.textureKey);
		model.setSamplerKey(mp.samplerKey);
		models.push_back(model);
		return true;
	}
	debug::printError(L"Error type model for Direct3D11GeometryBufferD");
	return false;
}

std::vector<ModelD>& Direct3D11GeometryBufferD::getModels()
{
	return models;
}

//int& Direct3D11GeometryBufferD::getNewSizeConstBuffWorld()
//{
//	return newSizeConstBuffWorld;
//}

ComPtr<ID3D11Buffer>& Direct3D11GeometryBufferD::getConstantBufferWorld()
{
	return constantsBufferWorld;
}

ComPtr<ID3D11Buffer>& Direct3D11GeometryBufferD::getConstantsBufferMaterial()
{
	return constantsBufferMaterial;
}

//std::vector<ComPtr<ID3D11Buffer>>& Direct3D11GeometryBufferD::getConstantsBufferWorld()
//{
//	return constantsBufferWorld;
//}

std::vector<VertexStaticLight>& Direct3D11GeometryBufferD::getVertices()
{
	return vertices;
}