#pragma once

#include "../../math_header.h"

struct VertexStaticLight
{
	gmath::Point3 position;
	gmath::Point3 normal;
	gmath::Point4 color; // x, y, -1, -1 -> texcoord; r, g, b, a -> color
};