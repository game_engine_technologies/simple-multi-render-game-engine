#pragma once

#include "../../../abstract/scene/model/IGeometryBuffer.h"
#include "ModelB.h"
#include "../../gl_header.h"

#include "../../../abstract/IDisposable.h"

class OpenGLGeometryBufferB : public IGeometryBuffer, public IDisposable
{
	VisitorGeometryBufferCreateAndDrawSmartPtr visitor;

	GLuint glVertexBuffer;
	GLuint glIndexBuffer;
	GLuint glVao;
	GLuint shaderProgram;
	std::vector<ModelB> models;
	GLint worldLocation;

	std::vector<VertexColor> vertices;
public:
	void init(const std::wstring& vs, const std::wstring& ps, VisitorGeometryBufferCreateAndDrawSmartPtr& visitor) override;
	void reinit() override;
	void draw() override;
	bool addModel(const ModelParameters& mp)override;
	void clear() override;

	GLuint& getVertexBuffer();
	GLuint& getIndexBuffer();
	GLuint& getVao();
	GLuint& getShaderProgram();
	GLuint getVertexBuffer()const;
	GLuint getIndexBuffer()const;
	GLuint getVao()const;
	GLuint getShaderProgram()const;
	void setVertexBuffer(GLuint verticesColor);
	void setIndexBuffer(GLuint verticesColor);
	void setVao(GLuint verticesColor);
	void setShaderProgram(GLuint verticesColor);

	std::vector<ModelB>& getModels();
	GLint& getWorldLocation();

	~OpenGLGeometryBufferB();

	std::vector<VertexColor>& getVertices();
};

class DeleterOpenGLGeometryBufferB
{
public:
	void operator()(OpenGLGeometryBufferB* gBuff);
};


using OpenGLGeometryBufferBSmartPtr = std::shared_ptr<OpenGLGeometryBufferB>;

// ������� ������:
// init

// �������� ->
// addModel (������� ������)
// reinit
