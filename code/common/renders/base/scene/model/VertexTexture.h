#pragma once

#include "../../math_header.h"

struct VertexTexture
{
	gmath::Point3 position; 
	gmath::UVCoordinate texCoord;
};