#include "OpenGLGeometryBufferA.h"

#include "IGeometryBufferVisitor.h"

void OpenGLGeometryBufferA::init(const std::wstring& vs, const std::wstring& ps, VisitorGeometryBufferCreateAndDrawSmartPtr& visitor)
{
	clearFlags();
	this->visitor = visitor;
	this->visitor->create(vs, ps, *this);
}

void OpenGLGeometryBufferA::reinit()
{
	visitor->reinit(*this);
}

void OpenGLGeometryBufferA::draw()
{
	visitor->draw(*this);
}

bool OpenGLGeometryBufferA::addModel(const ModelParameters& mp)
{
	if (mp.type == TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_A)
	{
		if (mp.verticesColor.size() == 0 || mp.indeces.size() == 0)
		{
			debug::printError(L"Error: vertex buffer or index buffer is empty");
			return false;
		}
		this->vertices.insert(this->vertices.end(), mp.verticesColor.begin(), mp.verticesColor.end());
		int size(this->indeces.size());
		if (this->indeces.size() != 0)
			std::transform(mp.indeces.begin(), mp.indeces.end(), std::back_inserter(this->indeces), [&size](Index indeces)
				{
					return indeces + size;
				});
		else
			this->indeces.insert(this->indeces.end(), mp.indeces.begin(), mp.indeces.end());
		_updateAll = true;
		return true;
	}
	debug::printError(L"Error type model for OpenGLGeometryBufferA");
	return false;
}

void OpenGLGeometryBufferA::clear()
{
	OpenGLFunctional::getInstance()->glDeleteVertexArrays(1, &glVao);
	OpenGLFunctional::getInstance()->glDeleteBuffers(1, &glVertexBuffer);
	OpenGLFunctional::getInstance()->glDeleteBuffers(1, &glIndexBuffer);
}

GLuint& OpenGLGeometryBufferA::getVertexBuffer()
{
	return glVertexBuffer;
}

GLuint& OpenGLGeometryBufferA::getIndexBuffer()
{
	return glIndexBuffer;
}

GLuint& OpenGLGeometryBufferA::getVao()
{
	return glVao;
}

GLuint& OpenGLGeometryBufferA::getShaderProgram()
{
	return shaderProgram;
}

GLuint OpenGLGeometryBufferA::getVertexBuffer()const
{
	return glVertexBuffer;
}

GLuint OpenGLGeometryBufferA::getIndexBuffer()const
{
	return glIndexBuffer;
}

GLuint OpenGLGeometryBufferA::getVao()const
{
	return glVao;
}

GLuint OpenGLGeometryBufferA::getShaderProgram()const
{
	return shaderProgram;
}

void OpenGLGeometryBufferA::setVertexBuffer(GLuint verticesColor)
{
	glVertexBuffer = verticesColor;
}

void OpenGLGeometryBufferA::setIndexBuffer(GLuint verticesColor)
{
	glIndexBuffer = verticesColor;
}

void OpenGLGeometryBufferA::setVao(GLuint verticesColor)
{
	glVao = verticesColor;
}

void OpenGLGeometryBufferA::setShaderProgram(GLuint verticesColor)
{
	shaderProgram = verticesColor;
}

OpenGLGeometryBufferA::~OpenGLGeometryBufferA()
{
	clear();
}

void DeleterOpenGLGeometryBufferA::operator()(OpenGLGeometryBufferA* gBuff)
{
	if (gBuff != nullptr)
	{
		OpenGLFunctional::getInstance()->glDeleteVertexArrays(1, &gBuff->getVao());
		OpenGLFunctional::getInstance()->glDeleteBuffers(1, &gBuff->getVertexBuffer());
		OpenGLFunctional::getInstance()->glDeleteBuffers(1, &gBuff->getIndexBuffer());
	}
}

std::vector<VertexColor>& OpenGLGeometryBufferA::getVertices()
{
	return vertices;
}
