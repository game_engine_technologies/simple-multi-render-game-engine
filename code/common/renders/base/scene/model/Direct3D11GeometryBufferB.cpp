#include "Direct3D11GeometryBufferB.h"

#include "IGeometryBufferVisitor.h"

void Direct3D11GeometryBufferB::init(const std::wstring& vs, const std::wstring& ps, VisitorGeometryBufferCreateAndDrawSmartPtr& visitor)
{
	//newSizeConstBuffWorld = 0;
	clearFlags();
	this->visitor = visitor;
	this->visitor->create(vs, ps, *this);
}

void Direct3D11GeometryBufferB::reinit()
{
	visitor->reinit(*this);
}

void Direct3D11GeometryBufferB::draw()
{
	visitor->draw(*this);
}


ComPtr<ID3D11Buffer>& Direct3D11GeometryBufferB::getVertexBuffer()
{
	return d3dVertexBuffer;
}

ComPtr<ID3D11Buffer>& Direct3D11GeometryBufferB::getIndexBuffer()
{
	return d3dIndexBuffer;
}

ComPtr<ID3D11InputLayout>& Direct3D11GeometryBufferB::getInputLayout()
{
	return d3dInputLayout;
}

ComPtr<ID3D11VertexShader>& Direct3D11GeometryBufferB::getVertexShader()
{
	return d3dVertexShader;
}

ComPtr<ID3D11PixelShader>& Direct3D11GeometryBufferB::getPixelShader()
{
	return d3dPixelShader;
}

bool Direct3D11GeometryBufferB::addModel(const ModelParameters& mp)
{
	ModelB model;
	if (mp.type == TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_B)
	{
		if (mp.key == L"")
		{
			debug::printError(L"Error: key is empty");
			return false;
		}
		auto iter = std::find_if(models.begin(), models.end(), [&mp](const ModelB& m)
			{
				return mp.key == m.getKey();
			});
		if (iter == models.end()) // ������ ��� - �������� �������
		{
			if (mp.verticesColor.size() == 0 || mp.indeces.size() == 0)
			{
				debug::printError(L"Error: vertex buffer or index buffer is empty");
				return false;
			}
			int size(this->vertices.size());
			this->vertices.insert(this->vertices.end(), mp.verticesColor.begin(), mp.verticesColor.end());
			model.setOffsetIndex(this->indeces.size());
			std::transform(mp.indeces.begin(), mp.indeces.end(), std::back_inserter(this->indeces), [&size](Index indeces)
				{
					return indeces + size;
				});
			model.setCountIndex(mp.indeces.size());
			model.setKey(mp.key);
			model.setStartIndex(indeces.size() - mp.indeces.size());
			model.setWorld(mp.world);
			models.push_back(model);
			_updateAll = true;
		}
		else // ������ ���� - ��������� � ������ �����
		{
			// ��� ����� ������������ ������� ��������
			model.setCountIndex(iter->getCountIndex());
			model.setKey(iter->getKey());
			model.setOffsetIndex(iter->getOffsetIndex());
			model.setStartIndex(iter->getStartIndex());
			model.setWorld(mp.world);
			models.push_back(model);
		}
		/*ComPtr<ID3D11Buffer> tmp;
		ConstantBufferWorld cbw;
		cbw.world = gmath::Matrix::Transponse(model.getWorld());
		d3dImmediateContext->UpdateSubresource(tmp.Get(), 0, NULL, &cbw, 0, 0);*/
		//newSizeConstBuffWorld++;
		return true;
	}
	debug::printError(L"Error type model for Direct3D11GeometryBufferB");
	return false;
}

std::vector<ModelB>& Direct3D11GeometryBufferB::getModels()
{
	return models;
}

//int& Direct3D11GeometryBufferB::getNewSizeConstBuffWorld()
//{
//	return newSizeConstBuffWorld;
//}

ComPtr<ID3D11Buffer>& Direct3D11GeometryBufferB::getConstantBufferWorld()
{
	return constantsBufferWorld;
}

//std::vector<ComPtr<ID3D11Buffer>>& Direct3D11GeometryBufferB::getConstantsBufferWorld()
//{
//	return constantsBufferWorld;
//}

std::vector<VertexColor>& Direct3D11GeometryBufferB::getVertices()
{
	return vertices;
}