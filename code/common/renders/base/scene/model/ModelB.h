#pragma once

#include <string>
#include <vector>
#include "../../math_header.h"

#include "Buffers.h"

class ModelB
{
	unsigned startIndex;
	unsigned offsetIndex;
	unsigned countIndex;
	std::wstring key;
	gmath::Matrix world;

public:
	unsigned getStartIndex() const;
	unsigned getOffsetIndex() const;
	unsigned getCountIndex()const;
	std::wstring getKey()const;
	gmath::Matrix getWorld()const;
	void setStartIndex(unsigned si);
	void setOffsetIndex(unsigned oi);
	void setCountIndex(unsigned ci);
	void setKey(const std::wstring& n);
	void setWorld(const gmath::Matrix& w);
};