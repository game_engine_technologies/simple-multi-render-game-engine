#include "IGeometryBufferVisitor.h"

#include "../../BufferCreator.h"

#include "Direct3D11GeometryBufferA.h"
#include "OpenGLGeometryBufferA.h"
#include "Direct3D11GeometryBufferB.h"
#include "OpenGLGeometryBufferB.h"
#include "Direct3D11GeometryBufferC.h"
#include "OpenGLGeometryBufferC.h"
#include "Direct3D11GeometryBufferD.h"
#include "OpenGLGeometryBufferD.h"
#include "Direct3D11GeometryBufferE.h"
#include "OpenGLGeometryBufferE.h"

VisitorGeometryBufferCreateAndDraw::VisitorGeometryBufferCreateAndDraw(const ComPtr<ID3D11Device>& d3dDevice,
	const ComPtr<ID3D11DeviceContext>& d3dImmediateContext,
	const Direct3D11ShadersBufferSmartPtr& shadersBufferD3d11):d3dDevice(d3dDevice), d3dImmediateContext(d3dImmediateContext),
	shadersBufferD3d11(shadersBufferD3d11)
{
}

VisitorGeometryBufferCreateAndDraw::VisitorGeometryBufferCreateAndDraw(const OpenGLShadersBufferSmartPtr& shadersBufferOpenGl):
	shadersBufferOpenGl(shadersBufferOpenGl)
{
}

VisitorGeometryBufferCreateAndDraw::VisitorGeometryBufferCreateAndDraw(const ComPtr<ID3D11Device>& d3dDevice, 
	const ComPtr<ID3D11DeviceContext>& d3dImmediateContext, 
	const Direct3D11ShadersBufferSmartPtr& shadersBufferD3d11, 
	const CameraSmartPtr& d3dCamera) : d3dDevice(d3dDevice), d3dImmediateContext(d3dImmediateContext),
	shadersBufferD3d11(shadersBufferD3d11), camera(d3dCamera)
{
}

VisitorGeometryBufferCreateAndDraw::VisitorGeometryBufferCreateAndDraw(const OpenGLShadersBufferSmartPtr& shadersBufferOpenGl, 
	const CameraSmartPtr& openGlCamera) : shadersBufferOpenGl(shadersBufferOpenGl), camera(openGlCamera)
{
}

VisitorGeometryBufferCreateAndDraw::VisitorGeometryBufferCreateAndDraw(const ComPtr<ID3D11Device>& d3dDevice, 
	const ComPtr<ID3D11DeviceContext>& d3dImmediateContext, 
	const Direct3D11ShadersBufferSmartPtr& shadersBufferD3d11, 
	const CameraSmartPtr& d3dCamera,
	const Direct3D11TexturesBufferSmartPtr& texturesBufferD3d11) : d3dDevice(d3dDevice), d3dImmediateContext(d3dImmediateContext),
	shadersBufferD3d11(shadersBufferD3d11), camera(d3dCamera), texturesBufferD3d11(texturesBufferD3d11)
{
}

VisitorGeometryBufferCreateAndDraw::VisitorGeometryBufferCreateAndDraw(const OpenGLShadersBufferSmartPtr& shadersBufferOpenGl,
	const CameraSmartPtr& openGlCamera,
	const OpenGLTexturesBufferSmartPtr& texturesBufferOpenGl) : shadersBufferOpenGl(shadersBufferOpenGl), camera(openGlCamera),
	texturesBufferOpenGl(texturesBufferOpenGl)
{
}

VisitorGeometryBufferCreateAndDraw::VisitorGeometryBufferCreateAndDraw(const ComPtr<ID3D11Device>& d3dDevice,
	const ComPtr<ID3D11DeviceContext>& d3dImmediateContext, 
	const Direct3D11ShadersBufferSmartPtr& shadersBufferD3d11, 
	const CameraSmartPtr& d3dCamera, 
	const Direct3D11TexturesBufferSmartPtr& texturesBufferD3d11,
	const StaticLightSmartPtr& staticLight,
	const MaterialsBufferSmartPtr& materialsBuffer) : d3dDevice(d3dDevice), d3dImmediateContext(d3dImmediateContext),
	shadersBufferD3d11(shadersBufferD3d11), camera(d3dCamera), texturesBufferD3d11(texturesBufferD3d11),
	staticLight(staticLight), materialsBuffer(materialsBuffer)
{
}

VisitorGeometryBufferCreateAndDraw::VisitorGeometryBufferCreateAndDraw(const OpenGLShadersBufferSmartPtr& shadersBufferGL, 
	const CameraSmartPtr& camera,
	const OpenGLTexturesBufferSmartPtr& texturesBufferGL,
	const StaticLightSmartPtr& staticLight, 
	const MaterialsBufferSmartPtr& materialsBuffer):shadersBufferOpenGl(shadersBufferGL), 
	camera(camera),
	texturesBufferOpenGl(texturesBufferGL),
	staticLight(staticLight),
	materialsBuffer(materialsBuffer)
{
}

void VisitorGeometryBufferCreateAndDraw::create(const std::wstring& vs, const std::wstring& ps, Direct3D11GeometryBufferA& a)
{
	// ���������� �������
	a.getVertexShader() = shadersBufferD3d11->getVertexShader(vs);
	a.getPixelShader() = shadersBufferD3d11->getPixelShader(ps);
	// �������� ������� ������
	std::vector<D3D11_INPUT_ELEMENT_DESC> layout =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	auto vsBlob = shadersBufferD3d11->getVertexShaderBlob(vs);
	HRESULT hr = d3dDevice->CreateInputLayout(layout.data(), static_cast<UINT>(layout.size()), vsBlob->GetBufferPointer(),
		vsBlob->GetBufferSize(), a.getInputLayout().GetAddressOf());
	if (FAILED(hr))
	 throw ExceptionDirect3D11GeometryBuffer(L"error create input layout!");
}

void VisitorGeometryBufferCreateAndDraw::create(const std::wstring& vs, const std::wstring& ps, OpenGLGeometryBufferA& a)
{
	GLuint vao(-1);
	OpenGLFunctional::getInstance()->glGenVertexArrays(1, &vao);
	a.setVao(vao);

	GLuint _vs = shadersBufferOpenGl->getShader(vs);
	GLuint _ps = shadersBufferOpenGl->getShader(ps);
	std::vector<unsigned> key = { _vs, _ps };
	a.setShaderProgram(shadersBufferOpenGl->getShaderProgram(key)); 
}

void VisitorGeometryBufferCreateAndDraw::reinit(Direct3D11GeometryBufferA& a)
{
	HRESULT hr;
	D3D11_BUFFER_DESC bd;  // ���������, ����������� ����������� �����
	D3D11_SUBRESOURCE_DATA InitData; // ���������, ���������� ������ ������


	clears::ReleaseCom(a.getVertexBuffer());
	a.getVertexBuffer() = Direct3D11CreatorBuffer::createBuffer(d3dDevice, static_cast<UINT>(sizeof(VertexColor) * a.getVertices().size()),
		D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_DEFAULT, 0, a.getVertices().data());

	clears::ReleaseCom(a.getIndexBuffer());
	a.getIndexBuffer() = Direct3D11CreatorBuffer::createBuffer(d3dDevice, static_cast<UINT>(sizeof(Index) * a.getIndeces().size()),
		D3D11_BIND_INDEX_BUFFER, D3D11_USAGE_DEFAULT, 0, a.getIndeces().data());
}

void VisitorGeometryBufferCreateAndDraw::reinit(OpenGLGeometryBufferA& a)
{
	GLuint vb(-1);
	GLuint ib(-1);
	OpenGLFunctional::getInstance()->glDeleteBuffers(1, &a.getVertexBuffer());
	OpenGLFunctional::getInstance()->glGenBuffers(1, &vb);
	OpenGLFunctional::getInstance()->glDeleteBuffers(1, &a.getIndexBuffer());
	OpenGLFunctional::getInstance()->glGenBuffers(1, &ib);
	a.setVertexBuffer(vb);
	a.setIndexBuffer(ib);

	OpenGLFunctional::getInstance()->glBindVertexArray(a.getVao());

	// �������� ���������� ������
	OpenGLFunctional::getInstance()->glBindBuffer(GL_ARRAY_BUFFER, a.getVertexBuffer());
	OpenGLFunctional::getInstance()->glBufferData(GL_ARRAY_BUFFER, sizeof(VertexColor) * a.getVertices().size(), a.getVertices().data(), GL_STATIC_DRAW);

	// �������� ���������� ������
	OpenGLFunctional::getInstance()->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, a.getIndexBuffer());
	OpenGLFunctional::getInstance()->glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Index) * a.getIndeces().size(), a.getIndeces().data(), GL_STATIC_DRAW);

	// �������� ������� ������
	OpenGLFunctional::getInstance()->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 7 * sizeof(GLfloat), (GLvoid*)0);
	OpenGLFunctional::getInstance()->glEnableVertexAttribArray(0);
	OpenGLFunctional::getInstance()->glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 7 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	OpenGLFunctional::getInstance()->glEnableVertexAttribArray(1);

	// ���������� ������
	OpenGLFunctional::getInstance()->glBindBuffer(GL_ARRAY_BUFFER, 0);
	OpenGLFunctional::getInstance()->glBindVertexArray(0);

}

void VisitorGeometryBufferCreateAndDraw::draw(Direct3D11GeometryBufferA& a)
{
	// ����������� ������� ������
	d3dImmediateContext->IASetInputLayout(a.getInputLayout().Get());
	// ��������� ���������� ������
	UINT stride = sizeof(VertexColor);
	UINT offset = 0;
	d3dImmediateContext->IASetVertexBuffers(0, 1, a.getVertexBuffer().GetAddressOf(), &stride, &offset);
	// ��������� ���������� ������
	d3dImmediateContext->IASetIndexBuffer(a.getIndexBuffer().Get(), DXGI_FORMAT_R32_UINT, 0);
	// ��������� ���������
	d3dImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	// ��������� ��������
	d3dImmediateContext->VSSetShader(a.getVertexShader().Get(), NULL, 0);
	d3dImmediateContext->PSSetShader(a.getPixelShader().Get(), NULL, 0);
	// ����� ���������	
	d3dImmediateContext->DrawIndexed(static_cast<UINT>(a.getIndeces().size()), 0, 0);
}

void VisitorGeometryBufferCreateAndDraw::draw(OpenGLGeometryBufferA& a)
{
	OpenGLFunctional::getInstance()->glUseProgram(a.getShaderProgram());
	OpenGLFunctional::getInstance()->glBindVertexArray(a.getVao());
	glDrawElements(GL_TRIANGLES, a.getIndeces().size(), GL_UNSIGNED_INT, 0);
	OpenGLFunctional::getInstance()->glBindVertexArray(0);
}


void VisitorGeometryBufferCreateAndDraw::create(const std::wstring& vs, const std::wstring& ps, Direct3D11GeometryBufferB& a)
{
	// ���������� �������
	a.getVertexShader() = shadersBufferD3d11->getVertexShader(vs);
	a.getPixelShader() = shadersBufferD3d11->getPixelShader(ps);
	// �������� ������� ������
	std::vector<D3D11_INPUT_ELEMENT_DESC> layout =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	auto vsBlob = shadersBufferD3d11->getVertexShaderBlob(vs);
	HRESULT hr = d3dDevice->CreateInputLayout(layout.data(), static_cast<UINT>(layout.size()), vsBlob->GetBufferPointer(),
		vsBlob->GetBufferSize(), a.getInputLayout().GetAddressOf());
	if (FAILED(hr))
		throw ExceptionDirect3D11GeometryBuffer(L"error create input layout!");
	a.getConstantBufferWorld() = Direct3D11CreatorBuffer::createBuffer(d3dDevice, static_cast<UINT>(sizeof(ConstantBufferWorld)),
		D3D11_BIND_CONSTANT_BUFFER, D3D11_USAGE_DEFAULT, 0);
}

void VisitorGeometryBufferCreateAndDraw::create(const std::wstring& vs, const std::wstring& ps, OpenGLGeometryBufferB& a)
{
	GLuint vao(-1);
	OpenGLFunctional::getInstance()->glGenVertexArrays(1, &vao);
	a.setVao(vao);

	GLuint _vs = shadersBufferOpenGl->getShader(vs);
	GLuint _ps = shadersBufferOpenGl->getShader(ps);
	std::vector<unsigned> key = { _vs, _ps };
	a.getShaderProgram() = shadersBufferOpenGl->getShaderProgram(key);
	a.getWorldLocation() = OpenGLFunctional::getInstance()->glGetUniformLocation(a.getShaderProgram(), "world");
}

void VisitorGeometryBufferCreateAndDraw::reinit(Direct3D11GeometryBufferB& a)
{
	HRESULT hr;
	D3D11_BUFFER_DESC bd;  // ���������, ����������� ����������� �����
	D3D11_SUBRESOURCE_DATA InitData; // ���������, ���������� ������ ������


	clears::ReleaseCom(a.getVertexBuffer());
	a.getVertexBuffer() = Direct3D11CreatorBuffer::createBuffer(d3dDevice, static_cast<UINT>(sizeof(VertexColor) * a.getVertices().size()),
		D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_DEFAULT, 0, a.getVertices().data());

	clears::ReleaseCom(a.getIndexBuffer());
	a.getIndexBuffer() = Direct3D11CreatorBuffer::createBuffer(d3dDevice, static_cast<UINT>(sizeof(Index) * a.getIndeces().size()),
		D3D11_BIND_INDEX_BUFFER, D3D11_USAGE_DEFAULT, 0, a.getIndeces().data());

}

void VisitorGeometryBufferCreateAndDraw::reinit(OpenGLGeometryBufferB& a)
{
	GLuint vb(-1);
	GLuint ib(-1);
	OpenGLFunctional::getInstance()->glDeleteBuffers(1, &a.getVertexBuffer());
	OpenGLFunctional::getInstance()->glGenBuffers(1, &vb);
	OpenGLFunctional::getInstance()->glDeleteBuffers(1, &a.getIndexBuffer());
	OpenGLFunctional::getInstance()->glGenBuffers(1, &ib);
	a.setVertexBuffer(vb);
	a.setIndexBuffer(ib);

	OpenGLFunctional::getInstance()->glBindVertexArray(a.getVao());

	// �������� ���������� ������
	OpenGLFunctional::getInstance()->glBindBuffer(GL_ARRAY_BUFFER, a.getVertexBuffer());
	OpenGLFunctional::getInstance()->glBufferData(GL_ARRAY_BUFFER, sizeof(VertexColor) * a.getVertices().size(), a.getVertices().data(), GL_STATIC_DRAW);

	// �������� ���������� ������
	OpenGLFunctional::getInstance()->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, a.getIndexBuffer());
	OpenGLFunctional::getInstance()->glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Index) * a.getIndeces().size(), a.getIndeces().data(), GL_STATIC_DRAW);

	// �������� ������� ������
	OpenGLFunctional::getInstance()->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 7 * sizeof(GLfloat), (GLvoid*)0);
	OpenGLFunctional::getInstance()->glEnableVertexAttribArray(0);
	OpenGLFunctional::getInstance()->glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 7 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	OpenGLFunctional::getInstance()->glEnableVertexAttribArray(1);

	// ���������� ������
	OpenGLFunctional::getInstance()->glBindBuffer(GL_ARRAY_BUFFER, 0);
	OpenGLFunctional::getInstance()->glBindVertexArray(0);
}

void VisitorGeometryBufferCreateAndDraw::draw(Direct3D11GeometryBufferB& a)
{
	// ����������� ������� ������
	d3dImmediateContext->IASetInputLayout(a.getInputLayout().Get());
	// ��������� ���������� ������
	UINT stride = sizeof(VertexColor);
	UINT offset = 0;
	d3dImmediateContext->IASetVertexBuffers(0, 1, a.getVertexBuffer().GetAddressOf(), &stride, &offset);
	// ��������� ���������� ������
	d3dImmediateContext->IASetIndexBuffer(a.getIndexBuffer().Get(), DXGI_FORMAT_R32_UINT, 0);
	// ��������� ���������
	d3dImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	// ��������� ��������
	d3dImmediateContext->VSSetShader(a.getVertexShader().Get(), NULL, 0);
	d3dImmediateContext->PSSetShader(a.getPixelShader().Get(), NULL, 0);
	camera->sendToGPU(-1);
	// ����� ���������	
	//int _offset(0);
	auto w = a.getConstantBufferWorld();
	for (int i(0); i < a.getModels().size(); i++)
	{
		auto m = a.getModels().at(i);
		d3dImmediateContext->VSSetConstantBuffers(1, 1, w.GetAddressOf());
		ConstantBufferWorld cbw;
		cbw.world = gmath::Matrix::Transponse(a.getModels().at(i).getWorld());
		d3dImmediateContext->UpdateSubresource(w.Get(), 0, NULL, &cbw, 0, 0);
		d3dImmediateContext->DrawIndexed(static_cast<UINT>(m.getCountIndex()), m.getStartIndex(), 0);
		//_offset += m.getOffsetIndex();
	}
}

void VisitorGeometryBufferCreateAndDraw::draw(OpenGLGeometryBufferB& a)
{
	OpenGLFunctional::getInstance()->glUseProgram(a.getShaderProgram());
	camera->sendToGPU(a.getShaderProgram());
	OpenGLFunctional::getInstance()->glBindVertexArray(a.getVao());
	for (auto&& m : a.getModels())
	{
		int si = m.getStartIndex() * sizeof(Index);
		OpenGLFunctional::getInstance()->glUniformMatrix4fv(a.getWorldLocation(), 1, GL_FALSE, m.getWorld().toArray().data());
		glDrawElements(GL_TRIANGLES, m.getCountIndex(), GL_UNSIGNED_INT, reinterpret_cast<const GLvoid*>(si));
	}
	OpenGLFunctional::getInstance()->glBindVertexArray(0);
}




void VisitorGeometryBufferCreateAndDraw::create(const std::wstring& vs, const std::wstring& ps, Direct3D11GeometryBufferC& a)
{
	a.getVertexShader() = shadersBufferD3d11->getVertexShader(vs);
	a.getPixelShader() = shadersBufferD3d11->getPixelShader(ps);
	std::vector<D3D11_INPUT_ELEMENT_DESC> layout =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	auto vsBlob = shadersBufferD3d11->getVertexShaderBlob(vs);
	HRESULT hr = d3dDevice->CreateInputLayout(layout.data(), static_cast<UINT>(layout.size()), vsBlob->GetBufferPointer(),
		vsBlob->GetBufferSize(), a.getInputLayout().GetAddressOf());
	if (FAILED(hr))
		throw ExceptionDirect3D11GeometryBuffer(L"error create input layout!");
	a.getConstantBufferWorld() = Direct3D11CreatorBuffer::createBuffer(d3dDevice, static_cast<UINT>(sizeof(ConstantBufferWorld)),
		D3D11_BIND_CONSTANT_BUFFER, D3D11_USAGE_DEFAULT, 0);
}

void VisitorGeometryBufferCreateAndDraw::create(const std::wstring& vs, const std::wstring& ps, OpenGLGeometryBufferC& a)
{
	GLuint vao(-1);
	OpenGLFunctional::getInstance()->glGenVertexArrays(1, &vao);
	a.setVao(vao);

	GLuint _vs = shadersBufferOpenGl->getShader(vs);
	GLuint _ps = shadersBufferOpenGl->getShader(ps);
	std::vector<unsigned> key = { _vs, _ps };
	a.getShaderProgram() = shadersBufferOpenGl->getShaderProgram(key);
	a.getWorldLocation() = OpenGLFunctional::getInstance()->glGetUniformLocation(a.getShaderProgram(), "world");
}

void VisitorGeometryBufferCreateAndDraw::reinit(Direct3D11GeometryBufferC& a)
{
	HRESULT hr;
	D3D11_BUFFER_DESC bd; 
	D3D11_SUBRESOURCE_DATA InitData;


	clears::ReleaseCom(a.getVertexBuffer());
	a.getVertexBuffer() = Direct3D11CreatorBuffer::createBuffer(d3dDevice, static_cast<UINT>(sizeof(VertexTexture) * a.getVertices().size()),
		D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_DEFAULT, 0, a.getVertices().data());
	clears::ReleaseCom(a.getIndexBuffer());
	a.getIndexBuffer() = Direct3D11CreatorBuffer::createBuffer(d3dDevice, static_cast<UINT>(sizeof(Index) * a.getIndeces().size()),
		D3D11_BIND_INDEX_BUFFER, D3D11_USAGE_DEFAULT, 0, a.getIndeces().data());
}

void VisitorGeometryBufferCreateAndDraw::reinit(OpenGLGeometryBufferC& a)
{
	GLuint vb(-1);
	GLuint ib(-1);
	OpenGLFunctional::getInstance()->glDeleteBuffers(1, &a.getVertexBuffer());
	OpenGLFunctional::getInstance()->glGenBuffers(1, &vb);
	OpenGLFunctional::getInstance()->glDeleteBuffers(1, &a.getIndexBuffer());
	OpenGLFunctional::getInstance()->glGenBuffers(1, &ib);
	a.setVertexBuffer(vb);
	a.setIndexBuffer(ib);

	OpenGLFunctional::getInstance()->glBindVertexArray(a.getVao());

	OpenGLFunctional::getInstance()->glBindBuffer(GL_ARRAY_BUFFER, a.getVertexBuffer());
	OpenGLFunctional::getInstance()->glBufferData(GL_ARRAY_BUFFER, sizeof(VertexTexture) * a.getVertices().size(), a.getVertices().data(), GL_STATIC_DRAW);

	OpenGLFunctional::getInstance()->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, a.getIndexBuffer());
	OpenGLFunctional::getInstance()->glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Index) * a.getIndeces().size(), a.getIndeces().data(), GL_STATIC_DRAW);

	OpenGLFunctional::getInstance()->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
	OpenGLFunctional::getInstance()->glEnableVertexAttribArray(0);
	OpenGLFunctional::getInstance()->glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	OpenGLFunctional::getInstance()->glEnableVertexAttribArray(1);

	OpenGLFunctional::getInstance()->glBindBuffer(GL_ARRAY_BUFFER, 0);
	OpenGLFunctional::getInstance()->glBindVertexArray(0);
}

void VisitorGeometryBufferCreateAndDraw::draw(Direct3D11GeometryBufferC& a)
{
	d3dImmediateContext->IASetInputLayout(a.getInputLayout().Get());
	UINT stride = sizeof(VertexTexture);
	UINT offset = 0;
	d3dImmediateContext->IASetVertexBuffers(0, 1, a.getVertexBuffer().GetAddressOf(), &stride, &offset);
	d3dImmediateContext->IASetIndexBuffer(a.getIndexBuffer().Get(), DXGI_FORMAT_R32_UINT, 0);
	d3dImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	d3dImmediateContext->VSSetShader(a.getVertexShader().Get(), NULL, 0);
	d3dImmediateContext->PSSetShader(a.getPixelShader().Get(), NULL, 0);
	camera->sendToGPU(-1);
	auto w = a.getConstantBufferWorld();
	for (int i(0); i < a.getModels().size(); i++)
	{
		auto m = a.getModels().at(i);
		d3dImmediateContext->VSSetConstantBuffers(1, 1, w.GetAddressOf());
		ConstantBufferWorld cbw;
		cbw.world = gmath::Matrix::Transponse(a.getModels().at(i).getWorld());
		d3dImmediateContext->UpdateSubresource(w.Get(), 0, NULL, &cbw, 0, 0);
		d3dImmediateContext->PSSetShaderResources(0, 1, texturesBufferD3d11->getTexture(m.getTextureKey()).GetAddressOf());
		d3dImmediateContext->PSSetSamplers(0, 1, texturesBufferD3d11->getSampler(m.getSamplerKey()).GetAddressOf());
		d3dImmediateContext->DrawIndexed(static_cast<UINT>(m.getCountIndex()), m.getStartIndex(), 0);
	}
}

void VisitorGeometryBufferCreateAndDraw::draw(OpenGLGeometryBufferC& a)
{
	OpenGLFunctional::getInstance()->glUseProgram(a.getShaderProgram());
	camera->sendToGPU(a.getShaderProgram());
	OpenGLFunctional::getInstance()->glBindVertexArray(a.getVao());
	for (auto&& m : a.getModels())
	{
		int si = m.getStartIndex() * sizeof(Index);
		OpenGLFunctional::getInstance()->glUniformMatrix4fv(a.getWorldLocation(), 1, GL_FALSE, m.getWorld().toArray().data());
		GLuint texture = texturesBufferOpenGl->getTexture(m.getTextureKey());
		OpenGLSamplerRepeat::setSampler(texture, m.getSamplerKey());
		/*OpenGLFunctional::getInstance()->glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture);
		OpenGLFunctional::getInstance()->glUniform1i(OpenGLFunctional::getInstance()->glGetUniformLocation(a.getShaderProgram(), "inTexture"), 0);*/
		OpenGLFunctional::getInstance()->glBindTextureUnit(0, texture);
		glDrawElements(GL_TRIANGLES, m.getCountIndex(), GL_UNSIGNED_INT, reinterpret_cast<const GLvoid*>(si));
	}
	OpenGLFunctional::getInstance()->glBindVertexArray(0);
}





void VisitorGeometryBufferCreateAndDraw::create(const std::wstring& vs, const std::wstring& ps, Direct3D11GeometryBufferD& a)
{
	// ���������� �������
	a.getVertexShader() = shadersBufferD3d11->getVertexShader(vs);
	a.getPixelShader() = shadersBufferD3d11->getPixelShader(ps);
	// �������� ������� ������
	std::vector<D3D11_INPUT_ELEMENT_DESC> layout =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	auto vsBlob = shadersBufferD3d11->getVertexShaderBlob(vs);
	HRESULT hr = d3dDevice->CreateInputLayout(layout.data(), static_cast<UINT>(layout.size()), vsBlob->GetBufferPointer(),
		vsBlob->GetBufferSize(), a.getInputLayout().GetAddressOf());
	if (FAILED(hr))
		throw ExceptionDirect3D11GeometryBuffer(L"error create input layout!");
	a.getConstantBufferWorld() = Direct3D11CreatorBuffer::createBuffer(d3dDevice, static_cast<UINT>(sizeof(ConstantBufferWorld)),
		D3D11_BIND_CONSTANT_BUFFER, D3D11_USAGE_DEFAULT, 0);
	a.getConstantsBufferMaterial() = Direct3D11CreatorBuffer::createBuffer(d3dDevice, static_cast<UINT>(sizeof(ConstantBufferMaterial)),
		D3D11_BIND_CONSTANT_BUFFER, D3D11_USAGE_DEFAULT, 0);
}

void VisitorGeometryBufferCreateAndDraw::create(const std::wstring& vs, const std::wstring& ps, OpenGLGeometryBufferD& a)
{
	GLuint vao(-1);
	OpenGLFunctional::getInstance()->glGenVertexArrays(1, &vao);
	a.setVao(vao);

	GLuint _vs = shadersBufferOpenGl->getShader(vs);
	GLuint _ps = shadersBufferOpenGl->getShader(ps);
	std::vector<unsigned> key = { _vs, _ps };
	a.getShaderProgram() = shadersBufferOpenGl->getShaderProgram(key);
	a.getWorldLocation() = OpenGLFunctional::getInstance()->glGetUniformLocation(a.getShaderProgram(), "world");
	a.getMaterialLocation() = OpenGLFunctional::getInstance()->glGetUniformLocation(a.getShaderProgram(), "material");
	a.getNormalsLocation() = OpenGLFunctional::getInstance()->glGetUniformLocation(a.getShaderProgram(), "normals");
}

void VisitorGeometryBufferCreateAndDraw::reinit(Direct3D11GeometryBufferD& a)
{
	HRESULT hr;
	D3D11_BUFFER_DESC bd;  // ���������, ����������� ����������� �����
	D3D11_SUBRESOURCE_DATA InitData; // ���������, ���������� ������ ������


	clears::ReleaseCom(a.getVertexBuffer());
	// �������� ���������� ������
	a.getVertexBuffer() = Direct3D11CreatorBuffer::createBuffer(d3dDevice, static_cast<UINT>(sizeof(VertexStaticLight) * a.getVertices().size()),
		D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_DEFAULT, 0, a.getVertices().data());
	clears::ReleaseCom(a.getIndexBuffer());
	// �������� ���������� ������
	a.getIndexBuffer() = Direct3D11CreatorBuffer::createBuffer(d3dDevice, static_cast<UINT>(sizeof(Index) * a.getIndeces().size()),
		D3D11_BIND_INDEX_BUFFER, D3D11_USAGE_DEFAULT, 0, a.getIndeces().data());
}

void VisitorGeometryBufferCreateAndDraw::reinit(OpenGLGeometryBufferD& a)
{
	GLuint vb(-1);
	GLuint ib(-1);
	OpenGLFunctional::getInstance()->glDeleteBuffers(1, &a.getVertexBuffer());
	OpenGLFunctional::getInstance()->glGenBuffers(1, &vb);
	OpenGLFunctional::getInstance()->glDeleteBuffers(1, &a.getIndexBuffer());
	OpenGLFunctional::getInstance()->glGenBuffers(1, &ib);
	a.setVertexBuffer(vb);
	a.setIndexBuffer(ib);

	OpenGLFunctional::getInstance()->glBindVertexArray(a.getVao());

	OpenGLFunctional::getInstance()->glBindBuffer(GL_ARRAY_BUFFER, a.getVertexBuffer());
	OpenGLFunctional::getInstance()->glBufferData(GL_ARRAY_BUFFER, sizeof(VertexStaticLight) * a.getVertices().size(), a.getVertices().data(), GL_STATIC_DRAW);

	OpenGLFunctional::getInstance()->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, a.getIndexBuffer());
	OpenGLFunctional::getInstance()->glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Index) * a.getIndeces().size(), a.getIndeces().data(), GL_STATIC_DRAW);

	OpenGLFunctional::getInstance()->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 10 * sizeof(GLfloat), (GLvoid*)0);
	OpenGLFunctional::getInstance()->glEnableVertexAttribArray(0);
	OpenGLFunctional::getInstance()->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 10 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	OpenGLFunctional::getInstance()->glEnableVertexAttribArray(1);
	OpenGLFunctional::getInstance()->glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, 10 * sizeof(GLfloat), (GLvoid*)(6 * sizeof(GLfloat)));
	OpenGLFunctional::getInstance()->glEnableVertexAttribArray(2);

	OpenGLFunctional::getInstance()->glBindBuffer(GL_ARRAY_BUFFER, 0);
	OpenGLFunctional::getInstance()->glBindVertexArray(0);
}

void VisitorGeometryBufferCreateAndDraw::draw(Direct3D11GeometryBufferD& a)
{
	// ����������� ������� ������
	d3dImmediateContext->IASetInputLayout(a.getInputLayout().Get());
	// ��������� ���������� ������
	UINT stride = sizeof(VertexStaticLight);
	UINT offset = 0;
	d3dImmediateContext->IASetVertexBuffers(0, 1, a.getVertexBuffer().GetAddressOf(), &stride, &offset);
	// ��������� ���������� ������
	d3dImmediateContext->IASetIndexBuffer(a.getIndexBuffer().Get(), DXGI_FORMAT_R32_UINT, 0);
	// ��������� ���������
	d3dImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	// ��������� ��������
	d3dImmediateContext->VSSetShader(a.getVertexShader().Get(), NULL, 0);
	d3dImmediateContext->PSSetShader(a.getPixelShader().Get(), NULL, 0);
	camera->sendToGPU(-1);
	staticLight->sendGPU(-1);
	// ����� ���������	
	auto w = a.getConstantBufferWorld();
	auto mat = a.getConstantsBufferMaterial();
	for (int i(0); i < a.getModels().size(); i++)
	{
		auto m = a.getModels().at(i);

		d3dImmediateContext->VSSetConstantBuffers(1, 1, w.GetAddressOf());
		ConstantBufferWorld cbw;
		cbw.world = gmath::Matrix::Transponse(a.getModels().at(i).getWorld());
		cbw.normals = gmath::Matrix::Transponse(a.getModels().at(i).getNormals());
		d3dImmediateContext->UpdateSubresource(w.Get(), 0, NULL, &cbw, 0, 0);

		d3dImmediateContext->PSSetConstantBuffers(1, 1, mat.GetAddressOf());
		ConstantBufferMaterial cbm;
		auto material = materialsBuffer->getMaterial(a.getModels().at(i).getMaterialKey());
		cbm.ambient = material.ambient;
		cbm.diffuse = material.diffuse;
		cbm.specular = material.specular;
		cbm.shininess = material.shininess;
		d3dImmediateContext->UpdateSubresource(mat.Get(), 0, NULL, &cbm, 0, 0);

		d3dImmediateContext->PSSetShaderResources(0, 1, texturesBufferD3d11->getTexture(m.getTextureKey()).GetAddressOf());
		d3dImmediateContext->PSSetSamplers(0, 1, texturesBufferD3d11->getSampler(m.getSamplerKey()).GetAddressOf());
		d3dImmediateContext->DrawIndexed(static_cast<UINT>(m.getCountIndex()), m.getStartIndex(), 0);
	}
}

void VisitorGeometryBufferCreateAndDraw::draw(OpenGLGeometryBufferD& a)
{
	OpenGLFunctional::getInstance()->glUseProgram(a.getShaderProgram());
	camera->sendToGPU(a.getShaderProgram());
	staticLight->sendGPU(a.getShaderProgram());
	OpenGLFunctional::getInstance()->glBindVertexArray(a.getVao());
	for (auto&& m : a.getModels())
	{
		int si = m.getStartIndex() * sizeof(Index);
		OpenGLFunctional::getInstance()->glUniformMatrix4fv(a.getWorldLocation(), 1, GL_FALSE, m.getWorld().toArray().data());
		OpenGLFunctional::getInstance()->glUniformMatrix4fv(a.getNormalsLocation(), 1, GL_FALSE, m.getNormals().toArray().data());

		auto material = materialsBuffer->getMaterial(m.getMaterialKey());
		OpenGLFunctional::getInstance()->glUniform4f(OpenGLFunctional::getInstance()->glGetUniformLocation(a.getShaderProgram(), "material.ambient"),
			material.ambient[gmtypes::vx],
			material.ambient[gmtypes::vy],
			material.ambient[gmtypes::vz],
			material.ambient[gmtypes::vw]);
		OpenGLFunctional::getInstance()->glUniform4f(OpenGLFunctional::getInstance()->glGetUniformLocation(a.getShaderProgram(), "material.diffuse"),
			material.diffuse[gmtypes::vx],
			material.diffuse[gmtypes::vy],
			material.diffuse[gmtypes::vz],
			material.diffuse[gmtypes::vw]);
		OpenGLFunctional::getInstance()->glUniform4f(OpenGLFunctional::getInstance()->glGetUniformLocation(a.getShaderProgram(), "material.specular"),
			material.specular[gmtypes::vx],
			material.specular[gmtypes::vy],
			material.specular[gmtypes::vz],
			material.specular[gmtypes::vw]);
		OpenGLFunctional::getInstance()->glUniform4f(OpenGLFunctional::getInstance()->glGetUniformLocation(a.getShaderProgram(), "material.shininess"),
			material.shininess,
			-1,
			-1,
			-1);

		GLuint texture = texturesBufferOpenGl->getTexture(m.getTextureKey());
		OpenGLSamplerRepeat::setSampler(texture, m.getSamplerKey());
		OpenGLFunctional::getInstance()->glBindTextureUnit(0, texture);
		glDrawElements(GL_TRIANGLES, m.getCountIndex(), GL_UNSIGNED_INT, reinterpret_cast<const GLvoid*>(si));
	}
	OpenGLFunctional::getInstance()->glBindVertexArray(0);
}





void VisitorGeometryBufferCreateAndDraw::create(const std::wstring& vs, const std::wstring& ps, Direct3D11GeometryBufferE& a)
{
	// ���������� �������
	a.getVertexShader() = shadersBufferD3d11->getVertexShader(vs);
	a.getPixelShader() = shadersBufferD3d11->getPixelShader(ps);
	// �������� ������� ������
	std::vector<D3D11_INPUT_ELEMENT_DESC> layout =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	auto vsBlob = shadersBufferD3d11->getVertexShaderBlob(vs);
	HRESULT hr = d3dDevice->CreateInputLayout(layout.data(), static_cast<UINT>(layout.size()), vsBlob->GetBufferPointer(),
		vsBlob->GetBufferSize(), a.getInputLayout().GetAddressOf());
	if (FAILED(hr))
		throw ExceptionDirect3D11GeometryBuffer(L"error create input layout!");
	a.getConstantBufferWorld() = Direct3D11CreatorBuffer::createBuffer(d3dDevice, static_cast<UINT>(sizeof(ConstantBufferWorld)),
		D3D11_BIND_CONSTANT_BUFFER, D3D11_USAGE_DEFAULT, 0);
	a.getConstantsBufferMaterial() = Direct3D11CreatorBuffer::createBuffer(d3dDevice, static_cast<UINT>(sizeof(ConstantBufferMaterial)),
		D3D11_BIND_CONSTANT_BUFFER, D3D11_USAGE_DEFAULT, 0);
}

void VisitorGeometryBufferCreateAndDraw::create(const std::wstring& vs, const std::wstring& ps, OpenGLGeometryBufferE& a)
{
	GLuint vao(-1);
	OpenGLFunctional::getInstance()->glGenVertexArrays(1, &vao);
	a.setVao(vao);

	GLuint _vs = shadersBufferOpenGl->getShader(vs);
	GLuint _ps = shadersBufferOpenGl->getShader(ps);
	std::vector<unsigned> key = { _vs, _ps };
	a.getShaderProgram() = shadersBufferOpenGl->getShaderProgram(key);
	a.getWorldLocation() = OpenGLFunctional::getInstance()->glGetUniformLocation(a.getShaderProgram(), "world");
	a.getMaterialLocation() = OpenGLFunctional::getInstance()->glGetUniformLocation(a.getShaderProgram(), "material");
	a.getNormalsLocation() = OpenGLFunctional::getInstance()->glGetUniformLocation(a.getShaderProgram(), "normals");
}

void VisitorGeometryBufferCreateAndDraw::reinit(Direct3D11GeometryBufferE& a)
{
	HRESULT hr;
	D3D11_BUFFER_DESC bd;  // ���������, ����������� ����������� �����
	D3D11_SUBRESOURCE_DATA InitData; // ���������, ���������� ������ ������


	clears::ReleaseCom(a.getVertexBuffer());
	// �������� ���������� ������
	a.getVertexBuffer() = Direct3D11CreatorBuffer::createBuffer(d3dDevice, static_cast<UINT>(sizeof(VertexStaticLight) * a.getVertices().size()),
		D3D11_BIND_VERTEX_BUFFER, D3D11_USAGE_DEFAULT, 0, a.getVertices().data());
	clears::ReleaseCom(a.getIndexBuffer());
	// �������� ���������� ������
	a.getIndexBuffer() = Direct3D11CreatorBuffer::createBuffer(d3dDevice, static_cast<UINT>(sizeof(Index) * a.getIndeces().size()),
		D3D11_BIND_INDEX_BUFFER, D3D11_USAGE_DEFAULT, 0, a.getIndeces().data());
}

void VisitorGeometryBufferCreateAndDraw::reinit(OpenGLGeometryBufferE& a)
{
	GLuint vb(-1);
	GLuint ib(-1);
	OpenGLFunctional::getInstance()->glDeleteBuffers(1, &a.getVertexBuffer());
	OpenGLFunctional::getInstance()->glGenBuffers(1, &vb);
	OpenGLFunctional::getInstance()->glDeleteBuffers(1, &a.getIndexBuffer());
	OpenGLFunctional::getInstance()->glGenBuffers(1, &ib);
	a.setVertexBuffer(vb);
	a.setIndexBuffer(ib);

	OpenGLFunctional::getInstance()->glBindVertexArray(a.getVao());

	OpenGLFunctional::getInstance()->glBindBuffer(GL_ARRAY_BUFFER, a.getVertexBuffer());
	OpenGLFunctional::getInstance()->glBufferData(GL_ARRAY_BUFFER, sizeof(VertexStaticLight) * a.getVertices().size(), a.getVertices().data(), GL_STATIC_DRAW);

	OpenGLFunctional::getInstance()->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, a.getIndexBuffer());
	OpenGLFunctional::getInstance()->glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Index) * a.getIndeces().size(), a.getIndeces().data(), GL_STATIC_DRAW);

	OpenGLFunctional::getInstance()->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 10 * sizeof(GLfloat), (GLvoid*)0);
	OpenGLFunctional::getInstance()->glEnableVertexAttribArray(0);
	OpenGLFunctional::getInstance()->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 10 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	OpenGLFunctional::getInstance()->glEnableVertexAttribArray(1);
	OpenGLFunctional::getInstance()->glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, 10 * sizeof(GLfloat), (GLvoid*)(6 * sizeof(GLfloat)));
	OpenGLFunctional::getInstance()->glEnableVertexAttribArray(2);

	OpenGLFunctional::getInstance()->glBindBuffer(GL_ARRAY_BUFFER, 0);
	OpenGLFunctional::getInstance()->glBindVertexArray(0);
}

void VisitorGeometryBufferCreateAndDraw::draw(Direct3D11GeometryBufferE& a)
{
	// ����������� ������� ������
	d3dImmediateContext->IASetInputLayout(a.getInputLayout().Get());
	// ��������� ���������� ������
	UINT stride = sizeof(VertexStaticLight);
	UINT offset = 0;
	d3dImmediateContext->IASetVertexBuffers(0, 1, a.getVertexBuffer().GetAddressOf(), &stride, &offset);
	// ��������� ���������� ������
	d3dImmediateContext->IASetIndexBuffer(a.getIndexBuffer().Get(), DXGI_FORMAT_R32_UINT, 0);
	// ��������� ���������
	d3dImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	// ��������� ��������
	d3dImmediateContext->VSSetShader(a.getVertexShader().Get(), NULL, 0);
	d3dImmediateContext->PSSetShader(a.getPixelShader().Get(), NULL, 0);
	camera->sendToGPU(-1);
	staticLight->sendGPU(-1);
	// ����� ���������	
	auto w = a.getConstantBufferWorld();
	auto mat = a.getConstantsBufferMaterial();
	for (int i(0); i < a.getModels().size(); i++)
	{
		auto m = a.getModels().at(i);

		d3dImmediateContext->VSSetConstantBuffers(1, 1, w.GetAddressOf());
		ConstantBufferWorld cbw;
		cbw.world = gmath::Matrix::Transponse(a.getModels().at(i).getWorld());
		cbw.normals = gmath::Matrix::Transponse(a.getModels().at(i).getNormals());
		d3dImmediateContext->UpdateSubresource(w.Get(), 0, NULL, &cbw, 0, 0);

		d3dImmediateContext->PSSetConstantBuffers(1, 1, mat.GetAddressOf());
		ConstantBufferMaterial cbm;
		auto material = materialsBuffer->getMaterial(a.getModels().at(i).getMaterialKey());
		cbm.ambient = material.ambient;
		cbm.diffuse = material.diffuse;
		cbm.specular = material.specular;
		cbm.shininess = material.shininess;
		d3dImmediateContext->UpdateSubresource(mat.Get(), 0, NULL, &cbm, 0, 0);

		d3dImmediateContext->DrawIndexed(static_cast<UINT>(m.getCountIndex()), m.getStartIndex(), 0);
	}
}

void VisitorGeometryBufferCreateAndDraw::draw(OpenGLGeometryBufferE& a)
{
	OpenGLFunctional::getInstance()->glUseProgram(a.getShaderProgram());
	camera->sendToGPU(a.getShaderProgram());
	staticLight->sendGPU(a.getShaderProgram());
	OpenGLFunctional::getInstance()->glBindVertexArray(a.getVao());
	for (auto&& m : a.getModels())
	{
		int si = m.getStartIndex() * sizeof(Index);
		OpenGLFunctional::getInstance()->glUniformMatrix4fv(a.getWorldLocation(), 1, GL_FALSE, m.getWorld().toArray().data());
		OpenGLFunctional::getInstance()->glUniformMatrix4fv(a.getNormalsLocation(), 1, GL_FALSE, m.getNormals().toArray().data());

		auto material = materialsBuffer->getMaterial(m.getMaterialKey());
		OpenGLFunctional::getInstance()->glUniform4f(OpenGLFunctional::getInstance()->glGetUniformLocation(a.getShaderProgram(), "material.ambient"),
			material.ambient[gmtypes::vx],
			material.ambient[gmtypes::vy],
			material.ambient[gmtypes::vz],
			material.ambient[gmtypes::vw]);
		OpenGLFunctional::getInstance()->glUniform4f(OpenGLFunctional::getInstance()->glGetUniformLocation(a.getShaderProgram(), "material.diffuse"),
			material.diffuse[gmtypes::vx],
			material.diffuse[gmtypes::vy],
			material.diffuse[gmtypes::vz],
			material.diffuse[gmtypes::vw]);
		OpenGLFunctional::getInstance()->glUniform4f(OpenGLFunctional::getInstance()->glGetUniformLocation(a.getShaderProgram(), "material.specular"),
			material.specular[gmtypes::vx],
			material.specular[gmtypes::vy],
			material.specular[gmtypes::vz],
			material.specular[gmtypes::vw]);
		OpenGLFunctional::getInstance()->glUniform4f(OpenGLFunctional::getInstance()->glGetUniformLocation(a.getShaderProgram(), "material.shininess"),
			material.shininess,
			-1,
			-1,
			-1);

		glDrawElements(GL_TRIANGLES, m.getCountIndex(), GL_UNSIGNED_INT, reinterpret_cast<const GLvoid*>(si));
	}
	OpenGLFunctional::getInstance()->glBindVertexArray(0);
}
