#pragma once

#include "../../math_header.h"

struct alignas(16) ConstantBufferWorld
{
	gmath::Matrix world;
	gmath::Matrix normals;
};

struct alignas(16) ConstantBufferMaterial
{
	gmath::Color ambient;
	gmath::Color diffuse;
	gmath::Color specular;
	gmath::Color shininess;
};

struct alignas(16) ConstantBufferColorText
{
	gmath::Color textColor;
};