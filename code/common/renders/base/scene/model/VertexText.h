#pragma once

#include "../../math_header.h"

struct VertexText
{
	gmath::Vector2 position;
	gmath::UVCoordinate texCoord;
};