#include "ModelD.h"

unsigned ModelD::getStartIndex() const
{
	return startIndex;
}

unsigned ModelD::getOffsetIndex() const
{
	return offsetIndex;
}

unsigned ModelD::getCountIndex() const
{
	return countIndex;
}

std::wstring ModelD::getKey()const
{
	return key;
}

gmath::Matrix ModelD::getWorld() const
{
	return world;
}

gmath::Matrix ModelD::getNormals() const
{
	return normals;
}

std::wstring ModelD::getTextureKey() const
{
	return textureKey;
}

std::wstring ModelD::getSamplerKey() const
{
	return samplerKey;
}

std::wstring ModelD::getMaterialKey() const
{
	return materialKey;
}

void ModelD::setStartIndex(unsigned si)
{
	startIndex = si;
}

void ModelD::setOffsetIndex(unsigned oi)
{
	offsetIndex = oi;
}

void ModelD::setCountIndex(unsigned ci)
{
	countIndex = ci;
}

void ModelD::setKey(const std::wstring& n)
{
	key = n;
}

void ModelD::setWorld(const gmath::Matrix& w)
{
	world = w;
}

void ModelD::setNormals(const gmath::Matrix& w)
{
	normals = w;
}

void ModelD::setTextureKey(const std::wstring& n)
{
	textureKey = n;
}

void ModelD::setSamplerKey(const std::wstring& n)
{
	samplerKey = n;
}

void ModelD::setMaterialKey(const std::wstring& n)
{
	materialKey = n;
}
