#include "ModelE.h"

unsigned ModelE::getStartIndex() const
{
	return startIndex;
}

unsigned ModelE::getOffsetIndex() const
{
	return offsetIndex;
}

unsigned ModelE::getCountIndex() const
{
	return countIndex;
}

std::wstring ModelE::getKey()const
{
	return key;
}

gmath::Matrix ModelE::getWorld() const
{
	return world;
}

gmath::Matrix ModelE::getNormals() const
{
	return normals;
}

std::wstring ModelE::getMaterialKey() const
{
	return materialKey;
}

void ModelE::setStartIndex(unsigned si)
{
	startIndex = si;
}

void ModelE::setOffsetIndex(unsigned oi)
{
	offsetIndex = oi;
}

void ModelE::setCountIndex(unsigned ci)
{
	countIndex = ci;
}

void ModelE::setKey(const std::wstring& n)
{
	key = n;
}

void ModelE::setWorld(const gmath::Matrix& w)
{
	world = w;
}

void ModelE::setNormals(const gmath::Matrix& w)
{
	normals = w;
}

void ModelE::setMaterialKey(const std::wstring& n)
{
	materialKey = n;
}
