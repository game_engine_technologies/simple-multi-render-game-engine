#include "OpenGLGeometryBufferE.h"

#include "IGeometryBufferVisitor.h"

void OpenGLGeometryBufferE::init(const std::wstring& vs, const std::wstring& ps, VisitorGeometryBufferCreateAndDrawSmartPtr& visitor)
{
	clearFlags();
	this->visitor = visitor;
	this->visitor->create(vs, ps, *this);
}

void OpenGLGeometryBufferE::reinit()
{
	visitor->reinit(*this);
}

void OpenGLGeometryBufferE::draw()
{
	visitor->draw(*this);
}

bool OpenGLGeometryBufferE::addModel(const ModelParameters& mp)
{
	ModelE model;
	if (mp.type == TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_E)
	{
		if (mp.key == L"")
		{
			debug::printError(L"Error: key is empty");
			return false;
		}
		auto iter = std::find_if(models.begin(), models.end(), [&mp](const ModelE& m)
			{
				return mp.key == m.getKey();
			});
		if (iter == models.end()) // ������ ��� - �������� �������
		{
			if (mp.verticesStaticLight.size() == 0 || mp.indeces.size() == 0)
			{
				debug::printError(L"Error: vertex buffer or index buffer is empty");
				return false;
			}
			int size(this->vertices.size());
			this->vertices.insert(this->vertices.end(), mp.verticesStaticLight.begin(), mp.verticesStaticLight.end());
			model.setOffsetIndex(this->indeces.size());
			std::transform(mp.indeces.begin(), mp.indeces.end(), std::back_inserter(this->indeces), [&size](Index indeces)
				{
					return indeces + size;
				});
			model.setCountIndex(mp.indeces.size());
			model.setKey(mp.key);
			model.setStartIndex(indeces.size() - mp.indeces.size());
			//model.setWorld(mp.world);
			_updateAll = true;
		}
		else // ������ ���� - ��������� � ������ �����
		{
			// ��� ����� ������������ ������� ��������
			model.setCountIndex(iter->getCountIndex());
			model.setKey(iter->getKey());
			model.setOffsetIndex(iter->getOffsetIndex());
			model.setStartIndex(iter->getStartIndex());
			//model.setWorld(mp.world);
		}
		model.setWorld(mp.world);
		model.setNormals(mp.normals);
		model.setMaterialKey(mp.materialKey);
		models.push_back(model);
		return true;
	}
	debug::printError(L"Error type model for OpenGLGeometryBufferE");
	return false;
}

void OpenGLGeometryBufferE::clear()
{
	OpenGLFunctional::getInstance()->glDeleteVertexArrays(1, &glVao);
	OpenGLFunctional::getInstance()->glDeleteBuffers(1, &glVertexBuffer);
	OpenGLFunctional::getInstance()->glDeleteBuffers(1, &glIndexBuffer);
}

GLuint& OpenGLGeometryBufferE::getVertexBuffer()
{
	return glVertexBuffer;
}

GLuint& OpenGLGeometryBufferE::getIndexBuffer()
{
	return glIndexBuffer;
}

GLuint& OpenGLGeometryBufferE::getVao()
{
	return glVao;
}

GLuint& OpenGLGeometryBufferE::getShaderProgram()
{
	return shaderProgram;
}

GLuint OpenGLGeometryBufferE::getVertexBuffer()const
{
	return glVertexBuffer;
}

GLuint OpenGLGeometryBufferE::getIndexBuffer()const
{
	return glIndexBuffer;
}

GLuint OpenGLGeometryBufferE::getVao()const
{
	return glVao;
}

GLuint OpenGLGeometryBufferE::getShaderProgram()const
{
	return shaderProgram;
}

void OpenGLGeometryBufferE::setVertexBuffer(GLuint verticesColor)
{
	glVertexBuffer = verticesColor;
}

void OpenGLGeometryBufferE::setIndexBuffer(GLuint verticesColor)
{
	glIndexBuffer = verticesColor;
}

void OpenGLGeometryBufferE::setVao(GLuint verticesColor)
{
	glVao = verticesColor;
}

void OpenGLGeometryBufferE::setShaderProgram(GLuint verticesColor)
{
	shaderProgram = verticesColor;
}

OpenGLGeometryBufferE::~OpenGLGeometryBufferE()
{
	clear();
}

std::vector<ModelE>& OpenGLGeometryBufferE::getModels()
{
	return models;
}

GLint& OpenGLGeometryBufferE::getWorldLocation()
{
	return worldLocation;
}

GLint& OpenGLGeometryBufferE::getMaterialLocation()
{
	return materialLocation;
}

GLint& OpenGLGeometryBufferE::getNormalsLocation()
{
	return normalsLocation;
}

void DeleterOpenGLGeometryBufferE::operator()(OpenGLGeometryBufferE* gBuff)
{
	if (gBuff != nullptr)
	{
		OpenGLFunctional::getInstance()->glDeleteVertexArrays(1, &gBuff->getVao());
		OpenGLFunctional::getInstance()->glDeleteBuffers(1, &gBuff->getVertexBuffer());
		OpenGLFunctional::getInstance()->glDeleteBuffers(1, &gBuff->getIndexBuffer());
	}
}

std::vector<VertexStaticLight>& OpenGLGeometryBufferE::getVertices()
{
	return vertices;
}
