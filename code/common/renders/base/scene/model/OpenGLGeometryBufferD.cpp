#include "OpenGLGeometryBufferD.h"

#include "IGeometryBufferVisitor.h"

void OpenGLGeometryBufferD::init(const std::wstring& vs, const std::wstring& ps, VisitorGeometryBufferCreateAndDrawSmartPtr& visitor)
{
	clearFlags();
	this->visitor = visitor;
	this->visitor->create(vs, ps, *this);
}

void OpenGLGeometryBufferD::reinit()
{
	visitor->reinit(*this);
}

void OpenGLGeometryBufferD::draw()
{
	visitor->draw(*this);
}

bool OpenGLGeometryBufferD::addModel(const ModelParameters& mp)
{
	ModelD model;
	if (mp.type == TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_D)
	{
		if (mp.key == L"")
		{
			debug::printError(L"Error: key is empty");
			return false;
		}
		auto iter = std::find_if(models.begin(), models.end(), [&mp](const ModelD& m)
			{
				return mp.key == m.getKey();
			});
		if (iter == models.end()) // ������ ��� - �������� �������
		{
			if (mp.verticesStaticLight.size() == 0 || mp.indeces.size() == 0)
			{
				debug::printError(L"Error: vertex buffer or index buffer is empty");
				return false;
			}
			int size(this->vertices.size());
			this->vertices.insert(this->vertices.end(), mp.verticesStaticLight.begin(), mp.verticesStaticLight.end());
			model.setOffsetIndex(this->indeces.size());
			std::transform(mp.indeces.begin(), mp.indeces.end(), std::back_inserter(this->indeces), [&size](Index indeces)
				{
					return indeces + size;
				});
			model.setCountIndex(mp.indeces.size());
			model.setKey(mp.key);
			model.setStartIndex(indeces.size() - mp.indeces.size());
			//model.setWorld(mp.world);
			_updateAll = true;
		}
		else // ������ ���� - ��������� � ������ �����
		{
			// ��� ����� ������������ ������� ��������
			model.setCountIndex(iter->getCountIndex());
			model.setKey(iter->getKey());
			model.setOffsetIndex(iter->getOffsetIndex());
			model.setStartIndex(iter->getStartIndex());
			//model.setWorld(mp.world);
		}
		model.setWorld(mp.world);
		model.setNormals(mp.normals);
		model.setMaterialKey(mp.materialKey);
		model.setTextureKey(mp.textureKey);
		model.setSamplerKey(mp.samplerKey);
		models.push_back(model);
		return true;
	}
	debug::printError(L"Error type model for OpenGLGeometryBufferD");
	return false;
}

void OpenGLGeometryBufferD::clear()
{
	OpenGLFunctional::getInstance()->glDeleteVertexArrays(1, &glVao);
	OpenGLFunctional::getInstance()->glDeleteBuffers(1, &glVertexBuffer);
	OpenGLFunctional::getInstance()->glDeleteBuffers(1, &glIndexBuffer);
}

GLuint& OpenGLGeometryBufferD::getVertexBuffer()
{
	return glVertexBuffer;
}

GLuint& OpenGLGeometryBufferD::getIndexBuffer()
{
	return glIndexBuffer;
}

GLuint& OpenGLGeometryBufferD::getVao()
{
	return glVao;
}

GLuint& OpenGLGeometryBufferD::getShaderProgram()
{
	return shaderProgram;
}

GLuint OpenGLGeometryBufferD::getVertexBuffer()const
{
	return glVertexBuffer;
}

GLuint OpenGLGeometryBufferD::getIndexBuffer()const
{
	return glIndexBuffer;
}

GLuint OpenGLGeometryBufferD::getVao()const
{
	return glVao;
}

GLuint OpenGLGeometryBufferD::getShaderProgram()const
{
	return shaderProgram;
}

void OpenGLGeometryBufferD::setVertexBuffer(GLuint verticesColor)
{
	glVertexBuffer = verticesColor;
}

void OpenGLGeometryBufferD::setIndexBuffer(GLuint verticesColor)
{
	glIndexBuffer = verticesColor;
}

void OpenGLGeometryBufferD::setVao(GLuint verticesColor)
{
	glVao = verticesColor;
}

void OpenGLGeometryBufferD::setShaderProgram(GLuint verticesColor)
{
	shaderProgram = verticesColor;
}

OpenGLGeometryBufferD::~OpenGLGeometryBufferD()
{
	clear();
}

std::vector<ModelD>& OpenGLGeometryBufferD::getModels()
{
	return models;
}

GLint& OpenGLGeometryBufferD::getWorldLocation()
{
	return worldLocation;
}

GLint& OpenGLGeometryBufferD::getMaterialLocation()
{
	return materialLocation;
}

GLint& OpenGLGeometryBufferD::getNormalsLocation()
{
	return normalsLocation;
}

void DeleterOpenGLGeometryBufferD::operator()(OpenGLGeometryBufferD* gBuff)
{
	if (gBuff != nullptr)
	{
		OpenGLFunctional::getInstance()->glDeleteVertexArrays(1, &gBuff->getVao());
		OpenGLFunctional::getInstance()->glDeleteBuffers(1, &gBuff->getVertexBuffer());
		OpenGLFunctional::getInstance()->glDeleteBuffers(1, &gBuff->getIndexBuffer());
	}
}

std::vector<VertexStaticLight>& OpenGLGeometryBufferD::getVertices()
{
	return vertices;
}
