#pragma once

#include <string>
#include <vector>
#include "../../math_header.h"

#include "Buffers.h"

class ModelC
{
	unsigned startIndex;
	unsigned offsetIndex;
	unsigned countIndex;
	std::wstring key;
	gmath::Matrix world;
	std::wstring textureKey;
	std::wstring samplerKey;

public:
	unsigned getStartIndex() const;
	unsigned getOffsetIndex() const;
	unsigned getCountIndex()const;
	std::wstring getKey()const;
	gmath::Matrix getWorld()const;
	std::wstring getTextureKey()const;
	std::wstring getSamplerKey()const;

	void setStartIndex(unsigned si);
	void setOffsetIndex(unsigned oi);
	void setCountIndex(unsigned ci);
	void setKey(const std::wstring& n);
	void setWorld(const gmath::Matrix& w);
	void setTextureKey(const std::wstring& n);
	void setSamplerKey(const std::wstring& n);
};