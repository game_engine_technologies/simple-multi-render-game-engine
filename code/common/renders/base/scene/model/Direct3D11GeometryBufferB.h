#pragma once

#include "../../../abstract/scene/model/IGeometryBuffer.h"
#include "ModelB.h"
#include "../../d3d11_header.h"

class Direct3D11GeometryBufferB : public IGeometryBuffer
{
	ComPtr<ID3D11Buffer> d3dVertexBuffer;
	ComPtr<ID3D11Buffer> d3dIndexBuffer;
	ComPtr<ID3D11InputLayout> d3dInputLayout;
	ComPtr<ID3D11VertexShader> d3dVertexShader;
	ComPtr<ID3D11PixelShader> d3dPixelShader;
	VisitorGeometryBufferCreateAndDrawSmartPtr visitor;
	std::vector<ModelB> models;
	//std::vector<ComPtr<ID3D11Buffer>> constantsBufferWorld;
	//int newSizeConstBuffWorld;
	ComPtr<ID3D11Buffer> constantsBufferWorld;

	std::vector<VertexColor> vertices;
public:
	void init(const std::wstring& vs, const std::wstring& ps, VisitorGeometryBufferCreateAndDrawSmartPtr& visitor) override;
	void reinit() override;
	void draw() override;
	bool addModel(const ModelParameters& mp)override;

	ComPtr<ID3D11Buffer>& getVertexBuffer();
	ComPtr<ID3D11Buffer>& getIndexBuffer();
	ComPtr<ID3D11InputLayout>& getInputLayout();
	ComPtr<ID3D11VertexShader>& getVertexShader();
	ComPtr<ID3D11PixelShader>& getPixelShader();

	std::vector<ModelB>& getModels();
	/*int& getNewSizeConstBuffWorld();
	std::vector<ComPtr<ID3D11Buffer>>& getConstantsBufferWorld();*/
	ComPtr<ID3D11Buffer>& getConstantBufferWorld();

	std::vector<VertexColor>& getVertices();
};

using Direct3D11GeometryBufferBSmartPtr = std::shared_ptr<Direct3D11GeometryBufferB>;
