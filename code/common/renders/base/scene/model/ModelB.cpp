#include "ModelB.h"

unsigned ModelB::getStartIndex() const
{
	return startIndex;
}

unsigned ModelB::getOffsetIndex() const
{
	return offsetIndex;
}

unsigned ModelB::getCountIndex() const
{
	return countIndex;
}

std::wstring ModelB::getKey()const
{
	return key;
}

gmath::Matrix ModelB::getWorld() const
{
	return world;
}

void ModelB::setStartIndex(unsigned si)
{
	startIndex = si;
}

void ModelB::setOffsetIndex(unsigned oi)
{
	offsetIndex = oi;
}

void ModelB::setCountIndex(unsigned ci)
{
	countIndex = ci;
}

void ModelB::setKey(const std::wstring& n)
{
	key = n;
}

void ModelB::setWorld(const gmath::Matrix& w)
{
	world = w;
}
