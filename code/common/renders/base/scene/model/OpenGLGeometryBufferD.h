#pragma once

#include "../../../abstract/scene/model/IGeometryBuffer.h"
#include "ModelD.h"
#include "../../gl_header.h"

#include "../../../abstract/IDisposable.h"

class OpenGLGeometryBufferD : public IGeometryBuffer, public IDisposable
{
	VisitorGeometryBufferCreateAndDrawSmartPtr visitor;

	GLuint glVertexBuffer;
	GLuint glIndexBuffer;
	GLuint glVao;
	GLuint shaderProgram;
	std::vector<ModelD> models;
	GLint worldLocation;
	GLint materialLocation;
	GLint normalsLocation;

	std::vector<VertexStaticLight> vertices;
public:
	void init(const std::wstring& vs, const std::wstring& ps, VisitorGeometryBufferCreateAndDrawSmartPtr& visitor) override;
	void reinit() override;
	void draw() override;
	bool addModel(const ModelParameters& mp)override;
	void clear() override;

	GLuint& getVertexBuffer();
	GLuint& getIndexBuffer();
	GLuint& getVao();
	GLuint& getShaderProgram();
	GLuint getVertexBuffer()const;
	GLuint getIndexBuffer()const;
	GLuint getVao()const;
	GLuint getShaderProgram()const;
	void setVertexBuffer(GLuint verticesColor);
	void setIndexBuffer(GLuint verticesColor);
	void setVao(GLuint verticesColor);
	void setShaderProgram(GLuint verticesColor);

	std::vector<ModelD>& getModels();
	GLint& getWorldLocation();
	GLint& getMaterialLocation();
	GLint& getNormalsLocation();

	~OpenGLGeometryBufferD();

	std::vector<VertexStaticLight>& getVertices();
};

class DeleterOpenGLGeometryBufferD
{
public:
	void operator()(OpenGLGeometryBufferD* gBuff);
};


using OpenGLGeometryBufferDSmartPtr = std::shared_ptr<OpenGLGeometryBufferD>;

// ������� ������:
// init

// �������� ->
// addModel (������� ������)
// reinit
