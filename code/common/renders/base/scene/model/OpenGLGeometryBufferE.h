#pragma once

#include "../../../abstract/scene/model/IGeometryBuffer.h"
#include "ModelE.h"
#include "../../gl_header.h"

#include "../../../abstract/IDisposable.h"

class OpenGLGeometryBufferE : public IGeometryBuffer, public IDisposable
{
	VisitorGeometryBufferCreateAndDrawSmartPtr visitor;

	GLuint glVertexBuffer;
	GLuint glIndexBuffer;
	GLuint glVao;
	GLuint shaderProgram;
	std::vector<ModelE> models;
	GLint worldLocation;
	GLint materialLocation;
	GLint normalsLocation;

	std::vector<VertexStaticLight> vertices;
public:
	void init(const std::wstring& vs, const std::wstring& ps, VisitorGeometryBufferCreateAndDrawSmartPtr& visitor) override;
	void reinit() override;
	void draw() override;
	bool addModel(const ModelParameters& mp)override;
	void clear() override;

	GLuint& getVertexBuffer();
	GLuint& getIndexBuffer();
	GLuint& getVao();
	GLuint& getShaderProgram();
	GLuint getVertexBuffer()const;
	GLuint getIndexBuffer()const;
	GLuint getVao()const;
	GLuint getShaderProgram()const;
	void setVertexBuffer(GLuint verticesColor);
	void setIndexBuffer(GLuint verticesColor);
	void setVao(GLuint verticesColor);
	void setShaderProgram(GLuint verticesColor);

	std::vector<ModelE>& getModels();
	GLint& getWorldLocation();
	GLint& getMaterialLocation();
	GLint& getNormalsLocation();

	~OpenGLGeometryBufferE();

	std::vector<VertexStaticLight>& getVertices();
};

class DeleterOpenGLGeometryBufferE
{
public:
	void operator()(OpenGLGeometryBufferE* gBuff);
};


using OpenGLGeometryBufferESmartPtr = std::shared_ptr<OpenGLGeometryBufferE>;

// ������� ������:
// init

// �������� ->
// addModel (������� ������)
// reinit
