#pragma once

#include "../../../abstract/scene/model/IGeometryBuffer.h"
#include "ModelE.h"
#include "../../d3d11_header.h"

class Direct3D11GeometryBufferE : public IGeometryBuffer // ����� ������������ ����� �����
{
	ComPtr<ID3D11Buffer> d3dVertexBuffer;
	ComPtr<ID3D11Buffer> d3dIndexBuffer;
	ComPtr<ID3D11InputLayout> d3dInputLayout;
	ComPtr<ID3D11VertexShader> d3dVertexShader;
	ComPtr<ID3D11PixelShader> d3dPixelShader;
	VisitorGeometryBufferCreateAndDrawSmartPtr visitor;
	std::vector<ModelE> models;
	ComPtr<ID3D11Buffer> constantsBufferWorld;
	ComPtr<ID3D11Buffer> constantsBufferMaterial;

	std::vector<VertexStaticLight> vertices;
public:
	void init(const std::wstring& vs, const std::wstring& ps, VisitorGeometryBufferCreateAndDrawSmartPtr& visitor) override;
	void reinit() override;
	void draw() override;
	bool addModel(const ModelParameters& mp)override;

	ComPtr<ID3D11Buffer>& getVertexBuffer();
	ComPtr<ID3D11Buffer>& getIndexBuffer();
	ComPtr<ID3D11InputLayout>& getInputLayout();
	ComPtr<ID3D11VertexShader>& getVertexShader();
	ComPtr<ID3D11PixelShader>& getPixelShader();

	std::vector<ModelE>& getModels();
	ComPtr<ID3D11Buffer>& getConstantBufferWorld();
	ComPtr<ID3D11Buffer>& getConstantsBufferMaterial();

	std::vector<VertexStaticLight>& getVertices();
};

using Direct3D11GeometryBufferESmartPtr = std::shared_ptr<Direct3D11GeometryBufferE>;
