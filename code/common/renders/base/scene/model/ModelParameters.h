#pragma once

#include "../../math_header.h"
#include "VertexColor.h"
#include "VertexTexture.h"
#include "VertexStaticLight.h"
#include "Index.h"

enum class TypeGeometryBuffer : int
{
	TYPE_GEOMETRY_BUFFER_A = 0, // ������: ����
	TYPE_GEOMETRY_BUFFER_B, // ������: ����, 1 �������
	TYPE_GEOMETRY_BUFFER_C, // ������: 1 ��������, 1 �������
	TYPE_GEOMETRY_BUFFER_D, // ������: 1 ��������, ����������� ��������� (1 directional, 1 point, 1 spot | phong & blinn - phong | ambient, diffuse, specular), 1 �������, 1 ��������
	TYPE_GEOMETRY_BUFFER_E // ������: ����, ����������� ��������� (1 directional, 1 point, 1 spot | phong & blinn - phong | ambient, diffuse, specular), 1 �������, 1 ��������
};

struct ModelParameters
{
	TypeGeometryBuffer type;
	std::vector<VertexColor> verticesColor;
	std::vector<VertexTexture> verticesTexture;
	std::vector<VertexStaticLight> verticesStaticLight;
	std::vector<Index> indeces;
	gmath::Matrix world;
	gmath::Matrix normals;
	std::wstring key;
	std::wstring textureKey;
	std::wstring samplerKey;
	std::wstring materialKey;
};