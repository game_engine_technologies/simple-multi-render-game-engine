#pragma once

class Direct3D11GeometryBufferA;
class OpenGLGeometryBufferA;
class Direct3D11GeometryBufferB;
class OpenGLGeometryBufferB;
class Direct3D11GeometryBufferC;
class OpenGLGeometryBufferC;
class Direct3D11GeometryBufferD;
class OpenGLGeometryBufferD;
class Direct3D11GeometryBufferE;
class OpenGLGeometryBufferE;

#include "../camera/Direct3D11Camera.h"
#include "../camera/OpenGLCamera.h"

#include "../../shaders/Direct3D11ShadersBuffer.h"
#include "../../shaders/OpenGLShadersBuffer.h"

#include "../../../../utils/base/ExceptionDirect3D11GeometryBuffer.h"
#include "../../../../utils/base/ExceptionOpenGLGeometryBuffer.h"

#include "../../textures/Direct3D11TexturesBuffer.h"
#include "../../textures/OpenGLTexturesBuffer.h"
#include "../../../abstract/scene/light/IStaticLight.h"
#include "../../../abstract/materials/IMaterialsBuffer.h"

class IGeometryBufferVisitorCreator
{
public:
	virtual void create(const std::wstring& vs, const std::wstring& ps, Direct3D11GeometryBufferA& a) = 0;
	virtual void create(const std::wstring& vs, const std::wstring& ps, OpenGLGeometryBufferA& a) = 0;
	virtual void reinit(Direct3D11GeometryBufferA& a) = 0;
	virtual void reinit(OpenGLGeometryBufferA& a) = 0;
	virtual void create(const std::wstring& vs, const std::wstring& ps, Direct3D11GeometryBufferB& a) = 0;
	virtual void create(const std::wstring& vs, const std::wstring& ps, OpenGLGeometryBufferB& a) = 0;
	virtual void reinit(Direct3D11GeometryBufferB& a) = 0;
	virtual void reinit(OpenGLGeometryBufferB& a) = 0;
	virtual void create(const std::wstring& vs, const std::wstring& ps, Direct3D11GeometryBufferC& a) = 0;
	virtual void create(const std::wstring& vs, const std::wstring& ps, OpenGLGeometryBufferC& a) = 0;
	virtual void reinit(Direct3D11GeometryBufferC& a) = 0;
	virtual void reinit(OpenGLGeometryBufferC& a) = 0;
	virtual void create(const std::wstring& vs, const std::wstring& ps, Direct3D11GeometryBufferD& a) = 0;
	virtual void create(const std::wstring& vs, const std::wstring& ps, OpenGLGeometryBufferD& a) = 0;
	virtual void reinit(Direct3D11GeometryBufferD& a) = 0;
	virtual void reinit(OpenGLGeometryBufferD& a) = 0;
	virtual void create(const std::wstring& vs, const std::wstring& ps, Direct3D11GeometryBufferE& a) = 0;
	virtual void create(const std::wstring& vs, const std::wstring& ps, OpenGLGeometryBufferE& a) = 0;
	virtual void reinit(Direct3D11GeometryBufferE& a) = 0;
	virtual void reinit(OpenGLGeometryBufferE& a) = 0;
};

class IGeometryBufferVisitorDrawer
{
public:
	virtual void draw(Direct3D11GeometryBufferA& a) = 0;
	virtual void draw(OpenGLGeometryBufferA& a) = 0;
	virtual void draw(Direct3D11GeometryBufferB& a) = 0;
	virtual void draw(OpenGLGeometryBufferB& a) = 0;
	virtual void draw(Direct3D11GeometryBufferC& a) = 0;
	virtual void draw(OpenGLGeometryBufferC& a) = 0;
	virtual void draw(Direct3D11GeometryBufferD& a) = 0;
	virtual void draw(OpenGLGeometryBufferD& a) = 0;
	virtual void draw(Direct3D11GeometryBufferE& a) = 0;
	virtual void draw(OpenGLGeometryBufferE& a) = 0;
};

class VisitorGeometryBufferCreateAndDraw : public IGeometryBufferVisitorCreator, public IGeometryBufferVisitorDrawer
{
	Direct3D11ShadersBufferSmartPtr shadersBufferD3d11;
	OpenGLShadersBufferSmartPtr shadersBufferOpenGl;
	ComPtr<ID3D11Device> d3dDevice;
	ComPtr<ID3D11DeviceContext> d3dImmediateContext;
	Direct3D11TexturesBufferSmartPtr texturesBufferD3d11;
	OpenGLTexturesBufferSmartPtr texturesBufferOpenGl;
	CameraSmartPtr camera;
	StaticLightSmartPtr staticLight;
	MaterialsBufferSmartPtr materialsBuffer;

public:
	VisitorGeometryBufferCreateAndDraw(const ComPtr<ID3D11Device>& d3dDevice, const ComPtr<ID3D11DeviceContext>& d3dImmediateContext,
		const Direct3D11ShadersBufferSmartPtr& shadersBufferD3d11);
	VisitorGeometryBufferCreateAndDraw(const OpenGLShadersBufferSmartPtr& shadersBufferOpenGl);
	
	VisitorGeometryBufferCreateAndDraw(const ComPtr<ID3D11Device>& d3dDevice, const ComPtr<ID3D11DeviceContext>& d3dImmediateContext,
		const Direct3D11ShadersBufferSmartPtr& shadersBufferD3d11, const CameraSmartPtr& d3dCamera);
	VisitorGeometryBufferCreateAndDraw(const OpenGLShadersBufferSmartPtr& shadersBufferOpenGl, const CameraSmartPtr& openGlCamera);
	
	VisitorGeometryBufferCreateAndDraw(const ComPtr<ID3D11Device>& d3dDevice, const ComPtr<ID3D11DeviceContext>& d3dImmediateContext,
		const Direct3D11ShadersBufferSmartPtr& shadersBufferD3d11, const CameraSmartPtr& d3dCamera,
		const Direct3D11TexturesBufferSmartPtr& texturesBufferD3d11);
	VisitorGeometryBufferCreateAndDraw(const OpenGLShadersBufferSmartPtr& shadersBufferOpenGl, const CameraSmartPtr& openGlCamera,
		const OpenGLTexturesBufferSmartPtr& texturesBufferOpenGl);

	VisitorGeometryBufferCreateAndDraw(const ComPtr<ID3D11Device>& d3dDevice, const ComPtr<ID3D11DeviceContext>& d3dImmediateContext,
		const Direct3D11ShadersBufferSmartPtr& shadersBufferD3d11, const CameraSmartPtr& d3dCamera,
		const Direct3D11TexturesBufferSmartPtr& texturesBufferD3d11, const StaticLightSmartPtr& staticLight, 
		const MaterialsBufferSmartPtr& materialsBuffer);
	VisitorGeometryBufferCreateAndDraw(const OpenGLShadersBufferSmartPtr& shadersBufferGL, const CameraSmartPtr& camera,
		const OpenGLTexturesBufferSmartPtr& texturesBufferGL, const StaticLightSmartPtr& staticLight,
		const MaterialsBufferSmartPtr& materialsBuffer);

	void create(const std::wstring& vs, const std::wstring& ps, Direct3D11GeometryBufferA& a) override;
	void create(const std::wstring& vs, const std::wstring& ps, OpenGLGeometryBufferA& a) override;
	void reinit(Direct3D11GeometryBufferA& a) override;
	void reinit(OpenGLGeometryBufferA& a) override;
	void draw(Direct3D11GeometryBufferA& a) override;
	void draw(OpenGLGeometryBufferA& a) override;

	void create(const std::wstring& vs, const std::wstring& ps, Direct3D11GeometryBufferB& a) override;
	void create(const std::wstring& vs, const std::wstring& ps, OpenGLGeometryBufferB& a) override;
	void reinit(Direct3D11GeometryBufferB& a) override;
	void reinit(OpenGLGeometryBufferB& a) override;
	void draw(Direct3D11GeometryBufferB& a) override;
	void draw(OpenGLGeometryBufferB& a) override;

	void create(const std::wstring& vs, const std::wstring& ps, Direct3D11GeometryBufferC& a) override;
	void create(const std::wstring& vs, const std::wstring& ps, OpenGLGeometryBufferC& a) override;
	void reinit(Direct3D11GeometryBufferC& a) override;
	void reinit(OpenGLGeometryBufferC& a) override;
	void draw(Direct3D11GeometryBufferC& a) override;
	void draw(OpenGLGeometryBufferC& a) override;

	void create(const std::wstring& vs, const std::wstring& ps, Direct3D11GeometryBufferD& a) override;
	void create(const std::wstring& vs, const std::wstring& ps, OpenGLGeometryBufferD& a) override;
	void reinit(Direct3D11GeometryBufferD& a) override;
	void reinit(OpenGLGeometryBufferD& a) override;
	void draw(Direct3D11GeometryBufferD& a) override;
	void draw(OpenGLGeometryBufferD& a) override;

	void create(const std::wstring& vs, const std::wstring& ps, Direct3D11GeometryBufferE& a) override;
	void create(const std::wstring& vs, const std::wstring& ps, OpenGLGeometryBufferE& a) override;
	void reinit(Direct3D11GeometryBufferE& a) override;
	void reinit(OpenGLGeometryBufferE& a) override;
	void draw(Direct3D11GeometryBufferE& a) override;
	void draw(OpenGLGeometryBufferE& a) override;
};
