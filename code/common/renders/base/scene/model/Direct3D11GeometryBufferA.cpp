#include "Direct3D11GeometryBufferA.h"

#include "IGeometryBufferVisitor.h"

void Direct3D11GeometryBufferA::init(const std::wstring& vs, const std::wstring& ps, VisitorGeometryBufferCreateAndDrawSmartPtr& visitor)
{
	clearFlags();
	this->visitor = visitor;
	this->visitor->create(vs, ps, *this);
}

void Direct3D11GeometryBufferA::reinit()
{
	visitor->reinit(*this);
}

void Direct3D11GeometryBufferA::draw()
{
	visitor->draw(*this);
}

bool Direct3D11GeometryBufferA::addModel(const ModelParameters& mp)
{
	if (mp.type == TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_A)
	{
		if (mp.verticesColor.size() == 0 || mp.indeces.size() == 0)
		{
			debug::printError(L"Error: vertex buffer or index buffer is empty");
			return false;
		}
		this->vertices.insert(this->vertices.end(), mp.verticesColor.begin(), mp.verticesColor.end());
		int size(this->indeces.size());
		if (this->indeces.size() != 0)
			std::transform(mp.indeces.begin(), mp.indeces.end(), std::back_inserter(this->indeces), [&size](Index indeces)
				{
					return indeces + size;
				});
		else
			this->indeces.insert(this->indeces.end(), mp.indeces.begin(), mp.indeces.end());
		_updateAll = true;
		return true;
	}
	debug::printError(L"Error type model for Direct3D11GeometryBufferA");
	return false;
}

ComPtr<ID3D11Buffer>& Direct3D11GeometryBufferA::getVertexBuffer()
{
	return d3dVertexBuffer;
}

ComPtr<ID3D11Buffer>& Direct3D11GeometryBufferA::getIndexBuffer()
{
	return d3dIndexBuffer;
}

ComPtr<ID3D11InputLayout>& Direct3D11GeometryBufferA::getInputLayout()
{
	return d3dInputLayout;
}

ComPtr<ID3D11VertexShader>& Direct3D11GeometryBufferA::getVertexShader()
{
	return d3dVertexShader;
}

ComPtr<ID3D11PixelShader>& Direct3D11GeometryBufferA::getPixelShader()
{
	return d3dPixelShader;
}

std::vector<VertexColor>& Direct3D11GeometryBufferA::getVertices()
{
	return vertices;
}