#include "OpenGLGeometryBufferC.h"

#include "IGeometryBufferVisitor.h"

void OpenGLGeometryBufferC::init(const std::wstring& vs, const std::wstring& ps, VisitorGeometryBufferCreateAndDrawSmartPtr& visitor)
{
	clearFlags();
	this->visitor = visitor;
	this->visitor->create(vs, ps, *this);
}

void OpenGLGeometryBufferC::reinit()
{
	visitor->reinit(*this);
}

void OpenGLGeometryBufferC::draw()
{
	visitor->draw(*this);
}

bool OpenGLGeometryBufferC::addModel(const ModelParameters& mp)
{
	ModelC model;
	if (mp.type == TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_C)
	{
		if (mp.key == L"")
		{
			debug::printError(L"Error: key is empty");
			return false;
		}
		auto iter = std::find_if(models.begin(), models.end(), [&mp](const ModelC& m)
			{
				return mp.key == m.getKey();
			});
		if (iter == models.end()) // ������ ��� - �������� �������
		{
			if (mp.verticesTexture.size() == 0 || mp.indeces.size() == 0)
			{
				debug::printError(L"Error: vertex buffer or index buffer is empty");
				return false;
			}
			int size(this->vertices.size());
			this->vertices.insert(this->vertices.end(), mp.verticesTexture.begin(), mp.verticesTexture.end());
			model.setOffsetIndex(this->indeces.size());
			std::transform(mp.indeces.begin(), mp.indeces.end(), std::back_inserter(this->indeces), [&size](Index indeces)
				{
					return indeces + size;
				});
			model.setCountIndex(mp.indeces.size());
			model.setKey(mp.key);
			model.setStartIndex(indeces.size() - mp.indeces.size());
			model.setWorld(mp.world);
			_updateAll = true;
		}
		else // ������ ���� - ��������� � ������ �����
		{
			// ��� ����� ������������ ������� ��������
			model.setCountIndex(iter->getCountIndex());
			model.setKey(iter->getKey());
			model.setOffsetIndex(iter->getOffsetIndex());
			model.setStartIndex(iter->getStartIndex());
			model.setWorld(mp.world);
		}
		model.setTextureKey(mp.textureKey);
		model.setSamplerKey(mp.samplerKey);
		models.push_back(model);
		return true;
	}
	debug::printError(L"Error type model for Direct3D11GeometryBufferB");
	return false;
}

void OpenGLGeometryBufferC::clear()
{
	OpenGLFunctional::getInstance()->glDeleteVertexArrays(1, &glVao);
	OpenGLFunctional::getInstance()->glDeleteBuffers(1, &glVertexBuffer);
	OpenGLFunctional::getInstance()->glDeleteBuffers(1, &glIndexBuffer);
}

GLuint& OpenGLGeometryBufferC::getVertexBuffer()
{
	return glVertexBuffer;
}

GLuint& OpenGLGeometryBufferC::getIndexBuffer()
{
	return glIndexBuffer;
}

GLuint& OpenGLGeometryBufferC::getVao()
{
	return glVao;
}

GLuint& OpenGLGeometryBufferC::getShaderProgram()
{
	return shaderProgram;
}

GLuint OpenGLGeometryBufferC::getVertexBuffer()const
{
	return glVertexBuffer;
}

GLuint OpenGLGeometryBufferC::getIndexBuffer()const
{
	return glIndexBuffer;
}

GLuint OpenGLGeometryBufferC::getVao()const
{
	return glVao;
}

GLuint OpenGLGeometryBufferC::getShaderProgram()const
{
	return shaderProgram;
}

void OpenGLGeometryBufferC::setVertexBuffer(GLuint verticesColor)
{
	glVertexBuffer = verticesColor;
}

void OpenGLGeometryBufferC::setIndexBuffer(GLuint verticesColor)
{
	glIndexBuffer = verticesColor;
}

void OpenGLGeometryBufferC::setVao(GLuint verticesColor)
{
	glVao = verticesColor;
}

void OpenGLGeometryBufferC::setShaderProgram(GLuint verticesColor)
{
	shaderProgram = verticesColor;
}

OpenGLGeometryBufferC::~OpenGLGeometryBufferC()
{
	clear();
}

std::vector<ModelC>& OpenGLGeometryBufferC::getModels()
{
	return models;
}

GLint& OpenGLGeometryBufferC::getWorldLocation()
{
	return worldLocation;
}

void DeleterOpenGLGeometryBuffer�::operator()(OpenGLGeometryBufferC* gBuff)
{
	if (gBuff != nullptr)
	{
		OpenGLFunctional::getInstance()->glDeleteVertexArrays(1, &gBuff->getVao());
		OpenGLFunctional::getInstance()->glDeleteBuffers(1, &gBuff->getVertexBuffer());
		OpenGLFunctional::getInstance()->glDeleteBuffers(1, &gBuff->getIndexBuffer());
	}
}

std::vector<VertexTexture>& OpenGLGeometryBufferC::getVertices()
{
	return vertices;
}
