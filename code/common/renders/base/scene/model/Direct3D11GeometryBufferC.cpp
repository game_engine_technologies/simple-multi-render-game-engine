#include "Direct3D11GeometryBufferC.h"

#include "IGeometryBufferVisitor.h"

void Direct3D11GeometryBufferC::init(const std::wstring& vs, const std::wstring& ps, VisitorGeometryBufferCreateAndDrawSmartPtr& visitor)
{
	//newSizeConstBuffWorld = 0;
	clearFlags();
	this->visitor = visitor;
	this->visitor->create(vs, ps, *this);
}

void Direct3D11GeometryBufferC::reinit()
{
	visitor->reinit(*this);
}

void Direct3D11GeometryBufferC::draw()
{
	visitor->draw(*this);
}


ComPtr<ID3D11Buffer>& Direct3D11GeometryBufferC::getVertexBuffer()
{
	return d3dVertexBuffer;
}

ComPtr<ID3D11Buffer>& Direct3D11GeometryBufferC::getIndexBuffer()
{
	return d3dIndexBuffer;
}

ComPtr<ID3D11InputLayout>& Direct3D11GeometryBufferC::getInputLayout()
{
	return d3dInputLayout;
}

ComPtr<ID3D11VertexShader>& Direct3D11GeometryBufferC::getVertexShader()
{
	return d3dVertexShader;
}

ComPtr<ID3D11PixelShader>& Direct3D11GeometryBufferC::getPixelShader()
{
	return d3dPixelShader;
}

bool Direct3D11GeometryBufferC::addModel(const ModelParameters& mp)
{
	ModelC model;
	if (mp.type == TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_C)
	{
		if (mp.key == L"")
		{
			debug::printError(L"Error: key is empty");
			return false;
		}
		auto iter = std::find_if(models.begin(), models.end(), [&mp](const ModelC& m)
			{
				return mp.key == m.getKey();
			});
		if (iter == models.end()) // ������ ��� - �������� �������
		{
			if (mp.verticesTexture.size() == 0 || mp.indeces.size() == 0)
			{
				debug::printError(L"Error: vertex buffer or index buffer is empty");
				return false;
			}
			int size(this->vertices.size());
			this->vertices.insert(this->vertices.end(), mp.verticesTexture.begin(), mp.verticesTexture.end());
			model.setOffsetIndex(this->indeces.size());
			std::transform(mp.indeces.begin(), mp.indeces.end(), std::back_inserter(this->indeces), [&size](Index indeces)
				{
					return indeces + size;
				});
			model.setCountIndex(mp.indeces.size());
			model.setKey(mp.key);
			model.setStartIndex(indeces.size() - mp.indeces.size());
			model.setWorld(mp.world);
			_updateAll = true;
		}
		else // ������ ���� - ��������� � ������ �����
		{
			// ��� ����� ������������ ������� ��������
			model.setCountIndex(iter->getCountIndex());
			model.setKey(iter->getKey());
			model.setOffsetIndex(iter->getOffsetIndex());
			model.setStartIndex(iter->getStartIndex());
			model.setWorld(mp.world);
		}
		/*ComPtr<ID3D11Buffer> tmp;
		ConstantBufferWorld cbw;
		cbw.world = gmath::Matrix::Transponse(model.getWorld());
		d3dImmediateContext->UpdateSubresource(tmp.Get(), 0, NULL, &cbw, 0, 0);*/
		//newSizeConstBuffWorld++;
		model.setTextureKey(mp.textureKey);
		model.setSamplerKey(mp.samplerKey);
		models.push_back(model);
		return true;
	}
	debug::printError(L"Error type model for Direct3D11GeometryBufferC");
	return false;
}

std::vector<ModelC>& Direct3D11GeometryBufferC::getModels()
{
	return models;
}

//int& Direct3D11GeometryBufferC::getNewSizeConstBuffWorld()
//{
//	return newSizeConstBuffWorld;
//}

ComPtr<ID3D11Buffer>& Direct3D11GeometryBufferC::getConstantBufferWorld()
{
	return constantsBufferWorld;
}

//std::vector<ComPtr<ID3D11Buffer>>& Direct3D11GeometryBufferC::getConstantsBufferWorld()
//{
//	return constantsBufferWorld;
//}

std::vector<VertexTexture>& Direct3D11GeometryBufferC::getVertices()
{
	return vertices;
}