#include "ModelC.h"

unsigned ModelC::getStartIndex() const
{
	return startIndex;
}

unsigned ModelC::getOffsetIndex() const
{
	return offsetIndex;
}

unsigned ModelC::getCountIndex() const
{
	return countIndex;
}

std::wstring ModelC::getKey()const
{
	return key;
}

gmath::Matrix ModelC::getWorld() const
{
	return world;
}

std::wstring ModelC::getTextureKey() const
{
	return textureKey;
}

std::wstring ModelC::getSamplerKey() const
{
	return samplerKey;
}

void ModelC::setStartIndex(unsigned si)
{
	startIndex = si;
}

void ModelC::setOffsetIndex(unsigned oi)
{
	offsetIndex = oi;
}

void ModelC::setCountIndex(unsigned ci)
{
	countIndex = ci;
}

void ModelC::setKey(const std::wstring& n)
{
	key = n;
}

void ModelC::setWorld(const gmath::Matrix& w)
{
	world = w;
}

void ModelC::setTextureKey(const std::wstring& n)
{
	textureKey = n;
}

void ModelC::setSamplerKey(const std::wstring& n)
{
	samplerKey = n;
}
