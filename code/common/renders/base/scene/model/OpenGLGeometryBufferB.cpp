#include "OpenGLGeometryBufferB.h"

#include "IGeometryBufferVisitor.h"

void OpenGLGeometryBufferB::init(const std::wstring& vs, const std::wstring& ps, VisitorGeometryBufferCreateAndDrawSmartPtr& visitor)
{
	clearFlags();
	this->visitor = visitor;
	this->visitor->create(vs, ps, *this);
}

void OpenGLGeometryBufferB::reinit()
{
	visitor->reinit(*this);
}

void OpenGLGeometryBufferB::draw()
{
	visitor->draw(*this);
}

bool OpenGLGeometryBufferB::addModel(const ModelParameters& mp)
{
	ModelB model;
	if (mp.type == TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_B)
	{
		if (mp.key == L"")
		{
			debug::printError(L"Error: key is empty");
			return false;
		}
		auto iter = std::find_if(models.begin(), models.end(), [&mp](const ModelB& m)
			{
				return mp.key == m.getKey();
			});
		if (iter == models.end()) // ������ ��� - �������� �������
		{
			if (mp.verticesColor.size() == 0 || mp.indeces.size() == 0)
			{
				debug::printError(L"Error: vertex buffer or index buffer is empty");
				return false;
			}
			int size(this->vertices.size());
			this->vertices.insert(this->vertices.end(), mp.verticesColor.begin(), mp.verticesColor.end());
			model.setOffsetIndex(this->indeces.size());
			std::transform(mp.indeces.begin(), mp.indeces.end(), std::back_inserter(this->indeces), [&size](Index indeces)
				{
					return indeces + size;
				});
			model.setCountIndex(mp.indeces.size());
			model.setKey(mp.key);
			model.setStartIndex(indeces.size() - mp.indeces.size());
			model.setWorld(mp.world);
			models.push_back(model);
			_updateAll = true;
		}
		else // ������ ���� - ��������� � ������ �����
		{
			// ��� ����� ������������ ������� ��������
			model.setCountIndex(iter->getCountIndex());
			model.setKey(iter->getKey());
			model.setOffsetIndex(iter->getOffsetIndex());
			model.setStartIndex(iter->getStartIndex());
			model.setWorld(mp.world);
			models.push_back(model);
		}
		return true;
	}
	debug::printError(L"Error type model for Direct3D11GeometryBufferB");
	return false;
}

void OpenGLGeometryBufferB::clear()
{
	OpenGLFunctional::getInstance()->glDeleteVertexArrays(1, &glVao);
	OpenGLFunctional::getInstance()->glDeleteBuffers(1, &glVertexBuffer);
	OpenGLFunctional::getInstance()->glDeleteBuffers(1, &glIndexBuffer);
}

GLuint& OpenGLGeometryBufferB::getVertexBuffer()
{
	return glVertexBuffer;
}

GLuint& OpenGLGeometryBufferB::getIndexBuffer()
{
	return glIndexBuffer;
}

GLuint& OpenGLGeometryBufferB::getVao()
{
	return glVao;
}

GLuint& OpenGLGeometryBufferB::getShaderProgram()
{
	return shaderProgram;
}

GLuint OpenGLGeometryBufferB::getVertexBuffer()const
{
	return glVertexBuffer;
}

GLuint OpenGLGeometryBufferB::getIndexBuffer()const
{
	return glIndexBuffer;
}

GLuint OpenGLGeometryBufferB::getVao()const
{
	return glVao;
}

GLuint OpenGLGeometryBufferB::getShaderProgram()const
{
	return shaderProgram;
}

void OpenGLGeometryBufferB::setVertexBuffer(GLuint verticesColor)
{
	glVertexBuffer = verticesColor;
}

void OpenGLGeometryBufferB::setIndexBuffer(GLuint verticesColor)
{
	glIndexBuffer = verticesColor;
}

void OpenGLGeometryBufferB::setVao(GLuint verticesColor)
{
	glVao = verticesColor;
}

void OpenGLGeometryBufferB::setShaderProgram(GLuint verticesColor)
{
	shaderProgram = verticesColor;
}

OpenGLGeometryBufferB::~OpenGLGeometryBufferB()
{
	clear();
}

std::vector<ModelB>& OpenGLGeometryBufferB::getModels()
{
	return models;
}

GLint& OpenGLGeometryBufferB::getWorldLocation()
{
	return worldLocation;
}

void DeleterOpenGLGeometryBufferB::operator()(OpenGLGeometryBufferB* gBuff)
{
	if (gBuff != nullptr)
	{
		OpenGLFunctional::getInstance()->glDeleteVertexArrays(1, &gBuff->getVao());
		OpenGLFunctional::getInstance()->glDeleteBuffers(1, &gBuff->getVertexBuffer());
		OpenGLFunctional::getInstance()->glDeleteBuffers(1, &gBuff->getIndexBuffer());
	}
}

std::vector<VertexColor>& OpenGLGeometryBufferB::getVertices()
{
	return vertices;
}
