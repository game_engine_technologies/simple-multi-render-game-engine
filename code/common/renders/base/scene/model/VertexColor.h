#pragma once

#include "../../math_header.h"

struct VertexColor
{
	gmath::Point3 position;
	gmath::Color color;
};