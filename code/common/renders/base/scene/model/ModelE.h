#pragma once

#include <string>
#include <vector>
#include "../../math_header.h"

#include "Buffers.h"

class ModelE
{
	unsigned startIndex;
	unsigned offsetIndex;
	unsigned countIndex;
	std::wstring key;
	gmath::Matrix world;
	gmath::Matrix normals;
	std::wstring materialKey;

public:
	unsigned getStartIndex() const;
	unsigned getOffsetIndex() const;
	unsigned getCountIndex()const;
	std::wstring getKey()const;
	gmath::Matrix getWorld()const;
	gmath::Matrix getNormals()const;
	std::wstring getMaterialKey()const;

	void setStartIndex(unsigned si);
	void setOffsetIndex(unsigned oi);
	void setCountIndex(unsigned ci);
	void setKey(const std::wstring& n);
	void setWorld(const gmath::Matrix& w);
	void setNormals(const gmath::Matrix& w);
	void setMaterialKey(const std::wstring& n);
};