#include "Direct3D11Scene.h"

#include "../shaders/IShadersBufferVisitor.h"
#include "../../../utils/base/loaders/TexturesLoaderSOIL.h"
#include "../textures/ITexturesBufferVisitor.h"
#include "../materials/MaterialsBuffer.h"


Direct3D11Scene::Direct3D11Scene(const ComPtr<ID3D11Device>& d3dDevice, 
	const ComPtr<ID3D11DeviceContext>& d3dImmediateContext):d3dDevice(d3dDevice),
	d3dImmediateContext(d3dImmediateContext)
{
}

void Direct3D11Scene::init(const std::wstring& path, SceneParameters rp)
{
	// ��������� ��������� ����� (����� �� ���� � ��)

	// materials buffer
	materialsBuffer = std::make_shared<MaterialsBuffer>();
	if (!materialsBuffer)
		throw ExceptionMemory(L"MaterialsBuffer is not create, memory not found!");
	materialsBuffer->init(L"configs/materials.pCfg");

	// shaders buffer
	shadersBuffer = std::make_shared<Direct3D11ShadersBuffer>(L"5_0");
	if (!shadersBuffer)
		throw ExceptionMemory(L"Direct3D11ShadersBuffer is not create, memory not found!");
	auto visitorSB = std::make_shared<VisitorShaderBufferCreateAndLoader>(d3dDevice);
	if (!visitorSB)
		throw ExceptionMemory(L"VisitorShaderBufferCreateAndLoader is not create, memory not found!");
	shadersBuffer->init(L"shaders/d3d11/", visitorSB);

	// textures buffer
	texturesBuffer = std::make_shared<Direct3D11TexturesBuffer>();
	if (!texturesBuffer)
		throw ExceptionMemory(L"Direct3D11TexturesBuffer is not create, memory not found!");
	auto visitorTB = std::make_shared<TexturesBufferVisitorCreatorAndAdded>(d3dDevice);
	if (!visitorTB)
		throw ExceptionMemory(L"TexturesBufferVisitorCreatorAndAdded is not create, memory not found!");
	auto texturesLoader = std::make_shared<TexturesLoaderSOIL>();
	if (!texturesLoader)
		throw ExceptionMemory(L"TexturesLoaderSOIL is not create, memory not found!");
	texturesBuffer->init(L"textures", texturesLoader, visitorTB);
}

bool Direct3D11Scene::update(SceneDynamicParameters& sdp)
{
	// ��������� ������� � ��
	return true;
}

Direct3D11Scene::~Direct3D11Scene()
{
	clear();
}

void Direct3D11Scene::clear()
{
	// ��������� �������
}