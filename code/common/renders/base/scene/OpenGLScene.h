#pragma once

#include "../../abstract/scene/IScene.h"
#include "../../abstract/IDisposable.h"

#include "../shaders/OpenGLShadersBuffer.h"
#include "../../../utils/base/ExceptionMemory.h"
#include "../textures/OpenGLTexturesBuffer.h"

#include <memory>

class OpenGLScene : public IScene, public IDisposable
{
protected:
	OpenGLShadersBufferSmartPtr shadersBuffer;
	OpenGLTexturesBufferSmartPtr texturesBuffer;

public:
	virtual void init(const std::wstring& path, SceneParameters rp) override;
	virtual bool update(SceneDynamicParameters& sdp) override;

	virtual ~OpenGLScene()override;
	virtual void clear();

	virtual void render() = 0;
};

using OpenGLSceneSmartPtr = std::shared_ptr<OpenGLScene>;