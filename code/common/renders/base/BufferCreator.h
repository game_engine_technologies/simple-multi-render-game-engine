#pragma once

#include "d3d11_header.h"
#include "../../utils/base/ExceptionDirect3D11BufferCreator.h"

class Direct3D11CreatorBuffer
{
	static D3D11_BUFFER_DESC createD3D11_BUFFER_DESC(UINT _size, const D3D11_BIND_FLAG& bind_flag, const D3D11_USAGE& usage, int cpuAccessFlag);
public:
	template <class Data>
	static ComPtr<ID3D11Buffer> createBuffer(const ComPtr<ID3D11Device>& d3dDevice, UINT _size, const D3D11_BIND_FLAG& bind_flag, const D3D11_USAGE& usage, int cpuAccessFlag, Data* data)
	{
		ComPtr<ID3D11Buffer> bufferOut;
		D3D11_BUFFER_DESC bd = createD3D11_BUFFER_DESC(_size, bind_flag, usage, cpuAccessFlag);
		HRESULT hr;
		D3D11_SUBRESOURCE_DATA InitData; // ���������, ���������� ������ ������
		ZeroMemory(&InitData, sizeof(InitData));
		InitData.pSysMem = data;
		hr = d3dDevice->CreateBuffer(&bd, &InitData, bufferOut.GetAddressOf());
		if (FAILED(hr))
			throw ExceptionDirect3D11BufferCreator(L"error create buffer!"); // �������� ������ �����
		return bufferOut;
	}

	static ComPtr<ID3D11Buffer> createBuffer(const ComPtr<ID3D11Device>& d3dDevice, UINT _size, const D3D11_BIND_FLAG& bind_flag, const D3D11_USAGE& usage, int cpuAccessFlag);
};

