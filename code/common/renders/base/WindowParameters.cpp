#include "WindowParameters.h"

HINSTANCE WindowParameters::getHinstance()
{
	return hInst;
}

int WindowParameters::getWidth()
{
	return w;
}

int WindowParameters::getHeight()
{
	return h;
}

void WindowParameters::setHinstance(HINSTANCE hInst)
{
	this->hInst = hInst;
}

void WindowParameters::setWidth(int w)
{
	this->w = w;
}

void WindowParameters::setHeight(int h)
{
	this->h = h;
}