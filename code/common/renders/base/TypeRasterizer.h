#pragma once

enum class TypeRasterizer : int
{
	WIREFRAME_RASTERIZED = 0,
	SOLID_RASTERIZED
};