#pragma once

#include <d3d11.h>
#include <d3dcompiler.h>

#include "d3d11/dxerr.h"

#include <system_error>

#include <atlbase.h>
#include <wrl.h>

using namespace Microsoft::WRL;

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")