#pragma once

#include <string>
#include <memory>

#include "../../abstract/materials/IMaterialsBuffer.h"

class MaterialsBuffer: public IMaterialsBuffer
{
public:
	void init(const std::wstring& path) override;
};