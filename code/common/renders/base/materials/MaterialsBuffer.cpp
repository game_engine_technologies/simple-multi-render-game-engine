#include "MaterialsBuffer.h"
#include "../../../utils/base/loaders/MaterialsLoader.h"

void MaterialsBuffer::init(const std::wstring& path)
{
	configs::MaterialsLoader loader;
	buffer = std::move(loader.loadMaterials(path));
}
