#include "OSWindowsDirect3D11Render.h"
#include "SingletonHelper.h"

#include <psapi.h>
#pragma comment(lib, "psapi.lib")

OSWindowsDirect3D11Render::OSWindowsDirect3D11Render()
{
	sizeWindowChanged = false;
	running = false;
	vsync = false;
	fullscreen = false;
	d3dEnable4xMsaa = true;
}

void OSWindowsDirect3D11Render::init(RenderParameters rp)
{
	SingletonHelper::getInstance()->setCurrentRender(this);
	hwnd = rp.getHwnd();
	versionAPI = 11;

	desiredColourFormat = DXGI_FORMAT_R8G8B8A8_UNORM;
	D3D_FEATURE_LEVEL featureLevel;

	// �����
	UINT createDeviceFlags = 0;
#if defined(DEBUG) || defined(_DEBUG)  
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG; // ����� �������
#endif

	// ������� �������
	D3D_FEATURE_LEVEL featureLevels[] = {
		D3D_FEATURE_LEVEL_12_0,
		D3D_FEATURE_LEVEL_11_1,
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
		D3D_FEATURE_LEVEL_9_3,
		D3D_FEATURE_LEVEL_9_2,
		D3D_FEATURE_LEVEL_9_1,
	};

	// ������ � ������� ���������� ��� �������� ��������� 4xMSAA
	HRESULT hr = D3D11CreateDeviceAndSwapChain(/*this->adapter*/NULL, /*D3D_DRIVER_TYPE_UNKNOWN*/D3D_DRIVER_TYPE_HARDWARE,
		NULL, createDeviceFlags, featureLevels, sizeof(featureLevels) / sizeof(D3D_FEATURE_LEVEL), D3D11_SDK_VERSION, NULL,
		NULL, d3dDevice.GetAddressOf(), &featureLevel, d3dImmediateContext.GetAddressOf());
	if (FAILED(hr)) // ������� �� �������?
	{
		throw ExceptionDirect3D11(L"D3D11CreateDevice Failed.");
	}

	debug::checkHresult(d3dDevice->CheckMultisampleQualityLevels(desiredColourFormat, 4, &d3d4xMsaaQuality));
	if (d3d4xMsaaQuality <= 0)
	{
		throw ExceptionDirect3D11(L"MSAA quality is not corrected.");
	}
	clears::ReleaseCom(d3dDevice);
	clears::ReleaseCom(d3dImmediateContext);

	// ������ ������� ������
	DXGI_SWAP_CHAIN_DESC sd;
	sd.BufferDesc.Width = rp.getClientWidth();
	sd.BufferDesc.Height = rp.getClientHeight();
	sd.BufferDesc.RefreshRate.Numerator = 60;
	sd.BufferDesc.RefreshRate.Denominator = 1;
	sd.BufferDesc.Format = desiredColourFormat;
	sd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	sd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

	// ���������� 4xMSAA?
	if (d3dEnable4xMsaa)
	{
		sd.SampleDesc.Count = 4;
		sd.SampleDesc.Quality = d3d4xMsaaQuality - 1;
	}
	// �� ����������
	else
	{
		sd.SampleDesc.Count = 1;
		sd.SampleDesc.Quality = 0;
	}

	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.BufferCount = 1;
	sd.OutputWindow = hwnd;
	sd.Windowed = true;
	sd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	sd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

	hr = D3D11CreateDeviceAndSwapChain(/*this->adapter*/NULL, /*D3D_DRIVER_TYPE_UNKNOWN*/D3D_DRIVER_TYPE_HARDWARE,
		NULL, createDeviceFlags, featureLevels, sizeof(featureLevels) / sizeof(D3D_FEATURE_LEVEL), D3D11_SDK_VERSION, &sd,
		d3dSwapChain.GetAddressOf(), d3dDevice.GetAddressOf(), &featureLevel, d3dImmediateContext.GetAddressOf());
	if (FAILED(hr)) // ������� �� �������?
	{
		throw ExceptionDirect3D11(L"D3D11CreateDevice Failed.");
	}
	if (featureLevel < D3D_FEATURE_LEVEL_11_0) // �������� ����������� d3d11 �� ��������� ��
	{
		throw ExceptionDirect3D11(L"Direct3D Feature Level 11 unsupported.");
	}

	// ��������� ����� �������
	D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
	ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));
	depthStencilDesc.DepthEnable = true;
	depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
	depthStencilDesc.StencilEnable = true;
	depthStencilDesc.StencilReadMask = 0xFF;
	depthStencilDesc.StencilWriteMask = 0xFF;
	depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	hr = d3dDevice->CreateDepthStencilState(&depthStencilDesc, d3dDepthStencilEnableDepth.GetAddressOf());
	if (FAILED(hr))
		throw ExceptionDirect3D11(L"Direct3D error create d3dDepthStencilEnableDepth");

	depthStencilDesc.DepthEnable = false;
	hr = d3dDevice->CreateDepthStencilState(&depthStencilDesc, d3dDepthStencilDisableDepth.GetAddressOf());
	if (FAILED(hr))
		throw ExceptionDirect3D11(L"Direct3D Feature Level 11 unsupported.");

	//d3dImmediateContext->OMSetDepthStencilState(d3dDepthStencilEnableDepth.Get(), 1);

	// set props
	setTypeRasterizer(TypeRasterizer::SOLID_RASTERIZED);

	// disable alt + enter
	ComPtr<IDXGIDevice> pDXGIDevice;
	hr = d3dDevice->QueryInterface(__uuidof(IDXGIDevice), (void**)pDXGIDevice.GetAddressOf());
	if (FAILED(hr))
		throw ExceptionDirect3D11(L"IDXGIDevice Failed.");

	ComPtr<IDXGIAdapter> pDXGIAdapter;
	hr = pDXGIDevice->GetParent(__uuidof(IDXGIAdapter), (void**)pDXGIAdapter.GetAddressOf());
	if (FAILED(hr))
		throw ExceptionDirect3D11(L"IDXGIAdapter Failed.");

	ComPtr<IDXGIFactory> pIDXGIFactory;
	hr = pDXGIAdapter->GetParent(__uuidof(IDXGIFactory), (void**)pIDXGIFactory.GetAddressOf());
	if (FAILED(hr))
		throw ExceptionDirect3D11(L"IDXGIFactory Failed.");

	hr = pIDXGIFactory->MakeWindowAssociation(hwnd, DXGI_MWA_NO_ALT_ENTER);
	if (FAILED(hr))
		throw ExceptionDirect3D11(L"MakeWindowAssociation Failed.");

	// fullscreen mode
	GetWindowRect(hwnd, &rectRestoreWindow);
	getAllSupportedModes();
	resizeViewport(rp.getClientWidth(), rp.getClientHeight());
	changeModeScreen(false); // init fullscreen

}

void OSWindowsDirect3D11Render::resizeViewport(int w, int h)
{
	//OutputDebugString(L"resize viewport\n");
	if (fullscreen)
	{
		w = fullscreenModeDescription.Width;
		h = fullscreenModeDescription.Height;
	}
	//OutputDebugString((std::to_wstring(w) + L"-" + std::to_wstring(h) + L"\n").c_str());
	setWidth(w);
	setHeight(h);
	// ��������, ��� ������ ����������
	if (!d3dImmediateContext || !d3dDevice || !d3dSwapChain)
		throw ExceptionDirect3D11(L"Error resize D3D11 render.");

	// ������ ������� ������
	clears::ReleaseCom(d3dRenderTargetView);
	clears::ReleaseCom(d3dDepthStencilView);
	clears::ReleaseCom(d3dDepthStencilBuffer);

	// ��������� ������ ������� ������
	//if(fullscreen)
	//	debug::checkHresult(d3dSwapChain->ResizeTarget(&biggestModeDescription)); // �����???
	//else
	debug::checkHresult(d3dSwapChain->ResizeBuffers(1, w, h, desiredColourFormat, 0));
	ComPtr<ID3D11Texture2D> backBuffer;
	debug::checkHresult(d3dSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(backBuffer.GetAddressOf())));
	debug::checkHresult(d3dDevice->CreateRenderTargetView(backBuffer.Get(), 0, d3dRenderTargetView.GetAddressOf()));
	clears::ReleaseCom(backBuffer);

	// ����������� ������ �� �����
	// ����� ������� / ��������� � ����
	D3D11_TEXTURE2D_DESC depthStencilDesc;

	depthStencilDesc.Width = w;
	depthStencilDesc.Height = h;
	depthStencilDesc.MipLevels = 1;
	depthStencilDesc.ArraySize = 1;
	depthStencilDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;

	// ���������� 4xMSAA?
	if (d3dEnable4xMsaa)
	{
		depthStencilDesc.SampleDesc.Count = 4;
		depthStencilDesc.SampleDesc.Quality = d3d4xMsaaQuality - 1;
	}
	// �� ����������
	else
	{
		depthStencilDesc.SampleDesc.Count = 1;
		depthStencilDesc.SampleDesc.Quality = 0;
	}

	depthStencilDesc.Usage = D3D11_USAGE_DEFAULT;
	depthStencilDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthStencilDesc.CPUAccessFlags = 0;
	depthStencilDesc.MiscFlags = 0;

	debug::checkHresult(d3dDevice->CreateTexture2D(&depthStencilDesc, 0, d3dDepthStencilBuffer.GetAddressOf()));
	debug::checkHresult(d3dDevice->CreateDepthStencilView(d3dDepthStencilBuffer.Get(), 0, d3dDepthStencilView.GetAddressOf()));
	
	PROCESS_MEMORY_COUNTERS pmc;
	GetProcessMemoryInfo(GetCurrentProcess(), &pmc, sizeof(pmc));
	OutputDebugString((std::to_wstring(pmc.WorkingSetSize / 1024 / 1024) + L"\n").c_str());
	// ������������� ����� ������� / ��������� � ���������
	d3dImmediateContext->OMSetRenderTargets(1, d3dRenderTargetView.GetAddressOf(), d3dDepthStencilView.Get()); // ���������� ������ � 20 �� �� 100 �� (???)
	GetProcessMemoryInfo(GetCurrentProcess(), &pmc, sizeof(pmc));
	OutputDebugString((std::to_wstring(pmc.WorkingSetSize / 1024 / 1024) + L"\n").c_str());

	// ������������� ������������ ����
	d3dScreenViewport.TopLeftX = 0;
	d3dScreenViewport.TopLeftY = 0;
	d3dScreenViewport.Width = static_cast<float>(w);
	d3dScreenViewport.Height = static_cast<float>(h);
	d3dScreenViewport.MinDepth = 0.0f;
	d3dScreenViewport.MaxDepth = 1.0f;

	d3dImmediateContext->RSSetViewports(1, &d3dScreenViewport);
}

void OSWindowsDirect3D11Render::resizeWindow(int x, int y, int w, int h)
{
	if (scene)
	{
		sizeWindowChanged = true;
		scene->setWindowBBox(x, y, w, h);
	}
}


void OSWindowsDirect3D11Render::clear()
{

}

void OSWindowsDirect3D11Render::setTypeRasterizer(const TypeRasterizer& tr)
{
	typeRasterizer = tr;
	D3D11_RASTERIZER_DESC rast;
	ZeroMemory(&rast, sizeof(D3D11_RASTERIZER_DESC));
	if (typeRasterizer == TypeRasterizer::WIREFRAME_RASTERIZED)
	{
		rast.FillMode = D3D11_FILL_WIREFRAME;
		rast.CullMode = D3D11_CULL_NONE;
	}
	else
	{
		rast.FillMode = D3D11_FILL_SOLID;
		rast.CullMode = D3D11_CULL_BACK;
	}
	if(FAILED(d3dDevice->CreateRasterizerState(&rast, d3dRasterizer.GetAddressOf())))
		throw ExceptionDirect3D11(L"Error create rasterizator D3D11 render.");
	d3dImmediateContext->RSSetState(d3dRasterizer.Get());
}

TypeRasterizer OSWindowsDirect3D11Render::getTypeRasterizer()
{
	return typeRasterizer;
}

void OSWindowsDirect3D11Render::render()
{
	debug::checkHresult(d3dSwapChain->Present(vsync ? 1 : 0, 0));
}

OSWindowsDirect3D11Render::~OSWindowsDirect3D11Render()
{

}

void OSWindowsDirect3D11Render::pressAltTab(bool stat)
{
	//OutputDebugString(L"pressAltTab\n");
	if (fullscreen)
	{
		if (stat && !IsIconic(hwnd)) // ������������ fullscreen & alt + tab
		{
			ShowWindow(hwnd, SW_SHOWMINNOACTIVE);
			if (FAILED(d3dSwapChain->SetFullscreenState(false, nullptr)))
				throw ExceptionDirect3D11(L"Error SetFullscreenState D3D11 render.");
			running = false;
		}
		else if (!stat && IsIconic(hwnd)) // �������������� ����� fullscreen & alt + tab
		{
			ShowWindow(hwnd, SW_SHOWNORMAL);
			running = true;
			HRESULT hr = d3dSwapChain->SetFullscreenState(true, nullptr);
			if(FAILED(hr))
				throw ExceptionDirect3D11(L"Error SetFullscreenState D3D11 render.");
		}
	}
	else
	{
		running = !stat;
	}
}

void OSWindowsDirect3D11Render::getAllSupportedModes()
{
	unsigned numberOfSupportedModes(0); // ���������� �������������� ����������

	// �������� ������� �������
	ComPtr<IDXGIOutput> output;
	if (FAILED(d3dSwapChain->GetContainingOutput(&output)))
		throw ExceptionDirect3D11(L"Unable to retrieve the output adapter!.");

	// �������� ���������� �������������� ����������
	if (FAILED(output->GetDisplayModeList(desiredColourFormat, 0, &numberOfSupportedModes, NULL)))
		throw ExceptionDirect3D11(L"Unable to list all supported display modes!");

	// �������� ������� �������������� ����������
	std::vector<DXGI_MODE_DESC> allModesScreen; // ������ ������������� ���������
	allModesScreen.resize(numberOfSupportedModes);
	ZeroMemory(allModesScreen.data(), sizeof(DXGI_MODE_DESC) * numberOfSupportedModes);

	// �������� �������������� ����������
	if (FAILED(output->GetDisplayModeList(desiredColourFormat, 0, &numberOfSupportedModes, allModesScreen.data())))
		throw ExceptionDirect3D11(L"Unable to retrieve all supported display modes!");

	// �������� ���������� � ���� ������
	//for (int i(0); i < numberOfSupportedModes; ++i)
	//	allModesScreen.push_back(*(supportedModes + i));
	//delete[] supportedModes;
	
	// ���������� ����� ������� ����������
	fullscreenModeDescription = allModesScreen.back();

	// ������� ��� ���������� �� �����
	/*for (auto&& m: allModesScreen)
		OutputDebugString((std::to_wstring(m.Width) + L" - " + std::to_wstring(m.Height) + L"\n").c_str());*/

}

void OSWindowsDirect3D11Render::changeModeScreen(bool fs)
{
	//OutputDebugString(L"change mode screen\n");
	if (fs)
		fullscreen = !fullscreen;

	if (fullscreen)
	{
		WINDOWPLACEMENT windowedPlacement;
		windowedPlacement.length = sizeof(WINDOWPLACEMENT);
		GetWindowPlacement(hwnd, &windowedPlacement);

		GetWindowRect(hwnd, &rectRestoreWindow);
		maximazeRestoreWindow = windowedPlacement.flags == WPF_RESTORETOMAXIMIZED;

		ShowWindow(hwnd, SW_SHOWMAXIMIZED);

	}
	if(FAILED(d3dSwapChain->SetFullscreenState(fullscreen, nullptr)))
		throw ExceptionDirect3D11(L"Error SetFullscreenState D3D11 render.");
	if (!fullscreen)
	{
		ShowWindow(hwnd, maximazeRestoreWindow ? SW_SHOWMAXIMIZED : SW_SHOWNORMAL);
		SetWindowPos(hwnd, NULL, rectRestoreWindow.left, rectRestoreWindow.top, rectRestoreWindow.right - rectRestoreWindow.left, rectRestoreWindow.bottom - rectRestoreWindow.top,
			SWP_SHOWWINDOW  | SWP_NOZORDER);


	}
}
