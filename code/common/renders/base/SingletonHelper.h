#pragma once

#include <memory>

#include "../abstract/IRender.h"

class SingletonHelper;
using SingletonHelperSmartPtr = std::shared_ptr<SingletonHelper>;

class SingletonHelper
{
	static SingletonHelperSmartPtr instance;

	IRender* render;

	SingletonHelper();
	SingletonHelper(const SingletonHelper&) = delete;
	void operator=(const SingletonHelper&) = delete;

public:
	static SingletonHelperSmartPtr& getInstance();

	IRender* getCurrentRender();

	void setCurrentRender(IRender* wsp);
};