#pragma once

#include "../abstract/IWindow.h"

class Application
{
	protected:
		//IWindow* window;
		WindowSmartPtr window;

	public:
		virtual void init(WindowParameters wp) = 0;
		
		void run();
		virtual ~Application();
};