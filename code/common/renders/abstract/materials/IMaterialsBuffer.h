#pragma once

#include <string>
#include <memory>

#include "../../../utils/base/loaders/Materials.h"

class IMaterialsBuffer
{
protected:
	Materials buffer;
public:
	virtual void init(const std::wstring& path) = 0;

	Material& getMaterial(const std::wstring& k);

	virtual ~IMaterialsBuffer() = default;
};

using MaterialsBufferSmartPtr = std::shared_ptr<IMaterialsBuffer>;