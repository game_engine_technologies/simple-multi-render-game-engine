#include "IScene.h"

void IScene::setWindowBBox(int x, int y, int w, int h)
{
	windowBBox = { x, y, w, h };
}

gmtypes::WindowBBox& IScene::getWindowBBox()
{
	return windowBBox;
}
