#pragma once

#include <memory>
#include <string>
#include <vector>
#include <algorithm>

#include "../../../base/scene/model/Index.h"

#include "../../../base/math_header.h"
#include "../../../base/scene/model/VertexTexture.h"

#include "../../../../utils/base/loaders/TextProperty.h"

#include <map>

class VisitorTextBufferCreateAndDraw;
using VisitorTextBufferCreateAndDrawSmartPtr = std::shared_ptr<VisitorTextBufferCreateAndDraw>;

struct TextA
{
	std::wstring text;
	gmath::Matrix position;
	gmath::Color textColor;
	unsigned start;
	unsigned count;
	int height;
};

class ITextBufferA
{
public:
	using PairEnterString = std::pair<int, int>;
protected:
	std::map<std::wstring, TextA> texts; // ����� ������
	unsigned countVertex; // ���������� ������
	unsigned maxCountVertex; // ������������ ���������� ������
	MapTextProperties metricsSymbol; // ����� �������� - ����������� �������
	VertexTexture* mappedArrayVertex; // ����������� ������ ������ � GPU
	std::vector<PairEnterString> countEnter; // ���������� ��������� enter

public:
	static unsigned DEFAULT_MAX_COUNT_VERTEX;

	virtual void init(const std::wstring& vs, const std::wstring& ps, 
		const std::wstring cfg, const std::wstring& sampler, VisitorTextBufferCreateAndDrawSmartPtr& visitor) = 0;
	
	virtual void mapVertexBuffer() = 0;
	virtual void unmapVertexBuffer() = 0;

	virtual int getHeightText(const std::wstring& key) = 0;

	virtual void addText(const std::wstring& key, const std::wstring& text, const gmath::Matrix4& pos, const gmath::Color& color, int h) = 0;
	virtual void replaceTextForEnter(const std::wstring& key, int ind_str, int ind_start, const std::wstring& new_string) = 0;
	// ������ ������ (�� \n)

	virtual void updatePosition(const std::wstring& key, const gmath::Matrix4& pos) = 0;
	virtual void updateColor(const std::wstring& key, const gmath::Color& c) = 0;

	virtual void reinit() = 0;
	virtual void draw() = 0;

	MapTextProperties& getMetricsSymbol();
	void setBufferSymbols(MapTextProperties);

	void setCountVertex(unsigned nc);
	unsigned getCountVertex();
	void setMaxCountVertex(unsigned nc);
	unsigned getMaxCountVertex();

	std::map<std::wstring, TextA>& getTexts();
	TextA& getTextFromBuffer(const std::wstring& k);
	void addTextFromBuffer(const std::wstring& k, const TextA& t);

	VertexTexture* getMappedArrayVertex();
	void setMappedArrayVertex(VertexTexture* mvb);

	std::vector<PairEnterString> getCountEnter();
	void addCountEnter(int cs, int ce);
};

// ������� ������:
// init

// �������� ->
// mapVertexBuffer
// addText (������� ������)
// updatePosition || replaceText (������� ������)
// unmapVertexBuffer