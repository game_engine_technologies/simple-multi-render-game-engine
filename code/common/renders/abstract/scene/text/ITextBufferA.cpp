#include "ITextBufferA.h"

unsigned ITextBufferA::DEFAULT_MAX_COUNT_VERTEX(1000);


MapTextProperties& ITextBufferA::getMetricsSymbol()
{
	return metricsSymbol;
}

void ITextBufferA::setBufferSymbols(MapTextProperties a)
{
	metricsSymbol = a;
}

void ITextBufferA::setCountVertex(unsigned nc)
{
	countVertex = nc;
}

unsigned ITextBufferA::getCountVertex()
{
	return countVertex;
}

void ITextBufferA::setMaxCountVertex(unsigned nc)
{
	maxCountVertex = nc;
}

unsigned ITextBufferA::getMaxCountVertex()
{
	return maxCountVertex;
}

std::map<std::wstring, TextA>& ITextBufferA::getTexts()
{
	return texts;
}

TextA& ITextBufferA::getTextFromBuffer(const std::wstring& k)
{
	// todo: ��������, ��� ����� ���� ����������
	return texts[k];
}

void ITextBufferA::addTextFromBuffer(const std::wstring& k, const TextA& t)
{
	texts.insert({ k, t });
}

VertexTexture* ITextBufferA::getMappedArrayVertex()
{
	return mappedArrayVertex;
}

void ITextBufferA::setMappedArrayVertex(VertexTexture* mvb)
{
	mappedArrayVertex = mvb;
}

std::vector<ITextBufferA::PairEnterString> ITextBufferA::getCountEnter()
{
	return countEnter;
}

void ITextBufferA::addCountEnter(int cs, int ce)
{
	countEnter.push_back({ cs, ce });
}

