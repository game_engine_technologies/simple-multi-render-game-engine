#include "ITextBufferB.h"

MapTextProperties& ITextBufferB::getMetricsSymbol()
{
	return metricsSymbol;
}

void ITextBufferB::setBufferSymbols(MapTextProperties a)
{
	metricsSymbol = a;
}

int ITextBufferB::getHeight()
{
	return height;
}

void ITextBufferB::setHeight(int h)
{
	height = h;
}
