#pragma once

#include <memory>
#include <string>
#include <vector>
#include <algorithm>

#include "../../../base/scene/model/Index.h"
#include "../../../base/math_header.h"
#include "../../../base/scene/model/VertexTexture.h"
#include "../../../../utils/base/loaders/TextProperty.h"
#include "../../../base/scene/text/TypeTextBuffer.h"

#include <map>

class VisitorTextBufferCreateAndDraw;
using VisitorTextBufferCreateAndDrawSmartPtr = std::shared_ptr<VisitorTextBufferCreateAndDraw>;

class ITextBufferB
{
public:
	using PairEnterString = std::pair<int, int>;
protected:
	int height;
	MapTextProperties metricsSymbol; // ����� �������� - ����������� �������

public:
	virtual void init(const std::wstring& vs, const std::wstring& ps, 
		const std::wstring cfg, const std::wstring& sampler, int h, VisitorTextBufferCreateAndDrawSmartPtr& visitor) = 0;

	virtual void addText(const std::wstring& key, const std::wstring& text, const gmath::Matrix4& pos, const gmath::Color& color, const TypeTextBufferB& type) = 0;

	virtual void replaceText(const std::wstring& key, const std::wstring& new_string) = 0;
	virtual void updatePosition(const std::wstring& key, const gmath::Matrix4& pos) = 0;
	virtual void updateColor(const std::wstring& key, const gmath::Color& c) = 0;

	virtual void reinit() = 0;
	virtual void draw() = 0;

	MapTextProperties& getMetricsSymbol();
	void setBufferSymbols(MapTextProperties);

	int getHeight();
	void setHeight(int h);
};