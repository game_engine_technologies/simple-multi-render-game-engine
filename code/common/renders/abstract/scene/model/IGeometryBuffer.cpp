#include "IGeometryBuffer.h"

std::vector<Index>& IGeometryBuffer::getIndeces()
{
	return indeces;
}

bool IGeometryBuffer::isUpdateAll() const
{
	return _updateAll;
}

bool IGeometryBuffer::isUpdateIndeces() const
{
	return _updateIndeces;
}

void IGeometryBuffer::clearFlags()
{
	_updateAll = _updateIndeces = false;
}


