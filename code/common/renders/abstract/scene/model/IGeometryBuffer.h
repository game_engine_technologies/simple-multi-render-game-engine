#pragma once

#include <memory>
#include <string>
#include <vector>
#include <algorithm>

#include "../../../base/scene/model/VertexColor.h"
#include "../../../base/scene/model/Index.h"
#include "../../../base/scene/model/VertexStaticLight.h"

#include "../../../base/scene/model/ModelParameters.h"


class VisitorGeometryBufferCreateAndDraw;
using VisitorGeometryBufferCreateAndDrawSmartPtr = std::shared_ptr<VisitorGeometryBufferCreateAndDraw>;

class IGeometryBuffer
{
protected:
	std::vector<Index> indeces;
	bool _updateAll;
	bool _updateIndeces;

public:
	virtual void init(const std::wstring& vs, const std::wstring& ps, VisitorGeometryBufferCreateAndDrawSmartPtr& visitor) = 0;
	virtual void reinit() = 0;
	virtual void draw() = 0;
	virtual bool addModel(const ModelParameters& mp) = 0; // ���������� ��� ������ �������������� �� ������

	std::vector<Index>& getIndeces();

	bool isUpdateAll()const;
	bool isUpdateIndeces() const;
	void clearFlags() ;
};

// ������� ������:
// init

// �������� ->
// addModel (������� ������)
// reinit