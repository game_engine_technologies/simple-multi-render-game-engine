#pragma once
#include <memory>

#include "../../../base/scene/light/StaticLight.h"

class StaticLightVisitor;
using StaticLightVisitorSmartPtr = std::shared_ptr<StaticLightVisitor>;

class IStaticLight
{
protected:
	DirectionalLight dirLight;
	bool upd;

public:
	void setDirectionalLight(const DirectionalLight& dirLight);
	// void setPointLight();
	// void setSpotLight();
	DirectionalLight& getDirectionalLight();
	// getPointLight();
	// getSpotLight();
	bool isUpdate();
	void setUpdate(bool upd);

	virtual void init(const StaticLightVisitorSmartPtr& visitor) = 0;
	virtual void update() = 0;
	virtual void sendGPU(int addData) = 0;
};

using StaticLightSmartPtr = std::shared_ptr<IStaticLight>;