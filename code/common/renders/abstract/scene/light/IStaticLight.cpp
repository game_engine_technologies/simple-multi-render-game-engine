#include "IStaticLight.h"

void IStaticLight::setDirectionalLight(const DirectionalLight& dirLight)
{
	upd = true;
	this->dirLight = dirLight;
}

DirectionalLight& IStaticLight::getDirectionalLight()
{
	return dirLight;
}

bool IStaticLight::isUpdate()
{
	return upd;
}

void IStaticLight::setUpdate(bool upd)
{
	this->upd = upd;
}
