#pragma once

#include <string>
#include <mutex>
#include <thread>

#include "../../base/RenderParameters.h"
#include "../../../utils/base/input/InputSystem.h"
#include "../../base/scene/SceneParameters.h"

#include "../../base/math_header.h"
#include "../../base/SceneDynamicParameters.h"
#include "../materials/IMaterialsBuffer.h"

class IScene
{
protected:
	MaterialsBufferSmartPtr materialsBuffer;
	gmtypes::WindowBBox windowBBox;

public:
	virtual void init(const std::wstring& path, SceneParameters rp) = 0;
	virtual bool update(SceneDynamicParameters& sdp) = 0;
	virtual ~IScene()=0{}

	virtual void setWindowBBox(int x, int y, int w, int h);
	gmtypes::WindowBBox& getWindowBBox();
};