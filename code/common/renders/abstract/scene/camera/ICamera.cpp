#include "ICamera.h"

bool ICamera::isUpdate() const
{
	return _update;
}

void ICamera::clearFlags()
{
	_update = false;
}

ICamera::ICamera():_update(false)
{
}

MatrixCPUAndGPU ICamera::getView() const
{
	return view;
}

MatrixCPUAndGPU ICamera::getPerspective() const
{
	return perspective;
}

MatrixCPUAndGPU ICamera::getOrtho() const
{
	return ortho;
}

float ICamera::getFov() const
{
	return fov;
}

float ICamera::getAspectW() const
{
	return aspectW;
}

float ICamera::getAspectH() const
{
	return aspectH;
}

float ICamera::getAspect() const
{
	return aspectW / aspectH;
}

float ICamera::getNearZPerspective() const
{
	return nearZPerspective;
}

float ICamera::getNearZOrtho() const
{
	return nearZOrtho;
}

void ICamera::setNearZPerspective(float nz) 
{
	nearZPerspective = nz;
}

void ICamera::setNearZOrtho(float nz)
{
	nearZOrtho = nz;
}

float ICamera::getFarZ() const
{
	return farZ;
}

void ICamera::setFarZ(float fz)
{
	farZ = fz;
}
