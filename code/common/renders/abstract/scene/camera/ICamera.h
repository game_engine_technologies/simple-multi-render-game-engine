#pragma once

#include <memory>

#include "../../../../utils/base/MatrixHelper.h"

class VisitorCameraCreateAndUpdate;
using VisitorCameraCreateAndUpdateSmartPtr = std::shared_ptr<VisitorCameraCreateAndUpdate>;

struct alignas(16) ConstantBufferCamera
{
	gmath::Matrix view;
	gmath::Matrix perspective;
	gmath::Matrix ortho;
};

class ICamera
{
protected:
	MatrixCPUAndGPU view;
	MatrixCPUAndGPU perspective;
	MatrixCPUAndGPU ortho;
	float fov;
	float aspectW;
	float aspectH;

	float nearZPerspective;
	float nearZOrtho;

	float farZ;

	bool _update;

public:
	ICamera();
	virtual void init(VisitorCameraCreateAndUpdateSmartPtr& visitor) = 0;
	virtual void updateOrtho(float w, float h) = 0;
	virtual void updatePerspective(float fov, float aspectW, float aspectH) = 0;
	virtual void updateView(const gmath::Vector3& p, const gmath::Vector3& d, const gmath::Vector3& h) = 0;
	virtual void update(double frameTime, gmtypes::WindowBBox& winBbox) = 0;
	virtual void sendToGPU(int additionalData) = 0;
	virtual ~ICamera() = default;

	MatrixCPUAndGPU getView() const;
	MatrixCPUAndGPU getPerspective() const;
	MatrixCPUAndGPU getOrtho() const;

	float getFov() const;
	float getAspectW() const;
	float getAspectH() const;
	float getAspect() const;

	float getNearZPerspective() const;
	float getNearZOrtho() const;
	void setNearZPerspective(float nz) ;
	void setNearZOrtho(float nz) ;

	float getFarZ() const;
	void setFarZ(float fz);

	bool isUpdate()const;
	void clearFlags();
};

using CameraSmartPtr = std::shared_ptr<ICamera>;

// ������ �����, �������:
// setFarZ, setNearZOrtho, setNearZPerspective
// updateView || updatePerspective
// init