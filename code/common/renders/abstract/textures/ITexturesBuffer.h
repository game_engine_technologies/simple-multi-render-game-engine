#pragma once

#include <map>

#include "../../../utils/abstract/loaders/ITexturesLoader.h"

#include "../../../utils/base/ExceptionTextures.h"

class TexturesBufferVisitorCreatorAndAdded;
using TexturesBufferVisitorCreatorAndAddedSmartPtr = std::shared_ptr<TexturesBufferVisitorCreatorAndAdded>;

class ITexturesBuffer
{
protected:
	TexturesLoaderSmartPtr texturesLoader;
	TexturesBufferVisitorCreatorAndAddedSmartPtr visitor;

public:
	virtual void init(const std::wstring path, const TexturesLoaderSmartPtr& loader, const TexturesBufferVisitorCreatorAndAddedSmartPtr& visitor) = 0;
	virtual void add(const std::wstring path) = 0;
	virtual TexturesLoaderSmartPtr& getLoaderTextures() = 0;
};