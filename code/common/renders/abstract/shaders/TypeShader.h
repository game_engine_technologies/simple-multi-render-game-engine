#pragma once

enum class TypeShader : unsigned
{
	VertexShaderType = 0,
	PixelShaderType,
	ComputerShaderType,
	GeometryShaderTYpe
};