#pragma once

#include "TypeShader.h"

#include <string>
#include <memory>

class VisitorShaderBufferCreateAndLoader;
using VisitorShaderBufferCreateAndLoaderSmartPtr = std::shared_ptr<VisitorShaderBufferCreateAndLoader>;

class IShadersBuffer
{
public:
	virtual void init(const std::wstring& path, VisitorShaderBufferCreateAndLoaderSmartPtr& shBuffCreator) = 0;
	virtual void loadShader(const std::wstring& path, const TypeShader& ts) = 0;
	virtual ~IShadersBuffer() = default;
};