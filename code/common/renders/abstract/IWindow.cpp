#include "IWindow.h"

LRESULT IWindow::wndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	return 0;
}

LRESULT CALLBACK IWindow::WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	IWindow* wndptr = (IWindow*)GetWindowLongPtr(hwnd, GWLP_USERDATA);
	if (!wndptr)
		return DefWindowProc(hwnd, msg, wParam, lParam);
	return wndptr->wndProc(hwnd, msg, wParam, lParam);
}
