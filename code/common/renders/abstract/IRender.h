#pragma once

#include "../../renders/base/RenderParameters.h"
#include <memory>
#include "../base/TypeRasterizer.h"
#include "../base/SceneDynamicParameters.h"

class IRender
{
protected:
	bool running;
	bool vsync;
	bool fullscreen;
	TypeRasterizer typeRasterizer;
	int width;
	int height;
	bool sizeWindowChanged;

public:
	virtual void init(RenderParameters wp) = 0;
	virtual void resizeViewport(int w, int h) = 0;
	virtual void resizeWindow(int x, int y, int w, int h) = 0;
	virtual bool updateScene(SceneDynamicParameters& sdp) = 0;
	virtual void initScene() = 0;
	virtual void render() = 0;
	virtual void setTypeRasterizer(const TypeRasterizer& tr) = 0;
	virtual TypeRasterizer getTypeRasterizer() = 0;
	virtual ~IRender() = 0 {}

	bool isRunning()const;
	void setRunning(bool r);
	bool isVsync()const;
	virtual void setVsync(bool r);
	bool isFullscreen()const;
	virtual void changeModeScreen(bool fs) = 0;
	void setWidth(int w);
	void setHeight(int h);
	int getWidth()const;
	int getHeight()const;

	virtual void pressAltTab(bool stat) = 0;
	virtual void getAllSupportedModes() = 0;
};

using RenderSmartPtr = std::shared_ptr<IRender>;