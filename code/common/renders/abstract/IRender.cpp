#include "IRender.h"

bool IRender::isRunning()const
{
	return running;
}

void IRender::setRunning(bool r)
{
	running = r;
}

bool IRender::isVsync() const
{
	return vsync;
}

void IRender::setVsync(bool r)
{
	vsync = r;
}

bool IRender::isFullscreen() const
{
	return fullscreen;
}

void IRender::setWidth(int w)
{
	width = w;
}

void IRender::setHeight(int h)
{
	height = h;
}

int IRender::getWidth() const
{
	return width;
}

int IRender::getHeight() const
{
	return height;
}
