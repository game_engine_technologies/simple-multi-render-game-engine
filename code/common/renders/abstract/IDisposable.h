#pragma once

class IDisposable
{
	public:
		virtual void clear() = 0;
		virtual ~IDisposable() = 0{}
};