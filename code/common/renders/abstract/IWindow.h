#pragma once

#include "IRender.h"
#include "../base/WindowParameters.h"

#include "../../utils/base/ExceptionWindow.h"

class IWindow
{
	protected:
		RenderSmartPtr render;

	public:
		virtual void init(WindowParameters wp) = 0;
		virtual void resizeViewport(int w, int h) = 0;
		virtual void resizeWindow(int x, int y, int w, int h) = 0;
		virtual void run() = 0;
		virtual ~IWindow() = 0 {}

		// OS windows
		virtual LRESULT wndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
		static LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
};

using WindowSmartPtr = std::shared_ptr<IWindow>;