#pragma once

#include "../abstract/IWindow.h"
#include "../abstract/IDisposable.h"

#include "gl_header.h"

class OpenGLWindow : public IWindow, public IDisposable
{
protected:
	GLFWwindow* window;
	std::wstring classWin;

public:
	virtual void init(WindowParameters wp) override;
	virtual void resizeViewport(int w, int h) override;
	virtual void resizeWindow(int x, int y, int w, int h) override;
	virtual void run() override;
	virtual void clear() override;
	virtual ~OpenGLWindow() override;

	static void SizeEvent(GLFWwindow* window, int w, int h);
	virtual void sizeEvent(GLFWwindow* window, int w, int h);

	static void KeyEvent(GLFWwindow* window, int key, int scancode, int action, int mode);
	void keyEvent(GLFWwindow* window, int key, int scancode, int action, int mode);

	static void MouseEvent(GLFWwindow* window, double xpos, double ypos);
	void mouseEvent(GLFWwindow* window, double xpos, double ypos);

};

class DeleterOpenGLWindow
{
public:
	void operator()(OpenGLWindow* window);
};
