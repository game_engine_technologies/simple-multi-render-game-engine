#include "OpenGLRender.h"

#include <sstream>

void debug::glDebugOutput(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam)
{
	// ignore non-significant error/warning codes
	if (id == 131169 || id == 131185 || id == 131218 || id == 131204)
		return;

	std::wstringstream error;

	error << "---------------" << std::endl;
	error << L"Debug message (" << id << L"): " << std::wstring(message, message + strlen(message)) << std::endl;

	switch (source)
	{
	case GL_DEBUG_SOURCE_API:
		error << "Source: API";
		break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
		error << "Source: Window System";
		break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER:
		error << "Source: Shader Compiler";
		break;
	case GL_DEBUG_SOURCE_THIRD_PARTY:
		error << "Source: Third Party";
		break;
	case GL_DEBUG_SOURCE_APPLICATION:
		error << "Source: Application";
		break;
	case GL_DEBUG_SOURCE_OTHER:
		error << "Source: Other";
		break;
	}
	error << std::endl;

	switch (type)
	{
	case GL_DEBUG_TYPE_ERROR:
		error << L"Type: Error";
		break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
		error << L"Type: Deprecated Behaviour";
		break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
		error << L"Type: Undefined Behaviour";
		break;
	case GL_DEBUG_TYPE_PORTABILITY:
		error << L"Type: Portability";
		break;
	case GL_DEBUG_TYPE_PERFORMANCE:
		error << L"Type: Performance";
		break;
	case GL_DEBUG_TYPE_MARKER:
		error << L"Type: Marker";
		break;
	case GL_DEBUG_TYPE_PUSH_GROUP:
		error << L"Type: Push Group";
		break;
	case GL_DEBUG_TYPE_POP_GROUP:
		error << L"Type: Pop Group";
		break;
	case GL_DEBUG_TYPE_OTHER:
		error << L"Type: Other";
		break;
	}
	error << std::endl;

	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:
		error << L"Severity: high";
		break;
	case GL_DEBUG_SEVERITY_MEDIUM:
		error << L"Severity: medium";
		break;
	case GL_DEBUG_SEVERITY_LOW:
		error << L"Severity: low";
		break;
	case GL_DEBUG_SEVERITY_NOTIFICATION:
		error << L"Severity: notification";
		break;
	}
	error << std::endl;
	error << std::endl;

	throw ExeptionOpenGL(L"Error render ExeptionOpenGL: \n" + error.str());
}


void OpenGLRender::init(RenderParameters rp)
{
	running = true;
	//��������� GLFW
	//�������� ����������� ��������� ������ OpenGL. 
	//�������� 
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	//��������
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	//��������� �������� ��� �������� ��������� ��������
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
	{
		throw ExeptionOpenGL(L"Failed to initialize GLEW");
	}

	// ��������� �������
	// std::cout << glGetString(GL_VERSION) << std::endl; // GET VERSION SUPPORTED
#if defined(DEBUG) | defined(_DEBUG)
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE); // �������� ������ � OpenGL 4.3
	GLint flags;
	glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
	if (flags & GL_CONTEXT_FLAG_DEBUG_BIT)
	{
		glEnable(GL_DEBUG_OUTPUT);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		glDebugMessageCallback(debug::glDebugOutput, nullptr);
		glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
	}
	else
	{
		debug::printError(L"Debug for OpenGL not supperted your system!");
	}
#endif

	//int width, height;
	//glfwGetFramebufferSize(rp->, &width, &height);
	resizeViewport(rp.getClientWidth(), rp.getClientHeight());
	setTypeRasterizer(TypeRasterizer::SOLID_RASTERIZED);

}

void OpenGLRender::resizeViewport(int w, int h)
{
	setWidth(w);
	setHeight(h);
	glViewport(0, 0, w, h);
}

void OpenGLRender::resizeWindow(int x, int y, int w, int h)
{
	if (scene)
		scene->setWindowBBox(x, y, w, h);
}

void OpenGLRender::clear()
{

}

void OpenGLRender::setTypeRasterizer(const TypeRasterizer& tr)
{
	typeRasterizer = tr;
	if (typeRasterizer == TypeRasterizer::WIREFRAME_RASTERIZED)
	{
		glPolygonMode(GL_FRONT, GL_LINE);
		glPolygonMode(GL_BACK, GL_LINE);
	}
	else
	{
		glPolygonMode(GL_FRONT, GL_FILL);
		glPolygonMode(GL_BACK, GL_FILL);
	}
}

TypeRasterizer OpenGLRender::getTypeRasterizer()
{
	return typeRasterizer;
}

OpenGLRender::~OpenGLRender()
{
	clear();
}

