#include "OpenGLWindowHelper.h"

OpenGLWindowHelper::OpenGLWindowHelper(GLFWwindow* w): window(w)
{
}

void OpenGLWindowHelper::setMouseCaptureFPSCamera()
{
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

void OpenGLWindowHelper::setMousePosition(int x, int y)
{
	glfwSetCursorPos(window, x, y);
}
