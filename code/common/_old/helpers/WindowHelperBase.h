#pragma once

#include "IWindowHelper.h"

#include "../gl_header.h"

class WindowHelperGeneral
{
	static WindowHelperSmartPtr windowHelper;

	WindowHelperGeneral(const WindowHelperGeneral&) = delete;
	void operator=(const WindowHelperGeneral&) = delete;

public:
	static WindowHelperSmartPtr& getInstance(GLFWwindow* window = nullptr);
};