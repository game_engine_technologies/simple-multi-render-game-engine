#pragma once

#include "IWindowHelper.h"

#include "../gl_header.h"


class OpenGLWindowHelper: public IWindowHelper
{
	GLFWwindow* window;
public:
	OpenGLWindowHelper(GLFWwindow* w);
	void setMouseCaptureFPSCamera() override; // capture & hide mouse
	void setMousePosition(int x, int y) override; // set mouse new pos

};

using OpenGLWindowHelperSmartPtr = std::shared_ptr<OpenGLWindowHelper>;