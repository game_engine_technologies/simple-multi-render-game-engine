#include "WindowHelperBase.h"
// #include "Direct3D11WindowHelper.h"
#include "OpenGLWindowHelper.h"

WindowHelperSmartPtr WindowHelperGeneral::windowHelper;

WindowHelperSmartPtr& WindowHelperGeneral::getInstance(GLFWwindow* window)
{
	if (!windowHelper)
		windowHelper = std::make_shared<OpenGLWindowHelper>(window);
	return windowHelper;
}
