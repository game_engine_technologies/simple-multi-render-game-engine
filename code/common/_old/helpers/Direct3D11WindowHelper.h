#pragma once

#include "IWindowHelper.h"

#include <Windows.h>

class Direct3D11WindowHelper: public IWindowHelper
{
	HWND hwnd;
public:
	Direct3D11WindowHelper(const HWND& hwnd);
	void setMouseCaptureFPSCamera() override; // capture & hide mouse
	void setMousePosition(int x, int y) override; // set mouse new pos

};

using Direct3D11WindowHelperSmartPtr = std::shared_ptr<Direct3D11WindowHelper>;