#pragma once

#include <memory>

class IWindowHelper
{
public:
	virtual void setMouseCaptureFPSCamera() = 0; // capture & hide mouse
	virtual void setMousePosition(int x, int y) = 0; // set mouse new pos

};

using WindowHelperSmartPtr = std::shared_ptr<IWindowHelper>;