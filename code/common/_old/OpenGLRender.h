#pragma once

#include "../abstract/IRender.h"
#include "../abstract/IDisposable.h"
#include "../../utils/base/ExeptionOpenGL.h"

//#include "gl_header.h"

#include "scene/OpenGLScene.h"
#include "textures/OpenGLTexturesBuffer.h"

namespace debug
{
	void GLAPIENTRY glDebugOutput(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam);
}

class OpenGLRender : public IRender, public IDisposable
{
protected:
	OpenGLSceneSmartPtr scene;
	OpenGLTexturesBufferSmartPtr texturesBuffer;

public:
	void init(RenderParameters wp) override;
	void resizeViewport(int w, int h) override;
	void resizeWindow(int x, int y, int w, int h) override;
	void clear()override;
	void setTypeRasterizer(const TypeRasterizer& tr)override;
	TypeRasterizer getTypeRasterizer() override;

	~OpenGLRender() override;
};
