#include "OpenGLWindow.h"
#include "../../utils/base/ExeptionMemory.h"
#include "../../os/base/LevelOperationSystemSingleton.h"

#include "helpers\WindowHelperBase.h"

void OpenGLWindow::init(WindowParameters wp)
{
	//glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	//glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

	
	//������������� GLFW
	glfwInit();
	//���������� ����������� ��������� ������� ����
	glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
	// �������� �������
#if defined(DEBUG) | defined(_DEBUG)
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE); // �������� ������ � OpenGL 4.3
#endif
	// create window
	// set class win to wstring!
	window = glfwCreateWindow(wp.getWidth(), wp.getHeight(), "LearnOpenGL", nullptr, nullptr);
	if (window == nullptr)
	{
		glfwTerminate();
		throw ExeptionWindow(L"Failed to create GLFW window");
	}
	// set resize callback
	glfwMakeContextCurrent(window);
	glfwSetWindowUserPointer(window, this);
	glfwSetWindowSizeCallback(window, SizeEvent);
	glfwSetKeyCallback(window, KeyEvent);
	glfwSetCursorPosCallback(window, MouseEvent);

	WindowHelperGeneral::getInstance(window);
}

void OpenGLWindow::resizeViewport(int w, int h)
{
	render->resizeViewport(w, h);
}

void OpenGLWindow::resizeWindow(int x, int y, int w, int h)
{
	render->resizeWindow(x, y, w, h);
}

void OpenGLWindow::run()
{
	LevelOperationSystemSingleton::getInstance()->startTimer();
	while (!glfwWindowShouldClose(window))
	{
		glfwPollEvents();
		if (render->isRunning())
		{
			double frameTime = LevelOperationSystemSingleton::getInstance()->getFrameTime();
			if (!render->updateScene(frameTime))
				break;
			render->render();
		}
		else
		{
#ifdef _WIN32
			Sleep(100);
#endif
		}
		glfwSwapBuffers(window);
	}
}

void OpenGLWindow::clear()
{
	glfwTerminate();
}

OpenGLWindow::~OpenGLWindow()
{
	clear();
}

void OpenGLWindow::SizeEvent(GLFWwindow* window, int w, int h)
{
	OpenGLWindow* win = static_cast<OpenGLWindow*>(glfwGetWindowUserPointer(window));
	win->sizeEvent(window, w, h);
}

void OpenGLWindow::sizeEvent(GLFWwindow* window, int w, int h)
{
	resizeViewport(w, h);
	
	int x(-1);
	int y(-1);
	glfwGetWindowPos(window, &x, &y);
	resizeWindow(x, y, w, h);
}

void OpenGLWindow::KeyEvent(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	OpenGLWindow* win = static_cast<OpenGLWindow*>(glfwGetWindowUserPointer(window));
	win->keyEvent(window, key, scancode, action, mode);
}

void OpenGLWindow::keyEvent(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		InputSystem::getInstance()->setKeyboardKey(InputSystemKeys::KEY_ESC, action);
		return;
	}
	InputSystem::getInstance()->setKeyboardKey(static_cast<InputSystemKeys>(key), action);
}

void OpenGLWindow::MouseEvent(GLFWwindow* window, double xpos, double ypos)
{
	OpenGLWindow* win = static_cast<OpenGLWindow*>(glfwGetWindowUserPointer(window));
	win->mouseEvent(window, xpos, ypos);
}

void OpenGLWindow::mouseEvent(GLFWwindow* window, double xpos, double ypos)
{
	InputSystem::getInstance()->setMousePosition(xpos, ypos);
}

void DeleterOpenGLWindow::operator()(OpenGLWindow* window)
{
	glfwTerminate();
}
