#pragma once

enum class TypeCamera : short
{
	STATIC_CAMERA = 0,
	FPS_CAMERA
};