#pragma once

#include "../abstract/IException.h"

class ExceptionWindow: public IException
{
public:
	ExceptionWindow(const wchar_t* e);
	ExceptionWindow(const std::wstring& e);
	std::wstring getError();
};
