#include "Utils.h"

namespace debug
{
	void printError(const std::wstring& er)

	{
		OutputDebugString((er + L"\n").c_str());
	}

}

namespace locale
{
	std::wistream& operator>>(std::wistream& is, LineUnicode& l)
	{
		std::getline(is, l, L'\n');
		return is;
	}

	std::istream& operator>>(std::istream& is, LineAscii& l)
	{
		std::getline(is, l, '\n');
		return is;
	}


}