#include "TexturesLoaderSOIL.h"

Texture TexturesLoaderSOIL::loadTexture(const std::wstring& path)
{
	Texture texture;
	std::string p(path.begin(), path.end());
	int channels(4);
	unsigned char* image = SOIL_load_image(p.c_str(), &texture.width, &texture.height, &channels, SOIL_LOAD_RGBA);
	if (!image)
		throw ExceptionLoader(L"Error in SOIL lib, error load texture " + path + L"\n");
	texture.image = { image , image + static_cast<long long>(texture.width * texture.height * channels) };
	SOIL_free_image_data(image); // ������� �������� �� SOIL, �� � ��������� ��� ���������
	return texture;
}

