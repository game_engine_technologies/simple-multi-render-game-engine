#pragma once

#include <map>
#include <string>

#include "../../../renders/base/math_header.h"

struct Material
{
	gmath::Color ambient;
	gmath::Color diffuse;
	gmath::Color specular;
	float shininess;

	Material();
	Material(const gmath::Color& a, const gmath::Color& d, const gmath::Color& s, const float& sn);
};

using Materials = std::map<std::wstring, Material>;