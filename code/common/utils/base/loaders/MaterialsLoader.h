#pragma once

#include <fstream>
#include <string>
#include <regex>
#include <sstream>
#include <fstream>

#include "../../abstract/loaders/IMaterialsLoader.h"

#include "../ExceptionLoader.h"

namespace configs
{
	class MaterialsLoader: public IMaterialsLoader
	{
	public:
		Materials loadMaterials(const std::wstring& path) override;
	};

}