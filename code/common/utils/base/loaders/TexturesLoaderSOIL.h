#pragma once

#include "../../abstract/loaders/ITexturesLoader.h"

#include "../../../../../libs/opengl/Simple OpenGL Image Library/src/SOIL.h"

#pragma comment(lib, "../../../../libs/opengl/Simple OpenGL Image Library/lib/SOIL.lib")

#include "../ExceptionLoader.h"

class TexturesLoaderSOIL: public ITexturesLoader
{
public:
	Texture loadTexture(const std::wstring& path) override;
};