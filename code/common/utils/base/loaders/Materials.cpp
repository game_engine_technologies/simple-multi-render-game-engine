#include "Materials.h"

Material::Material()
{
	
}

Material::Material(const gmath::Color& a, const gmath::Color& d, const gmath::Color& s, const float& sn):
	ambient(a), diffuse(d), specular(s), shininess(sn)
{
}
