#include "TextProperty.h"

bool KeyTextProperty::operator<(const KeyTextProperty& r)const
{
	return (symbol < r.symbol) || ((symbol == r.symbol) && (height < r.height));
}