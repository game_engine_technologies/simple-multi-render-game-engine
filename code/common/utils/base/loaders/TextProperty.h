#pragma once

#include <map>
#include <string>

#include "../../../renders/base/math_header.h"


struct TextProperty
{
	int width;
	gmath::UVCoordinate leftTop;
	gmath::UVCoordinate rightTop;
	gmath::UVCoordinate rightBottom;
	gmath::UVCoordinate leftBottom;
};

struct KeyTextProperty
{
	wchar_t symbol;
	int height;
	
	bool operator<(const KeyTextProperty& r)const;
};

struct TextConfigParsed
{
	std::wstring pathTexture;
	std::wstring pathConfig;
	int height;
};

using MapTextProperties = std::map<KeyTextProperty, TextProperty>;
using ArrayTextConfigParsed = std::vector<TextConfigParsed>;
using TexturesProperties = std::vector<std::pair<int, std::wstring>>;
