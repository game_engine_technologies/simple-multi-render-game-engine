#pragma once

#include <fstream>
#include <string>
#include <regex>
#include <sstream>

#include "../../abstract/loaders/ITextConfigLoader.h"

#include "../ExceptionLoader.h"

namespace configs
{
	class TextConfigLoader : public ITextConfigLoader
	{
		float to_float(const std::wstring& str);
		int to_int(const std::wstring& str);
		int to_wchar_t(const std::wstring& str);

		ArrayTextConfigParsed getListConfigs(const std::wstring& path);
	public:
		void loadConfig(const std::wstring& path, MapTextProperties* mtp, TexturesProperties* textures) override;
	};
}