#include "TextConfigLoader.h"

namespace configs
{
	float TextConfigLoader::to_float(const std::wstring& str)
	{
		float _out;
		std::wstringstream _to_double;
		_to_double << str;
		_to_double >> _out;
		return _out;
	}

	int TextConfigLoader::to_int(const std::wstring& str)
	{
		int _out;
		std::wstringstream _to_int;
		_to_int << str;
		_to_int >> _out;
		return _out;
	}

	int TextConfigLoader::to_wchar_t(const std::wstring& str)
	{
		return static_cast<wchar_t>(to_int(str));
	}

	ArrayTextConfigParsed TextConfigLoader::getListConfigs(const std::wstring& path)
	{
		ArrayTextConfigParsed cfgParsed;
		std::wifstream file(path);
		std::wregex regular(L"([\\w.\\/]+)");
		if (!file)
			throw ExceptionLoader(L"Error in load text cfg: error open file -> " + path + L"\n");
		while (true)
		{
			std::wstring tmp;
			std::getline(file, tmp, L'\n');
			if (!file)
				break;
			std::vector<std::wstring> parts;
			for (auto it = std::wsregex_iterator(tmp.cbegin(), tmp.cend(), regular); it != std::wsregex_iterator(); ++it)
				parts.push_back(it->str());
			if (parts.size() != 3)
				throw ExceptionLoader(L"Error in read text cfg: error read file -> " + path + L"\n");
			TextConfigParsed tcParsed;
			tcParsed.height = to_int(parts[0]);
			tcParsed.pathConfig = parts[1];
			tcParsed.pathTexture = parts[2];
			cfgParsed.emplace_back(tcParsed);
		}
		return cfgParsed;
	}

	void TextConfigLoader::loadConfig(const std::wstring& path, MapTextProperties* mtp, TexturesProperties* textures)
	{
		ArrayTextConfigParsed arrCfg = getListConfigs(path);
		MapTextProperties textProps;
		TexturesProperties texturesProps;

		for (auto&& cfg : arrCfg)
		{
			std::wifstream file(cfg.pathConfig);
			std::wregex regular(L"([\\w.]+)");
			if (!file)
				throw ExceptionLoader(L"Error in load text cfg: error open file -> " + path + L"\n");
			while (true)
			{
				std::wstring tmp;
				std::getline(file, tmp, L'\n');
				if (!file)
					break;
				std::vector<std::wstring> parts;
				for (auto it = std::wsregex_iterator(tmp.cbegin(), tmp.cend(), regular); it != std::wsregex_iterator(); ++it)
					parts.push_back(it->str());
				if (parts.size() != 10)
					throw ExceptionLoader(L"Error in read text cfg: error read file -> " + path + L"\n");
				TextProperty tprop;
				tprop.width = to_int(parts[1]);
				tprop.leftTop = { to_float(parts[2]), to_float(parts[3]) };
				tprop.rightTop = { to_float(parts[4]), to_float(parts[5]) };
				tprop.rightBottom = { to_float(parts[6]), to_float(parts[7]) };
				tprop.leftBottom = { to_float(parts[8]), to_float(parts[9]) };
				KeyTextProperty key;
				key.symbol = to_wchar_t(parts[0]);
				key.height = cfg.height;
				textProps.emplace(key, tprop);
			}
			texturesProps.push_back({ cfg.height, cfg.pathTexture });
		}
		*mtp = std::move(textProps);
		*textures = std::move(texturesProps);
	}

}