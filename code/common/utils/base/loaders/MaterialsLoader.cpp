#pragma once

#include <fstream>
#include <string>
#include <regex>
#include <sstream>
#include <functional>

#include "../../abstract/loaders/ITextConfigLoader.h"

#include "../ExceptionLoader.h"
#include "MaterialsLoader.h"

namespace configs
{
	Materials MaterialsLoader::loadMaterials(const std::wstring& path)
	{
		Materials buffer;
		std::wstring currentKey;
		Material currentMaterial;

		std::wifstream materialCfg(path.c_str());
		if (!materialCfg)
			throw ExceptionLoader(L"Error open file material config");
		std::map<std::wstring, std::function<void(const std::wstring&)>> sectionsParsers =
		{
			{
				L"name", [&currentKey](const std::wstring& data)
				{
					currentKey = data;
				}
			},
			{
				L"ambient", [&currentMaterial](const std::wstring& data)
				{
					std::wregex regular(L"\\s+");
					std::vector<std::wstring> results;
					std::for_each(std::wsregex_token_iterator(data.begin(), data.end(), regular, -1), 
						std::wsregex_token_iterator(), 
						[&results](const std::wstring& data)
						{
							std::wstring tmp = std::regex_replace(data, std::wregex(L"^[ ]*(.*?)[ ]*$"), L"$1");
							results.push_back(tmp);
						}
					);
					const int sizenNumber(4);
					float number[sizenNumber];
					for (size_t i(0); i < results.size(); ++i)
					{
						std::wstringstream stream(results[i]);
						stream >> number[i];
						if (!stream)
							throw ExceptionLoader(L"Error get vector component for material: ambient");
					}
					currentMaterial.ambient = { number[0], number[1], number[2], number[3] };
				}
			},
			{
				L"diffuse", [&currentMaterial](const std::wstring& data)
				{
					std::wregex regular(L"\\s+");
					std::vector<std::wstring> results;
					std::for_each(std::wsregex_token_iterator(data.begin(), data.end(), regular, -1),
						std::wsregex_token_iterator(),
						[&results](const std::wstring& data)
						{
							std::wstring tmp = std::regex_replace(data, std::wregex(L"^[ ]*(.*?)[ ]*$"), L"$1");
							results.push_back(tmp);
						}
					);
					const int sizenNumber(4);
					float number[sizenNumber];
					for (size_t i(0); i < results.size(); ++i)
					{
						std::wstringstream stream(results[i]);
						stream >> number[i];
						if (!stream)
							throw ExceptionLoader(L"Error get vector component for material: diffuse");
					}
					currentMaterial.diffuse = { number[0], number[1], number[2], number[3] };
				}
			},
			{
				L"specular", [&currentMaterial](const std::wstring& data)
				{
					std::wregex regular(L"\\s+");
					std::vector<std::wstring> results;
					std::for_each(std::wsregex_token_iterator(data.begin(), data.end(), regular, -1),
						std::wsregex_token_iterator(),
						[&results](const std::wstring& data)
						{
							std::wstring tmp = std::regex_replace(data, std::wregex(L"^[ ]*(.*?)[ ]*$"), L"$1");
							results.push_back(tmp);
						}
					);
					const int sizenNumber(4);
					float number[sizenNumber];
					for (size_t i(0); i < results.size(); ++i)
					{
						std::wstringstream stream(results[i]);
						stream >> number[i];
						if (!stream)
							throw ExceptionLoader(L"Error get vector component for material: specular");
					}
					currentMaterial.specular = { number[0], number[1], number[2], number[3] };
				}
			},
			{
				L"shininess", [&currentMaterial](const std::wstring& data)
				{
					std::wstringstream stream(data);
					stream >> currentMaterial.shininess;
					if (!stream)
						throw ExceptionLoader(L"Error get number for material: shininess");
				}
			}
		};

		for (auto iter = std::istream_iterator<locale::LineUnicode, wchar_t>(materialCfg); 
			iter != std::istream_iterator<locale::LineUnicode, wchar_t>(); iter++)
		{
			std::wstring data(*iter);
			size_t ind(0);
			if ((ind = data.find(L";")) != std::wstring::npos)
				data.erase(ind);
			data = std::regex_replace(data, std::wregex(L"^[ ]*(.*?)[ ]*$"), L"$1");
			if (data.empty())
				continue;
			std::wregex regular(L"=");
			std::vector<std::wstring> results;
			std::for_each(std::wsregex_token_iterator(data.begin(), data.end(), regular, -1),
				std::wsregex_token_iterator(),
				[&results](const std::wstring& data)
				{
					std::wstring tmp = std::regex_replace(data, std::wregex(L"^[ ]*(.*?)[ ]*$"), L"$1");
					results.push_back(tmp);
				}
			);
			if (results.size() != 2)
				throw ExceptionLoader(L"Error parsing line in material config");
			if (sectionsParsers.find(results[0]) != sectionsParsers.end())
				sectionsParsers[results[0]](results[1]);
			if(results[0] == L"shininess")
				buffer.emplace(currentKey, currentMaterial);
		}
		return buffer;
	}
}