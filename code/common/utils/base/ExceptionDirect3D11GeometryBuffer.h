#pragma once

#include "../abstract/IException.h"

class ExceptionDirect3D11GeometryBuffer : public IException
{
public:
	ExceptionDirect3D11GeometryBuffer(const wchar_t* e);
	ExceptionDirect3D11GeometryBuffer(const std::wstring& e);
	std::wstring getError();
};


