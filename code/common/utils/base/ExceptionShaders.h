#pragma once

#include "../abstract/IException.h"

class ExceptionShaders : public IException
{
public:
	ExceptionShaders(const wchar_t* e);
	ExceptionShaders(const std::wstring& e);
	std::wstring getError();
};
