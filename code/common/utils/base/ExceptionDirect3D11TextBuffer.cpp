#include "ExceptionDirect3D11TextBuffer.h"

ExceptionDirect3D11TextBuffer::ExceptionDirect3D11TextBuffer(const wchar_t* e):IException(e)
{
}

ExceptionDirect3D11TextBuffer::ExceptionDirect3D11TextBuffer(const std::wstring& e):IException(e)
{
}

std::wstring ExceptionDirect3D11TextBuffer::getError()
{
	return L"Error in Direct3D11TextBufferA! Description: " + error;
}
