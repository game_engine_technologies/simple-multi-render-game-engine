#include "ExceptionDirect3D11BufferCreator.h"

ExceptionDirect3D11BufferCreator::ExceptionDirect3D11BufferCreator(const wchar_t* e):IException(e)
{
}

ExceptionDirect3D11BufferCreator::ExceptionDirect3D11BufferCreator(const std::wstring& e):IException(e)
{
}

std::wstring ExceptionDirect3D11BufferCreator::getError()
{
	return L"Error create buffer d3d11! Description: " + error;
}
