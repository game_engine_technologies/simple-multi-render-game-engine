#pragma once

#include "../abstract/IException.h"

class ExceptionDirect3D11TextBuffer : public IException
{
public:
	ExceptionDirect3D11TextBuffer(const wchar_t* e);
	ExceptionDirect3D11TextBuffer(const std::wstring& e);
	std::wstring getError();
};


