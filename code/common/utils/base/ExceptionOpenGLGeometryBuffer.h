#pragma once

#include "../abstract/IException.h"

class ExceptionOpenGLGeometryBuffer : public IException
{
public:
	ExceptionOpenGLGeometryBuffer(const wchar_t* e);
	ExceptionOpenGLGeometryBuffer(const std::wstring& e);
	std::wstring getError();
};


