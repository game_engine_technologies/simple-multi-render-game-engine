#include "InputSystem.h"
#include "../ExceptionMemory.h"

MousePosition::MousePosition() :x(0), y(0)
{
	
}

MousePosition::MousePosition(int x, int y):x(x), y(y)
{
}

InputSystem::InputSystem():countMouseWheel(0)
{
	keyboardKeys = 
	{
		{InputSystemKeys::KEY_HOME, false},
		{InputSystemKeys::KEY_END, false},
		{InputSystemKeys::KEY_ESC, false},
		{InputSystemKeys::KEY_W, false},
		{InputSystemKeys::KEY_A, false},
		{InputSystemKeys::KEY_S, false},
		{InputSystemKeys::KEY_D, false}
	};
	mouseKeys =
	{
		{InputSystemMouse::MOUSE_LEFT, false},
		{InputSystemMouse::MOUSE_RIGHT, false},
		{InputSystemMouse::MOUSE_WHEEL, false}
	};
}

InputSystemSharedSmartPtr InputSystem::instance;

InputSystemSharedSmartPtr& InputSystem::getInstance()
{
	if (!instance)
	{
		instance.reset(new InputSystem());
		if(!instance)
			throw ExceptionMemory(L"InputSystemSmartPtr is not create, memory not found!");
	}
	return instance;
}

void InputSystem::setKeyboardKey(InputSystemKeys k, bool status)
{
	auto key = keyboardKeys.find(k);
	if (key == keyboardKeys.end())
		return;
		//throw ExceptionInputSystem(L"Error set keyboard key: " + std::to_wstring(static_cast<int>(k)));
	key->second = status;
}

void InputSystem::setMouseKey(InputSystemMouse k, bool status)
{
	auto key = mouseKeys.find(k);
	if (key == mouseKeys.end())
		return;
		//throw ExceptionInputSystem(L"Error set mouse key: " + std::to_wstring(static_cast<int>(k)));
	key->second = status;

}

void InputSystem::setMousePosition(int x, int y)
{
	mousePosition.x = x;
	mousePosition.y = y;
}

void InputSystem::setMouseWheel(int wh)
{
	countMouseWheel = wh;
}

bool InputSystem::getKeyboardKey(InputSystemKeys k)
{
	auto key = keyboardKeys.find(k);
	if (key == keyboardKeys.end())
		throw ExceptionInputSystem(L"Error get keyboard key: " + std::to_wstring(static_cast<int>(k)));
	return key->second;
}

bool InputSystem::getMouseKey(InputSystemMouse k)
{
	auto key = mouseKeys.find(k);
	if (key == mouseKeys.end())
		throw ExceptionInputSystem(L"Error get mouse key: " + std::to_wstring(static_cast<int>(k)));
	return key->second;
}

MousePosition InputSystem::getMousePosition()
{
	return mousePosition;
}

int InputSystem::getMouseWheel()
{
	return countMouseWheel;
}

int input_system::keyboard_key_cast(InputSystemKeys kk)
{
	return static_cast<int>(kk);
}

int input_system::mouse_key_cast(InputSystemMouse kk)
{
	return static_cast<int>(kk);
}
