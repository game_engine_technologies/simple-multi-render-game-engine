#pragma once

#include <map>
#include <memory>

#include "../ExceptionInputSystem.h"

struct MousePosition
{
	int x;
	int y;

	MousePosition();
	MousePosition(int x, int y);
};

using CenterWindow = MousePosition;
using DeltaMousePosition = MousePosition;

enum class InputSystemKeys : short
{
	KEY_HOME = 36,
	KEY_END = 35,
	KEY_ESC = 27,
	KEY_W = 87,
	KEY_A = 65,
	KEY_S = 83,
	KEY_D = 68
};

enum class InputSystemMouse : short
{
	MOUSE_LEFT = 1,
	MOUSE_RIGHT = 2,
	MOUSE_WHEEL = 4
};

namespace input_system
{
	int keyboard_key_cast(InputSystemKeys kk);
	int mouse_key_cast(InputSystemMouse kk);

}

class InputSystem;

//class DeleterInputSystem
//{
//public:
//	void operator()(InputSystem* is)
//	{
//		if (is)
//			delete is;
//	}
//};

using InputSystemSharedSmartPtr = std::shared_ptr<InputSystem>;
using InputSystemWeakSmartPtr = std::weak_ptr<InputSystem>;

class InputSystem
{
	std::map<InputSystemKeys, bool> keyboardKeys;
	std::map<InputSystemMouse, bool> mouseKeys;
	MousePosition mousePosition;
	int countMouseWheel;

	InputSystem();
	InputSystem(const InputSystem& is) = delete;
	void operator=(const InputSystem& is) = delete;

	static InputSystemSharedSmartPtr instance;

public:
	static InputSystemSharedSmartPtr& getInstance();

	void setKeyboardKey(InputSystemKeys k, bool status);
	void setMouseKey(InputSystemMouse k, bool status);
	void setMousePosition(int x, int y);
	void setMouseWheel(int wh);
	bool getKeyboardKey(InputSystemKeys k);
	bool getMouseKey(InputSystemMouse k);
	MousePosition getMousePosition();
	int getMouseWheel();

};

