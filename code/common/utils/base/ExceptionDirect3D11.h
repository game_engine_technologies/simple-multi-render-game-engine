#pragma once

#include "../abstract/IException.h"

class ExceptionDirect3D11: public IException
{
public:
	ExceptionDirect3D11(const wchar_t* e);
	ExceptionDirect3D11(const std::wstring& e);
	std::wstring getError();
};


