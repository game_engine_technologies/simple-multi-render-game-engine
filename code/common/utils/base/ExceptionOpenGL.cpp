#include "ExceptionOpenGL.h"

ExceptionOpenGL::ExceptionOpenGL(const wchar_t* e):IException(e)
{
}

ExceptionOpenGL::ExceptionOpenGL(const std::wstring& e):IException(e)
{
}

std::wstring ExceptionOpenGL::getError()
{
	return L"Error in OpenGL render! Description: " + error;
}
