#include "ExceptionDirect3D11GeometryBuffer.h"

ExceptionDirect3D11GeometryBuffer::ExceptionDirect3D11GeometryBuffer(const wchar_t* e):IException(e)
{
}

ExceptionDirect3D11GeometryBuffer::ExceptionDirect3D11GeometryBuffer(const std::wstring& e):IException(e)
{
}

std::wstring ExceptionDirect3D11GeometryBuffer::getError()
{
	return L"Error in Direct3D11GeometryBuffer! Description: " + error;
}
