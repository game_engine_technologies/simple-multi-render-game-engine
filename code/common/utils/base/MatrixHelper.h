#pragma once

#include "../../math/mathematics.h"

class MatrixCPUAndGPU
{
	gmath::Matrix4 _m;
	gmath::Matrix4 _mT;

	void createTransponse();

public:
	MatrixCPUAndGPU();

	MatrixCPUAndGPU(const gmath::Matrix4& m);

	gmath::Matrix4& getCPUMatrix();

	gmath::Matrix4& getGPUMatrix();

	void updateCPUMatrix(const gmath::Matrix4& m);

};