#pragma once

#include "../abstract/IException.h"

class ExceptionInputSystem : public IException
{
public:
	ExceptionInputSystem(const wchar_t* e);
	ExceptionInputSystem(const std::wstring& e);
	std::wstring getError();
};
