#pragma once

#include "../abstract/IException.h"

class ExceptionLoader: public IException
{
public:
	ExceptionLoader(const wchar_t* e);
	ExceptionLoader(const std::wstring& e);
	std::wstring getError();
};
