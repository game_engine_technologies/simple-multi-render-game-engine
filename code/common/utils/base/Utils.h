#define WIN32_LEAN_AND_MEAN

#pragma once

#include <memory>
#include <string>
#include <windows.h>
#include <fstream>

namespace clears
{
    template<class T>
    void ReleaseCom(T& com)
    {
        if (com)
            com.Reset();
        com = nullptr;
    }

}


namespace cast
{
    template<typename Derived, typename Base, typename Del>
    std::unique_ptr<Derived, Del> static_unique_ptr_cast(std::unique_ptr<Base, Del>&& p)
    {
        auto d = static_cast<Derived*>(p.release());
        return std::unique_ptr<Derived, Del>(d, std::move(p.get_deleter()));
    }

    template<typename Derived, typename Base, typename Del>
    std::unique_ptr<Derived, Del> dynamic_unique_ptr_cast(std::unique_ptr<Base, Del>&& p)
    {
        if (Derived* result = dynamic_cast<Derived*>(p.get()))
        {
            p.release();
            return std::unique_ptr<Derived, Del>(result, std::move(p.get_deleter()));
        }
        return std::unique_ptr<Derived, Del>(nullptr, p.get_deleter());
    }
}

namespace debug
{
    void printError(const std::wstring& er);
}

namespace locale
{
	class LineUnicode : public std::wstring
	{
	public:
		friend std::wistream& operator>>(std::wistream& is, LineUnicode& l);

	};

	class LineAscii : public std::string
	{
	public:
		friend std::istream& operator>>(std::istream& is, LineAscii& l);

	};

}