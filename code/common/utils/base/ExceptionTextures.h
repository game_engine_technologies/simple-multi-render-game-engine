#pragma once

#include "../abstract/IException.h"

class ExceptionTextures : public IException
{
public:
	ExceptionTextures(const wchar_t* e);
	ExceptionTextures(const std::wstring& e);
	std::wstring getError();
};
