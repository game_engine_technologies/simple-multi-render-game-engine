#include "ExceptionOpenGLGeometryBuffer.h"

ExceptionOpenGLGeometryBuffer::ExceptionOpenGLGeometryBuffer(const wchar_t* e):IException(e)
{
}

ExceptionOpenGLGeometryBuffer::ExceptionOpenGLGeometryBuffer(const std::wstring& e):IException(e)
{
}

std::wstring ExceptionOpenGLGeometryBuffer::getError()
{
	return L"Error in OpenGLGeometryBuffer! Description: " + error;
}
