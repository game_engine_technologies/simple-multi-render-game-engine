#pragma once

#include "../abstract/IException.h"

class ExceptionOpenGL : public IException
{
public:
	ExceptionOpenGL(const wchar_t* e);
	ExceptionOpenGL(const std::wstring& e);
	std::wstring getError();
};


