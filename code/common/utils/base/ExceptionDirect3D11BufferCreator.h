#pragma once

#include "../abstract/IException.h"

class ExceptionDirect3D11BufferCreator : public IException
{
public:
	ExceptionDirect3D11BufferCreator(const wchar_t* e);
	ExceptionDirect3D11BufferCreator(const std::wstring& e);
	std::wstring getError();
};


