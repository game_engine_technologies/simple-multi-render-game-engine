#pragma once

#include "../abstract/IException.h"

class ExceptionMemory: public IException
{
public:
	ExceptionMemory(const wchar_t* e);
	ExceptionMemory(const std::wstring& e);
	std::wstring getError();
};
