#include "ExceptionDirect3D11.h"

ExceptionDirect3D11::ExceptionDirect3D11(const wchar_t* e):IException(e)
{
}

ExceptionDirect3D11::ExceptionDirect3D11(const std::wstring& e):IException(e)
{
}

std::wstring ExceptionDirect3D11::getError()
{
	return L"Error in Direct3D11 render! Description: " + error;
}
