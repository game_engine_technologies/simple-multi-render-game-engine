#include "MatrixHelper.h"

void MatrixCPUAndGPU::createTransponse()
{
	_mT = gmath::Matrix4::Transponse(_m);
}

MatrixCPUAndGPU::MatrixCPUAndGPU() :_m(gmath::Matrix::Identity()), _mT(gmath::Matrix::Identity()) {}

MatrixCPUAndGPU::MatrixCPUAndGPU(const gmath::Matrix4& m) :_m(m)
{
	createTransponse();
}

gmath::Matrix4& MatrixCPUAndGPU::getCPUMatrix()
{
	return _m;
}

gmath::Matrix4& MatrixCPUAndGPU::getGPUMatrix()
{
	return _mT;
}

void MatrixCPUAndGPU::updateCPUMatrix(const gmath::Matrix4& m)
{
	_m = m;
	createTransponse();
}
