#pragma once

#include <memory>
#include "../../base/loaders/Materials.h"


namespace configs
{
	class IMaterialsLoader
	{
	public:
		virtual Materials loadMaterials(const std::wstring& path) = 0;
	};

	using MaterialsLoaderSmartPtr = std::shared_ptr<IMaterialsLoader>;

}