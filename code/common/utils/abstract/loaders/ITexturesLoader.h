#pragma once

#include <vector>
#include <string>
#include <memory>

using _byte = unsigned char;

struct Texture
{
	std::vector<_byte> image;
	int width;
	int height;
};

class ITexturesLoader
{
public:
	virtual Texture loadTexture(const std::wstring& path) = 0;
};

using TexturesLoaderSmartPtr = std::shared_ptr<ITexturesLoader>;