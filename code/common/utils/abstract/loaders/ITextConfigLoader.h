#pragma once

#include <map>
#include <string>
#include <memory>

#include "../../base/loaders/TextProperty.h"

namespace configs
{
	class ITextConfigLoader
	{
	public:
		virtual void loadConfig(const std::wstring& path, MapTextProperties* mtp, TexturesProperties* textures) = 0;
	};

	using TextConfigLoaderSmartPtr = std::shared_ptr<ITextConfigLoader>;

}