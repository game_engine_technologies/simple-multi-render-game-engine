#pragma once

#include <string>

#include "../../utils/base/Utils.h"

class IException
{
protected:
	std::wstring error;
public:
	IException(const wchar_t* e);
	IException(const std::wstring& e);
	virtual std::wstring getError() = 0;
};

