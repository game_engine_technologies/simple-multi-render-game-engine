#pragma once

#include "../abstract/ILevelOperationSystem.h"

class LevelOperationSystemWindows : public ILevelOperationSystem
{
	double countsPerSecond;
	long long counterStart;
	long long frameTimeOld;

public:
	LevelOperationSystemWindows();
	void init() override;

	void startTimer() override;
	double getTime() override;
	double getFrameTime() override;
	void setCursorCenterScreen(int x, int y) override;
	void showCursor() override;
	void showMessageBox(const std::wstring& m) override;
	void printDebugMessage(const std::wstring& m) override;
};

using LevelOperationSystemWindowsSmartPtr = std::shared_ptr<LevelOperationSystemWindows>;
