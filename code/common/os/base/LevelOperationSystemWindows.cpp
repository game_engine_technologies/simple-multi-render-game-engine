#include "LevelOperationSystemWindows.h"

LevelOperationSystemWindows::LevelOperationSystemWindows() :countsPerSecond(0.), counterStart(0.), frameTimeOld(0.)
{
}

void LevelOperationSystemWindows::init()
{

}

void LevelOperationSystemWindows::startTimer()
{
	LARGE_INTEGER frequencyCount;
	QueryPerformanceFrequency(&frequencyCount);

	countsPerSecond = double(frequencyCount.QuadPart);

	QueryPerformanceCounter(&frequencyCount);
	counterStart = frequencyCount.QuadPart;
}

double LevelOperationSystemWindows::getTime()
{
	LARGE_INTEGER currentTime;
	QueryPerformanceCounter(&currentTime);
	return double(currentTime.QuadPart - counterStart) / countsPerSecond;
}

double LevelOperationSystemWindows::getFrameTime()
{
	LARGE_INTEGER currentTime;
	__int64 tickCount;
	QueryPerformanceCounter(&currentTime);

	tickCount = currentTime.QuadPart - frameTimeOld;
	frameTimeOld = currentTime.QuadPart;

	if (tickCount < 0.0f)
		tickCount = 0.0f;

	return float(tickCount) / countsPerSecond;
}

void LevelOperationSystemWindows::setCursorCenterScreen(int x, int y)
{
	SetCursorPos(x, y);
}

void LevelOperationSystemWindows::showCursor()
{
	while (ShowCursor(0) > 0);
}

void LevelOperationSystemWindows::showMessageBox(const std::wstring& m)
{
	MessageBox(NULL, m.c_str(), L"Fatal error", MB_OK | MB_ICONERROR);
}

void LevelOperationSystemWindows::printDebugMessage(const std::wstring& m)
{
	OutputDebugString(m.c_str());
}



