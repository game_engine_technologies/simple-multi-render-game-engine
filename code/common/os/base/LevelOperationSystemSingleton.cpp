#include "LevelOperationSystemSingleton.h"
#include "../../utils/base/ExceptionMemory.h"
#include "LevelOperationSystemWindows.h"

LevelOperationSystemSmartPtr LevelOperationSystemSingleton::instance;

LevelOperationSystemSmartPtr& LevelOperationSystemSingleton::getInstance()
{
	if (!instance)
	{
#ifdef WIN32
		instance = std::make_shared<LevelOperationSystemWindows>();
#endif
		if (!instance)
			throw ExceptionMemory(L"LevelOperationSystem is not create, memory not found!");
		instance->init();
	}
	return instance;
}
