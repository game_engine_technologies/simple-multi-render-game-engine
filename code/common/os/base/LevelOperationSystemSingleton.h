#pragma once

#include "../abstract/ILevelOperationSystem.h"

class LevelOperationSystemSingleton
{
	static LevelOperationSystemSmartPtr instance;

	LevelOperationSystemSingleton() = delete;
	LevelOperationSystemSingleton(const LevelOperationSystemSingleton&) = delete;
	void operator=(const LevelOperationSystemSingleton&) = delete;


public:
	static LevelOperationSystemSmartPtr& getInstance();
};
