#pragma once

#include <mutex>
#include <memory>

#include "../../utils/base/input/InputSystem.h"
#include "../../utils/base/TypeCamera.h"
#include "../../math/mathematics.h"

class ILevelOperationSystem
{
protected:

public:
	virtual void init() = 0;

	// timers for frame
	virtual void startTimer() = 0;
	virtual double getTime() = 0;
	virtual double getFrameTime() = 0;
	virtual void setCursorCenterScreen(int x, int y) = 0;
	virtual void showCursor() = 0;
	virtual void showMessageBox(const std::wstring& m) = 0;
	virtual void printDebugMessage(const std::wstring& m) = 0;
};

using LevelOperationSystemSmartPtr = std::shared_ptr<ILevelOperationSystem>;
