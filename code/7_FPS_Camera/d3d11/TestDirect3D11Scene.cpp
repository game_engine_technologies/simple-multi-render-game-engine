#include "TestDirect3D11Scene.h"

#include "../../common/renders/base/scene/model/IGeometryBufferVisitor.h"
#include "../../common/renders/base/scene/camera/ICameraVisitor.h"

TestDirect3D11Scene::TestDirect3D11Scene(const ComPtr<ID3D11Device>& d3dDevice,
	const ComPtr<ID3D11DeviceContext>& d3dImmediateContext):Direct3D11Scene(d3dDevice, d3dImmediateContext)
{
	
}

void TestDirect3D11Scene::init(const std::wstring& path, SceneParameters rp)
{
	Direct3D11Scene::init(path, rp);
	// init camera
	VisitorCameraCreateAndUpdateSmartPtr visitorCamera = std::make_shared<VisitorCameraCreateAndUpdate>(d3dDevice, d3dImmediateContext);
	if(!visitorCamera)
		throw ExceptionMemory(L"VisitorCameraCreateAndUpdate is not create, memory not found!");
	camera = std::make_shared<Direct3D11FPSCamera>();
	if (!camera)
		throw ExceptionMemory(L"Direct3D11FPSCamera is not create, memory not found!");
	camera->setFarZ(1000.f);
	camera->setNearZPerspective(0.001f);
	camera->updatePerspective(gmath::GeneralMath::PI_4, rp.getClientWidth(), rp.getClientHeight());
	camera->updateView(gmath::Vector3(0.f, 5.f, -20.f), gmath::Vector3(0.f, 5.f, 0.f), gmath::Vector3(0.f, 1.f, 0.f));
	camera->init(visitorCamera);

	// ��������
	bufferTexture = std::make_shared<Direct3D11GeometryBufferC>();
	if(!bufferTexture)
		throw ExceptionMemory(L"Direct3D11GeometryBufferC is not create, memory not found!");
	// init buffer
	VisitorGeometryBufferCreateAndDrawSmartPtr visitorTextures = std::make_shared<VisitorGeometryBufferCreateAndDraw>(
		d3dDevice,
		d3dImmediateContext, 
		shadersBuffer,
		camera,
		texturesBuffer);
	if (!visitorTextures)
		throw ExceptionMemory(L"VisitorGeometryBufferCreateAndDraw is not create, memory not found!");
	bufferTexture->init(L"shaders/d3d11/texture_camera.d3d11_vs", L"shaders/d3d11/texture_camera.d3d11_ps", visitorTextures);

	// ������� �������
	std::vector<Index> indeces =
	{
		3,1,0,
		2,1,3,
		6,4,5,
		7,4,6,
		11,9,8,
		10,9,11,
		14,12,13,
		15,12,14,
		19,17,16,
		18,17,19,
		22,20,21,
		23,20,22
	};
	ModelParameters mp;
	mp.samplerKey = L"default";
	mp.type = TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_C;
	mp.indeces = indeces;

	// ��� 1
	std::vector<VertexTexture> vertices =
	{
		{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f)},
		{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f)},
		{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f)},
		{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f)},
		{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::UVCoordinate(0.0f, 1.0f)},
		{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f)}
	};
	// ������ 1
	mp.key = L"cube1";
	mp.textureKey = L"textures/wood1.dds";
	mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(-5.f, 0.f, 5.f);
	mp.verticesTexture = vertices;
	if (!bufferTexture->addModel(mp))
		return;

	// ��� 2
	// ������ 2
	mp.textureKey = L"textures/wood2.dds";
	mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(5.f, 0.f, 5.f);
	if (!bufferTexture->addModel(mp))
		return;
	
	bufferTexture->reinit();


	// ����
	bufferColor = std::make_shared<Direct3D11GeometryBufferB>();
	if (!bufferColor)
		throw ExceptionMemory(L"Direct3D11GeometryBufferB is not create, memory not found!");
	// init buffer
	VisitorGeometryBufferCreateAndDrawSmartPtr visitorColor = std::make_shared<VisitorGeometryBufferCreateAndDraw>(
		d3dDevice,
		d3dImmediateContext,
		shadersBuffer,
		camera);
	if (!visitorColor)
		throw ExceptionMemory(L"VisitorGeometryBufferCreateAndDraw is not create, memory not found!");
	bufferColor->init(L"shaders/d3d11/color_camera.d3d11_vs", L"shaders/d3d11/color_camera.d3d11_ps", visitorColor);

	// ������� �������
	indeces =
	{
		3,1,0,
		2,1,3,
		0,5,4,
		1,5,0,
		3,4,7,
		0,4,3,
		1,6,5,
		2,6,1,
		2,7,6,
		3,7,2,
		6,4,5,
		7,4,6,
	};
	mp.type = TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_B;
	mp.indeces = indeces;

	// ��� 3
	std::vector<VertexColor> v =
	{
		{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::Color(0.0f, 0.0f, 1.0f, 1.0f) },
		{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::Color(0.0f, 1.0f, 0.0f, 1.0f) },
		{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::Color(0.0f, 1.0f, 1.0f, 1.0f) },
		{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::Color(1.0f, 0.0f, 0.0f, 1.0f) },
		{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::Color(1.0f, 0.0f, 1.0f, 1.0f) },
		{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::Color(1.0f, 1.0f, 0.0f, 1.0f) },
		{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::Color(1.0f, 1.0f, 1.0f, 1.0f) },
		{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::Color(0.0f, 0.0f, 0.0f, 1.0f) }
	};
	// ������ 3
	mp.key = L"cube2";
	mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(-5.f, 0.f, -5.f);
	mp.verticesColor = v;
	if (!bufferColor->addModel(mp))
		return;

	bufferColor->reinit();


	// ��� 4
	// ������ 4
	mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(5.f, 0.f, -5.f);
	mp.verticesColor = v;
	if (!bufferColor->addModel(mp))
		return;

	// ���������
	float uv(15.f);
	vertices =
	{
		{ gmath::Point3(-100.0f, 0.0f, -100.0f), gmath::UVCoordinate(-uv, uv)},
		{ gmath::Point3(-100.0f, 0.0f, 100.0f), gmath::UVCoordinate(-uv, -uv)},
		{ gmath::Point3(100.0f, 0.0f, 100.0f), gmath::UVCoordinate(uv, -uv)},
		{ gmath::Point3(100.0f, 0.0f, -100.0f), gmath::UVCoordinate(uv, uv)}
	};
	indeces =
	{
		0, 1, 2,
		0, 2, 3
	};
	mp.type = TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_C;
	mp.samplerKey = L"default";
	mp.indeces = indeces;
	mp.key = L"terrain";
	mp.textureKey = L"textures/grass1.dds";
	mp.world = gmath::Matrix::Identity();
	mp.verticesTexture = vertices;
	mp.world = gmath::Matrix::CreateTranslationMatrixY(-2.f);
	if (!bufferTexture->addModel(mp))
		return;

	bufferTexture->reinit();

}

bool TestDirect3D11Scene::update(SceneDynamicParameters& sdp)
{
	// �������� �� ������� esc
	if (InputSystem::getInstance()->getKeyboardKey(InputSystemKeys::KEY_ESC))
		return false;

	// ���������� ������
	camera->update(sdp.frameTime, getWindowBBox());
	return true;
}

TestDirect3D11Scene::~TestDirect3D11Scene()
{
	clear();
}

void TestDirect3D11Scene::clear()
{
	Direct3D11Scene::clear();
}

void TestDirect3D11Scene::render(const ComPtr<ID3D11DeviceContext>& d3dImmediateContext, const ComPtr<ID3D11RenderTargetView>& d3dRenderTargetView,
	const ComPtr<ID3D11DepthStencilView>& d3dDepthStencilView, const ComPtr<ID3D11DepthStencilState>& d3dDepthStencilEnableDepth,
	const ComPtr<ID3D11DepthStencilState>& d3dDepthStencilDisableDepth)
{
	gmath::Color color = { 0.0f, 0.0f, 1.0f, 1.0f };
	d3dImmediateContext->ClearRenderTargetView(d3dRenderTargetView.Get(), color.toArray().data());
	d3dImmediateContext->ClearDepthStencilView(d3dDepthStencilView.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
	// ������ ������
	bufferTexture->draw();
	bufferColor->draw();
}
