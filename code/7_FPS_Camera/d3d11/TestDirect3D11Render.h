#pragma once

#include "../../common/renders/base/OSWindowsDirect3D11Render.h"
#include "TestDirect3D11Scene.h"

class TestDirect3D11Render : public OSWindowsDirect3D11Render
{
public:
	void init(RenderParameters rp) override;
	bool updateScene(SceneDynamicParameters& sdp) override;
	void initScene() override;
	void render() override;
};