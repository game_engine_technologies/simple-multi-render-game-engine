#include "TestOpenGLScene.h"

#include "../../common/renders/base/scene/model/IGeometryBufferVisitor.h"
#include "../../common/renders/base/scene/camera/ICameraVisitor.h"

void TestOpenGLScene::init(const std::wstring& path, SceneParameters rp)
{
	OpenGLScene::init(path, rp);
	// init camera
	VisitorCameraCreateAndUpdateSmartPtr visitorCamera = std::make_shared<VisitorCameraCreateAndUpdate>();
	if (!visitorCamera)
		throw ExceptionMemory(L"VisitorCameraCreateAndUpdate is not create, memory not found!");
	camera = std::make_shared<OpenGLFPSCamera>();
	if (!camera)
		throw ExceptionMemory(L"OpenGLCamera is not create, memory not found!");
	camera->setFarZ(1000.f);
	camera->setNearZPerspective(0.1f);
	camera->updatePerspective(gmath::GeneralMath::PI_4, rp.getClientWidth(), rp.getClientHeight());
	camera->updateView(gmath::Vector3(0.f, 5.f, 20.f), gmath::Vector3(0.f, 5.f, 0.f), gmath::Vector3(0.f, 1.f, 0.f));
	camera->init(visitorCamera);
	ModelParameters mp;

	// ����
	bufferColors = std::make_shared<OpenGLGeometryBufferB>();
	if (!bufferColors)
		throw ExceptionMemory(L"OpenGLGeometryBufferB is not create, memory not found!");
	// init buffer
	VisitorGeometryBufferCreateAndDrawSmartPtr visitorColor = std::make_shared<VisitorGeometryBufferCreateAndDraw>(
		shadersBuffer,
		camera);
	if (!visitorColor)
		throw ExceptionMemory(L"VisitorGeometryBufferCreateAndDraw is not create, memory not found!");
	bufferColors->init(L"shaders/opengl/color_camera.gl_vs", L"shaders/opengl/color_camera.gl_ps", visitorColor);

	// ��������
	bufferTextures = std::make_shared<OpenGLGeometryBufferC>();
	if (!bufferTextures)
		throw ExceptionMemory(L"OpenGLGeometryBufferC is not create, memory not found!");
	// init buffer
	VisitorGeometryBufferCreateAndDrawSmartPtr visitorTexture = std::make_shared<VisitorGeometryBufferCreateAndDraw>(
		shadersBuffer,
		camera,
		texturesBuffer);
	if (!visitorTexture)
		throw ExceptionMemory(L"VisitorGeometryBufferCreateAndDraw is not create, memory not found!");
	bufferTextures->init(L"shaders/opengl/texture_camera.gl_vs", L"shaders/opengl/texture_camera.gl_ps", visitorTexture);

	//// ������� �������
	//std::vector<Index> ind =
	//{
	//	0, 1, 2,
	//	0, 2, 3,
	//	2, 1, 5,
	//	2, 5, 6,
	//	3, 2, 6,
	//	3, 6, 7,
	//	0, 3, 7,
	//	0, 7, 4,
	//	1, 0, 4,
	//	1, 4, 5,
	//	6, 5, 4,
	//	6, 4, 7
	//};
	//mp.type = TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_B;
	//mp.indeces = ind;

	//// ��� 1
	//std::vector<VertexColor> vert =
	//{
	//	{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::Color(0.0f, 0.0f, 1.0f, 1.0f) },
	//	{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::Color(0.0f, 1.0f, 0.0f, 1.0f) },
	//	{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::Color(0.0f, 1.0f, 1.0f, 1.0f) },
	//	{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::Color(1.0f, 0.0f, 0.0f, 1.0f) },
	//	{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::Color(1.0f, 0.0f, 1.0f, 1.0f) },
	//	{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::Color(1.0f, 1.0f, 0.0f, 1.0f) },
	//	{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::Color(1.0f, 1.0f, 1.0f, 1.0f) },
	//	{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::Color(0.0f, 0.0f, 0.0f, 1.0f) }
	//};

	//// ������ 3
	//mp.key = L"cube2";
	//mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(-5.f, 0.f, 5.f);
	//mp.verticesColor = vert;
	//if (!bufferColors->addModel(mp))
	//	return;

	//// ��� 4
	//// ������ 4
	//mp.key = L"cube2";
	//mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(5.f, 0.f, 5.f);
	//mp.verticesColor = vert;
	//if (!bufferColors->addModel(mp))
	//	return;

	//bufferColors->reinit();

	//// ��������
	//bufferTextures = std::make_shared<OpenGLGeometryBufferC>();
	//if (!bufferTextures)
	//	throw ExceptionMemory(L"OpenGLGeometryBufferC is not create, memory not found!");
	//// init buffer
	//VisitorGeometryBufferCreateAndDrawSmartPtr visitorTexture = std::make_shared<VisitorGeometryBufferCreateAndDraw>(
	//	shadersBuffer,
	//	camera,
	//	texturesBuffer);
	//if (!visitorTexture)
	//	throw ExceptionMemory(L"VisitorGeometryBufferCreateAndDraw is not create, memory not found!");
	//bufferTextures->init(L"shaders/opengl/texture_camera.gl_vs", L"shaders/opengl/texture_camera.gl_ps", visitorTexture);

	//// ������� �������
	//ind =
	//{
	//	0, 3, 1,  1, 3, 2, // front
	//	4, 7, 5,  5, 7, 6, // back
	//	8,11, 9,  9,11,10, // top
	//	12,15,13, 13,15,14, // bottom
	//	16,19,17, 17,19,18, // left
	//	20,23,21, 21,23,22  // right
	//};
	//mp.samplerKey = L"default";
	//mp.type = TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_C;
	//mp.indeces = ind;

	//// ��� 1
	//std::vector<VertexTexture> v =
	//{
	//	{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f) },
	//	{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::UVCoordinate(1.0f, 1.0f) },
	//	{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::UVCoordinate(1.0f, 0.0f) },
	//	{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 0.0f) },
	//	{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::UVCoordinate(0.0f, 1.0f) },
	//	{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 1.0f) },
	//	{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f) },
	//	{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::UVCoordinate(0.0f, 0.0f) },
	//	{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::UVCoordinate(0.0f, 1.0f) },
	//	{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 1.0f) },
	//	{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::UVCoordinate(1.0f, 0.0f) },
	//	{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 0.0f) },
	//	{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::UVCoordinate(0.0f, 1.0f) },
	//	{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 1.0f) },
	//	{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::UVCoordinate(1.0f, 0.0f) },
	//	{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 0.0f) },
	//	{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::UVCoordinate(0.0f, 1.0f) },
	//	{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::UVCoordinate(1.0f, 1.0f) },
	//	{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::UVCoordinate(1.0f, 0.0f) },
	//	{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::UVCoordinate(0.0f, 0.0f) },
	//	{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f) },
	//	{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 1.0f) },
	//	{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f) },
	//	{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 0.0f) }
	//};
	//// ������ 1
	//mp.key = L"cube1";
	//mp.textureKey = L"textures/wood1.dds";
	//mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(-5.f, 0.f, -5.f);
	//mp.verticesTexture = v;
	//if (!bufferTextures->addModel(mp))
	//	return;

	//// ��� 2
	//// ������ 2
	//mp.textureKey = L"textures/wood2.dds";
	//mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(5.f, 0.f, -5.f);
	//if (!bufferTextures->addModel(mp))
	//	return;

	//bufferTextures->reinit();

	//// ���������
	//float uv(15.f);
	//v =
	//{
	//	{ gmath::Point3(100.0f, 0.0f, 100.0f), gmath::UVCoordinate(uv, uv)}, // tr
	//	{ gmath::Point3(100.0f, 0.0f, -100.0f), gmath::UVCoordinate(uv, -uv)}, // br
	//	{ gmath::Point3(-100.0f, 0.0f, -100.0f), gmath::UVCoordinate(-uv, -uv)}, // bl
	//	{ gmath::Point3(-100.0f, 0.0f, 100.0f), gmath::UVCoordinate(-uv, uv)}, // tl
	//};
	//ind =
	//{
	//	0, 1, 3,
	//	1, 2, 3
	//};
	//mp.type = TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_C;
	//mp.samplerKey = L"default";
	//mp.indeces = ind;
	//mp.key = L"terrain";
	//mp.textureKey = L"textures/grass1.dds";
	//mp.world = gmath::Matrix::Identity();
	//mp.verticesTexture = v;
	//mp.world = gmath::Matrix::CreateTranslationMatrixY(-2.f);
	//if (!bufferTextures->addModel(mp))
	//	return;

	//bufferTextures->reinit();

	// ������� �������
	std::vector<Index> indeces =
	{
		3,1,0,
		2,1,3,
		6,4,5,
		7,4,6,
		11,9,8,
		10,9,11,
		14,12,13,
		15,12,14,
		19,17,16,
		18,17,19,
		22,20,21,
		23,20,22
	};
	mp.samplerKey = L"default";
	mp.type = TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_C;
	mp.indeces = indeces;

	// ��� 1
	std::vector<VertexTexture> vertices =
	{
		{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f)},
		{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f)},
		{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f)},
		{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f)},
		{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::UVCoordinate(0.0f, 1.0f)},
		{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f)}
	};
	// ������ 1
	mp.key = L"cube1";
	mp.textureKey = L"textures/wood1.dds";
	mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(-5.f, 0.f, 5.f);
	mp.verticesTexture = vertices;
	if (!bufferTextures->addModel(mp))
		return;

	// ��� 2
	// ������ 2
	mp.textureKey = L"textures/wood2.dds";
	mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(5.f, 0.f, 5.f);
	if (!bufferTextures->addModel(mp))
		return;

	bufferTextures->reinit();

	// ������� �������
	indeces =
	{
		3,1,0,
		2,1,3,
		0,5,4,
		1,5,0,
		3,4,7,
		0,4,3,
		1,6,5,
		2,6,1,
		2,7,6,
		3,7,2,
		6,4,5,
		7,4,6,
	};
	mp.type = TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_B;
	mp.indeces = indeces;

	// ��� 3
	std::vector<VertexColor> v =
	{
		{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::Color(0.0f, 0.0f, 1.0f, 1.0f) },
		{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::Color(0.0f, 1.0f, 0.0f, 1.0f) },
		{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::Color(0.0f, 1.0f, 1.0f, 1.0f) },
		{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::Color(1.0f, 0.0f, 0.0f, 1.0f) },
		{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::Color(1.0f, 0.0f, 1.0f, 1.0f) },
		{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::Color(1.0f, 1.0f, 0.0f, 1.0f) },
		{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::Color(1.0f, 1.0f, 1.0f, 1.0f) },
		{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::Color(0.0f, 0.0f, 0.0f, 1.0f) }
	};
	// ������ 3
	mp.key = L"cube2";
	mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(-5.f, 0.f, -5.f);
	mp.verticesColor = v;
	if (!bufferColors->addModel(mp))
		return;

	bufferColors->reinit();


	// ��� 4
	// ������ 4
	mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(5.f, 0.f, -5.f);
	mp.verticesColor = v;
	if (!bufferColors->addModel(mp))
		return;

	// ���������
	float uv(15.f);
	vertices =
	{
		{ gmath::Point3(-100.0f, 0.0f, -100.0f), gmath::UVCoordinate(-uv, uv)},
		{ gmath::Point3(-100.0f, 0.0f, 100.0f), gmath::UVCoordinate(-uv, -uv)},
		{ gmath::Point3(100.0f, 0.0f, 100.0f), gmath::UVCoordinate(uv, -uv)},
		{ gmath::Point3(100.0f, 0.0f, -100.0f), gmath::UVCoordinate(uv, uv)}
	};
	indeces =
	{
		0, 1, 2,
		0, 2, 3
	};
	mp.type = TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_C;
	mp.samplerKey = L"default";
	mp.indeces = indeces;
	mp.key = L"terrain";
	mp.textureKey = L"textures/grass1.dds";
	mp.world = gmath::Matrix::Identity();
	mp.verticesTexture = vertices;
	mp.world = gmath::Matrix::CreateTranslationMatrixY(-2.f);
	if (!bufferTextures->addModel(mp))
		return;

	bufferTextures->reinit();

}

bool TestOpenGLScene::update(SceneDynamicParameters& sdp)
{
	// �������� �� ������� esc
	if (InputSystem::getInstance()->getKeyboardKey(InputSystemKeys::KEY_ESC))
		return false;

	// ���������� ������
	camera->update(sdp.frameTime, getWindowBBox());
	return true;
}

TestOpenGLScene::~TestOpenGLScene()
{
	clear();
}

void TestOpenGLScene::clear()
{
	OpenGLScene::clear();
}

void TestOpenGLScene::render()
{
	gmath::Color color = { 0.0f, 0.0f, 1.0f, 1.0f };
	glClearColor(color[gmtypes::cr], color[gmtypes::cg], color[gmtypes::cb], color[gmtypes::ca]);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // ������� ��� ����� � ����� �������

	// ������ ������
	bufferColors->draw();
	bufferTextures->draw();
}