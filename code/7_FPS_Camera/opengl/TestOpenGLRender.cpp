#include "TestOpenGLRender.h"

void TestOpenGLRender::init(RenderParameters rp)
{
	vsync = false;
	fullscreen = false;
	OSWindowsOpenGLRender::init(rp);
	glEnable(GL_DEPTH_TEST); // �������� ���� �������
}

bool TestOpenGLRender::updateScene(SceneDynamicParameters& sdp)
{
	// ������� � ������ ����� ��� �������
	if (InputSystem::getInstance()->getKeyboardKey(InputSystemKeys::KEY_HOME))
		changeModeScreen(true);
	return scene->update(sdp);
}

void TestOpenGLRender::initScene()
{
	scene.reset(new TestOpenGLScene());
	if(!scene)
		throw ExceptionMemory(L"TestOpenGLScene is not create, memory not found!");
	SceneParameters rp;
	rp.setClientWidth(getWidth());
	rp.setClientHeight(getHeight());
	scene->init(L"empty", rp);
}

void TestOpenGLRender::render()
{
	scene->render();
	OSWindowsOpenGLRender::render();
}
