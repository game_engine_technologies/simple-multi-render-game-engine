#include "TestDirect3D11Render.h"

void TestDirect3D11Render::init(RenderParameters rp)
{
	vsync = false;
	d3dEnable4xMsaa = true;
	OSWindowsDirect3D11Render::init(rp);
}

bool TestDirect3D11Render::updateScene(SceneDynamicParameters& sdp)
{
	return scene->update(sdp);
}

void TestDirect3D11Render::initScene()
{
	scene.reset(new TestDirect3D11Scene(d3dDevice, d3dImmediateContext));
	if(!scene)
		throw ExceptionMemory(L"TestDirect3D11Scene is not create, memory not found!");
	SceneParameters rp;
	rp.setClientWidth(getWidth());
	rp.setClientHeight(getHeight());
	scene->init(L"empty", rp);
}

void TestDirect3D11Render::render()
{
	if(!d3dImmediateContext || !d3dSwapChain)
		throw new ExceptionDirect3D11(L"Error draw D3D11 render.");

	scene->render(d3dImmediateContext, d3dRenderTargetView, d3dDepthStencilView, nullptr, nullptr);

	OSWindowsDirect3D11Render::render();
}
