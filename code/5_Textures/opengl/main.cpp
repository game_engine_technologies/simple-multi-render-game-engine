#include "TestApplication.h"

int APIENTRY wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPWSTR lpCmdLine, _In_ int nCmdShow)
{
	try
	{
		WindowParameters wp;
		wp.setWidth(750);
		wp.setHeight(500);

		std::unique_ptr<TestApplication> app(new TestApplication());
		if (!app)
			throw ExceptionMemory(L"TestApplication is not create, memory not found!");
		app->init(wp);
		app->run();
	}
	catch (IException& ex)
	{
		OutputDebugString(ex.getError().c_str());
	}
	return 0;
}
