#include "TestOpenGLScene.h"

#include "../../common/renders/base/scene/model/IGeometryBufferVisitor.h"
#include "../../common/renders/base/scene/camera/ICameraVisitor.h"

void TestOpenGLScene::init(const std::wstring& path, SceneParameters rp)
{
	OpenGLScene::init(path, rp);
	// init camera
	VisitorCameraCreateAndUpdateSmartPtr visitorCamera = std::make_shared<VisitorCameraCreateAndUpdate>();
	if (!visitorCamera)
		throw ExceptionMemory(L"VisitorCameraCreateAndUpdate is not create, memory not found!");
	camera = std::make_shared<OpenGLCamera>();
	if (!camera)
		throw ExceptionMemory(L"OpenGLCamera is not create, memory not found!");
	camera->setFarZ(1000.f);
	camera->setNearZPerspective(0.1f);
	camera->updatePerspective(gmath::GeneralMath::PI_4, rp.getClientWidth(), rp.getClientHeight());
	camera->updateView(gmath::Vector3(-8.f, 15.f, 20.f), gmath::Vector3(0.f, 0.f, 0.f), gmath::Vector3(0.f, 1.f, 0.f));
	camera->init(visitorCamera);
	// ��� ������� ������ � ���������
	buffer = std::make_shared<OpenGLGeometryBufferC>();
	if (!buffer)
		throw ExceptionMemory(L"OpenGLGeometryBufferC is not create, memory not found!");
	// init buffer
	VisitorGeometryBufferCreateAndDrawSmartPtr visitor = std::make_shared<VisitorGeometryBufferCreateAndDraw>(
		shadersBuffer,
		camera,
		texturesBuffer);
	if (!visitor)
		throw ExceptionMemory(L"VisitorGeometryBufferCreateAndDraw is not create, memory not found!");
	buffer->init(L"shaders/opengl/texture_camera.gl_vs", L"shaders/opengl/texture_camera.gl_ps", visitor);

	//// ������� �������
	//std::vector<Index> ind =
	//{
	//	0, 3, 1,  1, 3, 2, // front
	//	4, 7, 5,  5, 7, 6, // back
	//	8,11, 9,  9,11,10, // top
	//	12,15,13, 13,15,14, // bottom
	//	16,19,17, 17,19,18, // left
	//	20,23,21, 21,23,22  // right
	//};
	//ModelParameters mp;
	//mp.samplerKey = L"default";
	//mp.type = TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_C;
	//mp.indeces = ind;

	//// ��� 1
	//std::vector<VertexTexture> v =
	//{
	//	{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f) },
	//	{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::UVCoordinate(1.0f, 1.0f) },
	//	{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::UVCoordinate(1.0f, 0.0f) },
	//	{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 0.0f) },
	//	{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::UVCoordinate(0.0f, 1.0f) },
	//	{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 1.0f) },
	//	{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f) },
	//	{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::UVCoordinate(0.0f, 0.0f) },
	//	{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::UVCoordinate(0.0f, 1.0f) },
	//	{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 1.0f) },
	//	{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::UVCoordinate(1.0f, 0.0f) },
	//	{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 0.0f) },
	//	{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::UVCoordinate(0.0f, 1.0f) },
	//	{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 1.0f) },
	//	{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::UVCoordinate(1.0f, 0.0f) },
	//	{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 0.0f) },
	//	{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::UVCoordinate(0.0f, 1.0f) },
	//	{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::UVCoordinate(1.0f, 1.0f) },
	//	{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::UVCoordinate(1.0f, 0.0f) },
	//	{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::UVCoordinate(0.0f, 0.0f) },
	//	{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f) },
	//	{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 1.0f) },
	//	{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f) },
	//	{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 0.0f) }
	//};
	//// ������ 1
	//mp.key = L"cube1";
	//mp.textureKey = L"textures/wood1.dds";
	//mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(-5.f, 0.f, -5.f);
	//mp.verticesTexture = v;
	//if (!buffer->addModel(mp))
	//	return;

	//// ��� 2
	//// ������ 2
	//mp.textureKey = L"textures/wood2.dds";
	//mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(5.f, 0.f, -5.f);
	//if (!buffer->addModel(mp))
	//	return;

	//// ��� 3
	//v =
	//{
	//	{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f) },
	//	{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::UVCoordinate(1.0f, 1.0f) },
	//	{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::UVCoordinate(1.0f, 0.0f) },
	//	{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 0.0f) },
	//	{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::UVCoordinate(0.0f, 1.0f) },
	//	{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 1.0f) },
	//	{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f) },
	//	{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::UVCoordinate(0.0f, 0.0f) },
	//	{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::UVCoordinate(0.0f, 1.0f) },
	//	{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 1.0f) },
	//	{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::UVCoordinate(1.0f, 0.0f) },
	//	{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 0.0f) },
	//	{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::UVCoordinate(0.0f, 1.0f) },
	//	{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 1.0f) },
	//	{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::UVCoordinate(1.0f, 0.0f) },
	//	{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 0.0f) },
	//	{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::UVCoordinate(0.0f, 1.0f) },
	//	{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::UVCoordinate(1.0f, 1.0f) },
	//	{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::UVCoordinate(1.0f, 0.0f) },
	//	{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::UVCoordinate(0.0f, 0.0f) },
	//	{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f) },
	//	{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 1.0f) },
	//	{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f) },
	//	{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 0.0f) }
	//};
	//// ������ 1
	//mp.key = L"cube2";
	//mp.textureKey = L"textures/wood3.dds";
	//mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(-5.f, 0.f, 5.f);
	//mp.verticesTexture = v;
	//if (!buffer->addModel(mp))
	//	return;

	//// ��� 4
	//// ������ 4
	//mp.key = L"cube2";
	//mp.textureKey = L"textures/wood4.dds";
	//mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(5.f, 0.f, 5.f);
	//if (!buffer->addModel(mp))
	//	return;

	//buffer->reinit();

	// ������� �������
	std::vector<Index> indeces =
	{
		3,1,0,
		2,1,3,
		6,4,5,
		7,4,6,
		11,9,8,
		10,9,11,
		14,12,13,
		15,12,14,
		19,17,16,
		18,17,19,
		22,20,21,
		23,20,22
	};
	ModelParameters mp;
	mp.samplerKey = L"default";
	mp.type = TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_C;
	mp.indeces = indeces;

	// ��� 1
	std::vector<VertexTexture> vertices =
	{
		{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f)},
		{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f)},
		{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f)},
		{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f)},
		{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::UVCoordinate(0.0f, 1.0f)},
		{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f)}
	};
	// ������ 1
	mp.key = L"cube1";
	mp.textureKey = L"textures/wood1.dds";
	mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(-5.f, 0.f, 5.f);
	mp.verticesTexture = vertices;
	if (!buffer->addModel(mp))
		return;

	// ��� 2
	// ������ 2
	mp.textureKey = L"textures/wood2.dds";
	mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(5.f, 0.f, 5.f);
	if (!buffer->addModel(mp))
		return;
	
	// ��� 3
	vertices =
	{
		{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f)},
		{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f)},
		{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f)},
		{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f)},
		{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::UVCoordinate(0.0f, 1.0f)},
		{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f)}
	};
	// ������ 3
	mp.key = L"cube2";
	mp.textureKey = L"textures/wood3.dds";
	mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(-5.f, 0.f, -5.f);
	mp.verticesTexture = vertices;
	if (!buffer->addModel(mp))
		return;

	// ��� 4
	// ������ 4
	mp.textureKey = L"textures/wood4.dds";
	mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(5.f, 0.f, -5.f);
	mp.verticesTexture = vertices;
	if (!buffer->addModel(mp))
		return;

	buffer->reinit();
}

bool TestOpenGLScene::update(SceneDynamicParameters& sdp)
{
	camera->update(sdp.frameTime, getWindowBBox());
	return OpenGLScene::update(sdp);
}

TestOpenGLScene::~TestOpenGLScene()
{
	clear();
}

void TestOpenGLScene::clear()
{
	OpenGLScene::clear();
}

void TestOpenGLScene::render()
{
	gmath::Color color = { 0.0f, 0.0f, 1.0f, 1.0f };
	glClearColor(color[gmtypes::cr], color[gmtypes::cg], color[gmtypes::cb], color[gmtypes::ca]);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // ������� ��� ����� � ����� �������

	// ������ ������ (�����������)
	buffer->draw();
}
