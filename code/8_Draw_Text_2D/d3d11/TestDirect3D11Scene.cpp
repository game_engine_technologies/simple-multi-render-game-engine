#include "TestDirect3D11Scene.h"

#include "../../common/renders/base/scene/model/IGeometryBufferVisitor.h"
#include "../../common/renders/base/scene/camera/ICameraVisitor.h"
#include "../../common/renders/base/scene/text/ITextBufferVisitor.h"

TestDirect3D11Scene::TestDirect3D11Scene(const ComPtr<ID3D11Device>& d3dDevice,
	const ComPtr<ID3D11DeviceContext>& d3dImmediateContext):Direct3D11Scene(d3dDevice, d3dImmediateContext)
{
	
}

void TestDirect3D11Scene::init(const std::wstring& path, SceneParameters rp)
{
	Direct3D11Scene::init(path, rp);

	// init camera
	VisitorCameraCreateAndUpdateSmartPtr visitorCamera = std::make_shared<VisitorCameraCreateAndUpdate>(d3dDevice, d3dImmediateContext);
	if(!visitorCamera)
		throw ExceptionMemory(L"VisitorCameraCreateAndUpdate is not create, memory not found!");
	camera = std::make_shared<Direct3D11FPSCamera>(); // Direct3D11FPSCamera
	if (!camera)
		throw ExceptionMemory(L"Direct3D11FPSCamera is not create, memory not found!");
	camera->setNearZPerspective(0.001f);
	camera->setNearZOrtho(0.0f);
	camera->setFarZ(1000.f);
	camera->updatePerspective(gmath::GeneralMath::PI_4, rp.getClientWidth(), rp.getClientHeight());
	camera->updateView(gmath::Vector3(0.f, 5.f, -20.f), gmath::Vector3(0.f, 5.f, 0.f), gmath::Vector3(0.f, 1.f, 0.f));
	camera->updateOrtho(rp.getClientWidth(), rp.getClientHeight());
	camera->init(visitorCamera);

	// init text buffer
	/*VisitorTextBufferCreateAndDrawSmartPtr visitorTextBuffer = std::make_shared<VisitorTextBufferCreateAndDraw>(d3dDevice, d3dImmediateContext,
		shadersBuffer, texturesBuffer, camera);
	if (!visitorTextBuffer)
		throw ExceptionMemory(L"VisitorTextBufferCreateAndDraw is not create, memory not found!");
	textBuffer = std::make_shared<Direct3D11TextBuffer>();
	textBuffer->init(L"shaders/d3d11/text.d3d11_vs", L"shaders/d3d11/text.d3d11_ps", L"configs/text/direct3d/list_font_files.pText", L"default",
		visitorTextBuffer);
	textBuffer->mapVertexBuffer();
	textBuffer->addText(L"render_label", L"Current render: Direct3D   \n"
		"Current FPS: 0    \nFullscreen: off\nV-Sync: off\nSize window: 0 x 0      \n",
		gmath::Matrix::CreateTranslationMatrixXYZ(-(rp.getClientWidth() / 2) + 1, rp.getClientHeight() / 2 - (20 + 1), 0.f), gmath::Color(1.f, 1.f, 1.f, 1.f), 20);
	textBuffer->unmapVertexBuffer();*/

	// init text buffer // B
	VisitorTextBufferCreateAndDrawSmartPtr visitorTextBuffer = std::make_shared<VisitorTextBufferCreateAndDraw>(d3dDevice, d3dImmediateContext,
		shadersBuffer, texturesBuffer, camera);	
	if (!visitorTextBuffer)
		throw ExceptionMemory(L"VisitorTextBufferCreateAndDraw is not create, memory not found!");
	textBuffer = std::make_shared<Direct3D11TextBufferB>();
	int h(20);
	textBuffer->init(L"shaders/d3d11/text.d3d11_vs", L"shaders/d3d11/text.d3d11_ps", L"configs/text/direct3d/list_font_files.pText", L"default",
		h, visitorTextBuffer);
	textBuffer->addText(L"l_stat", L"Current render: Direct3D " + rp.getVersionAPI() + L"\n"
		"Current FPS:\nFullscreen:\nV-Sync:\nSize window:\n",
		gmath::Matrix::CreateTranslationMatrixXYZ(-(getWindowBBox().w / 2) + 1.f, getWindowBBox().h / 2 - (h + 1), 0.f), gmath::Color(1.f, 1.f, 1.f, 1.f), TypeTextBufferB::STATIC_TEXT_BUFFER);
	textBuffer->addText(L"l_fps", L"0    ",
		gmath::Matrix::CreateTranslationMatrixXYZ(-(rp.getClientWidth() / 2) + 115, rp.getClientHeight() / 2 - ((h * 2) + 1), 0.f), gmath::Color(1.f, 1.f, 1.f, 1.f), TypeTextBufferB::DYNAMIC_TEXT_BUFFER);
	textBuffer->addText(L"l_fscreen", L"off",
		gmath::Matrix::CreateTranslationMatrixXYZ(-(rp.getClientWidth() / 2) + 100, rp.getClientHeight() / 2 - ((h * 3) + 1), 0.f), gmath::Color(1.f, 1.f, 1.f, 1.f), TypeTextBufferB::DYNAMIC_TEXT_BUFFER);
	textBuffer->addText(L"l_vsunc", L"off",
		gmath::Matrix::CreateTranslationMatrixXYZ(-(rp.getClientWidth() / 2) + 75, rp.getClientHeight() / 2 - ((h * 4) + 1), 0.f), gmath::Color(1.f, 1.f, 1.f, 1.f), TypeTextBufferB::DYNAMIC_TEXT_BUFFER);
	textBuffer->addText(L"l_sizewin", L"0 x 0        ",
		gmath::Matrix::CreateTranslationMatrixXYZ(-(rp.getClientWidth() / 2) + 120, rp.getClientHeight() / 2 - ((h * 5) + 1), 0.f), gmath::Color(1.f, 1.f, 1.f, 1.f), TypeTextBufferB::DYNAMIC_TEXT_BUFFER);


	// ��������
	bufferTexture = std::make_shared<Direct3D11GeometryBufferC>();
	if(!bufferTexture)
		throw ExceptionMemory(L"Direct3D11GeometryBufferC is not create, memory not found!");
	// init buffer
	VisitorGeometryBufferCreateAndDrawSmartPtr visitorTextures = std::make_shared<VisitorGeometryBufferCreateAndDraw>(
		d3dDevice,
		d3dImmediateContext, 
		shadersBuffer,
		camera,
		texturesBuffer);
	if (!visitorTextures)
		throw ExceptionMemory(L"VisitorGeometryBufferCreateAndDraw is not create, memory not found!");
	bufferTexture->init(L"shaders/d3d11/texture_camera.d3d11_vs", L"shaders/d3d11/texture_camera.d3d11_ps", visitorTextures);

	// ������� �������
	std::vector<Index> indeces =
	{
		3,1,0,
		2,1,3,
		6,4,5,
		7,4,6,
		11,9,8,
		10,9,11,
		14,12,13,
		15,12,14,
		19,17,16,
		18,17,19,
		22,20,21,
		23,20,22
	};
	ModelParameters mp;
	mp.samplerKey = L"default";
	mp.type = TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_C;
	mp.indeces = indeces;

	// ��� 1
	std::vector<VertexTexture> vertices =
	{
		{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f)},
		{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f)},
		{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f)},
		{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f)},
		{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::UVCoordinate(0.0f, 1.0f)},
		{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f)}
	};
	// ������ 1
	mp.key = L"cube1";
	mp.textureKey = L"textures/wood1.dds";
	mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(-5.f, 0.f, 5.f);
	mp.verticesTexture = vertices;
	if (!bufferTexture->addModel(mp))
		return;

	// ��� 2
	// ������ 2
	mp.textureKey = L"textures/wood2.dds";
	mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(5.f, 0.f, 5.f);
	if (!bufferTexture->addModel(mp))
		return;
	
	bufferTexture->reinit();


	// ����
	bufferColor = std::make_shared<Direct3D11GeometryBufferB>();
	if (!bufferColor)
		throw ExceptionMemory(L"Direct3D11GeometryBufferB is not create, memory not found!");
	// init buffer
	VisitorGeometryBufferCreateAndDrawSmartPtr visitorColor = std::make_shared<VisitorGeometryBufferCreateAndDraw>(
		d3dDevice,
		d3dImmediateContext,
		shadersBuffer,
		camera);
	if (!visitorColor)
		throw ExceptionMemory(L"VisitorGeometryBufferCreateAndDraw is not create, memory not found!");
	bufferColor->init(L"shaders/d3d11/color_camera.d3d11_vs", L"shaders/d3d11/color_camera.d3d11_ps", visitorColor);

	// ������� �������
	indeces =
	{
		3,1,0,
		2,1,3,
		0,5,4,
		1,5,0,
		3,4,7,
		0,4,3,
		1,6,5,
		2,6,1,
		2,7,6,
		3,7,2,
		6,4,5,
		7,4,6,
	};
	mp.type = TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_B;
	mp.indeces = indeces;

	// ��� 3
	std::vector<VertexColor> v =
	{
		{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::Color(0.0f, 0.0f, 1.0f, 1.0f) },
		{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::Color(0.0f, 1.0f, 0.0f, 1.0f) },
		{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::Color(0.0f, 1.0f, 1.0f, 1.0f) },
		{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::Color(1.0f, 0.0f, 0.0f, 1.0f) },
		{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::Color(1.0f, 0.0f, 1.0f, 1.0f) },
		{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::Color(1.0f, 1.0f, 0.0f, 1.0f) },
		{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::Color(1.0f, 1.0f, 1.0f, 1.0f) },
		{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::Color(0.0f, 0.0f, 0.0f, 1.0f) }
	};
	// ������ 3
	mp.key = L"cube2";
	mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(-5.f, 0.f, -5.f);
	mp.verticesColor = v;
	if (!bufferColor->addModel(mp))
		return;

	// ��� 4
	// ������ 4
	mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(5.f, 0.f, -5.f);
	mp.verticesColor = v;
	if (!bufferColor->addModel(mp))
		return;

	bufferColor->reinit();

	// ���������
	float uv(15.f);
	vertices =
	{
		{ gmath::Point3(-100.0f, 0.0f, -100.0f), gmath::UVCoordinate(-uv, uv)},
		{ gmath::Point3(-100.0f, 0.0f, 100.0f), gmath::UVCoordinate(-uv, -uv)},
		{ gmath::Point3(100.0f, 0.0f, 100.0f), gmath::UVCoordinate(uv, -uv)},
		{ gmath::Point3(100.0f, 0.0f, -100.0f), gmath::UVCoordinate(uv, uv)}
	};
	indeces =
	{
		0, 1, 2,
		0, 2, 3
	};
	mp.type = TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_C;
	mp.samplerKey = L"default";
	mp.indeces = indeces;
	mp.key = L"terrain";
	mp.textureKey = L"textures/grass1.dds";
	mp.world = gmath::Matrix::Identity();
	mp.verticesTexture = vertices;
	mp.world = gmath::Matrix::CreateTranslationMatrixY(-2.f);
	if (!bufferTexture->addModel(mp))
		return;
	bufferTexture->reinit();

	// ���� ��������
	vertices =
	{
		{ gmath::Point3(-3.f, 6.0f, -1.f), gmath::UVCoordinate(0.f, 0.f)}, // 0
		{ gmath::Point3(3.f, 6.0f, -1.f), gmath::UVCoordinate(1.f, 0.f)}, // 1
		{ gmath::Point3(3.f, 0.0f, -1.f), gmath::UVCoordinate(1.f, 1.f)}, // 2
		{ gmath::Point3(-3.f, 0.0f, -1.f), gmath::UVCoordinate(0.f, 1.f)} // 3
	};
	indeces =
	{
		0, 1, 2,
		0, 2, 3
	};
	mp.textureKey = L"textures/wood1_test.dds";
	mp.world = gmath::Matrix::Identity();
	mp.indeces = indeces;
	mp.verticesTexture = vertices;
	mp.key = L"triangle_1";
	if (!bufferTexture->addModel(mp))
		return;
	bufferTexture->reinit();

}

bool TestDirect3D11Scene::update(SceneDynamicParameters& sdp)
{
	// �������� �� ������� esc
	if (InputSystem::getInstance()->getKeyboardKey(InputSystemKeys::KEY_ESC))
		return false;

	// ���������� ������
	camera->update(sdp.frameTime, getWindowBBox());
	// ���������� ������
	//if (!sdp.textBuffStack.empty())
	//{
	//	textBuffer->mapVertexBuffer();
	//	while (!sdp.textBuffStack.empty())
	//	{
	//		auto textParam = sdp.textBuffStack.top();
	//		auto coordinates = textParam->get_positions();
	//		//auto outp = textParam->to_wstring() + L'\n';
	//		//OutputDebugString(outp.c_str());
	//		sdp.textBuffStack.pop();
	//		textBuffer->replaceTextForEnter(L"render_label", coordinates.first, coordinates.second, 
	//			textParam->to_wstring());
	//	}
	//	textBuffer->unmapVertexBuffer();
	//}

	if (!sdp.textBuffStack.empty()) // B
	{
		while (!sdp.textBuffStack.empty())
		{
			auto textParam = sdp.textBuffStack.top();
			sdp.textBuffStack.pop();
			textBuffer->replaceText(textParam->get_key(), textParam->to_wstring());
		}
	}

	// ���������� ������� ������
	int h(textBuffer->getHeight());
	textBuffer->updatePosition(L"l_stat", gmath::Matrix::CreateTranslationMatrixXYZ(-(getWindowBBox().w / 2) + 1, getWindowBBox().h / 2 - (h + 1), 0.f));
	textBuffer->updatePosition(L"l_fps", gmath::Matrix::CreateTranslationMatrixXYZ(-(getWindowBBox().w / 2) + 115, getWindowBBox().h / 2 - (h * 2 + 1), 0.f));
	textBuffer->updatePosition(L"l_fscreen", gmath::Matrix::CreateTranslationMatrixXYZ(-(getWindowBBox().w / 2) + 100, getWindowBBox().h / 2 - (h * 3 + 1), 0.f));
	textBuffer->updatePosition(L"l_vsunc", gmath::Matrix::CreateTranslationMatrixXYZ(-(getWindowBBox().w / 2) + 75, getWindowBBox().h / 2 - (h * 4 + 1), 0.f));
	textBuffer->updatePosition(L"l_sizewin", gmath::Matrix::CreateTranslationMatrixXYZ(-(getWindowBBox().w / 2) + 120, getWindowBBox().h / 2 - (h * 5 + 1), 0.f));

	return true;
}

TestDirect3D11Scene::~TestDirect3D11Scene()
{
	clear();
}

void TestDirect3D11Scene::clear()
{
	Direct3D11Scene::clear();
}

void TestDirect3D11Scene::render(const ComPtr<ID3D11DeviceContext>& d3dImmediateContext, const ComPtr<ID3D11RenderTargetView>& d3dRenderTargetView,
	const ComPtr<ID3D11DepthStencilView>& d3dDepthStencilView, const ComPtr<ID3D11DepthStencilState>& d3dDepthStencilEnableDepth,
	const ComPtr<ID3D11DepthStencilState>& d3dDepthStencilDisableDepth)
{
	gmath::Color color = { 0.0f, 0.0f, 1.0f, 1.0f };
	d3dImmediateContext->ClearRenderTargetView(d3dRenderTargetView.Get(), color.toArray().data());
	d3dImmediateContext->ClearDepthStencilView(d3dDepthStencilView.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
	// ������ ������
	bufferTexture->draw();
	bufferColor->draw();

	// ������ �����
	d3dImmediateContext->OMSetDepthStencilState(d3dDepthStencilDisableDepth.Get(), 1); // ��������� ���� �������
	textBuffer->draw(); // ������
	d3dImmediateContext->OMSetDepthStencilState(d3dDepthStencilEnableDepth.Get(), 1); // �������� ���� �������
	
}
