#include "TestDirect3D11Render.h"

void TestDirect3D11Render::init(RenderParameters rp)
{
	vsync = false;
	fullscreen = false;
	d3dEnable4xMsaa = true;
	OSWindowsDirect3D11Render::init(rp);
}

bool TestDirect3D11Render::updateScene(SceneDynamicParameters& sdp)
{
	// ������� � ������ ����� ��� �������
	if (InputSystem::getInstance()->getKeyboardKey(InputSystemKeys::KEY_HOME))
	{
		changeModeScreen(true);
		sdp.textBuffStack.push(std::make_shared<TextBufferDynamicParameterFullscreen>(fullscreen));
	}
	if (sizeWindowChanged)
	{
		sdp.textBuffStack.push(std::make_shared<TextBufferDynamicParameterSizeWindow>(width, height));
		sizeWindowChanged = false;
	}
	if (sdp.start) // ��������� ������������� ������ �� �����
	{
		sdp.textBuffStack.push(std::make_shared<TextBufferDynamicParameterFullscreen>(fullscreen));
		sdp.textBuffStack.push(std::make_shared<TextBufferDynamicParameterVSync>(vsync));
		sdp.start = false;
	}
	return scene->update(sdp);
}

void TestDirect3D11Render::initScene()
{
	scene.reset(new TestDirect3D11Scene(d3dDevice, d3dImmediateContext));
	if(!scene)
		throw ExceptionMemory(L"TestDirect3D11Scene is not create, memory not found!");
	SceneParameters rp;
	rp.setClientWidth(getWidth());
	rp.setClientHeight(getHeight());
	rp.setVersionAPI(std::to_wstring(versionAPI));
	scene->init(L"empty", rp);
}

void TestDirect3D11Render::render()
{
	if(!d3dImmediateContext || !d3dSwapChain)
		throw new ExceptionDirect3D11(L"Error draw D3D11 render.");

	scene->render(d3dImmediateContext, d3dRenderTargetView, d3dDepthStencilView, d3dDepthStencilEnableDepth, d3dDepthStencilDisableDepth);

	OSWindowsDirect3D11Render::render();
}
