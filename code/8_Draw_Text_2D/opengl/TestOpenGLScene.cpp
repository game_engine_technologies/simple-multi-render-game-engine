#include "TestOpenGLScene.h"

#include "../../common/renders/base/scene/model/IGeometryBufferVisitor.h"
#include "../../common/renders/base/scene/camera/ICameraVisitor.h"
#include "../../common/renders/base/scene/text/ITextBufferVisitor.h"

void TestOpenGLScene::init(const std::wstring& path, SceneParameters rp)
{
	OpenGLScene::init(path, rp);
	// init camera
	VisitorCameraCreateAndUpdateSmartPtr visitorCamera = std::make_shared<VisitorCameraCreateAndUpdate>();
	if (!visitorCamera)
		throw ExceptionMemory(L"VisitorCameraCreateAndUpdate is not create, memory not found!");
	camera = std::make_shared<OpenGLFPSCamera>();
	if (!camera)
		throw ExceptionMemory(L"OpenGLCamera is not create, memory not found!");
	camera->setNearZPerspective(0.1f);
	camera->setNearZOrtho(0.0f);
	camera->setFarZ(1000.f);
	camera->updatePerspective(gmath::GeneralMath::PI_4, rp.getClientWidth(), rp.getClientHeight());
	camera->updateView(gmath::Vector3(0.f, 5.f, 20.f), gmath::Vector3(0.f, 5.f, 0.f), gmath::Vector3(0.f, 1.f, 0.f));
	camera->updateOrtho(rp.getClientWidth(), rp.getClientHeight());
	camera->init(visitorCamera);
	ModelParameters mp;

	// init text buffer // A
	//VisitorTextBufferCreateAndDrawSmartPtr visitorTextBuffer = std::make_shared<VisitorTextBufferCreateAndDraw>(shadersBuffer, texturesBuffer, camera);
	//if (!visitorTextBuffer)
	//	throw ExceptionMemory(L"VisitorTextBufferCreateAndDraw is not create, memory not found!");
	//textBuffer = std::make_shared<OpenGLTextBufferA>();
	//textBuffer->init(L"shaders/opengl/text.gl_vs", L"shaders/opengl/text.gl_ps", L"configs/text/opengl/list_font_files.pText", L"default",
	//	visitorTextBuffer);
	//textBuffer->mapVertexBuffer();
	//textBuffer->addText(L"render_label", L"Current render: OpenGL    \n"
	//	"Current FPS: 0    \nFullscreen: off\nV-Sync: off\nSize window: 0 x 0      \n",
	//	gmath::Matrix::CreateTranslationMatrixXYZ(-(rp.getClientWidth() / 2) + 1, rp.getClientHeight() / 2 - (20 + 1), 0.f), gmath::Color(1.f, 1.f, 1.f, 1.f), 20);
	//textBuffer->unmapVertexBuffer();
	
	// init text buffer // B
	VisitorTextBufferCreateAndDrawSmartPtr visitorTextBuffer = std::make_shared<VisitorTextBufferCreateAndDraw>(shadersBuffer, texturesBuffer, camera);
	if (!visitorTextBuffer)
		throw ExceptionMemory(L"VisitorTextBufferCreateAndDraw is not create, memory not found!");
	textBuffer = std::make_shared<OpenGLTextBufferB>();
	int h(20);
	textBuffer->init(L"shaders/opengl/text.gl_vs", L"shaders/opengl/text.gl_ps", L"configs/text/opengl/list_font_files.pText", L"default",
		h, visitorTextBuffer);
	textBuffer->addText(L"l_stat", L"Current render: OpenGL " + rp.getVersionAPI() + L"\n" 
		"Current FPS:\nFullscreen:\nV-Sync:\nSize window:\n",
		gmath::Matrix::CreateTranslationMatrixXYZ(-(getWindowBBox().w / 2) + 1.f, getWindowBBox().h / 2 - (h + 1), 0.f), gmath::Color(1.f, 1.f, 1.f, 1.f), TypeTextBufferB::STATIC_TEXT_BUFFER);
	textBuffer->addText(L"l_fps", L"0    ",
		gmath::Matrix::CreateTranslationMatrixXYZ(-(rp.getClientWidth() / 2) + 115, rp.getClientHeight() / 2 - ((h * 2) + 1), 0.f), gmath::Color(1.f, 1.f, 1.f, 1.f), TypeTextBufferB::DYNAMIC_TEXT_BUFFER);
	textBuffer->addText(L"l_fscreen", L"off",
		gmath::Matrix::CreateTranslationMatrixXYZ(-(rp.getClientWidth() / 2) + 100, rp.getClientHeight() / 2 - ((h * 3) + 1), 0.f), gmath::Color(1.f, 1.f, 1.f, 1.f), TypeTextBufferB::DYNAMIC_TEXT_BUFFER);
	textBuffer->addText(L"l_vsunc", L"off",
		gmath::Matrix::CreateTranslationMatrixXYZ(-(rp.getClientWidth() / 2) + 75, rp.getClientHeight() / 2 - ((h * 4) + 1), 0.f), gmath::Color(1.f, 1.f, 1.f, 1.f), TypeTextBufferB::DYNAMIC_TEXT_BUFFER);
	textBuffer->addText(L"l_sizewin", L"0 x 0        ",
		gmath::Matrix::CreateTranslationMatrixXYZ(-(rp.getClientWidth() / 2) + 120, rp.getClientHeight() / 2 - ((h * 5) + 1), 0.f), gmath::Color(1.f, 1.f, 1.f, 1.f), TypeTextBufferB::DYNAMIC_TEXT_BUFFER);


	// ����
	bufferColors = std::make_shared<OpenGLGeometryBufferB>();
	if (!bufferColors)
		throw ExceptionMemory(L"OpenGLGeometryBufferB is not create, memory not found!");
	// init buffer
	VisitorGeometryBufferCreateAndDrawSmartPtr visitorColor = std::make_shared<VisitorGeometryBufferCreateAndDraw>(
		shadersBuffer,
		camera);
	if (!visitorColor)
		throw ExceptionMemory(L"VisitorGeometryBufferCreateAndDraw is not create, memory not found!");
	bufferColors->init(L"shaders/opengl/color_camera.gl_vs", L"shaders/opengl/color_camera.gl_ps", visitorColor);

	// ��������
	bufferTextures = std::make_shared<OpenGLGeometryBufferC>();
	if (!bufferTextures)
		throw ExceptionMemory(L"OpenGLGeometryBufferC is not create, memory not found!");
	// init buffer
	VisitorGeometryBufferCreateAndDrawSmartPtr visitorTexture = std::make_shared<VisitorGeometryBufferCreateAndDraw>(
		shadersBuffer,
		camera,
		texturesBuffer);
	if (!visitorTexture)
		throw ExceptionMemory(L"VisitorGeometryBufferCreateAndDraw is not create, memory not found!");
	bufferTextures->init(L"shaders/opengl/texture_camera.gl_vs", L"shaders/opengl/texture_camera.gl_ps", visitorTexture);

	// ������� �������
	std::vector<Index> indeces =
	{
		3,1,0,
		2,1,3,
		6,4,5,
		7,4,6,
		11,9,8,
		10,9,11,
		14,12,13,
		15,12,14,
		19,17,16,
		18,17,19,
		22,20,21,
		23,20,22
	};
	mp.samplerKey = L"default";
	mp.type = TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_C;
	mp.indeces = indeces;

	// ��� 1
	std::vector<VertexTexture> vertices =
	{
		{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f)},
		{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f)},
		{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f)},
		{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f)},
		{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::UVCoordinate(0.0f, 1.0f)},
		{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::UVCoordinate(0.0f, 0.0f)},
		{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::UVCoordinate(1.0f, 0.0f)},
		{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::UVCoordinate(1.0f, 1.0f)},
		{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::UVCoordinate(0.0f, 1.0f)}
	};
	// ������ 1
	mp.key = L"cube1";
	mp.textureKey = L"textures/wood1.dds";
	mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(-5.f, 0.f, 5.f);
	mp.verticesTexture = vertices;
	if (!bufferTextures->addModel(mp))
		return;

	// ��� 2
	// ������ 2
	mp.textureKey = L"textures/wood2.dds";
	mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(5.f, 0.f, 5.f);
	if (!bufferTextures->addModel(mp))
		return;

	bufferTextures->reinit();

	// ������� �������
	indeces =
	{
		3,1,0,
		2,1,3,
		0,5,4,
		1,5,0,
		3,4,7,
		0,4,3,
		1,6,5,
		2,6,1,
		2,7,6,
		3,7,2,
		6,4,5,
		7,4,6,
	};
	mp.type = TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_B;
	mp.indeces = indeces;

	// ��� 3
	std::vector<VertexColor> v =
	{
		{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::Color(0.0f, 0.0f, 1.0f, 1.0f) },
		{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::Color(0.0f, 1.0f, 0.0f, 1.0f) },
		{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::Color(0.0f, 1.0f, 1.0f, 1.0f) },
		{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::Color(1.0f, 0.0f, 0.0f, 1.0f) },
		{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::Color(1.0f, 0.0f, 1.0f, 1.0f) },
		{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::Color(1.0f, 1.0f, 0.0f, 1.0f) },
		{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::Color(1.0f, 1.0f, 1.0f, 1.0f) },
		{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::Color(0.0f, 0.0f, 0.0f, 1.0f) }
	};
	// ������ 3
	mp.key = L"cube2";
	mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(-5.f, 0.f, -5.f);
	mp.verticesColor = v;
	if (!bufferColors->addModel(mp))
		return;

	// ��� 4
	// ������ 4
	mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(5.f, 0.f, -5.f);
	mp.verticesColor = v;
	if (!bufferColors->addModel(mp))
		return;

	bufferColors->reinit();

	// ���������
	float uv(15.f);
	vertices =
	{
		{ gmath::Point3(-100.0f, 0.0f, -100.0f), gmath::UVCoordinate(-uv, uv)},
		{ gmath::Point3(-100.0f, 0.0f, 100.0f), gmath::UVCoordinate(-uv, -uv)},
		{ gmath::Point3(100.0f, 0.0f, 100.0f), gmath::UVCoordinate(uv, -uv)},
		{ gmath::Point3(100.0f, 0.0f, -100.0f), gmath::UVCoordinate(uv, uv)}
	};
	indeces =
	{
		0, 1, 2,
		0, 2, 3
	};
	mp.type = TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_C;
	mp.samplerKey = L"default";
	mp.indeces = indeces;
	mp.key = L"terrain";
	mp.textureKey = L"textures/grass1.dds";
	mp.world = gmath::Matrix::Identity();
	mp.verticesTexture = vertices;
	mp.world = gmath::Matrix::CreateTranslationMatrixY(-2.f);
	if (!bufferTextures->addModel(mp))
		return;
	bufferTextures->reinit();

	// ���� ��������
	vertices =
	{
		{ gmath::Point3(-3.f, 6.0f, -1.f), gmath::UVCoordinate(0.f, 0.f)}, // 0
		{ gmath::Point3(3.f, 6.0f, -1.f), gmath::UVCoordinate(1.f, 0.f)}, // 1
		{ gmath::Point3(3.f, 0.0f, -1.f), gmath::UVCoordinate(1.f, 1.f)}, // 2
		{ gmath::Point3(-3.f, 0.0f, -1.f), gmath::UVCoordinate(0.f, 1.f)} // 3
	};
	indeces =
	{
		0, 1, 2,
		0, 2, 3
	};
	mp.textureKey = L"textures/wood1_test.dds";
	mp.world = gmath::Matrix::Identity();
	mp.indeces = indeces;
	mp.verticesTexture = vertices;
	mp.key = L"triangle_1";
	if (!bufferTextures->addModel(mp))
		return;
	bufferTextures->reinit();

}

bool TestOpenGLScene::update(SceneDynamicParameters& sdp)
{
	// �������� �� ������� esc
	if (InputSystem::getInstance()->getKeyboardKey(InputSystemKeys::KEY_ESC))
		return false;

	// ���������� ������
	camera->update(sdp.frameTime, getWindowBBox());
	// ���������� ������
	//if (!sdp.textBuffStack.empty())
	//{
	//	while (!sdp.textBuffStack.empty()) // A
	//	{
	//		auto textParam = sdp.textBuffStack.top();
	//		auto coordinates = textParam->get_positions();
	//		//auto outp = textParam->to_wstring() + L'\n';
	//		//OutputDebugString(outp.c_str());
	//		sdp.textBuffStack.pop();
	//		textBuffer->replaceText(L"render_label", coordinates.first, coordinates.second,
	//			textParam->to_wstring());
	//	}
	//}

	if (!sdp.textBuffStack.empty()) // B
	{
		while (!sdp.textBuffStack.empty())
		{
			auto textParam = sdp.textBuffStack.top();
			sdp.textBuffStack.pop();
			textBuffer->replaceText(textParam->get_key(), textParam->to_wstring());
		}
	}

	// ���������� ������� ������
	int h(textBuffer->getHeight());
	textBuffer->updatePosition(L"l_stat", gmath::Matrix::CreateTranslationMatrixXYZ(-(getWindowBBox().w / 2) + 1, getWindowBBox().h / 2 - (h + 1), 0.f));
	textBuffer->updatePosition(L"l_fps", gmath::Matrix::CreateTranslationMatrixXYZ(-(getWindowBBox().w / 2) + 115, getWindowBBox().h / 2 - (h * 2 + 1), 0.f));
	textBuffer->updatePosition(L"l_fscreen", gmath::Matrix::CreateTranslationMatrixXYZ(-(getWindowBBox().w / 2) + 100, getWindowBBox().h / 2 - (h * 3 + 1), 0.f));
	textBuffer->updatePosition(L"l_vsunc", gmath::Matrix::CreateTranslationMatrixXYZ(-(getWindowBBox().w / 2) + 75, getWindowBBox().h / 2 - (h * 4 + 1), 0.f));
	textBuffer->updatePosition(L"l_sizewin", gmath::Matrix::CreateTranslationMatrixXYZ(-(getWindowBBox().w / 2) + 120, getWindowBBox().h / 2 - (h * 5 + 1), 0.f));

	return true;
}

TestOpenGLScene::~TestOpenGLScene()
{
	clear();
}

void TestOpenGLScene::clear()
{
	OpenGLScene::clear();
}

void TestOpenGLScene::render()
{
	gmath::Color color = { 0.0f, 0.0f, 1.0f, 1.0f };
	glClearColor(color[gmtypes::cr], color[gmtypes::cg], color[gmtypes::cb], color[gmtypes::ca]);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // ������� ��� ����� � ����� �������

	// ������ ������
	bufferColors->draw();
	bufferTextures->draw();

	glDisable(GL_DEPTH_TEST); // ��������� ���� �������
	textBuffer->draw(); // ������
	glEnable(GL_DEPTH_TEST); // �������� ���� �������
}

