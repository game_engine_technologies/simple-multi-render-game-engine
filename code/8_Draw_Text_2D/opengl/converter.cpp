#include <iostream>
#include <fstream>
#include <regex>
#include <vector>
#include <string>
#include <sstream>
#include <iomanip>

using namespace std;

int main()
{
	ifstream input("font.fnt");
	vector<string> strings;
	cmatch result;
	regex regular("([\\w=-]+)");

	if (!input)
	{

		cout << "error open cfg" << endl;
		return -1;
	}

	while (true)
	{
		string tmp;
		getline(input, tmp, '\n');
		if (!input)
			break;
		stringstream newString;
		string x;
		string y;
		string _width;
		for (auto it = std::sregex_iterator(tmp.cbegin(), tmp.cend(), regular); it != std::sregex_iterator(); ++it)
		{
			string part(it->str());
			if (part.find("id=") != string::npos)
				newString << setw(5) << part.substr(3) << " ";
			else if (part.find("width=") != string::npos)
			{
				newString << setw(5) << part.substr(6) << " ";
				_width = part.substr(6);
			}
			else if (part.find("x=") != string::npos)
				x = part.substr(2);
			else if (part.find("y=") != string::npos)
				y = part.substr(2);
		}
		// x
		double u;
		stringstream to_double;
		to_double << x;
		to_double >> u; 
		u /= 512;
		// y
		double v;
		to_double.str().clear();
		to_double.clear();
		to_double << y;
		to_double >> v;
		v /= 512;
		// width
		double width;
		to_double.str().clear();
		to_double.clear();
		to_double << _width;
		to_double >> width;
		width /= 512;
		// height static
		double height;
		to_double.str().clear();
		to_double.clear();
		to_double << 19;
		to_double >> height;
		height /= 512;

		// ������ ����� uv ���������� 4� ������ ������� �� �������
		newString << setw(15) << u << " " << setw(15) << v << " "; // lt
		newString << setw(15) << u + width << " " << setw(15) << v << " "; // rt
		newString << setw(15) << u + width << " " << setw(15) << v + height << " "; // rb
		newString << setw(15) << u << " " << setw(15) << v + height << " "; // lb

		strings.push_back(newString.str());
		cout << newString.str() << endl;

	}
	input.close();


	ofstream output("font.pCfg");
	if (!output)
	{
		cout << "error create cfg" << endl;
		return -1;
	}
	for (auto&& s : strings)
		output << s << endl;
	output.close();


	return 0;
}
