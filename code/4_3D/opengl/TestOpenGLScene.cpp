#include "TestOpenGLScene.h"

#include "../../common/renders/base/scene/model/IGeometryBufferVisitor.h"
#include "../../common/renders/base/scene/camera/ICameraVisitor.h"

void TestOpenGLScene::init(const std::wstring& path, SceneParameters rp)
{
	OpenGLScene::init(path, rp);
	// init camera
	VisitorCameraCreateAndUpdateSmartPtr visitorCamera = std::make_shared<VisitorCameraCreateAndUpdate>();
	if (!visitorCamera)
		throw ExceptionMemory(L"VisitorCameraCreateAndUpdate is not create, memory not found!");
	camera = std::make_shared<OpenGLCamera>();
	if (!camera)
		throw ExceptionMemory(L"OpenGLCamera is not create, memory not found!");
	camera->setFarZ(1000.f);
	camera->setNearZPerspective(0.1f);
	camera->updatePerspective(gmath::GeneralMath::PI_4, rp.getClientWidth(), rp.getClientHeight());
	camera->updateView(gmath::Vector3(0.f, 15.f, 39.f), gmath::Vector3(0.f, 0.f, 0.f), gmath::Vector3(0.f, 1.f, 0.f));
	camera->init(visitorCamera);
	// ��� ������� ������ � ���������
	buffer = std::make_shared<OpenGLGeometryBufferB>();
	if (!buffer)
		throw ExceptionMemory(L"OpenGLGeometryBufferB is not create, memory not found!");
	// init buffer
	VisitorGeometryBufferCreateAndDrawSmartPtr visitor = std::make_shared<VisitorGeometryBufferCreateAndDraw>(
		shadersBuffer,
		camera);
	if (!visitor)
		throw ExceptionMemory(L"VisitorGeometryBufferCreateAndDraw is not create, memory not found!");
	buffer->init(L"shaders/opengl/color_camera.gl_vs", L"shaders/opengl/color_camera.gl_ps", visitor);

	//// ������� �������
	//std::vector<Index> ind =
	//{
	//	0, 1, 2,
	//	0, 2, 3,
	//	2, 1, 5,
	//	2, 5, 6,
	//	3, 2, 6,
	//	3, 6, 7,
	//	0, 3, 7,
	//	0, 7, 4,
	//	1, 0, 4,
	//	1, 4, 5,
	//	6, 5, 4,
	//	6, 4, 7
	//};
	//ModelParameters mp;
	//mp.type = TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_B;
	//mp.indeces = ind;

	//// ��� 1
	//std::vector<VertexColor> v =
	//{
	//	{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::Color(0.0f, 0.0f, 1.0f, 1.0f) },
	//	{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::Color(0.0f, 1.0f, 0.0f, 1.0f) },
	//	{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::Color(0.0f, 1.0f, 1.0f, 1.0f) },
	//	{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::Color(1.0f, 0.0f, 0.0f, 1.0f) },
	//	{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::Color(1.0f, 0.0f, 1.0f, 1.0f) },
	//	{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::Color(1.0f, 1.0f, 0.0f, 1.0f) },
	//	{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::Color(1.0f, 1.0f, 1.0f, 1.0f) },
	//	{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::Color(0.0f, 0.0f, 0.0f, 1.0f) }
	//};
	//// ������ 1
	//mp.key = L"cube1";
	//mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(-5.f, 0.f, -5.f);
	//mp.verticesColor = v;
	//if (!buffer->addModel(mp))
	//	return;

	//// ��� 2
	//// ������ 2
	//mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(5.f, 0.f, -5.f);
	//if (!buffer->addModel(mp))
	//	return;

	//// ��� 3
	//v =
	//{
	//	{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::Color(1.0f, 0.0f, 0.0f, 1.0f) },
	//	{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::Color(1.0f, 0.0f, 0.0f, 1.0f) },
	//	{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::Color(1.0f, 0.0f, 0.0f, 1.0f) },
	//	{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::Color(1.0f, 0.0f, 0.0f, 1.0f) },
	//	{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::Color(1.0f, 0.0f, 0.0f, 1.0f) },
	//	{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::Color(1.0f, 0.0f, 0.0f, 1.0f) },
	//	{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::Color(1.0f, 0.0f, 0.0f, 1.0f) },
	//	{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::Color(1.0f, 0.0f, 0.0f, 1.0f) }
	//};
	//// ������ 3
	//mp.key = L"cube2";
	//mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(-5.f, 0.f, 5.f);
	//mp.verticesColor = v;
	//if (!buffer->addModel(mp))
	//	return;

	//// ��� 4
	//// ������ 4
	//mp.key = L"cube2";
	//mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(5.f, 0.f, 5.f);
	//mp.verticesColor = v;
	//if (!buffer->addModel(mp))
	//	return;

	//// triangle
	//// ������� �������
	//v =
	//{
	//	{gmath::Point3(0.f, 0.5f, 0.5f), gmath::Color(1.f, 0.f, 0.f, 1.f)},
	//	{gmath::Point3(0.5f, -0.5f, 0.5f), gmath::Color(0.f, 1.f, 0.f, 1.f)},
	//	{gmath::Point3(-0.5f, -0.5f, 0.5f), gmath::Color(1.f, 0.49f, 0.31f, 1.f)}
	//};
	//// ������� �������
	//ind =
	//{
	//	2, 1, 0
	//};
	//// ������
	//mp.world = gmath::Matrix::CreateRotateMatrixY(gmath::GeneralMath::degreesToRadians(90.f));
	//mp.verticesColor = v;
	//mp.indeces = ind;
	//mp.key = L"triangle1";
	//if (!buffer->addModel(mp))
	//	return;

	//buffer->reinit();

		// ������� �������
	std::vector<Index> ind =
	{
		3,1,0,
		2,1,3,
		0,5,4,
		1,5,0,
		3,4,7,
		0,4,3,
		1,6,5,
		2,6,1,
		2,7,6,
		3,7,2,
		6,4,5,
		7,4,6,
	};
	ModelParameters mp;
	mp.type = TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_B;
	mp.indeces = ind;

	// ��� 1
	std::vector<VertexColor> v =
	{
		{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::Color(0.0f, 0.0f, 1.0f, 1.0f) },
		{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::Color(0.0f, 1.0f, 0.0f, 1.0f) },
		{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::Color(0.0f, 1.0f, 1.0f, 1.0f) },
		{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::Color(1.0f, 0.0f, 0.0f, 1.0f) },
		{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::Color(1.0f, 0.0f, 1.0f, 1.0f) },
		{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::Color(1.0f, 1.0f, 0.0f, 1.0f) },
		{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::Color(1.0f, 1.0f, 1.0f, 1.0f) },
		{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::Color(0.0f, 0.0f, 0.0f, 1.0f) }
	};
	// ������ 1
	mp.key = L"cube1";
	mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(-5.f, 0.f, 5.f);
	mp.verticesColor = v;
	if (!buffer->addModel(mp))
		return;

	// ��� 2
	// ������ 2
	mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(5.f, 0.f, 5.f);
	if (!buffer->addModel(mp))
		return;

	// ��� 3
	v =
	{
		{ gmath::Point3(-1.0f, 1.0f, -1.0f), gmath::Color(1.0f, 0.0f, 0.0f, 1.0f) },
		{ gmath::Point3(1.0f, 1.0f, -1.0f), gmath::Color(0.0f, 1.0f, 0.0f, 1.0f) },
		{ gmath::Point3(1.0f, 1.0f, 1.0f), gmath::Color(0.0f, 1.0f, 0.0f, 1.0f) },
		{ gmath::Point3(-1.0f, 1.0f, 1.0f), gmath::Color(1.0f, 1.0f, 0.0f, 1.0f) },
		{ gmath::Point3(-1.0f, -1.0f, -1.0f), gmath::Color(1.0f, 0.0f, 1.0f, 1.0f) },
		{ gmath::Point3(1.0f, -1.0f, -1.0f), gmath::Color(1.0f, 1.0f, 1.0f, 1.0f) },
		{ gmath::Point3(1.0f, -1.0f, 1.0f), gmath::Color(1.0f, 1.0f, 1.0f, 1.0f) },
		{ gmath::Point3(-1.0f, -1.0f, 1.0f), gmath::Color(0.0f, 0.0f, 1.0f, 1.0f) }
	};
	// ������ 1
	mp.key = L"cube2";
	mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(-5.f, 0.f, -5.f);
	mp.verticesColor = v;
	if (!buffer->addModel(mp))
		return;

	// ��� 4
	// ������ 4
	mp.key = L"cube2";
	mp.world = gmath::Matrix::CreateTranslationMatrixXYZ(5.f, 0.f, -5.f);
	mp.verticesColor = v;
	if (!buffer->addModel(mp))
		return;

	buffer->reinit();
}

bool TestOpenGLScene::update(SceneDynamicParameters& sdp)
{
	camera->update(sdp.frameTime, getWindowBBox());
	return OpenGLScene::update(sdp);
}

TestOpenGLScene::~TestOpenGLScene()
{
	clear();
}

void TestOpenGLScene::clear()
{
	OpenGLScene::clear();
}

void TestOpenGLScene::render()
{
	gmath::Color color = { 0.0f, 0.0f, 1.0f, 1.0f };
	glClearColor(color[gmtypes::cr], color[gmtypes::cg], color[gmtypes::cb], color[gmtypes::ca]);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // ������� ��� ����� � ����� �������

	// ������ ������ (�����������)
	buffer->draw();
}
