#pragma once

#include "../../common/renders/base/scene/Direct3D11Scene.h"
#include "../../common/renders/base/scene/model/Direct3D11GeometryBufferB.h"
#include "../../common/utils/base/ExceptionMemory.h"
#include "../../common/renders/base/scene/camera/Direct3D11Camera.h"

class TestDirect3D11Scene : public Direct3D11Scene
{
	Direct3D11GeometryBufferBSmartPtr buffer;
	Direct3D11CameraSmartPtr camera;

public:
	TestDirect3D11Scene(const ComPtr<ID3D11Device>& d3dDevice, const ComPtr<ID3D11DeviceContext>& d3dImmediateContext);
	void init(const std::wstring& path, SceneParameters rp) override;
	bool update(SceneDynamicParameters& sdp) override;

	~TestDirect3D11Scene()override;
	void clear()override;

	void render(const ComPtr<ID3D11DeviceContext>& d3dImmediateContext, const ComPtr<ID3D11RenderTargetView>& d3dRenderTargetView,
		const ComPtr<ID3D11DepthStencilView>& d3dDepthStencilView, const ComPtr<ID3D11DepthStencilState>& d3dDepthStencilEnableDepth,
		const ComPtr<ID3D11DepthStencilState>& d3dDepthStencilDisableDepth) override;
};