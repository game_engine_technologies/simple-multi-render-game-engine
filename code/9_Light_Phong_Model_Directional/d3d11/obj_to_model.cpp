// конвертер obj файла в код моей модели (как представлено у меня в движке)
// поддержка ТОЛЬКО ОДНОЙ текстуры
// структуруа вершины: позиция, нормаль, текстурные координаты
// также генерируется буфер индексов

#include <string>
#include <iostream>
#include <algorithm>
#include <iterator>
#include <fstream>
#include <vector>
#include <sstream>
#include "../../common/math/mathematics.h"
#include "../../common/renders/base/scene/model/Index.h"

using namespace std;
using namespace gmath;
using namespace gmtypes;

bool loadFromFileObj(const string& path, const string& output_path);

int main(int argc, char **argv)
{
	string message;
	string path;
	string output_path("model.pCfg");

	if (argc == 1)
	{
		message = "Can't find argv[1]!\n";
		copy(message.begin(), message.end(), ostream_iterator<char>(cerr, ""));
		return -1;
	}

	path = argv[1];
	if (!loadFromFileObj(path, output_path))
		return -1;

	message = "Create model is success.\n";
	copy(message.begin(), message.end(), ostream_iterator<char>(cout, ""));

	return 0;
}


bool loadFromFileObj(const string& path, const string& output_path)
{
	string message;
	ifstream file_model;
	ofstream file_out;

	string temp; // временная строка
	vector<Index> indBuf; // индексы
	vector<Point3> verBuf; // вершины
	vector<Normal> normBuf; // нормали
	vector<UVCoordinate> uv; // текстуры

	file_model.open(path);
	if (!file_model)
	{
		message = "Error open .obj file!\n";
		copy(message.begin(), message.end(), ostream_iterator<char>(cerr, ""));
		return false;
	}

	// cначала грузим файл материалов
	//string name_file_material(path); name_file_material.resize(name_file_material.length() - 3);
	//ifstream file_textures(name_file_material + "mtl");
	//if (file_textures)
	//{
	//	while (true)
	//	{
	//		getline(file_textures, temp, '\n');
	//		if (file_textures.eof())
	//			break;
	//		if (!file_textures)
	//		{
	//			/*wstring _f(name_file_material + ".mtl");
	//			string out = "Error open file "; out += string(_f.begin(), _f.end()); out += "!";
	//			LOGMESSAGE(out.c_str());*/
	//			return false;
	//		}
	//		// тут грузим текстуры
	//		if (temp.find("newmtl ") != -1)
	//		{
	//			string mtl(temp.begin() + 7, temp.end());
	//			pathTextures.insert({ mtl, "" }); // добавили материал
	//			for (int i(0); i < 5; i++)
	//			{
	//				getline(file_textures, temp, '\n');
	//				if (!file_textures)
	//				{
	//					/*wstring _f(name_file_material + ".mtl");
	//					string out = "Error open file "; out += string(_f.begin(), _f.end()); out += "!";
	//					LOGMESSAGE(out.c_str());*/
	//					return false;
	//				}
	//			}
	//			// записываем путь к текстуре
	//			//string _temp("engine_resource/textures/");
	//			//temp.insert(0, _temp);
	//			//if (!fs::exists(fs::path(temp))) // текстуры нет
	//			//	temp = "engine_resource/textures/no_texture.dds"; // заменяем заглушкой
	//			temp.erase(0, 7);
	//			if (indexTexture.find(temp) == indexTexture.end())
	//				indexTexture.insert(make_pair(temp, (int)indexTexture.size()));
	//			pathTextures[mtl] = temp;
	//			getline(file_textures, temp, '\n'); // читаем ненужную строку
	//			if (!file_textures)
	//			{
	//				/*wstring _f(name_file_material + L".mtl");
	//				string out = "Error open file "; out += string(_f.begin(), _f.end()); out += "!";
	//				LOGMESSAGE(out.c_str());*/
	//				return false;
	//			}
	//		}
	//	}
	//	isTexture = true;
	//}	
	//file_textures.close();
	//for_each(indexTexture.begin(), indexTexture.end(), [&convertListTextures](const auto& el) {// переносим индексы в ключи, а текстуры в значение
	//	convertListTextures.insert({ el.second, el.first });
	//});
	//for_each(convertListTextures.begin(), convertListTextures.end(), [&texturesListOut](const auto& el) { // конструируем массив имен текстур
	//	texturesListOut.push_back({el.second.begin(), el.second.end()});
	//});

	// считываем данные модели
	while (true)
	{
		getline(file_model, temp, '\n');
		if (file_model.eof())
			break;
		if (!file_model)
		{
			message = "Error read data .obj file!\n";
			copy(message.begin(), message.end(), ostream_iterator<char>(cerr, ""));
			return false;
		}

		//if (isTexture && temp.find("usemtl ") != -1) // добавляем текстуру и количество соответствующих ей вершин
		//{
		//	auto key = pathTextures[string(temp.begin() + 7, temp.end())]; // получаем имя текстуры по имени материала
		//	currentIndexTexture = indexTexture[key]; // получаем индекс текстуры по имени текстуры
		//}

		string number;
		vector<float> tmp;
		string sel;

		if (temp.length() >= 2)
			sel = temp.substr(0, 2);
		else
			sel = temp;

		if (sel == "v ") // вершины
		{
			temp += " ";
			temp.erase(0, 2);
			for_each(temp.begin(), temp.end(), [&number, &tmp](const char& e) { // итератор вылетает за границы
				if (e != ' ')
					number += e;
				else
				{
					tmp.push_back(0.f);
					stringstream ostream(number);
					ostream >> tmp.back();
					number.clear();
				}
			});
			// переворачиваем отзеркаленную модель
			/*D3DXMATRIX mirror;
			D3DXMatrixTranslation(&mirror, 0, 0, 0);
			mirror(2, 2) = -1;
			D3DXVec3TransformCoord(&vect, &vect, &mirror);*/
			// добавляем в буфер вершин
			verBuf.push_back({ tmp[0], tmp[1], -tmp[2] });
		}
		else if (sel == "vn") // нормали
		{
			temp += " ";
			temp.erase(0, 3);
			for_each(temp.begin(), temp.end(), [&number, &tmp](const char& e) {
				if (e != ' ')
					number += e;
				else
				{
					tmp.push_back(0.f);
					stringstream ostream(number);
					ostream >> tmp.back();
					number.clear();
				}
			});
			normBuf.push_back({ tmp[0], tmp[1], -tmp[2] });
		}
		else if (sel == "vt") // текстуры
		{
			temp += " ";
			temp.erase(0, 3);
			for_each(temp.begin(), temp.end(), [&number, &tmp](const char& e) {
				if (e != ' ')
					number += e;
				else
				{
					tmp.push_back(0.f);
					stringstream ostream(number);
					ostream >> tmp.back();
					number.clear();
				}
			});
			uv.push_back({ tmp[0], 1 - tmp[1] });
		}
		else if (sel == "f ") // фейсы(индексы)
		{
			vector<int> Tmp;
			temp += " ";
			temp.erase(0, 2);
			for_each(temp.begin(), temp.end(), [&number, &Tmp](const char& e) {
				if (e != '/' && e != ' ')
					number += e;
				else
				{
					Tmp.push_back(0);
					stringstream ostream(number);
					ostream >> Tmp.back();
					Tmp.back()--;
					number.clear();
				}
			});

			for (int i(0); i < 3; i++)
				swap(Tmp[i], Tmp[Tmp.size() - 3 + i]);

			for (int i(0); i < Tmp.size(); i += 3)
			{
				indBuf.push_back(Tmp[i]); // индекс вершины
				indBuf.push_back(Tmp[i + 1]); // индекс нормали
				indBuf.push_back(Tmp[i + 2]); // индекст текстурных координат
			}
		}
	}
	file_model.close();

	file_out.open(output_path);
	if (!file_out)
	{
		message = "Error open model file!\n";
		copy(message.begin(), message.end(), ostream_iterator<char>(cerr, ""));
		return false;
	}

	vector<Index> indecesBuffer;
	vector<pair<string, Index>> vertIndBuff;

	for (int j(0), k(0); j < indBuf.size(); j += 3)
	{
		int _0(indBuf[j]); 
		int _1(indBuf[j + 1]);
		int _2(indBuf[j + 2]); 
		ColorUV uvAndIndex = { uv[_1][tu], uv[_1][tv], -1, -1 };

		stringstream _v;
		_v << "    { gmath::Point3(" << verBuf[_0][vx] << ", " << verBuf[_0][vy] << ", " << verBuf[_0][vz] << "), " <<
			"gmath::Normal(" << normBuf[_2][vx] << ", " << normBuf[_2][vy] << ", " << normBuf[_2][vz] << "), " <<
			"gmath::ColorUV(" << uvAndIndex[vx] << ", " << uvAndIndex[vy] << ", " << uvAndIndex[vz] << ", " << uvAndIndex[vw] << ")},\n";
		string vertex(_v.str());
		auto iter = find_if(vertIndBuff.begin(), vertIndBuff.end(), [&vertex](const pair<string, Index>& p) -> bool
			{
				return p.first == vertex;
			});
		if (iter != vertIndBuff.end())
			indecesBuffer.push_back(iter->second);
		else
		{
			vertIndBuff.push_back({vertex, k});
			indecesBuffer.push_back(k);
			++k;
		}
	}

	file_out << "std::vector<VertexStaticLight> vertices = \n" << "{\n";
	for (auto&& v : vertIndBuff)
	{
		file_out << v.first;
		if (!file_out)
		{
			message = "Error write model file!\n";
			copy(message.begin(), message.end(), ostream_iterator<char>(cerr, ""));
			return false;
		}
	}
	file_out << "};\n\n";

	file_out << "std::vector<Index> indeces = \n" << "{";
	int i(0);
	for (auto&& ind : indecesBuffer)
	{
		if (i % 3 == 0)
			file_out << "\n    " << ind << ", ";
		else
			file_out << ind << ", ";
		if (!file_out)
		{
			message = "Error write model file!\n";
			copy(message.begin(), message.end(), ostream_iterator<char>(cerr, ""));
			return false;
		}
		++i;
	}
	file_out << "\n};\n\n";

	file_out.close();

	return true;
}
