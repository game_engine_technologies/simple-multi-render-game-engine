#include "TestDirect3D11Scene.h"

#include "../../common/renders/base/scene/model/IGeometryBufferVisitor.h"
#include "../../common/renders/base/scene/camera/ICameraVisitor.h"
#include "../../common/renders/base/scene/text/ITextBufferVisitor.h"
#include "../../common/renders/base/scene/light/IStaticLightVisitor.h"
#include "../../common/renders/base/scene/light/Direct3D11DirectionalStaticLight.h"

TestDirect3D11Scene::TestDirect3D11Scene(const ComPtr<ID3D11Device>& d3dDevice,
	const ComPtr<ID3D11DeviceContext>& d3dImmediateContext):Direct3D11Scene(d3dDevice, d3dImmediateContext)
{
	
}

void TestDirect3D11Scene::init(const std::wstring& path, SceneParameters rp)
{
	Direct3D11Scene::init(path, rp);

	ModelParameters mp;

	// init camera
	VisitorCameraCreateAndUpdateSmartPtr visitorCamera = std::make_shared<VisitorCameraCreateAndUpdate>(d3dDevice, d3dImmediateContext);
	if(!visitorCamera)
		throw ExceptionMemory(L"VisitorCameraCreateAndUpdate is not create, memory not found!");
	camera = std::make_shared<Direct3D11FPSCamera>(); // Direct3D11FPSCamera
	if (!camera)
		throw ExceptionMemory(L"Direct3D11FPSCamera is not create, memory not found!");
	camera->setNearZPerspective(0.001f);
	camera->setNearZOrtho(0.0f);
	camera->setFarZ(1000.f);
	camera->updatePerspective(gmath::GeneralMath::PI_4, rp.getClientWidth(), rp.getClientHeight());
	camera->updateView(gmath::Vector3(0.f, 5.f, -20.f), gmath::Vector3(0.f, 5.f, 0.f), gmath::Vector3(0.f, 1.f, 0.f));
	camera->updateOrtho(rp.getClientWidth(), rp.getClientHeight());
	camera->init(visitorCamera);

	// init text buffer
	VisitorTextBufferCreateAndDrawSmartPtr visitorTextBuffer = std::make_shared<VisitorTextBufferCreateAndDraw>(d3dDevice, d3dImmediateContext,
		shadersBuffer, texturesBuffer, camera);	
	if (!visitorTextBuffer)
		throw ExceptionMemory(L"VisitorTextBufferCreateAndDraw is not create, memory not found!");
	textBuffer = std::make_shared<Direct3D11TextBufferB>();
	int h(20);
	textBuffer->init(L"shaders/d3d11/text.d3d11_vs", L"shaders/d3d11/text.d3d11_ps", L"configs/text/direct3d/list_font_files.pText", L"default",
		h, visitorTextBuffer);
	textBuffer->addText(L"l_stat", L"Current render: Direct3D " + rp.getVersionAPI() + L"\n"
		"Current FPS:\nFullscreen:\nV-Sync:\nSize window:\n",
		gmath::Matrix::CreateTranslationMatrixXYZ(-(getWindowBBox().w / 2) + 1.f, getWindowBBox().h / 2 - (h + 1), 0.f), gmath::Color(1.f, 1.f, 1.f, 1.f), TypeTextBufferB::STATIC_TEXT_BUFFER);
	textBuffer->addText(L"l_fps", L"0    ",
		gmath::Matrix::CreateTranslationMatrixXYZ(-(rp.getClientWidth() / 2) + 115, rp.getClientHeight() / 2 - ((h * 2) + 1), 0.f), gmath::Color(1.f, 1.f, 1.f, 1.f), TypeTextBufferB::DYNAMIC_TEXT_BUFFER);
	textBuffer->addText(L"l_fscreen", L"off",
		gmath::Matrix::CreateTranslationMatrixXYZ(-(rp.getClientWidth() / 2) + 100, rp.getClientHeight() / 2 - ((h * 3) + 1), 0.f), gmath::Color(1.f, 1.f, 1.f, 1.f), TypeTextBufferB::DYNAMIC_TEXT_BUFFER);
	textBuffer->addText(L"l_vsunc", L"off",
		gmath::Matrix::CreateTranslationMatrixXYZ(-(rp.getClientWidth() / 2) + 75, rp.getClientHeight() / 2 - ((h * 4) + 1), 0.f), gmath::Color(1.f, 1.f, 1.f, 1.f), TypeTextBufferB::DYNAMIC_TEXT_BUFFER);
	textBuffer->addText(L"l_sizewin", L"0 x 0        ",
		gmath::Matrix::CreateTranslationMatrixXYZ(-(rp.getClientWidth() / 2) + 120, rp.getClientHeight() / 2 - ((h * 5) + 1), 0.f), gmath::Color(1.f, 1.f, 1.f, 1.f), TypeTextBufferB::DYNAMIC_TEXT_BUFFER);

	// init static dir light
	StaticLightVisitorSmartPtr staticDirLightVisitor = std::make_shared<StaticLightVisitor>(
		d3dDevice,
		d3dImmediateContext);
	if (!staticDirLightVisitor)
		throw ExceptionMemory(L"StaticLightVisitor is not create, memory not found!");
	Direct3D11DirectionalStaticLightSmartPtr staticDirectionalLight = std::make_shared<Direct3D11DirectionalStaticLight>();
	if (!staticDirectionalLight)
		throw ExceptionMemory(L"Direct3D11DirectionalStaticLight is not create, memory not found!");
	staticDirectionalLight->init(staticDirLightVisitor);
	DirectionalLight dirLight;
	dirLight.camPos = camera->getPosition();
	dirLight.color = gmath::Color(0.93f, 1.f, 1.f, 1.f);
	dirLight.directional = gmath::Point(0.f, -50.f, 0.f);
	staticDirectionalLight->setDirectionalLight(dirLight);

	sun = std::make_shared<Sun>();
	if (!sun)
		throw ExceptionMemory(L"Sun is not create, memory not found!");
	sun->init(staticDirectionalLight, gmath::Vector(-5000.f, 0.f, 0.f, 0.f), 0.2f);

	// �������� ��� �������������� �������
	VisitorGeometryBufferCreateAndDrawSmartPtr visitorGeomBuffers = std::make_shared<VisitorGeometryBufferCreateAndDraw>(
		d3dDevice,
		d3dImmediateContext,
		shadersBuffer,
		camera,
		texturesBuffer,
		staticDirectionalLight,
		materialsBuffer);
	if (!visitorGeomBuffers)
		throw ExceptionMemory(L"VisitorGeometryBufferCreateAndDraw is not create, memory not found!");

	// ���������� ������, ��������
	bufferTexturesStaticLight = std::make_shared<Direct3D11GeometryBufferD>();
	if(!bufferTexturesStaticLight)
		throw ExceptionMemory(L"Direct3D11GeometryBufferD is not create, memory not found!");
	bufferTexturesStaticLight->init(L"shaders/d3d11/directional_static_light.d3d11_vs", L"shaders/d3d11/directional_static_light_textures.d3d11_ps", visitorGeomBuffers);

	std::vector<Index> indeces =
	{
		 0, 1, 2,
	3, 1, 0,
	4, 5, 6,
	7, 5, 4,
	8, 9, 10,
	11, 9, 8,
	12, 13, 14,
	15, 13, 12,
	16, 17, 18,
	19, 17, 16,
	20, 21, 22,
	23, 21, 20,
	};
	std::vector<VertexStaticLight> vertStatLight =
	{
				{ gmath::Point3(-1, 1, -1), gmath::Normal(0, 0, -1), gmath::ColorUV(0.001992, 0.00199199, -1, -1)},
	{ gmath::Point3(1, -1, -1), gmath::Normal(0, 0, -1), gmath::ColorUV(0.998008, 0.998008, -1, -1)},
	{ gmath::Point3(-1, -1, -1), gmath::Normal(0, 0, -1), gmath::ColorUV(0.001992, 0.998008, -1, -1)},
	{ gmath::Point3(1, 1, -1), gmath::Normal(0, 0, -1), gmath::ColorUV(0.998008, 0.00199199, -1, -1)},
	{ gmath::Point3(-1, 1, 1), gmath::Normal(0, 1, -0), gmath::ColorUV(0.001992, 0.00199199, -1, -1)},
	{ gmath::Point3(1, 1, -1), gmath::Normal(0, 1, -0), gmath::ColorUV(0.998008, 0.998008, -1, -1)},
	{ gmath::Point3(-1, 1, -1), gmath::Normal(0, 1, -0), gmath::ColorUV(0.001992, 0.998008, -1, -1)},
	{ gmath::Point3(1, 1, 1), gmath::Normal(0, 1, -0), gmath::ColorUV(0.998008, 0.00199199, -1, -1)},
	{ gmath::Point3(-1, -1, 1), gmath::Normal(0, 0, 1), gmath::ColorUV(0.001992, 0.00199199, -1, -1)},
	{ gmath::Point3(1, 1, 1), gmath::Normal(0, 0, 1), gmath::ColorUV(0.998008, 0.998008, -1, -1)},
	{ gmath::Point3(-1, 1, 1), gmath::Normal(0, 0, 1), gmath::ColorUV(0.001992, 0.998008, -1, -1)},
	{ gmath::Point3(1, -1, 1), gmath::Normal(0, 0, 1), gmath::ColorUV(0.998008, 0.00199199, -1, -1)},
	{ gmath::Point3(-1, -1, -1), gmath::Normal(0, -1, -0), gmath::ColorUV(0.001992, 0.00199199, -1, -1)},
	{ gmath::Point3(1, -1, 1), gmath::Normal(0, -1, -0), gmath::ColorUV(0.998008, 0.998008, -1, -1)},
	{ gmath::Point3(-1, -1, 1), gmath::Normal(0, -1, -0), gmath::ColorUV(0.001992, 0.998008, -1, -1)},
	{ gmath::Point3(1, -1, -1), gmath::Normal(0, -1, -0), gmath::ColorUV(0.998008, 0.00199199, -1, -1)},
	{ gmath::Point3(1, 1, -1), gmath::Normal(1, 0, -0), gmath::ColorUV(0.001992, 0.00199199, -1, -1)},
	{ gmath::Point3(1, -1, 1), gmath::Normal(1, 0, -0), gmath::ColorUV(0.998008, 0.998008, -1, -1)},
	{ gmath::Point3(1, -1, -1), gmath::Normal(1, 0, -0), gmath::ColorUV(0.001992, 0.998008, -1, -1)},
	{ gmath::Point3(1, 1, 1), gmath::Normal(1, 0, -0), gmath::ColorUV(0.998008, 0.00199199, -1, -1)},
	{ gmath::Point3(-1, 1, 1), gmath::Normal(-1, 0, -0), gmath::ColorUV(0.001992, 0.00199199, -1, -1)},
	{ gmath::Point3(-1, -1, -1), gmath::Normal(-1, 0, -0), gmath::ColorUV(0.998008, 0.998008, -1, -1)},
	{ gmath::Point3(-1, -1, 1), gmath::Normal(-1, 0, -0), gmath::ColorUV(0.001992, 0.998008, -1, -1)},
	{ gmath::Point3(-1, 1, -1), gmath::Normal(-1, 0, -0), gmath::ColorUV(0.998008, 0.00199199, -1, -1)},
	};
	mp.samplerKey = L"default";
	mp.type = TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_D;
	mp.indeces = indeces;
	mp.materialKey = L"default";

	// ��� 1
	mp.key = L"cube1";
	mp.textureKey = L"textures/wood1.dds";
	mp.world = gmath::Matrix::CreateScaleMatrix(2.f) * gmath::Matrix::CreateTranslationMatrixXYZ(-15.f, 2.f, 15.f);
	mp.normals = gmath::Matrix::Transponse(gmath::Matrix::Inverse(mp.world));
	mp.verticesStaticLight = vertStatLight;
	if (!bufferTexturesStaticLight->addModel(mp))
		return;

	// ��� 1
	mp.key = L"cube2";
	mp.textureKey = L"textures/wood2.dds";
	mp.world = gmath::Matrix::CreateScaleMatrix(2.f) * gmath::Matrix::CreateTranslationMatrixXYZ(15.f, 2.f, 15.f);
	mp.normals = gmath::Matrix::Transponse(gmath::Matrix::Inverse(mp.world));
	mp.verticesStaticLight = vertStatLight;
	if (!bufferTexturesStaticLight->addModel(mp))
		return;

	bufferTexturesStaticLight->reinit();


	// ���������� ������, ����
	mp.type = TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_E;

	bufferColorsStaticLight = std::make_shared<Direct3D11GeometryBufferE>();
	if (!bufferColorsStaticLight)
		throw ExceptionMemory(L"Direct3D11GeometryBufferD is not create, memory not found!");
	bufferColorsStaticLight->init(L"shaders/d3d11/directional_static_light.d3d11_vs", L"shaders/d3d11/directional_static_light_color.d3d11_ps", visitorGeomBuffers);
	vertStatLight =
	{
				{ gmath::Point3(-1, 1, -1), gmath::Normal(0, 0, -1), gmath::ColorUV(1, 0, 0, 1)},
	{ gmath::Point3(1, -1, -1), gmath::Normal(0, 0, -1), gmath::ColorUV(0, 1, 0, 1)},
	{ gmath::Point3(-1, -1, -1), gmath::Normal(0, 0, -1), gmath::ColorUV(0, 0, 1, 1)},
	{ gmath::Point3(1, 1, -1), gmath::Normal(0, 0, -1), gmath::ColorUV(0, 0, 0, 1)},
	{ gmath::Point3(-1, 1, 1), gmath::Normal(0, 1, -0), gmath::ColorUV(1, 0, 0, 1)},
	{ gmath::Point3(1, 1, -1), gmath::Normal(0, 1, -0), gmath::ColorUV(0, 1, 0, 1)},
	{ gmath::Point3(-1, 1, -1), gmath::Normal(0, 1, -0), gmath::ColorUV(0, 0, 1, 1)},
	{ gmath::Point3(1, 1, 1), gmath::Normal(0, 1, -0), gmath::ColorUV(0, 0, 0, 1)},
	{ gmath::Point3(-1, -1, 1), gmath::Normal(0, 0, 1), gmath::ColorUV(1, 0, 0, 1)},
	{ gmath::Point3(1, 1, 1), gmath::Normal(0, 0, 1), gmath::ColorUV(0, 1, 0, 1)},
	{ gmath::Point3(-1, 1, 1), gmath::Normal(0, 0, 1), gmath::ColorUV(0, 0, 1, 1)},
	{ gmath::Point3(1, -1, 1), gmath::Normal(0, 0, 1), gmath::ColorUV(0, 0, 0, 1)},
	{ gmath::Point3(-1, -1, -1), gmath::Normal(0, -1, -0), gmath::ColorUV(1, 0, 0, 1)},
	{ gmath::Point3(1, -1, 1), gmath::Normal(0, -1, -0), gmath::ColorUV(0, 1, 0, 1)},
	{ gmath::Point3(-1, -1, 1), gmath::Normal(0, -1, -0), gmath::ColorUV(0, 0, 1, 1)},
	{ gmath::Point3(1, -1, -1), gmath::Normal(0, -1, -0), gmath::ColorUV(0, 0, 0, 1)},
	{ gmath::Point3(1, 1, -1), gmath::Normal(1, 0, -0), gmath::ColorUV(1, 0, 0, 1)},
	{ gmath::Point3(1, -1, 1), gmath::Normal(1, 0, -0), gmath::ColorUV(0, 1, 0, 1)},
	{ gmath::Point3(1, -1, -1), gmath::Normal(1, 0, -0), gmath::ColorUV(0, 0, 1, 1)},
	{ gmath::Point3(1, 1, 1), gmath::Normal(1, 0, -0), gmath::ColorUV(0, 0, 0, 1)},
	{ gmath::Point3(-1, 1, 1), gmath::Normal(-1, 0, -0), gmath::ColorUV(1, 0, 0, 1)},
	{ gmath::Point3(-1, -1, -1), gmath::Normal(-1, 0, -0), gmath::ColorUV(0, 1, 0, 1)},
	{ gmath::Point3(-1, -1, 1), gmath::Normal(-1, 0, -0), gmath::ColorUV(0, 0, 1, 1)},
	{ gmath::Point3(-1, 1, -1), gmath::Normal(-1, 0, -0), gmath::ColorUV(0, 0, 0, 1)},
	};

	// ��� 3
	mp.key = L"cube3";
	mp.world = gmath::Matrix::CreateScaleMatrix(2.f) * gmath::Matrix::CreateTranslationMatrixXYZ(-15.f, 2.f, -15.f);
	mp.normals = gmath::Matrix::Transponse(gmath::Matrix::Inverse(mp.world));
	mp.verticesStaticLight = vertStatLight;
	if (!bufferColorsStaticLight->addModel(mp))
		return;

	// ��� 4
	mp.key = L"cube4";
	mp.world = gmath::Matrix::CreateScaleMatrix(2.f) * gmath::Matrix::CreateTranslationMatrixXYZ(15.f, 2.f, -15.f);
	mp.normals = gmath::Matrix::Transponse(gmath::Matrix::Inverse(mp.world));
	mp.verticesStaticLight = vertStatLight;
	if (!bufferColorsStaticLight->addModel(mp))
		return;

	bufferColorsStaticLight->reinit();

	// ������� ����������
	float uv(15.f);
	vertStatLight =
	{
		{ gmath::Point3(-100.0f, 0.0f, -100.0f), gmath::Normal(0, 1, 0), gmath::ColorUV(-uv, uv, -1, -1)},
		{ gmath::Point3(-100.0f, 0.0f, 100.0f), gmath::Normal(0, 1, 0), gmath::ColorUV(-uv, -uv, -1, -1)},
		{ gmath::Point3(100.0f, 0.0f, 100.0f), gmath::Normal(0, 1, 0),gmath::ColorUV(uv, -uv, -1, -1)},
		{ gmath::Point3(100.0f, 0.0f, -100.0f), gmath::Normal(0, 1, 0), gmath::ColorUV(uv, uv, -1, -1)}
	};
	indeces =
	{
		0, 1, 2,
		0, 2, 3
	};
	mp.type = TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_D;
	mp.indeces = indeces;
	mp.key = L"terrain";
	mp.textureKey = L"textures/grass1.dds";
	mp.verticesStaticLight = vertStatLight;
	mp.world = gmath::Matrix::Identity();
	mp.normals = gmath::Matrix::Transponse(gmath::Matrix::Inverse(mp.world));
	if (!bufferTexturesStaticLight->addModel(mp))
		return;

	bufferTexturesStaticLight->reinit();

	// ���������������� ������ �� ����������
	//bufferTextures = std::make_shared<Direct3D11GeometryBufferC>();
	//if (!bufferTextures)
	//	throw ExceptionMemory(L"Direct3D11GeometryBufferC is not create, memory not found!");
	//bufferTextures->init(L"shaders/d3d11/texture_camera.d3d11_vs", L"shaders/d3d11/texture_camera.d3d11_ps", visitorGeomBuffers);

	//// ���������
	//float uv(15.f);
	//std::vector<VertexTexture> vertTextures =
	//{
	//	{ gmath::Point3(-100.0f, 0.0f, -100.0f), gmath::UVCoordinate(-uv, uv)},
	//	{ gmath::Point3(-100.0f, 0.0f, 100.0f), gmath::UVCoordinate(-uv, -uv)},
	//	{ gmath::Point3(100.0f, 0.0f, 100.0f), gmath::UVCoordinate(uv, -uv)},
	//	{ gmath::Point3(100.0f, 0.0f, -100.0f), gmath::UVCoordinate(uv, uv)}
	//};
	//indeces =
	//{
	//	0, 1, 2,
	//	0, 2, 3
	//};
	//mp.type = TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_C;
	//mp.samplerKey = L"default";
	//mp.indeces = indeces;
	//mp.key = L"terrain";
	//mp.textureKey = L"textures/grass1.dds";
	//mp.verticesTexture = vertTextures;
	//mp.world = gmath::Matrix::Identity();
	//if (!bufferTextures->addModel(mp))
	//	return;
	//bufferTextures->reinit();
}

bool TestDirect3D11Scene::update(SceneDynamicParameters& sdp)
{
	// �������� �� ������� esc
	if (InputSystem::getInstance()->getKeyboardKey(InputSystemKeys::KEY_ESC))
		return false;

	// ���������� ������
	camera->update(sdp.frameTime, getWindowBBox());
	// ���������� ������
	if (!sdp.textBuffStack.empty()) // B
	{
		while (!sdp.textBuffStack.empty())
		{
			auto textParam = sdp.textBuffStack.top();
			sdp.textBuffStack.pop();
			textBuffer->replaceText(textParam->get_key(), textParam->to_wstring());
		}
	}

	// ���������� �����
	if (sdp.second) // ������� ��� � ������� ������ ������
	{
		sun->updateDirectional();
		OutputDebugString(L"update sun\n");
		sdp.second = false;
	}
	auto dirLight = sun->getDirectionalLight();
	DirectionalLight dirLightBuffer = dirLight->getDirectionalLight();
	dirLightBuffer.camPos = camera->getPosition();
	dirLight->setDirectionalLight(dirLightBuffer);
	dirLight->update();
	//DirectionalLight dirLight = staticDirectionalLight->getDirectionalLight();
	//dirLight.camPos = camera->getPosition();
	//staticDirectionalLight->setDirectionalLight(dirLight);
	//staticDirectionalLight->update();

	// ���������� ������� ������
	int h(textBuffer->getHeight());
	textBuffer->updatePosition(L"l_stat", gmath::Matrix::CreateTranslationMatrixXYZ(-(getWindowBBox().w / 2) + 1, getWindowBBox().h / 2 - (h + 1), 0.f));
	textBuffer->updatePosition(L"l_fps", gmath::Matrix::CreateTranslationMatrixXYZ(-(getWindowBBox().w / 2) + 115, getWindowBBox().h / 2 - (h * 2 + 1), 0.f));
	textBuffer->updatePosition(L"l_fscreen", gmath::Matrix::CreateTranslationMatrixXYZ(-(getWindowBBox().w / 2) + 100, getWindowBBox().h / 2 - (h * 3 + 1), 0.f));
	textBuffer->updatePosition(L"l_vsunc", gmath::Matrix::CreateTranslationMatrixXYZ(-(getWindowBBox().w / 2) + 75, getWindowBBox().h / 2 - (h * 4 + 1), 0.f));
	textBuffer->updatePosition(L"l_sizewin", gmath::Matrix::CreateTranslationMatrixXYZ(-(getWindowBBox().w / 2) + 120, getWindowBBox().h / 2 - (h * 5 + 1), 0.f));

	return true;
}

void TestDirect3D11Scene::setWindowBBox(int x, int y, int w, int h)
{
	IScene::setWindowBBox(x, y, w, h);
	camera->updatePerspective(gmath::GeneralMath::PI_4, w, h);
	camera->updateOrtho(w, h);
}

TestDirect3D11Scene::~TestDirect3D11Scene()
{
	clear();
}

void TestDirect3D11Scene::clear()
{
	Direct3D11Scene::clear();
}

void TestDirect3D11Scene::render(const ComPtr<ID3D11DeviceContext>& d3dImmediateContext, const ComPtr<ID3D11RenderTargetView>& d3dRenderTargetView,
	const ComPtr<ID3D11DepthStencilView>& d3dDepthStencilView, const ComPtr<ID3D11DepthStencilState>& d3dDepthStencilEnableDepth,
	const ComPtr<ID3D11DepthStencilState>& d3dDepthStencilDisableDepth)
{
	gmath::Color color = { 0.0f, 0.0f, 0.0f, 1.0f };
	d3dImmediateContext->ClearRenderTargetView(d3dRenderTargetView.Get(), color.toArray().data());
	d3dImmediateContext->ClearDepthStencilView(d3dDepthStencilView.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	// ������ ������
	bufferTexturesStaticLight->draw();
	bufferColorsStaticLight->draw();
	// bufferTextures->draw();

	// ������ �����
	d3dImmediateContext->OMSetDepthStencilState(d3dDepthStencilDisableDepth.Get(), 1); // ��������� ���� �������
	textBuffer->draw(); // ������
	d3dImmediateContext->OMSetDepthStencilState(d3dDepthStencilEnableDepth.Get(), 1); // �������� ���� �������
	
}
