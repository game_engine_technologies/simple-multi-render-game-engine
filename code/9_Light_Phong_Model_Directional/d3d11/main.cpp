#include "TestApplication.h"
#include "../../common/os/base/LevelOperationSystemSingleton.h"

int APIENTRY wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPWSTR lpCmdLine, _In_ int nCmdShow)
{
	try
	{
		WindowParameters wp;
		wp.setHinstance(hInstance);
		wp.setWidth(1920);
		wp.setHeight(1080);

		std::unique_ptr<TestApplication> app(new TestApplication());
		if (!app)
			throw ExceptionMemory(L"TestApplication is not create, memory not found!");
		app->init(wp);
		app->run();
	}
	catch (IException& ex)
	{
		LevelOperationSystemSingleton::getInstance()->printDebugMessage(ex.getError());
		LevelOperationSystemSingleton::getInstance()->showMessageBox(ex.getError());
	}
	return 0;
}

// TODO:
// ��������� ������������ ���� ��� gl(����� ���������� + �������� ����������):
// ��� ��������� -> ��������� �� IStaticLIght
// ��������� �������� ��� ���������
// �������� ����� ��� ��������������� ������
// ��������� �������� ��� ��������������� ������
// ��� ������� ��� ����� � ��������
// ��������� �������� ����� � ��������� � 4 ������ + ������
// ����

// ����� ����������� ������� ������ ���� ���������� ����� (���� ������ ��� 60 fps)

// ����� point light
