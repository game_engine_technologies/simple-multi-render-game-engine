#pragma once

#include "../../common/renders/base/scene/OpenGLScene.h"
#include "../../common/renders/base/scene/model/OpenGLGeometryBufferC.h"
#include "../../common/utils/base/ExceptionMemory.h"
#include "../../common/renders/base/scene/camera/OpenGLCamera.h"
#include "../../common/renders/base/scene/model/OpenGLGeometryBufferB.h"
#include "../../common/renders/base/scene/light/Sun.h"
#include "../../common/renders/base/scene/text/OpenGLTextBufferB.h"
#include "../../common/renders/base/scene/model/OpenGLGeometryBufferD.h"
#include "../../common/renders/base/scene/model/OpenGLGeometryBufferE.h"

class TestOpenGLScene : public OpenGLScene
{
	OpenGLGeometryBufferDSmartPtr bufferTexturesStaticLight;
	OpenGLGeometryBufferESmartPtr bufferColorsStaticLight;
	OpenGLGeometryBufferCSmartPtr bufferTextures;
	OpenGLFPSCameraSmartPtr camera;
	OpenGLTextBufferBSmartPtr textBuffer;
	SunSmartPtr sun;

public:
	void init(const std::wstring& path, SceneParameters rp) override;
	bool update(SceneDynamicParameters& sdp) override;

	~TestOpenGLScene()override;
	void clear()override;

	void render();
};