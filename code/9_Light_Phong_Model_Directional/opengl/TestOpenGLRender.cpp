#include "TestOpenGLRender.h"

void TestOpenGLRender::init(RenderParameters rp)
{
	vsync = false;
	fullscreen = false;
	OSWindowsOpenGLRender::init(rp);
}

bool TestOpenGLRender::updateScene(SceneDynamicParameters& sdp)
{
	// ������� � ������ ����� ��� �������
	if (InputSystem::getInstance()->getKeyboardKey(InputSystemKeys::KEY_HOME))
	{
		changeModeScreen(true);
		sdp.textBuffStack.push(std::make_shared<TextBufferDynamicParameterFullscreen>(fullscreen));
	}
	if (sizeWindowChanged)
	{
		sdp.textBuffStack.push(std::make_shared<TextBufferDynamicParameterSizeWindow>(width, height));
		sizeWindowChanged = false;
	}
	if (sdp.start) // ��������� ������������� ������ �� �����
	{
		sdp.textBuffStack.push(std::make_shared<TextBufferDynamicParameterFullscreen>(fullscreen));
		sdp.textBuffStack.push(std::make_shared<TextBufferDynamicParameterVSync>(vsync));
		sdp.start = false;
	}
	return scene->update(sdp);
}

void TestOpenGLRender::initScene()
{
	scene.reset(new TestOpenGLScene());
	if(!scene)
		throw ExceptionMemory(L"TestOpenGLScene is not create, memory not found!");
	SceneParameters rp;
	rp.setClientWidth(getWidth());
	rp.setClientHeight(getHeight());
	rp.setVersionAPI(std::to_wstring(versionAPIMajor) + L"." + std::to_wstring(versionAPIMinor));
	scene->init(L"empty", rp);
}

void TestOpenGLRender::render()
{
	scene->render();
	OSWindowsOpenGLRender::render();
}
