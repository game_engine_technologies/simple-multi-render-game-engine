#include "TestOpenGLScene.h"

#include "../../common/renders/base/scene/model/IGeometryBufferVisitor.h"
#include "../../common/renders/base/scene/camera/ICameraVisitor.h"
#include "../../common/renders/base/scene/text/ITextBufferVisitor.h"
#include "../../common/renders/base/scene/light/IStaticLightVisitor.h"
#include "../../common/renders/base/scene/light/OpenGLDirectionalStaticLight.h"

void TestOpenGLScene::init(const std::wstring& path, SceneParameters rp)
{
	OpenGLScene::init(path, rp);

	ModelParameters mp;

	// init camera
	VisitorCameraCreateAndUpdateSmartPtr visitorCamera = std::make_shared<VisitorCameraCreateAndUpdate>();
	if (!visitorCamera)
		throw ExceptionMemory(L"VisitorCameraCreateAndUpdate is not create, memory not found!");
	camera = std::make_shared<OpenGLFPSCamera>();
	if (!camera)
		throw ExceptionMemory(L"OpenGLCamera is not create, memory not found!");
	camera->setNearZPerspective(0.1f);
	camera->setNearZOrtho(0.0f);
	camera->setFarZ(1000.f);
	camera->updatePerspective(gmath::GeneralMath::PI_4, rp.getClientWidth(), rp.getClientHeight());
	camera->updateView(gmath::Vector3(0.f, 5.f, 20.f), gmath::Vector3(0.f, 5.f, 0.f), gmath::Vector3(0.f, 1.f, 0.f));
	camera->updateOrtho(rp.getClientWidth(), rp.getClientHeight());
	camera->init(visitorCamera);

	// init text buffer // B
	VisitorTextBufferCreateAndDrawSmartPtr visitorTextBuffer = std::make_shared<VisitorTextBufferCreateAndDraw>(shadersBuffer, texturesBuffer, camera);
	if (!visitorTextBuffer)
		throw ExceptionMemory(L"VisitorTextBufferCreateAndDraw is not create, memory not found!");
	textBuffer = std::make_shared<OpenGLTextBufferB>();
	int h(20);
	textBuffer->init(L"shaders/opengl/text.gl_vs", L"shaders/opengl/text.gl_ps", L"configs/text/opengl/list_font_files.pText", L"default",
		h, visitorTextBuffer);
	textBuffer->addText(L"l_stat", L"Current render: OpenGL " + rp.getVersionAPI() + L"\n" 
		"Current FPS:\nFullscreen:\nV-Sync:\nSize window:\n",
		gmath::Matrix::CreateTranslationMatrixXYZ(-(getWindowBBox().w / 2) + 1.f, getWindowBBox().h / 2 - (h + 1), 0.f), gmath::Color(1.f, 1.f, 1.f, 1.f), TypeTextBufferB::STATIC_TEXT_BUFFER);
	textBuffer->addText(L"l_fps", L"0    ",
		gmath::Matrix::CreateTranslationMatrixXYZ(-(rp.getClientWidth() / 2) + 115, rp.getClientHeight() / 2 - ((h * 2) + 1), 0.f), gmath::Color(1.f, 1.f, 1.f, 1.f), TypeTextBufferB::DYNAMIC_TEXT_BUFFER);
	textBuffer->addText(L"l_fscreen", L"off",
		gmath::Matrix::CreateTranslationMatrixXYZ(-(rp.getClientWidth() / 2) + 100, rp.getClientHeight() / 2 - ((h * 3) + 1), 0.f), gmath::Color(1.f, 1.f, 1.f, 1.f), TypeTextBufferB::DYNAMIC_TEXT_BUFFER);
	textBuffer->addText(L"l_vsunc", L"off",
		gmath::Matrix::CreateTranslationMatrixXYZ(-(rp.getClientWidth() / 2) + 75, rp.getClientHeight() / 2 - ((h * 4) + 1), 0.f), gmath::Color(1.f, 1.f, 1.f, 1.f), TypeTextBufferB::DYNAMIC_TEXT_BUFFER);
	textBuffer->addText(L"l_sizewin", L"0 x 0        ",
		gmath::Matrix::CreateTranslationMatrixXYZ(-(rp.getClientWidth() / 2) + 120, rp.getClientHeight() / 2 - ((h * 5) + 1), 0.f), gmath::Color(1.f, 1.f, 1.f, 1.f), TypeTextBufferB::DYNAMIC_TEXT_BUFFER);

	// init static dir light
	StaticLightVisitorSmartPtr staticDirLightVisitor = std::make_shared<StaticLightVisitor>();
	if (!staticDirLightVisitor)
		throw ExceptionMemory(L"StaticLightVisitor is not create, memory not found!");
	OpenGLDirectionalStaticLightSmartPtr staticDirectionalLight = std::make_shared<OpenGLDirectionalStaticLight>();
	if (!staticDirectionalLight)
		throw ExceptionMemory(L"OpenGLDirectionalStaticLight is not create, memory not found!");
	staticDirectionalLight->init(staticDirLightVisitor);
	DirectionalLight dirLight;
	dirLight.camPos = camera->getPosition();
	dirLight.color = gmath::Color(0.93f, 1.f, 1.f, 1.f);
	dirLight.directional = gmath::Point(0.f, -50.f, 0.f);
	staticDirectionalLight->setDirectionalLight(dirLight);

	sun = std::make_shared<Sun>();
	if (!sun)
		throw ExceptionMemory(L"Sun is not create, memory not found!");
	sun->init(staticDirectionalLight, gmath::Vector(-5000.f, 0.f, 0.f, 0.f), 0.2f);

	// �������� ��� �������������� �������
	VisitorGeometryBufferCreateAndDrawSmartPtr visitorGeomBuffers = std::make_shared<VisitorGeometryBufferCreateAndDraw>(
		shadersBuffer,
		camera,
		texturesBuffer,
		staticDirectionalLight,
		materialsBuffer);
	if (!visitorGeomBuffers)
		throw ExceptionMemory(L"VisitorGeometryBufferCreateAndDraw is not create, memory not found!");

	// ���������� ������, ��������
	bufferTexturesStaticLight = std::make_shared<OpenGLGeometryBufferD>();
	if (!bufferTexturesStaticLight)
		throw ExceptionMemory(L"OpenGLGeometryBufferD is not create, memory not found!");
	bufferTexturesStaticLight->init(L"shaders/opengl/directional_static_light.gl_vs", L"shaders/opengl/directional_static_light_textures.gl_ps", visitorGeomBuffers);

	std::vector<Index> indeces =
	{
		 0, 1, 2,
	3, 1, 0,
	4, 5, 6,
	7, 5, 4,
	8, 9, 10,
	11, 9, 8,
	12, 13, 14,
	15, 13, 12,
	16, 17, 18,
	19, 17, 16,
	20, 21, 22,
	23, 21, 20,
	};
	std::vector<VertexStaticLight> vertStatLight =
	{
				{ gmath::Point3(-1, 1, -1), gmath::Normal(0, 0, -1), gmath::ColorUV(0.001992, 0.00199199, -1, -1)},
	{ gmath::Point3(1, -1, -1), gmath::Normal(0, 0, -1), gmath::ColorUV(0.998008, 0.998008, -1, -1)},
	{ gmath::Point3(-1, -1, -1), gmath::Normal(0, 0, -1), gmath::ColorUV(0.001992, 0.998008, -1, -1)},
	{ gmath::Point3(1, 1, -1), gmath::Normal(0, 0, -1), gmath::ColorUV(0.998008, 0.00199199, -1, -1)},
	{ gmath::Point3(-1, 1, 1), gmath::Normal(0, 1, -0), gmath::ColorUV(0.001992, 0.00199199, -1, -1)},
	{ gmath::Point3(1, 1, -1), gmath::Normal(0, 1, -0), gmath::ColorUV(0.998008, 0.998008, -1, -1)},
	{ gmath::Point3(-1, 1, -1), gmath::Normal(0, 1, -0), gmath::ColorUV(0.001992, 0.998008, -1, -1)},
	{ gmath::Point3(1, 1, 1), gmath::Normal(0, 1, -0), gmath::ColorUV(0.998008, 0.00199199, -1, -1)},
	{ gmath::Point3(-1, -1, 1), gmath::Normal(0, 0, 1), gmath::ColorUV(0.001992, 0.00199199, -1, -1)},
	{ gmath::Point3(1, 1, 1), gmath::Normal(0, 0, 1), gmath::ColorUV(0.998008, 0.998008, -1, -1)},
	{ gmath::Point3(-1, 1, 1), gmath::Normal(0, 0, 1), gmath::ColorUV(0.001992, 0.998008, -1, -1)},
	{ gmath::Point3(1, -1, 1), gmath::Normal(0, 0, 1), gmath::ColorUV(0.998008, 0.00199199, -1, -1)},
	{ gmath::Point3(-1, -1, -1), gmath::Normal(0, -1, -0), gmath::ColorUV(0.001992, 0.00199199, -1, -1)},
	{ gmath::Point3(1, -1, 1), gmath::Normal(0, -1, -0), gmath::ColorUV(0.998008, 0.998008, -1, -1)},
	{ gmath::Point3(-1, -1, 1), gmath::Normal(0, -1, -0), gmath::ColorUV(0.001992, 0.998008, -1, -1)},
	{ gmath::Point3(1, -1, -1), gmath::Normal(0, -1, -0), gmath::ColorUV(0.998008, 0.00199199, -1, -1)},
	{ gmath::Point3(1, 1, -1), gmath::Normal(1, 0, -0), gmath::ColorUV(0.001992, 0.00199199, -1, -1)},
	{ gmath::Point3(1, -1, 1), gmath::Normal(1, 0, -0), gmath::ColorUV(0.998008, 0.998008, -1, -1)},
	{ gmath::Point3(1, -1, -1), gmath::Normal(1, 0, -0), gmath::ColorUV(0.001992, 0.998008, -1, -1)},
	{ gmath::Point3(1, 1, 1), gmath::Normal(1, 0, -0), gmath::ColorUV(0.998008, 0.00199199, -1, -1)},
	{ gmath::Point3(-1, 1, 1), gmath::Normal(-1, 0, -0), gmath::ColorUV(0.001992, 0.00199199, -1, -1)},
	{ gmath::Point3(-1, -1, -1), gmath::Normal(-1, 0, -0), gmath::ColorUV(0.998008, 0.998008, -1, -1)},
	{ gmath::Point3(-1, -1, 1), gmath::Normal(-1, 0, -0), gmath::ColorUV(0.001992, 0.998008, -1, -1)},
	{ gmath::Point3(-1, 1, -1), gmath::Normal(-1, 0, -0), gmath::ColorUV(0.998008, 0.00199199, -1, -1)},
	};
	mp.samplerKey = L"default";
	mp.type = TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_D;
	mp.indeces = indeces;
	mp.materialKey = L"default";

	// ��� 1
	mp.key = L"cube1";
	mp.textureKey = L"textures/wood1.dds";
	mp.world = gmath::Matrix::CreateScaleMatrix(2.f) * gmath::Matrix::CreateTranslationMatrixXYZ(-15.f, 2.f, 15.f);
	mp.normals = gmath::Matrix::Transponse(gmath::Matrix::Inverse(mp.world));
	mp.verticesStaticLight = vertStatLight;
	if (!bufferTexturesStaticLight->addModel(mp))
		return;

	// ��� 1
	mp.key = L"cube2";
	mp.textureKey = L"textures/wood2.dds";
	mp.world = gmath::Matrix::CreateScaleMatrix(2.f) * gmath::Matrix::CreateTranslationMatrixXYZ(15.f, 2.f, 15.f);
	mp.normals = gmath::Matrix::Transponse(gmath::Matrix::Inverse(mp.world));
	mp.verticesStaticLight = vertStatLight;
	if (!bufferTexturesStaticLight->addModel(mp))
		return;

	bufferTexturesStaticLight->reinit();


	// ���������� ������, ����
	mp.type = TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_E;

	bufferColorsStaticLight = std::make_shared<OpenGLGeometryBufferE>();
	if (!bufferColorsStaticLight)
		throw ExceptionMemory(L"OpenGLGeometryBufferE is not create, memory not found!");
	bufferColorsStaticLight->init(L"shaders/opengl/directional_static_light.gl_vs", L"shaders/opengl/directional_static_light_color.gl_ps", visitorGeomBuffers);
	vertStatLight =
	{
				{ gmath::Point3(-1, 1, -1), gmath::Normal(0, 0, -1), gmath::ColorUV(1, 0, 0, 1)},
	{ gmath::Point3(1, -1, -1), gmath::Normal(0, 0, -1), gmath::ColorUV(0, 1, 0, 1)},
	{ gmath::Point3(-1, -1, -1), gmath::Normal(0, 0, -1), gmath::ColorUV(0, 0, 1, 1)},
	{ gmath::Point3(1, 1, -1), gmath::Normal(0, 0, -1), gmath::ColorUV(0, 0, 0, 1)},
	{ gmath::Point3(-1, 1, 1), gmath::Normal(0, 1, -0), gmath::ColorUV(1, 0, 0, 1)},
	{ gmath::Point3(1, 1, -1), gmath::Normal(0, 1, -0), gmath::ColorUV(0, 1, 0, 1)},
	{ gmath::Point3(-1, 1, -1), gmath::Normal(0, 1, -0), gmath::ColorUV(0, 0, 1, 1)},
	{ gmath::Point3(1, 1, 1), gmath::Normal(0, 1, -0), gmath::ColorUV(0, 0, 0, 1)},
	{ gmath::Point3(-1, -1, 1), gmath::Normal(0, 0, 1), gmath::ColorUV(1, 0, 0, 1)},
	{ gmath::Point3(1, 1, 1), gmath::Normal(0, 0, 1), gmath::ColorUV(0, 1, 0, 1)},
	{ gmath::Point3(-1, 1, 1), gmath::Normal(0, 0, 1), gmath::ColorUV(0, 0, 1, 1)},
	{ gmath::Point3(1, -1, 1), gmath::Normal(0, 0, 1), gmath::ColorUV(0, 0, 0, 1)},
	{ gmath::Point3(-1, -1, -1), gmath::Normal(0, -1, -0), gmath::ColorUV(1, 0, 0, 1)},
	{ gmath::Point3(1, -1, 1), gmath::Normal(0, -1, -0), gmath::ColorUV(0, 1, 0, 1)},
	{ gmath::Point3(-1, -1, 1), gmath::Normal(0, -1, -0), gmath::ColorUV(0, 0, 1, 1)},
	{ gmath::Point3(1, -1, -1), gmath::Normal(0, -1, -0), gmath::ColorUV(0, 0, 0, 1)},
	{ gmath::Point3(1, 1, -1), gmath::Normal(1, 0, -0), gmath::ColorUV(1, 0, 0, 1)},
	{ gmath::Point3(1, -1, 1), gmath::Normal(1, 0, -0), gmath::ColorUV(0, 1, 0, 1)},
	{ gmath::Point3(1, -1, -1), gmath::Normal(1, 0, -0), gmath::ColorUV(0, 0, 1, 1)},
	{ gmath::Point3(1, 1, 1), gmath::Normal(1, 0, -0), gmath::ColorUV(0, 0, 0, 1)},
	{ gmath::Point3(-1, 1, 1), gmath::Normal(-1, 0, -0), gmath::ColorUV(1, 0, 0, 1)},
	{ gmath::Point3(-1, -1, -1), gmath::Normal(-1, 0, -0), gmath::ColorUV(0, 1, 0, 1)},
	{ gmath::Point3(-1, -1, 1), gmath::Normal(-1, 0, -0), gmath::ColorUV(0, 0, 1, 1)},
	{ gmath::Point3(-1, 1, -1), gmath::Normal(-1, 0, -0), gmath::ColorUV(0, 0, 0, 1)},
	};

	// ��� 3
	mp.key = L"cube3";
	mp.world = gmath::Matrix::CreateScaleMatrix(2.f) * gmath::Matrix::CreateTranslationMatrixXYZ(-15.f, 2.f, -15.f);
	mp.normals = gmath::Matrix::Transponse(gmath::Matrix::Inverse(mp.world));
	mp.verticesStaticLight = vertStatLight;
	if (!bufferColorsStaticLight->addModel(mp))
		return;

	// ��� 4
	mp.key = L"cube4";
	mp.world = gmath::Matrix::CreateScaleMatrix(2.f) * gmath::Matrix::CreateTranslationMatrixXYZ(15.f, 2.f, -15.f);
	mp.normals = gmath::Matrix::Transponse(gmath::Matrix::Inverse(mp.world));
	mp.verticesStaticLight = vertStatLight;
	if (!bufferColorsStaticLight->addModel(mp))
		return;

	bufferColorsStaticLight->reinit();

	// ������� ����������
	float uv(15.f);
	vertStatLight =
	{
		{ gmath::Point3(-100.0f, 0.0f, -100.0f), gmath::Normal(0, 1, 0), gmath::ColorUV(-uv, uv, -1, -1)},
		{ gmath::Point3(-100.0f, 0.0f, 100.0f), gmath::Normal(0, 1, 0), gmath::ColorUV(-uv, -uv, -1, -1)},
		{ gmath::Point3(100.0f, 0.0f, 100.0f), gmath::Normal(0, 1, 0),gmath::ColorUV(uv, -uv, -1, -1)},
		{ gmath::Point3(100.0f, 0.0f, -100.0f), gmath::Normal(0, 1, 0), gmath::ColorUV(uv, uv, -1, -1)}
	};
	indeces =
	{
		0, 1, 2,
		0, 2, 3
	};
	mp.type = TypeGeometryBuffer::TYPE_GEOMETRY_BUFFER_D;
	mp.indeces = indeces;
	mp.key = L"terrain";
	mp.textureKey = L"textures/grass1.dds";
	mp.verticesStaticLight = vertStatLight;
	mp.world = gmath::Matrix::Identity();
	mp.normals = gmath::Matrix::Transponse(gmath::Matrix::Inverse(mp.world));
	if (!bufferTexturesStaticLight->addModel(mp))
		return;

	bufferTexturesStaticLight->reinit();
}

bool TestOpenGLScene::update(SceneDynamicParameters& sdp)
{
	// �������� �� ������� esc
	if (InputSystem::getInstance()->getKeyboardKey(InputSystemKeys::KEY_ESC))
		return false;

	// ���������� ������
	camera->update(sdp.frameTime, getWindowBBox());
	// ���������� ������
	if (!sdp.textBuffStack.empty()) // B
	{
		while (!sdp.textBuffStack.empty())
		{
			auto textParam = sdp.textBuffStack.top();
			sdp.textBuffStack.pop();
			textBuffer->replaceText(textParam->get_key(), textParam->to_wstring());
		}
	}

	// ���������� �����
	if (sdp.second) // ������� ��� � ������� ������ ������
	{
		sun->updateDirectional();
		OutputDebugString(L"update sun\n");
		sdp.second = false;
	}
	auto dirLight = sun->getDirectionalLight();
	DirectionalLight dirLightBuffer = dirLight->getDirectionalLight();
	dirLightBuffer.camPos = camera->getPosition();
	dirLight->setDirectionalLight(dirLightBuffer);
	dirLight->update();

	// ���������� ������� ������
	int h(textBuffer->getHeight());
	textBuffer->updatePosition(L"l_stat", gmath::Matrix::CreateTranslationMatrixXYZ(-(getWindowBBox().w / 2) + 1, getWindowBBox().h / 2 - (h + 1), 0.f));
	textBuffer->updatePosition(L"l_fps", gmath::Matrix::CreateTranslationMatrixXYZ(-(getWindowBBox().w / 2) + 115, getWindowBBox().h / 2 - (h * 2 + 1), 0.f));
	textBuffer->updatePosition(L"l_fscreen", gmath::Matrix::CreateTranslationMatrixXYZ(-(getWindowBBox().w / 2) + 100, getWindowBBox().h / 2 - (h * 3 + 1), 0.f));
	textBuffer->updatePosition(L"l_vsunc", gmath::Matrix::CreateTranslationMatrixXYZ(-(getWindowBBox().w / 2) + 75, getWindowBBox().h / 2 - (h * 4 + 1), 0.f));
	textBuffer->updatePosition(L"l_sizewin", gmath::Matrix::CreateTranslationMatrixXYZ(-(getWindowBBox().w / 2) + 120, getWindowBBox().h / 2 - (h * 5 + 1), 0.f));

	return true;
}

TestOpenGLScene::~TestOpenGLScene()
{
	clear();
}

void TestOpenGLScene::clear()
{
	OpenGLScene::clear();
}

void TestOpenGLScene::render()
{
	gmath::Color color = { 0.0f, 0.0f, 0.0f, 1.0f };
	glClearColor(color[gmtypes::cr], color[gmtypes::cg], color[gmtypes::cb], color[gmtypes::ca]);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // ������� ��� ����� � ����� �������

	// ������ ������
	bufferTexturesStaticLight->draw();
	bufferColorsStaticLight->draw();

	glDisable(GL_DEPTH_TEST); // ��������� ���� �������
	textBuffer->draw(); // ������
	glEnable(GL_DEPTH_TEST); // �������� ���� �������
}

