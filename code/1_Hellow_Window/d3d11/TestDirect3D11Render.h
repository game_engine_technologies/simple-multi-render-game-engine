#pragma once

#include "../../common/renders/base/OSWindowsDirect3D11Render.h"

class TestDirect3D11Render : public OSWindowsDirect3D11Render
{
public:
	void init(RenderParameters rp);
	bool updateScene(SceneDynamicParameters& sdp) override;
	void initScene() override;
	void render() override;
};