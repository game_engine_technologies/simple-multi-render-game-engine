#include "TestDirect3D11Render.h"

void TestDirect3D11Render::init(RenderParameters rp)
{
	vsync = false;
	fullscreen = false;
	d3dEnable4xMsaa = true;
	OSWindowsDirect3D11Render::init(rp);
}

bool TestDirect3D11Render::updateScene(SceneDynamicParameters& sdp)
{
	return true;
}

void TestDirect3D11Render::initScene()
{
	
}

void TestDirect3D11Render::render()
{
	if(!d3dImmediateContext || !d3dSwapChain)
		throw new ExceptionDirect3D11(L"Error draw D3D11 render.");

	const float color[] = { 0.0f, 0.0f, 1.0f, 1.0f };
	d3dImmediateContext->ClearRenderTargetView(d3dRenderTargetView.Get(), reinterpret_cast<const float*>(&color));
	d3dImmediateContext->ClearDepthStencilView(d3dDepthStencilView.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	OSWindowsDirect3D11Render::render();
}
