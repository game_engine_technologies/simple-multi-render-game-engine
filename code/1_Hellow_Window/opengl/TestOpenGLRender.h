#pragma once

#include "../../common/renders/base/OSWindowsOpenGLRender.h"

class TestOpenGLRender : public OSWindowsOpenGLRender
{
public:
	void init(RenderParameters rp) override;
	bool updateScene(SceneDynamicParameters& sdp) override;
	void initScene() override;
	void render() override;
};