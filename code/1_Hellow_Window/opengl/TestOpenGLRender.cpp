#include "TestOpenGLRender.h"

void TestOpenGLRender::init(RenderParameters rp)
{
	vsync = false;
	fullscreen = false;
	OSWindowsOpenGLRender::init(rp);
}

bool TestOpenGLRender::updateScene(SceneDynamicParameters& sdp)
{
	return true;
}

void TestOpenGLRender::initScene()
{

}

void TestOpenGLRender::render()
{
	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	OSWindowsOpenGLRender::render();
}

