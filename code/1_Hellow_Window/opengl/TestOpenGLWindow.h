#pragma once

#include "../../common/renders/base/OSWindowsWindow.h"
#include "../../common/utils/base/ExceptionMemory.h"
#include "TestOpenGLRender.h"

class TestOpenGLWindow : public OSWindowsWindow
{
public:
	void init(WindowParameters wp)override;
};