struct PhongModelLight
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
};

struct LightParamInput
{
	vec4 posWorld;
	vec3 normal;
	mat4 normals;
	vec3 camPos;
};

struct LightParametrs
{
	vec4 ambientIntensity; // яркость фонового освещения
	vec4 diffuseIntensity; // яркость рассеянного освещения
	vec4 specularIntensity; // яркость блика
	float specularSize;// размер блика
};

LightParametrs initLightParametrs(vec4 ai, vec4 di, vec4 si, float ss)
{
	LightParametrs lp;
	lp.ambientIntensity = ai;
	lp.diffuseIntensity = di;
	lp.specularIntensity = si;
	lp.specularSize = ss;
	return lp;
}

PhongModelLight calculatePhongLight(vec3 lightDir, vec4 color, LightParamInput vertex_light, LightParametrs lightParam)
{
	PhongModelLight lightModel;
	// ambient
	lightModel.ambient = color * lightParam.ambientIntensity;
	// diffuse
	vec3 normal = (vec4(vertex_light.normal, 0.f) * vertex_light.normals).xyz;
	vec3 dirLight = normalize(lightDir);
	lightModel.diffuse = color * max(dot(dirLight, normalize(normal)), 0.f) * lightParam.diffuseIntensity;
	// specular
	vec3 viewDir = normalize(vertex_light.camPos - vertex_light.posWorld.xyz); // вектор от фрагмента к позиции камеры
	// Фонг
	vec3 reflectDir = normalize(reflect(normalize(dirLight), normalize(normal))); // отражённый вектор, просто Фонг
	float spec = pow(max(dot(normal, reflectDir), 0.f), lightParam.specularSize);
	lightModel.specular = color * lightParam.specularIntensity * spec;
	// return
	return lightModel;
}
