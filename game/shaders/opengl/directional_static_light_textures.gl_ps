#version 330 core

#include "static_light.h"

struct ConstantBufferLight
{
	vec4 camPos;
	vec4 directionalLightDirectional;
	vec4 directionalLightColor;
};

struct ConstantBufferMaterial
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 shininess;
};


out vec4 FragColor;

in vec4 outPosWorld;
in vec3 outNormal;
in vec4 outColor;
in vec4 outNormals[4];

uniform ConstantBufferLight light;
uniform ConstantBufferMaterial material;
uniform sampler2D inTexture;

void main()
{
	vec4 texturesColor = texture(inTexture, vec2(outColor.x, outColor.y));
	if(texturesColor.a <= 0.25f)
		discard;
		
	LightParamInput lightVertex;
	lightVertex.posWorld = outPosWorld;
	lightVertex.normal = outNormal;
	lightVertex.normals[0] = outNormals[0];
	lightVertex.normals[1] = outNormals[1];
	lightVertex.normals[2] = outNormals[2];
	lightVertex.normals[3] = outNormals[3];
	lightVertex.camPos = light.camPos.xyz;

	LightParametrs lp; 
	lp.ambientIntensity = material.ambient;
	lp.diffuseIntensity = material.diffuse;
	lp.specularIntensity = material.specular;
	lp.specularSize = material.shininess.x;

	PhongModelLight modelLightDirectional = calculatePhongLight(-light.directionalLightDirectional.xyz, light.directionalLightColor, lightVertex, lp);
	
	vec4 result = texturesColor * (modelLightDirectional.ambient + modelLightDirectional.diffuse + modelLightDirectional.specular);
	result.a = 1.f;

	FragColor = result;
}