struct PhongModelLight
{
	float4 ambient;
	float4 diffuse;
	float4 specular;
};

struct LightParamInput
{
	float4 posWorld;
	float3 normal;
	matrix normals;
	float3 camPos;
};

struct LightParametrs
{
	float4 ambientIntensity; // яркость фонового освещения
	float4 diffuseIntensity; // яркость рассеянного освещения
	float4 specularIntensity; // яркость блика
	float specularSize;// размер блика
};

PhongModelLight calculatePhongLight(float3 lightDir, float4 color, LightParamInput vertex_light, LightParametrs lightParam)
{
	PhongModelLight lightModel;
	// ambient
	lightModel.ambient = color * lightParam.ambientIntensity;
	// diffuse
	float3 normal = mul(float4(vertex_light.normal, 0.f), vertex_light.normals).xyz;
	float3 dirLight = normalize(lightDir);
	lightModel.diffuse = color * (float4)max(dot(dirLight, normalize(normal)), 0.f) * lightParam.diffuseIntensity;
	// specular
	float3 viewDir = normalize(vertex_light.camPos - (float3)vertex_light.posWorld); // вектор от фрагмента к позиции камеры
	// Фонг
	float3 reflectDir = normalize(reflect(normalize(dirLight), normalize(normal))); // отражённый вектор, просто Фонг
	float spec = pow(max(dot(viewDir, reflectDir), 0.f), lightParam.specularSize);
	lightModel.specular = color * lightParam.specularIntensity * spec;
	// return
	return lightModel;
}